<#if context.user.guest>
    <div class="content">

</#if> 

<#-- <div class="content form-signin">  -->
  <div class="span6">
 <#--   <#if !context.user.guest>
      <h2>${message('label.edit.profile')}</h2>
    <#else>
        <h2>${message('label.nuova.registrazione')}</h2>
      </#if> -->
      <#if context.user.guest>
   		   <h2>${message('label.nuova.registrazione')}</h2>
      </#if>
      <div>
       <#if context.user.guest>
   		  ${message('label.registrazione.nuova.registrazione.requisiti')}
	   </#if>
	    <#if context.user.guest>
     	    </div>
		      <div>
		        <div>
		        <div>
		          <h4>${message('label.problemi')}</h4>
		          <p>${message('label.contatto.tel')}</p>
		          <p>${message('label.contatto.e-mail')}</p>
		          <p>${message('label.contatto.e-mail.pec')}</p>
		            </div>
		        </div>
		    </div>
		  </div>
   </#if>
   <#if !context.user.guest>
           <div class="span5" style="margin-left:50%;">
      <#else>
           <div class="span5" >
      </#if>
 
      <div id="account" class="form-signin">
      <#if !context.user.guest>
          <legend>${message('label.edit.profile')}</legend>
      <#else>
          <legend>${message('label.registrazione')}</legend>
      </#if>
      
    </div>
      <#if !context.user.guest>
     	  <h5 style="margin:10px;">${message('label.problemi')}</h5>
      </#if>
  </div>
</div>