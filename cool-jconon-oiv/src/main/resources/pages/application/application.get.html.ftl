<div class="header container">
  <div class="jumbotron">
    <h4><div id="application-title"></div></h4>
    <h4><div id="call-desc"></div></h4>
    <h5><div id="appl-rich"></div></h5>
    <h5><div id="call-desc-rid"></div></h5>
    <h5>${message('application.text.dichiarazione.responsabilita')}</h5>
  </div>
</div>

<div class="container">
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span3">
        <div class="cnr-sidenav">
          <div id="toolbar-call" style="display:none">
            <div class="btn-group">
	             <div class="btn-text-application"> <button id="home" class="btn btn-application" type="button" title="${message('button.home')}"><i class=" icon-home"></i></button>	 <p>Torna alla home</p></div> 
	             <div class="btn-text-application"> <button id="save" class="btn btn-application" type="button" title="${message('button.save')}"><i class="icon-save"></i></button>     <p >Salva modifiche</p> </div>  
	             <div class="btn-text-application"> <button id="send" class="btn btn-application" type="button" title="${message('button.send')}"><i class=" icon-share"></i></button>	 <p>Invia iscrizione</p> </div>
	             <div class="btn-text-application"> <button id="cambio_fascia" class="btn btn-application" type="button" title="${message('button.cambio_fascia')}"><i class="icon-circle-arrow-up"></i></button>	 <p>Invia cambio fascia</p> </div>
	             <div class="btn-text-application"> <button id="rinnovo" class="btn btn-application" type="button" title="${message('button.rinnovo')}"><i class="icon-circle-arrow-up"></i></button>	 <p>Invia rinnovo</p> </div>
	             <div class="btn-text-application"> <button id="close" class="btn btn-application" type="button" title="${message('button.exit')}"><i class="icon-off"></i></button>	 <p>Esci</p> </div>
            </div>
          </div>
          <ul class="nav nav-list cnraffix"></ul>
        </div>
      </div><!--/span-->
      <div id="field" class="span9<#if validateAllegatiLinkedEmpty??> error-allegati-empty</#if>" <#if validateAllegatiLinkedEmpty??>data-message="${validateAllegatiLinkedEmpty}"</#if>>
      </div>        
    </div><!--/row-->
  </div>
</div> <!-- /container -->