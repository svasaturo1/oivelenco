<html>
<body>
<#assign aDateTime = .now>
<p>${message('mail.confirm.application.1')} <b>${folder.getPropertyValue("jconon_application:nome")?cap_first} ${folder.getPropertyValue("jconon_application:cognome")?cap_first}</b>,</p>
<p>${message('mail.rinnovo.application.5', folder.getPropertyValue("jconon_application:data_invio_rinnovo_elenco").time?string("dd/MM/yyyy '(h. ' HH:mm:ss')'"))}</b></p>
<p>${message('mail.rinnovo.application.6', folder.getPropertyValue("jconon_application:progressivo_iscrizione_elenco"), folder.getPropertyValue("jconon_application:data_rinnovo_elenco").time?string("dd/MM/yyyy"))}</b></p>
<p>${message('mail.rinnovo.application.3')}</b></p>
<p>${message('mail.rinnovo.application.4')}</b></p>
<p>${message('mail.confirm.application.5')}</b></p>


<br>
${call.getPropertyValue("jconon_call:requisiti")}
</body>
</html>
