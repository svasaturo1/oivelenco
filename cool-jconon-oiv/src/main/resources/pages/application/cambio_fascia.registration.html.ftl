<html>
<body>
<hr/>
<#if call.getPropertyValue("jconon_call:sede")??>
	<#assign sede = call.getPropertyValue("jconon_call:sede")>
<#else>	
	<#assign sede = "">
</#if>

<p>${message('mail.confirm.application.1')} <b>${folder.getPropertyValue("jconon_application:nome")} ${folder.getPropertyValue("jconon_application:cognome")}</b>,</p>
<p>${message('mail.registrazione.cambio.fascia.application.2')}<br>${message('mail.registrazione.cambio.fascia.application.3',contextURL)}</p>

<p>${message('mail.confirm.application.5')}</p> 
<p>${message('mail.rinnovo.application.10')}</p> 

<p>${message('mail.append.helpdesk.1')} <a href="${contextURL}/helpdesk">${message('mail.append.helpdesk.2')}</a> ${message('mail.append.helpdesk.3')}</p>
<hr/>
<p>${message('mail.append.helpdesk.4')}<br>${message('mail.append.helpdesk.5')}</p>
</body>
</html>
