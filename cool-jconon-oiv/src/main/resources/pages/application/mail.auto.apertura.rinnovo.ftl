<html>
<body>


<p>${message('mail.confirm.application.1')} <b>${folder.getPropertyValue("jconon_application:nome")} ${folder.getPropertyValue("jconon_application:cognome")}</b>,</p>
<p>${message('mail.apertura.rinnovo.1')}<br></p>

<p>${message('mail.apertura.rinnovo.2')} <b>${dataCancellazione}.</b><br></p> 
<p>${message('mail.apertura.rinnovo.3')}</p>
<p>${message('mail.apertura.rinnovo.7')}</p>
<p>${message('mail.cambio.fascia.application.5')}</p> 
<br>
${call.getPropertyValue("jconon_call:requisiti")}
</body>
</html>
