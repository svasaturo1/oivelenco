<html>
<body>
<#assign aDateTime = .now>
<p>${message('mail.confirm.application.1')} <b>${folder.getPropertyValue("jconon_application:nome")?cap_first} ${folder.getPropertyValue("jconon_application:cognome")?cap_first}</b>,</p>
<p>${message('mail.cambio.fascia.application.2', folder.getPropertyValue("jconon_application:data_invio_cambio_fascia").time?string("dd/MM/yyyy '(h. ' HH:mm:ss')'"))}</p>
<p>${message('mail.cambio.fascia.application.3')} ${message('mail.iscrizione.application.fascia.' + folder.getPropertyValue("jconon_application:fascia_professionale_attribuita"))} ${message('mail.cambio.fascia.application.dal', folder.getPropertyValue("jconon_application:data_cambio_fascia").time?string("dd/MM/yyyy"))}</p>
<p>${message('mail.iscrizione.application.4')}</p>

<p>${message('mail.iscrizione.application.5')}</p>
<p>${message('mail.iscrizione.application.6')}</p>
<br>
${call.getPropertyValue("jconon_call:requisiti")}
</body>
</html>
