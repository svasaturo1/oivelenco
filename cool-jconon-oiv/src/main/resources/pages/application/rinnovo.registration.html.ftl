<html>
<body>
<hr/>
<#if call.getPropertyValue("jconon_call:sede")??>
	<#assign sede = call.getPropertyValue("jconon_call:sede")>
<#else>	
	<#assign sede = "">
</#if>

<p>${message('mail.confirm.application.1')} <b>${folder.getPropertyValue("jconon_application:nome")} ${folder.getPropertyValue("jconon_application:cognome")}</b>,</p>
<p>${message('mail.registrazione.rinnovo.5')}<br></p>
<p>${message('mail.registrazione.rinnovo.7')}<br></p>

<p>${message('mail.registrazione.rinnovo.3',contextURL)}<br></p> 
<p>${message('mail.confirm.application.5')},</p>
<p>${message('mail.rinnovo.application.10')}</p> 
<p>${message('mail.append.helpdesk.1')} <a href="${contextURL}/helpdesk">${message('mail.append.helpdesk.2')}</a> ${message('mail.append.helpdesk.3')}</p>
<hr/>
<p>${message('message.auto')}</p>
<p>Si prega di non rispondere.</p>
</body>
</html>
