define(['jquery', 'header', 'json!common', 'cnr/cnr.bulkinfo', 'cnr/cnr.search', 'cnr/cnr.url', 'i18n', 
  'cnr/cnr.ui', 'cnr/cnr.actionbutton', 'cnr/cnr.jconon', 'handlebars', 'cnr/cnr', 
  'moment', 'cnr/cnr.application', 'cnr/cnr.criteria', 'cnr/cnr.ace', 'cnr/cnr.call', 
  'cnr/cnr.node', 'json!cache', 'fp/fp.application','cnr/cnr.ui.widgets', 'cnr/cnr.ui.wysiwyg'],
  function ($, header, common, BulkInfo, Search, URL, i18n, 
    UI, ActionButton, jconon, Handlebars, CNR, 
    moment, Application, Criteria, Ace, Call, 
    Node, cache, ApplicationFp, Widgets, Wysiwyg) {
  "use strict";
  Widgets['ui.wysiwyg'] = Wysiwyg;
  var search,
    rootTypeId = 'F:jconon_application:folder',
    typeId = 'F:jconon_call:folder',
    myType = 'jconon_application:folder',
    elements = {
      table: $('#items'),
      pagination: $('#itemsPagination'),
      orderBy: $('#orderBy'),
      label: $('#emptyResultset'),
      total: $('#total')
    },
    bulkInfo,
    criteria = $('#criteria'),
    callId = URL.querystring.from['cmis:objectId'],
    callCodice = URL.querystring.from['callCodice'];

console.log("main");

  function displayAttachments(el, type, displayFn, i18nModal) {
	 // console.log("contesto "+contesto);
	 //   console.log("i18nModal "+i18nModal);
    var content = $('<div></div>').addClass('modal-inner-fix');
    jconon.findAllegati(el.id, content, type, true, displayFn, true, el);
	
    UI.modal(i18n[i18nModal || 'actions.attachments'], content, undefined, undefined, true);
  }
  function displayCorsi(el, type, displayFn, i18nModal) {
		    var content = $('<div class="modal-inner-fix"><table class="table table-striped tableCorsi" style="top:0;">'+
					'<tbody><tr><th style="width: 15%; border: 1px solid #dddddd; text-align: center; padding: 8px 0px 0px 0px;">Nome ente</th><th style="width: 10%; border: 1px solid #dddddd; text-align: center; padding: 8px 0px 0px 0px;">Data corso</th>'+
					'<th style="width: 24%; border: 1px solid #dddddd; text-align: center; padding: 8px 0px 0px 0px;">Nome corso</th><th style="width: 24%; border: 1px solid #dddddd; text-align: center; padding: 8px 0px 0px 0px;">Nome modulo</th>'+
					'<th style="width: 8%; border: 1px solid #dddddd; text-align: center; padding: 8px 0px 0px 0px;">Numero crediti maturati</th><th style="width: 6.5%; border: 1px solid #dddddd; text-align: center; padding: 8px 0px 0px 0px;">Azioni</th></tr></tbody></div>');
	    jconon.findAllegati(el.id, content, type, true, displayFn, true, el);
		
		  UI.modal(i18n[i18nModal || 'actions.attachments'], content, undefined, undefined, true);	           
  }
  
  
  function manageUrlParams() {
    if (callId) {
      URL.Data.node.node({
        data: {
          nodeRef : callId
        },
        success: function (data) {
          $('#items caption h2').text('DOMANDE RELATIVE AL BANDO ' + data['jconon_call:codice'] + ' - ' + data['jconon_call:sede']);
          bulkInfo.render();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          CNR.log(jqXHR, textStatus, errorThrown);
        }
      });
    } else if (callCodice) {
      URL.Data.search.query({
        data: {
          q: "select cmis:objectId " +
            "from jconon_call:folder where jconon_call:codice = '" + callCodice + "'"
        },
        success: function (data) {
          callId = data.items[0]['cmis:objectId'];
          $('#items caption h2').text('DOMANDE RELATIVE AL BANDO ' + callCodice);
          bulkInfo.render();
          $('#items caption h2').after($('<div class="btn-group pull-right">').append(
              $('<a id="export-elenco" class="btn btn-success"><i class="icon-table"></i> Elenco pubblicato</a>')).append(
    		  $('<a id="export-elenco-esperienze" class="btn btn-primary"><i class="icon-table"></i> Scarico domande con esperienze</a>')).append(
              $('<a id="export-xls" class="btn btn-success"><i class="icon-table"></i> Scarico domande</a>')));
        },
        error: function (jqXHR, textStatus, errorThrown) {
          CNR.log(jqXHR, textStatus, errorThrown);
        }
      });
    } else {
      bulkInfo.render();
    }
  }

  // filter ajax resultSet according to the Criteria form
  function filterFn(data) {

    var filtered = $.grep(data.items, function (el) {
      var callCode = bulkInfo.getDataValueById('filters-codice'),
        callFromDate = bulkInfo.getDataValueById('filters-da_data'),
        callToDate = bulkInfo.getDataValueById('filters-a_data'),
        callStatus = callId ? 'tutti' : bulkInfo.getDataValueById('filters-attivi_scaduti'),
        call = el.relationships.parent ? el.relationships.parent[0] : {},
        now = new Date(common.now),
        isActive = call['jconon_call:data_fine_invio_domande'] === "" ||
          (new Date(call['jconon_call:data_inizio_invio_domande']) < now && new Date(call['jconon_call:data_fine_invio_domande']) > now);

      if (callCode) {
        if (!new RegExp(callCode, "gi").test(call['jconon_call:codice'])) {
          return false;
        }
      }

      if (callFromDate) {
        if (new Date(call['jconon_call:data_fine_invio_domande']) < new Date(callFromDate)) {
          return false;
        }
      }

      if (callToDate) {
        if (new Date(call['jconon_call:data_fine_invio_domande']) > new Date(callToDate)) {
          return false;
        }
      }

      if (callStatus) {
        if (callStatus === 'attivi' && !isActive) {
          return false;
        }
        if (callStatus === 'scaduti' && isActive) {
          return false;
        }
      }
      return true;
    });
    data.items = filtered;
    data.totalNumItems = filtered.length;
    data.hasMoreItems = filtered.length > data.maxItemsPerPage;

    return data;
  }

  function filter(flag) {
	  var btns = criteria.find('.btn-group-vertical .btn');
	  if ( flag && flag.target && flag.target.id && !flag.target.id.includes('data') ){
	      btns
	        .removeClass('active');
	
	      criteria
	        .find('.btn-group-vertical')
	        .find('.default')
	        .addClass('active');
	  }
      if ( flag == true )
    	  search.execute(undefined , true);
      else
    	  search.execute();
//	if($('#user').val()!=""){
//		//$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+', '+$('#user').val());
//		$('#filtriApplicati').text($('#filtriApplicati').text()+', '+$('#user').val());
//	}
	/*else{
		$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text());
	}*/
    setTimeout(function(){ 
    	if($('.widget').hasClass('success') && $(".typeahead").length > 0 && $(".typeahead").css("display").includes("none")){
    		//$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+', '+$('#user').val());
    		$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+', '+$('#user').val());
    	}else if ( $("#filters-da_data").val() != '' && $("#filters-a_data").val() != '' ) {
    		$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+' dal '+$("#filters-da_data").val()+' al '+$("#filters-a_data").val() );
    	}else if( $("#filters-da_data").val() != '' ){
    		$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+' dal '+$("#filters-da_data").val() );
    	}else if( $("#filters-a_data").val() != '' ){
    		$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+' fino al '+$("#filters-a_data").val() );
    	}
    	
    }, 700);
//    if($('.widget').hasClass('success') && $(".typeahead").css("display").includes("none")){
//		//$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+', '+$('#user').val());
//		$('#filtriApplicati').text($('#filtriApplicati').text()+', '+$('#user').val());
//	}
	/*else{
		$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text());
	}*/
  }

  function allegaDocumentoAllaDomanda(type, objectId, successCallback, bigmodal, callbackModal, requiresFile, forbidArchives, maxUploadSize, title, aspects, multiple, rel, successMetadataCallback) {
    return Node.submission({
      nodeRef: objectId,
      objectType: type,
      crudStatus: "INSERT",
      requiresFile: requiresFile === undefined ? true : requiresFile,
      bigmodal: bigmodal,
      callbackModal: callbackModal,
      showFile: true,
      multiple: multiple,
      externalData: aspects || [
        {
          name: 'aspect',
          value: 'P:jconon_attachment:generic_document'
        },
        {
          name: 'aspect',
          value: 'P:jconon_attachment:document_from_rdp'
        },
        {
          name: 'jconon_attachment:user',
          value: common.User.id
        }
      ],
      input: rel,
      modalTitle: title || i18n[type],
      success: function (attachmentsData, data) {
        if (successCallback) {
          successCallback(attachmentsData, data);
        } else {
          $('#applyFilter').click();
        }
      },
      successMetadata : function (data) {
        if (successMetadataCallback) {
            successMetadataCallback(data);
        } else {
            $('#applyFilter').click();
        }
      },
      forbidArchives: forbidArchives === undefined ? true : forbidArchives,
      maxUploadSize: maxUploadSize === undefined ? false : maxUploadSize
    });
  }
  
//  Handlebars.registerHelper('ultimaModifica', function declare(code, dataInvioDomanda, dataUltimaModifica, dataScadenza) {
//	    var dateFormat = "DD/MM/YYYY HH:mm:ss",
//	      isTemp = (code === 'P' || code === 'I'),
//	    //  msg = i18n['label.application.stato.' + (code === 'I' ? 'P' : code)],
//	      item = $('<label class="label"></label>')
//	        .addClass('label-info')
//	        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
//	      //  .append(msg)
//	        .append(('Profilo utente - ultima modifica ' + CNR.Date.format(dataUltimaModifica, '-', dateFormat)) );
//	    return $('<div>').append(item).html();
//	  });
  
  //-----INIZIO
   
  Handlebars.registerHelper('applicationStatus', function declare(code, dataInvioDomanda, dataUltimaModifica, dataScadenza, fl_prof, fl_dir) {
	  if(code === 'P' || code === 'I'){
		  var dateFormat = "DD/MM/YYYY HH:mm:ss",
	      isTemp = (code === 'P' || code === 'I'),
	    //  msg = i18n['label.application.stato.' + (code === 'I' ? 'P' : code)],
	      item = $('<label class="label"></label>')
	        .addClass('label-success')
	        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
	      //  .append(msg)
	        .append(('Profilo utente - ultima modifica ' + CNR.Date.format(dataUltimaModifica, '-', dateFormat)) ); 
		  return $('<div>').append(item).html();
		  
	  }else{
		  
		  var dateFormat = "DD/MM/YYYY HH:mm:ss",
	      isTemp = (code === 'P' || code === 'I'),
	    //  msg = i18n['label.application.stato.' + (code === 'I' ? 'P' : code)],
	      item = $('<label class="label"></label>')
	        .addClass('label-success')
	        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
	      //  .append(msg)
	        .append(('Profilo utente - ultima modifica ' + CNR.Date.format(dataUltimaModifica, '-', dateFormat)) );
		  
		  var dateFormat2 = "DD/MM/YYYY",
	      isTemp2 = (code === 'P' || code === 'I'),
	  //    msg2 = i18n['label.application.stato.' + ( code)],
	      msg2 = "Domanda di iscrizione inviata",
	      item2;
		  if ( fl_prof || fl_dir ){
			  if ( fl_prof ){
				  item2 = $('<label class="label"></label>')
			        .addClass( 'label-success')
			        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
			        .append(msg2)
			        .append((' il ' + CNR.Date.format(dataInvioDomanda, '-', dateFormat)+ ' - percorso professionale'));
				  return $('<div>').append(item).html() +'<br>'+ $('<div>').append(item2).html();
			  }else{
				  
					  item2 = $('<label class="label"></label>')
				        .addClass( 'label-success')
				        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
				        .append(msg2)
				        .append((' il ' + CNR.Date.format(dataInvioDomanda, '-', dateFormat)+ ' - percorso dirigenziale'));
					  return $('<div>').append(item).html() +'<br>'+ $('<div>').append(item2).html();
				  
			  }
		  }else{
			 
				  item2 = $('<label class="label"></label>')
			        .addClass( 'label-success')
			        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
			        .append(msg2)
			        .append((' il ' + CNR.Date.format(dataInvioDomanda, '-', dateFormat)));
				  return $('<div>').append(item).html() +'<br>'+ $('<div>').append(item2).html();
		  
	
		  }
	  }
		  
//    var dateFormat = "DD/MM/YYYY HH:mm:ss",
//      isTemp = (code === 'P' || code === 'I'),
//      msg = i18n['label.application.stato.' + ( code)],
//      item = $('<label class="label"></label>')
//        .addClass( 'label-success')
//        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
//        .append(msg)
//        .append((' il ' + CNR.Date.format(dataInvioDomanda, '-', dateFormat)));
//    return $('<div>').append(item).html();
  });
  
 // -------FINE
  
//  
//// AGGIUNTO PER SECONDO RILASCIO 
//  Handlebars.registerHelper('applicationStatus', function declare(code, dataInvioDomanda, dataUltimaModifica, dataScadenza) {
//	    var dateFormat = "DD/MM/YYYY HH:mm:ss",
//	      isTemp = (code === 'P' || code === 'I'),
//	      msg = i18n['label.application.stato.' + (code === 'I' ? 'P' : code)],
//	      item = $('<label class="label"></label>')
//	        .addClass(isTemp ? 'label-info' : 'label-success')
//	        .addClass(dataScadenza !== "" && (moment().diff(dataScadenza, 'days') > -7) ? 'animated flash' : '')
//	        .append(msg)
//	        .append(isTemp ? (' - ultima modifica ' + CNR.Date.format(dataUltimaModifica, '-', dateFormat)) : (' il ' + CNR.Date.format(dataInvioDomanda, '-', dateFormat)));
//	    return $('<div>').appjconon_attachment_esperienza_non_coerente_dataend(item).html();
//	  });
//// ----FINE  
  
  Handlebars.registerHelper('rinnovoInviato', function declare(dataInvio) {
	  
		   var dateFormat = "DD/MM/YYYY",
		       item = $('<label class="label label-success"></label>')
		        .append('Domanda di rinnovo inviata il ' + CNR.Date.format(dataInvio, '-', dateFormat) );
		    return $('<div>').append(item).html();
	   
	  });
  
  Handlebars.registerHelper('cambioFasciaInviato', function declare(dataInvio, fl_prof, fl_dir) {
	  if ( fl_prof || fl_dir ){
		  if ( fl_prof ){
		   var dateFormat = "DD/MM/YYYY",
		       item = $('<label class="label label-success"></label>')
		        .append('Domanda di cambio fascia inviata il ' + CNR.Date.format(dataInvio, '-', dateFormat) + ' - percorso professionale'  );
		    return $('<div>').append(item).html();
		  }else{
			  var dateFormat = "DD/MM/YYYY",
		       item = $('<label class="label label-success"></label>')
		        .append('Domanda di cambio fascia inviata il ' + CNR.Date.format(dataInvio, '-', dateFormat) + ' - percorso dirigenziale'  );
		    return $('<div>').append(item).html();
		  }
	  }else{
		  var dateFormat = "DD/MM/YYYY",
	       item = $('<label class="label label-success"></label>')
	        .append('Domanda di cambio fascia inviata il ' + CNR.Date.format(dataInvio, '-', dateFormat) );
	    return $('<div>').append(item).html();
	  }
 });
  
  Handlebars.registerHelper('iscrizioneElenco', function declare(numero, data, fascia_professionale_validata, fascia_professionale_attribuita) {
    var dateFormat = "DD/MM/YYYY",
      fascia = fascia_professionale_validata ,
      item = $('<label class="label label-info"></label>')
        .append('Iscritto in Elenco il ' + CNR.Date.format(data, '-', dateFormat) + 
          ' con progressivo n° '+ numero + 
          ' e fascia: ' + fascia);
    return $('<div>').append(item).html();
  });
  
  Handlebars.registerHelper('rinnovoElenco', function declare(numero, data, fascia_professionale_validata, fascia_professionale_attribuita) {
	//   if(data){		  	   
		  var dateFormat = "DD/MM/YYYY",
		      fascia = fascia_professionale_validata || fascia_professionale_attribuita,
		      item = $('<label class="label label-info"></label>')
		        .append('Rinnovato dal ' + CNR.Date.format(data, '-', dateFormat) + 
		          ' con progressivo n° '+ numero);
		    return $('<div>').append(item).html();
//	   }else{
//		   var dateFormat = "DD/MM/YYYY",
//		      fascia = fascia_professionale_validata || fascia_professionale_attribuita,
//		      item = $('<label class="label label-success"></label>')
//		        .append('Rinnovo inviato il ' + CNR.Date.format(dataInvio, '-', dateFormat) );
//		    return $('<div>'jconon_attachment_esperienza_non_coerente_data).append(item).html();
//	   }
	  });
  
  Handlebars.registerHelper('cambioFasciaElenco', function declare(numero, data, fascia_professionale_validata, fascia_professionale_attribuita) {	  	   
			  var dateFormat = "DD/MM/YYYY",
			      fascia = fascia_professionale_validata || fascia_professionale_attribuita,
			      item = $('<label class="label label-info"></label>')
			        .append('Cambio fascia il ' + CNR.Date.format(data, '-', dateFormat) + 
			          ' con fascia '+ fascia_professionale_validata);
			    return $('<div>').append(item).html();
		  });

  Handlebars.registerHelper('esclusioneRinuncia', function esclusioneRinunciaFn(esclusioneRinuncia, statoDomanda, rimossoElenco, dataRimozione) {
    var a, testo = rimossoElenco === true ? "Cancellato dall'Elenco in data " : "Escluso dall'Elenco in data ";
    testo += CNR.Date.format(dataRimozione, "-", "DD/MM/YYYY");
    if (esclusioneRinuncia) {
        a = $('<span class="label label-important animated flash"></span>').append(testo);
    }
    return $('<div>').append(a).html();
  });

  Handlebars.registerHelper('comunicazione', function comunicazioneFn(dataComunicazione) {
    var a, testo = "Comunicazione inviata in data ";
    testo += CNR.Date.format(dataComunicazione, "-", "DD/MM/YYYY");
    a = $('<span class="label label-success animated flash"></span>').append(testo);
    return $('<div>').append(a).html();
  });

  Handlebars.registerHelper('scadenza', function scadenza(date) {
    var isExpired = CNR.Date.isPast(new Date(date)),
      a = $('<span>' + i18n[isExpired ? "label.th.jconon_bando_data_scadenza_expired" : "label.th.jconon_bando_data_scadenza"] + '</span>')
        .append(' ')
        .addClass(isExpired ? 'text-error' : '')
        .append(CNR.Date.format(date, "-", "DD/MM/YYYY HH:mm:ss"));
    return $('<div>').append(a).html();
  });
  Handlebars.registerHelper('annotazioni', function annotazioni(date) {
	  var a, testo = "Sono presenti annotazioni";
	 //   testo += CNR.Date.format(dataComunicazione, "-", "DD/MM/YYYY");
	    a = $('<span class="badge badge-warning annotazione"></span>').append(testo);
	    return $('<div>').append(a).html();
	  });
  Handlebars.registerHelper('nonCoerente', function nonCoerente(dataNonCoerente, isVisibleGen,dataNonCoerenteRinnovo, isVisibleRinnovo, isRdP) {
	// var  isRdP = Call.isRdP(callData['jconon_call:rdp']) || common.User.admin;
	  if ( dataNonCoerente ){
		  if ( isRdP || isVisibleGen ){
			  var data, dataNCR, dataNCG;
			  dataNCR = new Date (dataNonCoerenteRinnovo);
			  dataNCG = new Date (dataNonCoerente);
				if ( dataNCR.getTime() >= dataNCG.getTime() )		
				
					data = dataNCR;
				else
					 data = dataNCG;
			  var a, testo = "Presenti esperienze non coerenti per la fascia (ultima in data "+CNR.Date.format(data, "-", "DD/MM/YYYY")+")";
			 //   testo += CNR.Date.format(dataComunicazione, "-", "DD/MM/YYYY");
			    a = $('<span class="badge badge-warning nonCoerenti"></span>').append(testo);
			    return $('<div>').append(a).html();
		 } 
	  }
	 
	  });
  Handlebars.registerHelper('nonCoerenteRinnovo', function nonCoerente(dataNonCoerente, isVisibleGen,dataNonCoerenteRinnovo, isVisibleRinnovo, isRdP) {
	//if ( !dataNonCoerente ){
	  if ( isRdP || isVisibleRinnovo ){
	  var data = CNR.Date.format(dataNonCoerenteRinnovo, "-", "DD/MM/YYYY");
	  var a, testo = "Presenti esperienze non coerenti per il rinnovo (ultima in data "+data+")";
	 //   testo += CNR.Date.format(dataComunicazione, "-", "DD/MM/YYYY");
	    a = $('<span class="badge badge-warning nonCoerentiRinnovo"></span>').append(testo);
	    return $('<div>').append(a).html();
	  
	}else  if (!isRdP && isVisibleRinnovo ){
		  var data = CNR.Date.format(dataNonCoerenteRinnovo, "-", "DD/MM/YYYY");
		  var a, testo = "Presenti esperienze non coerenti per il rinnovo (ultima in data "+data+")";
		 //   testo += CNR.Date.format(dataComunicazione, "-", "DD/MM/YYYY");
		    a = $('<span class="badge badge-warning nonCoerentiRinnovo"></span>').append(testo);
		    return $('<div>').append(a).html();
		  }
 // }
	  });
  Handlebars.registerHelper('ifIn', function(elem, list, isRdp, options) {
    if(list.indexOf(elem) > -1 && isRdp) {
      return options.fn(this);
    }
    return options.inverse(this);
  });
  
  search = new Search({
    elements: elements,
    columns: ['cmis:parentId', 'jconon_application:stato_domanda', 'jconon_application:nome', 'jconon_application:cognome', 'jconon_application:data_domanda', 'jconon_application:codice_fiscale', 'jconon_application:data_nascita', 'jconon_application:esclusione_rinuncia', 'jconon_application:user', 'jconon_application:data_invio_rinnovo_elenco', 'jconon_application:data_invio_cambio_fascia', 'jconon_application:data_rinnovo_elenco', 'jconon_application:data_cambio_fascia'],
    fields: {
      'nome': null,
      'data di creazione': null,
      'Cognome': 'jconon_application:cognome',
      'Nome': 'jconon_application:nome',
      'Data domanda': 'jconon_application:data_domanda',
      'Data domanda rinnovo': 'jconon_application:data_invio_rinnovo_elenco',
      'Data domanda cambio fascia': 'jconon_application:data_invio_cambio_fascia',
      'Data ultimo salvataggio': 'cmis:lastModificationDate',      
      'Stato domanda': 'jconon_application:stato_domanda',
      'Numero di iscrizione in elenco':  'jconon_application:progressivo_iscrizione_elenco'
    },
    orderBy: [{
      field: 'jconon_application:data_domanda',
      desc: true
    }],
    type: myType,
    maxItems: callId||callCodice ? undefined : 100,
    dataSource: function (page, settings, getUrlParams, flag) {
      var deferred,
        baseCriteria = new Criteria().not(new Criteria().equals('jconon_application:stato_domanda', 'I').build()),
        criteria = new Criteria(),
        applicationStatus = bulkInfo.getDataValueById('filters-provvisorie_inviate'),
        applicationDateDa = bulkInfo.getDataValueById('filters-da_data'),
        applicationDateA = bulkInfo.getDataValueById('filters-a_data'),
        user = bulkInfo.getDataValueById('user'),
        url;
      console.log("filtro data dal "+applicationDateDa);
		if( flag )
			applicationStatus = "tutte";
      if (applicationStatus && applicationStatus !== 'tutte' && applicationStatus !== 'attive' && applicationStatus !== 'escluse' && 
    		  applicationStatus !== 'C' && applicationStatus !== 'rinnovo_inviato' && applicationStatus !== 'cambio_fascia_inviato' && 
    		  	applicationStatus !== 'rinnovati' && applicationStatus !== 'cambio_fascia_approvato' ) {
    	  	
    	  baseCriteria.and(new Criteria().equals('jconon_application:stato_domanda', applicationStatus).build());
      }
      
      if (applicationStatus && applicationStatus === 'C') {
    	 
          baseCriteria.and(new Criteria().equals('jconon_application:stato_domanda', applicationStatus).build());
          
      //    baseCriteria.and(new Criteria().lte('jconon_application:progressivo_iscrizione_elenco',0).build());
      
          //   baseCriteria.and(new Criteria().equals('jconon_application:progressivo_iscrizione_elenco', prova).build());
    //	  baseCriteria.and(new Criteria().not(new Criteria().isNotNull('jconon_application:progressivo_iscrizione_elenco').build()));
    //    baseCriteria.and(new Criteria().isNull('jconon_application:progressivo_iscrizione_elenco').build());
//          baseCriteria.and(new Criteria().not(new Criteria().contains('ASPECT:\'jconon_application:data_iscrizione_elenco\'')).build());
       //   NOT CONTAINS('ASPECT:\'myNs:myAspect\'')
       //   baseCriteria.and(new Criteria().equals('jconon_application:data_iscrizione_elenco', 'null').build());
 
        }
      if (applicationStatus && applicationStatus === 'rinnovo_inviato') {
          baseCriteria.and(new Criteria().isNotNull('jconon_application:data_invio_rinnovo_elenco').build());
      //    baseCriteria.and(new Criteria().exists('jconon_application:data_rinnovo_elenco').build());
        //  baseCriteria.and(new Criteria().equals('jconon_application:fascia_professionale_validata','').build());
    //      baseCriteria.and(new Criteria().lte('jconon_application:data_rinnovo_elenco','jconon_application:data_invio_rinnovo_elenco').build());
        //  baseCriteria.and( new Criteria().not( new Criteria().isNotNull('jconon_application:data_rinnovo_elenco').build() ) );
        }
      if (applicationStatus && applicationStatus === 'cambio_fascia_inviato') {
          baseCriteria.and(new Criteria().isNotNull('jconon_application:data_invio_cambio_fascia').build());
          
        }
    
      if (applicationStatus && applicationStatus === 'rinnovati') {
          baseCriteria.and(new Criteria().isNotNull('jconon_application:data_rinnovo_elenco').build());
        }

      if (applicationStatus && applicationStatus === 'attive') {
        baseCriteria.and(new Criteria().isNotNull('jconon_application:progressivo_iscrizione_elenco').build());
      }
      
      if (applicationStatus && applicationStatus === 'cambio_fascia_approvato') {
          baseCriteria.and(new Criteria().isNotNull('jconon_application:data_cambio_fascia').build());
        }
      if (applicationStatus && applicationStatus === 'escluse') {
        baseCriteria.and(new Criteria().equals('jconon_application:stato_domanda', 'C').build());
        baseCriteria.and(new Criteria().isNotNull('jconon_application:esclusione_rinuncia').build());
      }
      
      if ( applicationDateDa ) {
    	 if ( applicationStatus && applicationStatus === 'rinnovo_inviato' ){
    		 baseCriteria.and(new Criteria().gte('jconon_application:data_invio_rinnovo_elenco',   moment(applicationDateDa).toDate().getTime() ).build());
    	 }else if ( applicationStatus && applicationStatus === 'cambio_fascia_inviato' ){
    		 baseCriteria.and(new Criteria().gte('jconon_application:data_invio_cambio_fascia',   moment(applicationDateDa).toDate().getTime() ).build());
    	 }else if ( applicationStatus && applicationStatus === 'cambio_fascia_approvato' ){
    		 baseCriteria.and(new Criteria().gte('jconon_application:data_cambio_fascia',   moment(applicationDateDa).toDate().getTime() ).build());
    	 }else if ( applicationStatus && applicationStatus === 'rinnovati' ){
    		 baseCriteria.and(new Criteria().gte('jconon_application:data_rinnovo_elenco',   moment(applicationDateDa).toDate().getTime() ).build());
    	 }else if ( applicationStatus && applicationStatus === 'attive' ){
    		 baseCriteria.and(new Criteria().gte('jconon_application:data_iscrizione_elenco',   moment(applicationDateDa).toDate().getTime() ).build());
    	 }else if ( applicationStatus && applicationStatus === 'C' ){
    		 baseCriteria.and(new Criteria().gte('jconon_application:data_domanda',   moment(applicationDateDa).toDate().getTime() ).build());
    	 }else if ( applicationStatus && applicationStatus === 'escluse' ){
    		 baseCriteria.and(new Criteria().gte('jconon_application:data_rimozione_elenco',   moment(applicationDateDa).toDate().getTime() ).build());
    	 }else
    	  
    		 baseCriteria.and(new Criteria().gte('cmis:creationDate',   moment(applicationDateDa).toDate().getTime() ).build());
    	
    //	 $('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+', '+$('#user').val());
       
      }
      
      if ( applicationDateA ) {
     	 
    	  if ( applicationStatus && applicationStatus === 'rinnovo_inviato' ){
     		 baseCriteria.and(new Criteria().lte('jconon_application:data_invio_rinnovo_elenco',   moment(applicationDateA).toDate().getTime() ).build());
     	 }else if ( applicationStatus && applicationStatus === 'cambio_fascia_inviato' ){
     		 baseCriteria.and(new Criteria().lte('jconon_application:data_invio_cambio_fascia',   moment(applicationDateA).toDate().getTime() ).build());
     	 }else if ( applicationStatus && applicationStatus === 'cambio_fascia_approvato' ){
     		 baseCriteria.and(new Criteria().lte('jconon_application:data_cambio_fascia',   moment(applicationDateA).toDate().getTime() ).build());
     	 }else if ( applicationStatus && applicationStatus === 'rinnovati' ){
     		 baseCriteria.and(new Criteria().lte('jconon_application:data_rinnovo_elenco',   moment(applicationDateA).toDate().getTime() ).build());
     	 }else if ( applicationStatus && applicationStatus === 'attive' ){
     		 baseCriteria.and(new Criteria().lte('jconon_application:data_iscrizione_elenco',   moment(applicationDateA).toDate().getTime() ).build());
     	 }else if ( applicationStatus && applicationStatus === 'C' ){
     		 baseCriteria.and(new Criteria().lte('jconon_application:data_domanda',   moment(applicationDateA).toDate().getTime() ).build());
     	 }else if ( applicationStatus && applicationStatus === 'escluse' ){
    		 baseCriteria.and(new Criteria().lte('jconon_application:data_rimozione_elenco',   moment(applicationDateA).toDate().getTime() ).build());
    	 }else
     	  
     		 baseCriteria.and(new Criteria().lte('cmis:creationDate',   moment(applicationDateA).toDate().getTime() ).build());
    	  
        }
     
      if (callId) {
        if (cache['query.index.enable']) {
            criteria.inTree(callId);
        } else {
            criteria.inFolder(callId);
        }
        if (user) {
          criteria.and(new Criteria().equals('jconon_application:codice_fiscale', user).build());
        }
      } else {
        criteria.equals('jconon_application:user', common.User.id);
      }
      settings.lastCriteria = criteria.and(baseCriteria.build()).build();
//console.log("settings.lastCriteria "+JSON.stringify(settings.lastCriteria));
      $('#export-xls').off('click').on('click', function () {
        var close = UI.progress();
        jconon.Data.call.applications_single_call({
          type: 'GET',
          data:  getUrlParams(page),
          success: function (data) {
            var url = URL.template(jconon.URL.call.downloadXLS, {
              objectId: data.objectId,
              fileName: data.fileName,
              exportData: true,
              mimeType: 'application/vnd.ms-excel;charset=UTF-8'
            });
            window.location = url;
          },
          complete: close,
          error: URL.errorFn
        });

      });
      
      $('#export-elenco').off('click').on('click', function () {
    	
        var close = UI.progress(), data = getUrlParams(page);
        data.callId = callId;
        $.ajax({
          url: cache.baseUrl + "/rest/application-fp/applications-elenco.xls",
        //  url: cache.baseUrl + "/rest/application-fp/prova-scarico1",
          type: 'GET',
          data:  data,
          success: function (data) {
            var url = URL.template(jconon.URL.call.downloadXLS, {
              objectId: data.objectId,
              fileName: data.fileName,
              exportData: true,
              mimeType: 'application/vnd.ms-excel;charset=UTF-8'
            });     
            window.location = url;
          },
          complete: close,
          error: URL.errorFn
        });
               
      });
      
      $('#export-elenco-esperienze').off('click').on('click', function () {
      	
          var close = UI.progress(), data = getUrlParams(page);
          data.callId = callId;
      //    console.log("url param scarico esperienze "+JSON.stringify(data));
          $.ajax({
            url: cache.baseUrl + "/rest/application-fp/applications-elenco-esperienze.xls",
            type: 'GET',
            data:  data,
            success: function (data) {
              var url = URL.template(jconon.URL.call.downloadXLS, {
                objectId: data.objectId,
                fileName: data.fileName,
                exportData: true,
                mimeType: 'application/vnd.ms-excel;charset=UTF-8'
              });     
              window.location = url;
            },
            complete: close,
            error: URL.errorFn
          });
                 
        });
      
      $('#estraiScaricoFiltrato').off('click').on('click', function () {
        	
          var close = UI.progress(), data = getUrlParams(page);
          data.callId = callId;
          data.filtroDataDa = applicationDateDa ? moment(applicationDateDa).toDate().getTime() : undefined;
          data.filtroDataA = applicationDateA ? moment(applicationDateA).toDate().getTime() : undefined;
          
     //     console.log("url param scarico filtrato "+JSON.stringify(data));
          $.ajax({
            url: cache.baseUrl + "/rest/application-fp/applications-elenco-filtrato.xls",
            type: 'GET',
            data: data,
//            data: { data:data,
//	            filtroDataDa:	applicationDateDa ? moment(applicationDateDa).toDate().getTime() : null ,
//	            filtroDataA:	applicationDateA ? moment(applicationDateA).toDate().getTime() : null
//		          },
           
            success: function (data) {
              var url = URL.template(jconon.URL.call.downloadXLS, {
                objectId: data.objectId,
                fileName: data.fileName,
                exportData: true,
                mimeType: 'application/vnd.ms-excel;charset=UTF-8'
              });     
              window.location = url;
            },
            complete: close,
            error: URL.errorFn
          });
                 
        });
      
      $('#inviaMail').off('click').on('click', function () {
      	
          var close = UI.progress(), data = getUrlParams(page);
          data.callId = callId;
          data.filtroDataDa = applicationDateDa ? moment(applicationDateDa).toDate().getTime() : undefined;
          data.filtroDataA = applicationDateA ? moment(applicationDateA).toDate().getTime() : undefined;
          
     //     console.log("url param scarico filtrato "+JSON.stringify(data));
          $.ajax({
            url: cache.baseUrl + "/rest/application-fp/invia-mail",
            type: 'GET',
            data: data,
//            data: { data:data,
//	            filtroDataDa:	applicationDateDa ? moment(applicationDateDa).toDate().getTime() : null ,
//	            filtroDataA:	applicationDateA ? moment(applicationDateA).toDate().getTime() : null
//		          },
           
            success: function (data) {
//              var url = URL.template(jconon.URL.call.downloadXLS, {
//                objectId: data.objectId,
//                fileName: data.fileName,
//                exportData: true,
//                mimeType: 'application/vnd.ms-excel;charset=UTF-8'
//              });     
//              window.location = url;
            },
            complete: close,
            error: URL.errorFn
          });
                 
        });
      
      $('#cancellazioneMass').off('click').on('click', function () {
        	
          var close = UI.progress(), data = getUrlParams(page);
          data.callId = callId;
          data.filtroDataDa = applicationDateDa ? moment(applicationDateDa).toDate().getTime() : undefined;
          data.filtroDataA = applicationDateA ? moment(applicationDateA).toDate().getTime() : undefined;
          
     //     console.log("url param scarico filtrato "+JSON.stringify(data));
          $.ajax({
            url: cache.baseUrl + "/rest/application-fp/cancellazioneMassiva",
            type: 'GET',
            data: data,
//            data: { data:data,
//	            filtroDataDa:	applicationDateDa ? moment(applicationDateDa).toDate().getTime() : null ,
//	            filtroDataA:	applicationDateA ? moment(applicationDateA).toDate().getTime() : null
//		          },
           
            success: function (data) {
//              var url = URL.template(jconon.URL.call.downloadXLS, {
//                objectId: data.objectId,
//                fileName: data.fileName,
//                exportData: true,
//                mimeType: 'application/vnd.ms-excel;charset=UTF-8'
//              });     
//              window.location = url;
            },
            complete: close,
            error: URL.errorFn
          });
                 
        });
      

      deferred = URL.Data.search.query({
        cache: false,
        queue: true,
        data: $.extend({}, getUrlParams(page), {
          fetchCmisObject: true,
          relationship: 'parent'
        })
      });

      deferred.done(function (data) {
        if (elements.total) {
          elements.total.text(data.totalNumItems + ' elementi trovati in totale');
        }
      });

      if (!callId) {
        deferred = deferred.pipe(filterFn);
      }

      return deferred;
    },
    display: {
      resultSet: function (resultSet, target) {
        var xhr = new BulkInfo({
          target: $('<tbody>').appendTo(target),
          handlebarsId: 'application-main-results',
          path: typeId,
          metadata: resultSet,
          handlebarsSettings: {
            call_type: typeId === rootTypeId ? true : false,
            callId: callId,
            isRdP: (Call.isRdP(resultSet[0].relationships.parent[0]['jconon_call:rdp']) || common.User.admin),
            isUser: (!Call.isRdP(resultSet[0].relationships.parent[0]['jconon_call:rdp']) && !common.User.admin)
          }
        }).handlebars();

        xhr.done(function () {

          target
            .off('click')
            .on('click', '.requirements', function () {
              var data = $("<div></div>").addClass('modal-inner-fix').html($(this).data('content'));
              UI.modal('<i class="icon-info-sign text-info animated flash"></i> ' + i18n['label.th.jconon_bando_elenco_titoli_studio'], data);
            })
//            .on('click', '.annotazione', function () {
//              var data = $("<div></div>").addClass('modal-inner-fix').html($(this).data('content'));
//              UI.modal('<i class="icon-pencil text-info animated flash"></i> Annotazioni', data);
//              return false;
//            })
            .on('click', '.code', function () {
              var data = $("<div></div>").addClass('modal-inner-fix').html($(this).data('content'));
              UI.modal('<i class="icon-info-sign text-info animated flash"></i> ' + i18n['label.call'], data);
            })
            .on('click', '.user', function (event) {
              var authority = $(event.target).attr('data-user');
              Ace.showMetadata(authority);
            });

          var rows = target.find('tbody tr');
          $.each(resultSet, function (index, el) {
            var target = $(rows.get(index)).find('td:last'),
              callData = el.relationships.parent[0],
              callAllowableActions = callData.allowableActions,
              dropdowns = {},
              bandoInCorso = (callData['jconon_call:data_fine_invio_domande'] === "" ||
                new Date(callData['jconon_call:data_fine_invio_domande']) > new Date(common.now)),
              displayActionButton = true,
              defaultChoice,
              customButtons = {
                select: false,
                permissions: false,
                remove: false,
                copy: false,
                cut: false,
                print: function () {
                  Application.print(el.id, el['jconon_application:stato_domanda'], bandoInCorso, el['jconon_application:data_domanda']);
                }
                
              };
       	 if ( (common.User.admin || Call.isRdP(callData['jconon_call:rdp'] ) || common.User.id === el['jconon_application:user'] ) && el['jconon_application:data_invio_rinnovo_elenco'] != null && el['jconon_application:stato_domanda'] == 'C' ) { // && el['jconon_application:data_rinnovo_elenco'] == null
       		customButtons.printRinnovo= function () {
                Application.printRinnovo(el.id, el['jconon_application:stato_domanda'], bandoInCorso, el['jconon_application:data_domanda']);
              }
       	 }
       	 if ( (common.User.admin || Call.isRdP(callData['jconon_call:rdp'] ) || common.User.id === el['jconon_application:user'] ) && el['jconon_application:data_invio_cambio_fascia'] != null && el['jconon_application:stato_domanda'] == 'C' ) { // && el['jconon_application:data_rinnovo_elenco'] == null
        		customButtons.printCambioFascia= function () {
                 Application.printCambioFascia(el.id, el['jconon_application:stato_domanda'], bandoInCorso, el['jconon_application:data_domanda']);
               }
        	 }
            if (callData['jconon_call:elenco_sezioni_domanda'].indexOf('affix_tabTitoli') >= 0) {
              customButtons.attachments = function () {
                displayAttachments(el, 'jconon_attachment:generic_document', Application.displayTitoli,undefined,'actions.attachments');
              };
            }
            if (callData['jconon_call:elenco_sezioni_domanda'].indexOf('affix_tabCurriculum') >= 0) {
              customButtons.curriculum = function () {
                //Curriculum
                displayAttachments(el, 'jconon_attachment:cv_element', Application.displayCurriculum, 'actions.curriculum');
              };
            }
            if (callData['jconon_call:elenco_sezioni_domanda'].indexOf('affix_tabSchedaAnonima') >= 0) {
              customButtons.schedaAnonima = function () {
                //Scheda Anonima
                displayAttachments(el, 'jconon_scheda_anonima:document', ApplicationFp.displayEsperienzeOIV, 'actions.schedaAnonima');
              };
            }
       	 if ( el['jconon_application:data_invio_rinnovo_elenco'] != null && 
       			 	el['jconon_application:stato_domanda'] == 'C' ) { 

            customButtons.corsi = function () {
                //Corsi rinnovo
                displayCorsi(el, 'jconon_scheda_anonima:document_rinnovo', ApplicationFp.displayDettaglioCorsiOIVRinnovo, 'actions.corsi');      
              };
       	 }
            if (callData['jconon_call:elenco_sezioni_domanda'].indexOf('affix_tabElencoProdotti') >= 0) {
              customButtons.productList = function () {
                //Elenco Prodotti
                displayAttachments(el, 'cvpeople:noSelectedProduct', Application.displayProdotti, 'actions.productList');
              };
            }
            if (callData['jconon_call:elenco_sezioni_domanda'].indexOf('affix_tabProdottiScelti') >= 0) {
              customButtons.productSelected = function () {
                //Prodotti Scelti
                displayAttachments(el, 'cvpeople:selectedProduct', Application.displayProdottiScelti, 'actions.productSelected');
              };
            }
//            //  Modifica
//            customButtons.edit = function () {
//            	
//              window.location = jconon.URL.application.manage + '?callId=' + callData['cmis:objectId'] + '&applicationId=' + el['cmis:objectId'];
//				
//            };
            
//            customButtons.rinnovo = function () {
//                window.location = jconon.URL.application.rinnovo + '?callId=' + callData['cmis:objectId'] + '&applicationId=' + el['cmis:objectId'];
//  				
//              };
            if (common.User.admin || Call.isRdP(callData['jconon_call:rdp'])) {
		                customButtons.comunicazione = function () {
							console.log("invia");
		                  var bulkInfoAllegato = allegaDocumentoAllaDomanda('D_jconon_comunicazione_attachment',el['cmis:objectId'],
		                    
		                    function (attachmentsData, data) {
		                        $.ajax({
		                            url: cache.baseUrl + "/rest/application-fp/message",
		                            type: 'POST',
		                            data: {
		                              idDomanda : el['cmis:objectId'],
		                              nodeRefDocumento : data !== undefined ? data['alfcmis:nodeRef'] : undefined
		                            },
		                            success: function (data) {
		                               $('#applyFilter').click();
		                            },
		                            error: jconon.error
		                        });
		                    }, true, function (modal) {
		                        $(window).on('shown.bs.modal', function (event) {
		                            var nome = el['jconon_application:nome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
		                                return $word.toUpperCase();
		                            }), cognome = el['jconon_application:cognome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
		                                return $word.toUpperCase();
		                            });
		                            modal.find('#oggetto_notifica_email').val(i18n['app.name'] + ' - ' + i18n['mail.subject.comunicazione']);
		                            var testo = i18n['mail.confirm.application.1'];
		                                testo += el['jconon_application:sesso'] === 'M' ? ' dott.' : ' dott.ssa';
		                                testo += ' <b style="text-transform: capitalize;">' + nome + ' ' + cognome + '</b>,';
		                               // testo += callData['jconon_call:requisiti_en'];
		                                testo += i18n['mail.body.comunicazione'];
		                                testo += i18n['mail.firma.esclusione'];
		                            var textarea = modal.find('#testo_notifica_email');
		                            textarea.val(testo);
		                            var ck = textarea.ckeditor({
		                                toolbarGroups: [
		                                    { name: 'clipboard', groups: ['clipboard'] },
		                                    { name: 'basicstyles', groups: ['basicstyles'] },
		                                    { name: 'paragraph', groups: ['list', 'align'] }],
		                                    removePlugins: 'elementspath'
		                            });
		                            ck.editor.on('change', function () {
		                              var html = ck.val();
		                              textarea.parent().find('control-group widget').data('value', html || null);
		                            });
		
		                            ck.editor.on('setData', function (event) {
		                              var html = event.data.dataValue;
		                              textarea.parent().find('control-group widget').data('value', html || null);
		                            });
		                        });
		                    }
		                  , false, false, true);
		                };
            }
            
			customButtons.visualizza_comunicazioni = function () {
		//		console.log("visualizza "+JSON.stringify(el));
		//		console.log("visualizza "+'jconon_attachment:generic_document');
				
				displayAttachments(el, 'jconon_attachment:generic_document',  Application.displayComunicazioni, 'actions.visualizza_comunicazioni');
					
			};
		//	console.log("is rdp "+Call.isRdP(callData['jconon_call:rdp']));
		//	console.log("is rdp2 "+callData['jconon_call:rdp']);
		if (common.User.admin || Call.isRdP(callData['jconon_call:rdp'])) {
			
			customButtons.note = function () {
				console.log("note");
              var bulkInfoAllegato = allegaDocumentoAllaDomanda('D_jconon_note_attachment',
                el['cmis:objectId'],
                function (attachmentsData, data) {
                    $.ajax({
                        url: cache.baseUrl + "/rest/application-fp/message",
                        type: 'POST',
                        data: {
                          idDomanda : el['cmis:objectId'],
                          nodeRefDocumento : data !== undefined ? data['alfcmis:nodeRef'] : undefined
                        },
                        success: function (data) {
                           $('#applyFilter').click();
                        },
                        error: jconon.error
                    });
                }, true, function (modal) {
                 
              	 modal.find('#D_jconon_note_attachment #default .widget').css("display","none");
             //  	modal.find("#fl_invia_notifica_email button[data-value='false']").removeClass("active");
                    $(window).on('shown.bs.modal', function (event) {
                    	modal.find("#fl_invia_notifica_email button[data-value='true']").click();
                    	modal.find("label[for='oggetto_notifica_email']").text("Titolo")
                    //	 modal.find('#D_jconon_note_attachment #default .widget').css("display","none");
                        var nome = el['jconon_application:nome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                            return $word.toUpperCase();
                        }), cognome = el['jconon_application:cognome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                            return $word.toUpperCase();
                        });
                        modal.find('#oggetto_notifica_email').val(i18n['note.subject'] );
                        var textarea = modal.find('#testo_notifica_email');
                   //     textarea.val(testo);
                        var ck = textarea.ckeditor({
                            toolbarGroups: [
                                { name: 'clipboard', groups: ['clipboard'] },
                                { name: 'basicstyles', groups: ['basicstyles'] },
                                { name: 'paragraph', groups: ['list', 'align'] }],
                                removePlugins: 'elementspath'
                        });
                        ck.editor.on('change', function () {
                          var html = ck.val();
                          textarea.parent().find('control-group widget').data('value', html || null);
                        });

                        ck.editor.on('setData', function (event) {
                          var html = event.data.dataValue;
                          textarea.parent().find('control-group widget').data('value', html || null);
                        });
                    });
                  //  $("#D_jconon_note_attachment #default .widget").css("display","none");

                }
              , false, false, true);
            };
			
			customButtons.visualizza_note = function () {
			//	console.log("visualizza "+JSON.stringify(el));
			//	console.log("visualizza note "+'jconon_attachment:generic_document');
				
               displayAttachments(el, 'jconon_attachment:generic_document',  ApplicationFp.displayNote, 'actions.visualizza_note');
               
		  };
			
		  customButtons.assegna_fascia = function () {
			  console.log("assegna fascia");
            var content = $("<div></div>"),
              bulkinfo = new BulkInfo({
              target: content,
              path: "P:jconon_application:aspect_fascia_professionale_attribuita",
              objectId: el['cmis:objectId'],
              formclass: 'form-inline',
              name: 'default',
              callback : {
                afterCreateForm: function (form) {
                  form.find('#button_fascia_professionale_esegui_calcolo').off('click').on('click', function () {
                    $.ajax({
                      url: cache.baseUrl + "/rest/application-fp/applications-ricalcola-fascia",
                      type: 'GET',
                      data:  {
                        'applicationId' : el['cmis:objectId']
                      },
                      success: function (data) {
					//	  console.log(" fascia "+JSON.stringify(data)+" v   - "+data);
                        form.find('#fascia_professionale_attribuita').val(data['jconon_application:fascia_professionale_attribuita']);
                        UI.success("La fascia ricalcolata è: " + data['jconon_application:fascia_professionale_attribuita']);
                      },
                      error: URL.errorFn
                    });
                  });
                }
              }
            });
            bulkinfo.render();
            UI.modal('<i class="icon-edit"></i> Assegna Fascia', content, function () {
              var close = UI.progress(), d = bulkinfo.getData();
              d.push(
                {
                  id: 'cmis:objectId',
                  name: 'cmis:objectId',
                  value: el['cmis:objectId']
                },
                {
                  name: 'aspect', 
                  value: 'P:jconon_application:aspect_fascia_professionale_attribuita'
                },
                {
                  name: 'jconon_application:fascia_professionale_esegui_calcolo',
                  value: false
                }                        
              );
              jconon.Data.application.main({
                type: 'PUT',
                data: d,
                success: function (data) {
                  UI.success(i18n['message.aggiornamento.fascia.eseguito']);
                  $('#applyFilter').click();
                },
                complete: close,
                error: URL.errorFn
              });
            });
          };
          

          
          
          if (el['jconon_application:progressivo_iscrizione_elenco'] == null && el['jconon_application:stato_domanda'] == 'C' ) {
              customButtons.inserisci = function () {
            	  var close = UI.progress();
          //   		setTimeout(function(){ 
//              			close();
             		 
            	  var bulkInfoAllegato = allegaDocumentoAllaDomanda('D_jconon_iscrizione_attachment',
                          el['cmis:objectId'],
                          function (attachmentsData, data) {
                              $.ajax({
                                  url: cache.baseUrl + "/rest/application-fp/message",
                                  type: 'POST',
                                  data: {
                                    idDomanda : el['cmis:objectId'],
                                    nodeRefDocumento : data !== undefined ? data['alfcmis:nodeRef'] : undefined
                                  },
//                                  success: function (data) {
//                                	  UI.success('Iscrizione avvenuta correttamente.1');
//                                     $('#applyFilter').click();
//                                  },
                                  error: jconon.error
                              });
                          }
                          , true, function (modal) {
                        		
                        	  modal.find('.modal-inner-fix .fileupload').css("display","none");
                           	modal.find('#D_jconon_iscrizione_attachment #default .widget').css("display","none");
                           	modal.find('.modal-header').css("display","none");
                           	modal.find('.modal-body').css("display","none");
                           	modal.find('.modal-footer').css("display","none");
                        	  var provvisorioTemp;
                        	  $.ajax({
                                  url: cache.baseUrl + "/rest/application-fp/get-progressivo-temp",
                                  type: 'POST',
                                  data: {
                                    idDomanda : el['cmis:objectId']
                                   
                                  },
                                  success: function (data) {
                                
                                  		
                                	  provvisorioTemp = data;
                                //	  		$('.in .modal').css("display","block");;
                                	  $('.in .modal-header').css("display","block");
                                	  $('.in .modal-body').css("display","block");
                                	  $('.in .modal-footer').css("display","block");
                                           	$(".in #fl_invia_notifica_email button[data-value='true']").click();
                                           	
                                           //	 modal.find('#D_jconon_note_attachment #default .widget').css("display","none");
                                               var nome = el['jconon_application:nome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                                                   return $word.toUpperCase();
                                               }), cognome = el['jconon_application:cognome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                                                   return $word.toUpperCase();
                                               });
                                               $('.in #oggetto_notifica_email').val(i18n['app.name'] +' - '+ i18n['mail.subject.iscrizione'] );
                 

                                               var textarea = $('.in #testo_notifica_email');
                                               textarea.val(i18n['mail.confirm.application.1']+" "+"<b>"+nome+" "+cognome+"</b>"+",<br>"+
                                              		 i18n.prop('mail.iscrizione.application.1',new Date(el['jconon_application:data_domanda']).toLocaleString())+"<br>"+
                                              		 i18n['mail.iscrizione.application.2']+" Fascia "+el['jconon_application:fascia_professionale_attribuita']+"<br>"+
                                              		 i18n.prop('mail.iscrizione.application.3', provvisorioTemp, new Date().toLocaleString().split(',')[0] )+"<br>"+
                                              		 i18n['mail.iscrizione.application.4']+
                                              		 i18n['mail.iscrizione.application.5']+"<br>"+
                                              		 i18n['mail.iscrizione.application.6']+"<br>"+
                                           //   		 i18n['mail.iscrizione.application.8']+"<br>"+
                                              		 callData["jconon_call:requisiti"]);
                                              		// callData["jconon_call:requisiti"]);

                                              $('.in #D_jconon_iscrizione_attachment #default .control-group').css("display","none");
                                               if( $('.in .testoIscrizione').length <1 )
                                            	   $('<p class="testoIscrizione">'+i18n.prop('message.confirm.iscrizione.elenco', el['jconon_application:nome'], el['jconon_application:cognome'])+'<p>').insertAfter($('.modal-inner-fix #D_jconon_iscrizione_attachment'));
                                     
                                    
                                  
                                  },
                                  complete: close,
                                  error: jconon.error
                              });
                        	 
                        	  
                              var submitBtn=   modal.find(".submit");
                              submitBtn.on('click', function () {
                            //	  var close = UI.progress();
                                  jconon.Data.application.readmission({
                                    type: 'POST',
                                    data: {
                                      nodeRef : el['cmis:objectId']
                                    },
                                    success: function () {
                                      URL.Data.proxy.childrenGroup({
                                        type: 'POST',
                                        data: JSON.stringify({
                                          'parent_group_name': 'GROUP_ELENCO_OIV',
                                      
                                          'child_name': el['jconon_application:user']
                                        }),
                                        contentType: 'application/json'
                                      });
//                                      UI.success('Iscrizione avvenuta correttamente.2');
//                                      $('#applyFilter').click();
                                    },
                                 //   complete: close,
                                    error: jconon.error
                                  }); 
                                });
         
                          }
                   
                        , false, false, true);  
             //		}, 1000);

              }
            }
         
 if ( el['jconon_application:data_invio_rinnovo_elenco'] != null && el['jconon_application:stato_domanda'] == 'C' && new Date( el['jconon_application:data_rinnovo_elenco'] ).getTime() <  new Date( el['jconon_application:data_invio_rinnovo_elenco'] ).getTime()) { // && el['jconon_application:data_rinnovo_elenco'] == null

      	customButtons.rinnovo = function () {
			
      		
      		var bulkInfoAllegato = allegaDocumentoAllaDomanda('D_jconon_rinnovo_effettuato_attachment',
                    el['cmis:objectId'],
                    function (attachmentsData, data) {
                        $.ajax({
                            url: cache.baseUrl + "/rest/application-fp/message",
                            type: 'POST',
                            data: {
                              idDomanda : el['cmis:objectId'],
                              nodeRefDocumento : data !== undefined ? data['alfcmis:nodeRef'] : undefined
                            },
//                            success: function (data) {
//                          	  UI.success('Iscrizione avvenuta correttamente.1');
//                               $('#applyFilter').click();
//                            },
                            error: jconon.error
                        });
                    }, true, function (modal) {
                  	  var provvisorioTemp;

          //        	modal.find('.modal-body').text(i18n.prop('message.confirm.rinnovo.elenco', el['jconon_application:nome'], el['jconon_application:cognome']));
                     	 modal.find('.modal-inner-fix .fileupload').css("display","none");
                     	modal.find('#D_jconon_rinnovo_effettuato_attachment #default .widget').css("display","none");
                           $(window).on('shown.bs.modal', function (event) {
                           	modal.find("#fl_invia_notifica_email button[data-value='true']").click();
                           	
                           //	 modal.find('#D_jconon_note_attachment #default .widget').css("display","none");
                               var nome = el['jconon_application:nome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                                   return $word.toUpperCase();
                               }), cognome = el['jconon_application:cognome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                                   return $word.toUpperCase();
                               });
                               var dataIscrizione = new Date(el['jconon_application:data_iscrizione_elenco']), 
                           	actualDate = new Date(),
                           
                           	dataScadenza = new Date ( dataIscrizione);
                               dataScadenza.setFullYear(dataIscrizione.getFullYear() + 3);
                               var 	dataLimiteProroga = new Date(dataScadenza);
                               dataLimiteProroga.setMonth(dataIscrizione.getMonth() + 18);
                           	console.log("date riunnovo "+dataIscrizione);
                           	console.log("date riunnovo "+actualDate);
                           	console.log("date riunnovo "+dataScadenza);
                         	console.log("date riunnovo "+dataLimiteProroga);
                               
                               
                               modal.find('#oggetto_notifica_email').val( i18n['subject-confirm-aggiornamento-domanda-effettuato'] );
                               var textarea = modal.find('#testo_notifica_email'),
                               		testo;
                               if ( el['jconon_application:numero_mesi_rinnovo'] >= 36 ){
                               testo = i18n['mail.confirm.application.1']+" <b>"+nome+" "+cognome+"</b>,<br>";
                               testo +=	 i18n.prop('mail.rinnovo.application.5',new Date(el['jconon_application:data_invio_rinnovo_elenco']).toLocaleString())+"<br>";
                            //  		 i18n['mail.iscrizione.application.2']+" Fascia "+el['jconon_application:fascia_professionale_attribuita']+"<br>"+
                               if ( dataIscrizione.getTime() <= dataLimiteProroga.getTime() && actualDate.getTime() > dataScadenza.getTime()  )
                            	   testo +=	 i18n.prop('mail.rinnovo.application.6', el['jconon_application:progressivo_iscrizione_elenco'], new Date().toLocaleString().split(",")[0])+"<br>";
                               else if( actualDate.getTime() < dataScadenza.getTime()  )
                            	   testo +=	 i18n.prop('mail.rinnovo.application.6', el['jconon_application:progressivo_iscrizione_elenco'], dataScadenza.toLocaleString().split(",")[0])+"<br>";
                               testo +=		 i18n['mail.rinnovo.application.3']+"<br>";
                               testo +=	 i18n['mail.rinnovo.application.4']+"<br>";
                              	//	i18n['mail.rinnovo.application.5']+"<br>"+
                               testo +=	 callData["jconon_call:requisiti"];
                               }else{
                            	   modal.find('#oggetto_notifica_email').val( i18n['subject-confirm-rinnovo-domanda-effettuato'] );
                            	   testo = i18n['mail.confirm.application.1']+" <b>"+nome+" "+cognome+"</b>,<br>";
                            	   testo +=		 i18n.prop('mail.rinnovo.application.1',new Date(el['jconon_application:data_invio_rinnovo_elenco']).toLocaleString())+"<br>";
                                  //  		 i18n['mail.iscrizione.application.2']+" Fascia "+el['jconon_application:fascia_professionale_attribuita']+"<br>"+
                            	   if ( dataIscrizione.getTime() <= dataLimiteProroga.getTime() && actualDate.getTime() > dataScadenza.getTime()  )
                            		   testo +=		 i18n.prop('mail.rinnovo.application.2', el['jconon_application:progressivo_iscrizione_elenco'], new Date(el['jconon_application:data_iscrizione_elenco']).toLocaleString().split(",")[0], new Date().toLocaleString().split(",")[0], el['jconon_application:fascia_professionale_attribuita'])+"<br>";
                                   else if( actualDate.getTime() < dataScadenza.getTime()  )
                                	   testo +=		 i18n.prop('mail.rinnovo.application.2', el['jconon_application:progressivo_iscrizione_elenco'], new Date(el['jconon_application:data_iscrizione_elenco']).toLocaleString().split(",")[0], dataScadenza.toLocaleString().split(",")[0], el['jconon_application:fascia_professionale_attribuita'])+"<br>";
                            	 //  testo +=		 i18n.prop('mail.rinnovo.application.2', el['jconon_application:progressivo_iscrizione_elenco'], new Date(el['jconon_application:data_iscrizione_elenco']).toLocaleString().split(",")[0], new Date().toLocaleString().split(",")[0], el['jconon_application:fascia_professionale_attribuita'])+"<br>";
                            	   testo +=		 i18n['mail.rinnovo.application.3']+"<br>";
                            	   testo += 		 i18n['mail.rinnovo.application.4']+"<br>";
                            	   testo +=   		 i18n['mail.iscrizione.application.6']+"<br><br>";
                                      	//	 i18n['mail.iscrizione.application.8']+"<br>"+
                            	   testo +=   		 callData["jconon_call:requisiti"];
                               }
                               textarea.val(testo);
                               modal.find('#D_jconon_rinnovo_effettuato_attachment #default .control-group').css("display","none");
                               if( modal.find('.testoRinnovo').length <1 )
                            	   $('<p class="testoRinnovo">'+i18n.prop('message.confirm.rinnovo.elenco', el['jconon_application:nome'], el['jconon_application:cognome'])+'<p>').insertAfter($('.modal-inner-fix #D_jconon_rinnovo_effettuato_attachment'));
                           });
                        
                        var submitBtn=   modal.find(".submit");
                        submitBtn.on('click', function () {
                      //	  var close = UI.progress();
                            jconon.Data.application.readmissionRinnovo({
                              type: 'POST',
                              data: {
                                nodeRef : el['cmis:objectId']
                              },
                              success: function () {
                                URL.Data.proxy.childrenGroup({
                                  type: 'POST',
                                  data: JSON.stringify({
                                    'parent_group_name': 'GROUP_ELENCO_OIV',
                                 
                                    'child_name': el['jconon_application:user']
                                  }),
                                  contentType: 'application/json'
                                });
                              },
                           //   complete: close,
                              error: jconon.error
                            }); 
                          });
                                                           
                      
                    
                    }
                    
                    
                    
              
                  
                  , false, false, true);  
      		
       }
        
 }
 
 if ( el['jconon_application:data_invio_cambio_fascia'] != null && el['jconon_application:stato_domanda'] == 'C' && new Date( el['jconon_application:data_cambio_fascia'] ).getTime() <  new Date( el['jconon_application:data_invio_cambio_fascia'] ).getTime() ) { // && el['jconon_application:data_rinnovo_elenco'] == null

   	customButtons.conferma_cambio_fascia = function () {

   		var bulkInfoAllegato = allegaDocumentoAllaDomanda('D_jconon_cambio_fascia_effettuato_attachment',
                 el['cmis:objectId'],
                 function (attachmentsData, data) {
                     $.ajax({
                         url: cache.baseUrl + "/rest/application-fp/message",
                         type: 'POST',
                         data: {
                           idDomanda : el['cmis:objectId'],
                           nodeRefDocumento : data !== undefined ? data['alfcmis:nodeRef'] : undefined
                         },
//                         success: function (data) {
//                       	  UI.success('Iscrizione avvenuta correttamente.1');
//                            $('#applyFilter').click();
//                         },
                         error: jconon.error
                     });
                 }, true, function (modal) {
                     $(window).on('shown.bs.modal', function (event) {
                    	 modal.find('.modal-inner-fix .fileupload').css("display","none");
                    		modal.find("#fl_invia_notifica_email button[data-value='true']").click();
     					
                         var nome = el['jconon_application:nome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                             return $word.toUpperCase();
                         }), cognome = el['jconon_application:cognome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                             return $word.toUpperCase();
                         });
                         modal.find('#oggetto_notifica_email').val(i18n['subject-cambio-fascia-domanda-effettuato']);
                       
                         
                         var testo = i18n['mail.confirm.application.1'];
                             testo += el['jconon_application:sesso'] === 'M' ? ' dott.' : ' dott.ssa';
                             testo += ' <b style="text-transform: capitalize;">' + nome + ' ' + cognome + '</b>,<br>';
                             testo += i18n.prop('mail.cambio.fascia.application.2',new Date(el['jconon_application:data_invio_cambio_fascia']).toLocaleString())+"<br>";
                             testo += i18n['mail.cambio.fascia.application.3']+" Fascia "+el['jconon_application:fascia_professionale_attribuita']+" "+i18n.prop('mail.cambio.fascia.application.dal',new Date().toLocaleString().split(',')[0] )+"<br>";
                    	
                             testo += i18n['mail.iscrizione.application.4']+"<br>";
                             testo += i18n['mail.iscrizione.application.5']+"<br>";
                             testo += i18n['mail.iscrizione.application.6']+"<br><br>";
                             testo += callData["jconon_call:requisiti"];
                             
                         var textarea = modal.find('#testo_notifica_email');
                         textarea.val(testo);
                        
                         modal.find('#D_jconon_cambio_fascia_effettuato_attachment #default .control-group').css("display","none");
                         if( modal.find('.testoCambioFascia').length <1 )
                      	   $('<p class="testoCambioFascia">'+i18n.prop('message.confirm.cambio.fascia.elenco', el['jconon_application:nome'], el['jconon_application:cognome'])+'<p>').insertAfter($('.modal-inner-fix #D_jconon_cambio_fascia_effettuato_attachment'));
                     });                           
                   
                     
                     var submitBtn=   modal.find(".submit");
                     submitBtn.on('click', function () {
                   //	  var close = UI.progress();
                         jconon.Data.application.readmissionCambioFascia({
                           type: 'POST',
                           data: {
                             nodeRef : el['cmis:objectId']
                           },
                           success: function () {
                             URL.Data.proxy.childrenGroup({
                               type: 'POST',
                               data: JSON.stringify({
                                 'parent_group_name': 'GROUP_ELENCO_OIV',
                              
                                 'child_name': el['jconon_application:user']
                               }),
                               contentType: 'application/json'
                             });
                           },
                        //   complete: close,
                           error: jconon.error
                         }); 2
                       });
                 
                 }
                             
               , false, false, true);  

    }
     
}

 

 customButtons.sblocca_invii = function () {
     var content = $("<div></div>"),
       bulkinfo = new BulkInfo({
       target: content,
       path: "P:jconon_application:aspect_sblocco_invio_domande",
       objectId: el['cmis:objectId'],
       formclass: 'form-inline sblocco_invio_domande',
       name: 'default',
       callback : {
         afterCreateForm: function (form) {
        	 console.log("max");
        	 form.find('#fascia_professionale_attribuita').val(el['jconon_application:fascia_professionale_validata']);
         }
       }
     });
     bulkinfo.render();
   UI.modal('<i class="icon-edit"></i>Sblocca invio domande', content, function () {
	       var close = UI.progress(), d = bulkinfo.getData();
	       
			     
	    	   d.push(
			         {
			           id: 'cmis:objectId',
			           name: 'cmis:objectId',
			           value: el['cmis:objectId']
			         },
			         {
			           name: 'aspect', 
			           value: 'P:jconon_application:aspect_sblocco_invio_domande'
			         }              
			       );
	    	   if ( d[1].value == true || d[2].value == true ) {
	    		   
		    	   d.push(
				       
				         {
				             name: 'jconon_application:fascia_professionale_attribuita',
				             value: el['jconon_application:fascia_professionale_validata']
				           }                
				       );
	    	   }
	    		   
	       jconon.Data.application.main({
			         type: 'PUT',
			         data: d,
			         success: function (data) {
			           UI.success(i18n['message.sblocco.invio.eseguito']);
			           $('#applyFilter').click();
			         },
			         complete: close,
			         error: URL.errorFn
			       });
	       
       });
    
   };
          
          if (el['jconon_application:esclusione_rinuncia'] !== 'E' ) {
				if(el['jconon_application:progressivo_iscrizione_elenco']!=null){
          
          customButtons.escludi = function () {
              var bulkInfoAllegato = allegaDocumentoAllaDomanda('D_jconon_esclusione_attachment',
                el['cmis:objectId'],
                function (attachmentsData, data) {
                  jconon.Data.application.reject({
                    type: 'POST',
                    data: {
                      nodeRef : el['cmis:objectId'],
                      nodeRefDocumento : data['alfcmis:nodeRef']
                    },
                    success: function () {
                    	console.log("noderef "+el['cmis:objectId']);
                    	console.log("nodeRefDocumento "+data['alfcmis:nodeRef']);
                      $('#applyFilter').click();
                    },
                    error: jconon.error
                  });
                }, true, function (modal) {
                    $(window).on('shown.bs.modal', function (event) {
					
                        var nome = el['jconon_application:nome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                            return $word.toUpperCase();
                        }), cognome = el['jconon_application:cognome'].replace(/^(.)|(\s|\-)(.)/g, function($word) {
                            return $word.toUpperCase();
                        });
                        modal.find('#oggetto_notifica_email').val(i18n['mail.subject.esclusione']);
                        var testo = i18n['mail.confirm.application.1'];
                            testo += el['jconon_application:sesso'] === 'M' ? ' dott.' : ' dott.ssa';
                            testo += ' <b style="text-transform: capitalize;">' + nome + ' ' + cognome + '</b>,';
                            testo += i18n['mail.body.esclusione'];
                            testo += i18n['mail.firma.esclusione'];
                            
                        var textarea = modal.find('#testo_notifica_email');
                        textarea.val(testo);
                        var ck = textarea.ckeditor({
                            toolbarGroups: [
                                { name: 'clipboard', groups: ['clipboard'] },
                                { name: 'basicstyles', groups: ['basicstyles'] },
                                { name: 'paragraph', groups: ['list', 'align'] }],
                                removePlugins: 'elementspath'
                        });
                        ck.editor.on('change', function () {
                          var html = ck.val();
                          textarea.parent().find('control-group widget').data('value', html || null);
                        });

                        ck.editor.on('setData', function (event) {
                          var html = event.data.dataValue;
                          textarea.parent().find('control-group widget').data('value', html || null);
                        });
                    });
                }
              );
            };
				}
          }

            
            } // fine tasti visibili a rdp ed admin
		 if(el["jconon_application:esclusione_rinuncia"] != null && el["jconon_application:esclusione_rinuncia"].includes("E")){
				var data=new Date(el["jconon_application:data_rimozione_elenco"]);
				var dataCorrente=new Date();
				
				var dataLimite=new Date(data.setMonth(data.getMonth()+6));
				
		  }
		 if (  ( el["jconon_application:esclusione_rinuncia"] == null   &&
				
			        el['jconon_application:data_domanda'] == null && 
			        el['jconon_application:data_iscrizione_elenco'] == null  )  || 
   			 	el['jconon_application:fl_sblocco_invio_iscrizione'] == true || 
   			 ( el["jconon_application:esclusione_rinuncia"] != null  && el["jconon_application:esclusione_rinuncia"].includes("E") &&
						dataCorrente.getTime()>=dataLimite.getTime())) { 
			 
			  if (common.User.admin || common.User.id === el['jconon_application:user']) {
                  if (el.allowableActions.indexOf('CAN_CREATE_DOCUMENT') !== -1) {
                    customButtons.reopen = false;
                    customButtons.invia_iscrizione = function () {
						 window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId']+'&iscrizione=true';
					 };
                  } else {
                    customButtons.invia_iscrizione = function () {
                      Application.invia_iscrizione(el, function () {
                        window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId']+'&iscrizione=true';
                      });
                    };
                   // customButtons.invia_cambio_fascia = false;
                  }
                	
                }
			  
				
		 
		 }
		 
          if ( ( el["jconon_application:esclusione_rinuncia"] == null &&
  				
        		  el["jconon_application:fascia_professionale_validata"] != '3' &&
        		  new Date(el["jconon_application:data_invio_cambio_fascia"]).getTime() <= new Date(el["jconon_application:data_cambio_fascia"]).getTime() &&
			        el['jconon_application:data_iscrizione_elenco'] != null && 
 			 	el['jconon_application:stato_domanda'] == 'C' )  || 
 			 	el['jconon_application:fl_sblocco_invio_cambio_fascia'] == true ) { 
          
        	  if (common.User.admin || common.User.id === el['jconon_application:user']) {
                  if (el.allowableActions.indexOf('CAN_CREATE_DOCUMENT') !== -1) {
                    customButtons.reopen = false;
                    customButtons.invia_cambio_fascia = function () {
						 window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId']+'&cambio_fascia=true';
					 };
                  } else {
                    customButtons.invia_cambio_fascia = function () {
                      Application.invia_cambio_fascia(el, function () {
                        window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId']+'&cambio_fascia=true';
                      });
                    };
                   // customButtons.invia_cambio_fascia = false;
                  }
                	
                }
        	  
					
        }
			var dataDomanda, dataAperturaInvioRinnovo, dataCorrente;
	      	if( ( el["jconon_application:data_iscrizione_elenco"] != null )  ){
	      		 dataDomanda=new Date(el["jconon_application:data_iscrizione_elenco"]);
	      		 dataAperturaInvioRinnovo=new Date(dataDomanda.setMonth(dataDomanda.getMonth()+35));
	      		 dataCorrente=new Date();
	      	}
		 
		 	
	        	
		 if ( ( ( ( el["jconon_application:esclusione_rinuncia"] != null && !el["jconon_application:esclusione_rinuncia"].includes("E") || el["jconon_application:esclusione_rinuncia"] == null ) ) &&
				( dataCorrente != null && dataAperturaInvioRinnovo != null && dataCorrente.getTime() >= dataAperturaInvioRinnovo.getTime() &&
			        el['jconon_application:data_invio_rinnovo_elenco'] == null && 
			        el['jconon_application:data_rinnovo_elenco'] == null && 
    			 	el['jconon_application:stato_domanda'] == 'C' ) ) || 
    			 	el['jconon_application:fl_sblocco_invio_rinnovo'] == true ) { 

			  if (common.User.admin || common.User.id === el['jconon_application:user']) {
                  if (el.allowableActions.indexOf('CAN_CREATE_DOCUMENT') !== -1) {
                    customButtons.reopen = false;
                    customButtons.invia_rinnovo = function () {
						 window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId']+'&rinnovo=true';
					 };
                  } else {
                    customButtons.invia_rinnovo = function () {
                      Application.invia_rinnovo(el, function () {
                        window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId']+'&rinnovo=true';
                      });
                    };
                   // customButtons.invia_cambio_fascia = false;
                  }
                	
                }
			 
			
				
    	 }
	
			
 
            if (el['jconon_application:stato_domanda'] === 'P') {
              // provvisoria
              if (bandoInCorso) {
                if (common.User.admin || common.User.id === el['jconon_application:user']) {
                  defaultChoice = 'edit';
                } else {
                  customButtons.edit = false;
                  defaultChoice = 'print';
                }
              } else {
                //  label Scaduto
                $.each(customButtons, function (index, el) {
                  if (index !== "print" && index !== "duplicate") {
                    customButtons[index] = false;
                  }
                });
                customButtons.edit = false;
                displayActionButton = true;
              }
            } else if (el['jconon_application:stato_domanda'] === 'C') {
              // definitiva è editbile per ora solo da amministratori, poi sarà il RDP
              if (!common.User.admin) {
                customButtons.edit = false;
              }
              defaultChoice = 'print';
              
              }
            if (common.User.admin || Call.isRdP(callData['jconon_call:rdp']) ) {
  	            //  Modifica
  	            customButtons.edit = function () {
  	            	
  	              window.location = jconon.URL.application.manage + '?callId=' + callData['cmis:objectId'] + '&applicationId=' + el['cmis:objectId'];
  					
  	            };
            }
              if (bandoInCorso) {
                if (common.User.admin || common.User.id === el['jconon_application:user']) {
                  if (el.allowableActions.indexOf('CAN_CREATE_DOCUMENT') !== -1) {
                    customButtons.reopen = false;
                    customButtons.modificaProfilo = function () {
                      window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId'];                      
                    };
                  } else {
                    customButtons.reopen = function () {
                      Application.reopen(el, function () {
                        window.location = jconon.URL.application.manage + '?callId=' + el.relationships.parent[0]['cmis:objectId'] + '&applicationId=' + el['cmis:objectId'];
                      });
                    };
                    customButtons.modificaProfilo = false;
                  }
                	
                }
//                if (cache['flows.enable']) {
//                    customButtons.comunicazioni = function () {
//                        displayAttachments(el, 'jconon_attachment:generic_comunicazioni', Application.displayTitoli, 'actions.comunicazioni');
//                    };
//                }
                if (el['jconon_application:fl_preavviso_rigetto'] == true) {
                  customButtons.preavviso_rigetto = function () {
                    var bulkInfoAllegato = allegaDocumentoAllaDomanda(
                        'D_jconon_attachment_response_preavviso_rigetto',
                        el['cmis:objectId'],
                        undefined,
                        true,
                        function (modal) {
                            $(window).on('shown.bs.modal', function (event) {
                                var scaricaPreavvisoRigetto = $('<a class="btn btn-info" id="scarica_preavviso_rigetto">Scarica preavviso rigetto</a>')
                                    .off('click')
                                    .on('click', function () {
                                       $.ajax({
                                         url: cache.baseUrl + '/rest/application-fp/scarica-preavviso-rigetto?idDomanda=' + el['cmis:objectId'],
                                         type: 'GET',
                                         success: function (data) {
                                           window.location = cache.baseUrl + '/rest/content?nodeRef=' + data['cmis:objectId'];
                                         },
                                         error: URL.errorFn
                                       });
                                    });
                                if (modal.find('#scarica_preavviso_rigetto').length == 0){
                                    modal.find('.modal-footer').prepend(scaricaPreavvisoRigetto);
                                }
                                var textarea = modal.find('#testo');
                                var ck = textarea.ckeditor({
                                    toolbarGroups: [
                                        { name: 'clipboard', groups: ['clipboard'] },
                                        { name: 'basicstyles', groups: ['basicstyles'] },
                                        { name: 'paragraph', groups: ['list', 'align'] }],
                                        removePlugins: 'elementspath'
                                });
                                ck.editor.on('change', function () {
                                  var html = ck.val();
                                  textarea.parent().find('control-group widget').data('value', html || null);
                                });

                                ck.editor.on('setData', function (event) {
                                  var html = event.data.dataValue;
                                  textarea.parent().find('control-group widget').data('value', html || null);
                                });
                            });
                        },
                        true,
                        true,
                        true,
                        'Preavviso di Rigetto',
                        [
                            {name: 'jconon_attachment:user',value: common.User.id}
                        ],
                        true,
                        {
                            rel: {
                                'cmis:secondaryObjectTypeIds' : ['P:jconon_attachment:generic_comunicazioni'],
                                'cmis:relObjectTypeId' : 'R:jconon_attachment:response_preavviso_rigetto'
                             }
                        },
                        function (data) {
                            $.ajax({
                                url: cache.baseUrl + "/rest/application-fp/response-preavviso-rigetto",
                                type: 'POST',
                                data: {
                                  idDomanda : el['cmis:objectId'],
                                  idDocumento : data['cmis:objectId']
                                },
                                success: function (data) {
                                   window.location = cache.baseUrl + '/my-applications';
                                },
                                error: jconon.error
                            });
                        }
                    );
                  };
                }
                if (el['jconon_application:fl_soccorso_istruttorio'] == true) {
                  customButtons.soccorso_istruttorio = function () {
                    var bulkInfoAllegato = allegaDocumentoAllaDomanda(
                        'D_jconon_attachment_response_soccorso_istruttorio',
                        el['cmis:objectId'],
                        undefined,
                        true,
                        function (modal) {
                            $(window).on('shown.bs.modal', function (event) {
                                var scaricaSoccorso = $('<a class="btn btn-info" id="scarica_soccorso">Scarica soccorso istruttorio</a>')
                                    .off('click')
                                    .on('click', function () {
                                       $.ajax({
                                         url: cache.baseUrl + '/rest/application-fp/scarica-soccorso-istruttorio?idDomanda=' + el['cmis:objectId'],
                                         type: 'GET',
                                         success: function (data) {
                                           window.location = cache.baseUrl + '/rest/content?nodeRef=' + data['cmis:objectId'];
                                         },
                                         error: URL.errorFn
                                       });
                                    });
                                if (modal.find('#scarica_soccorso').length == 0){
                                    modal.find('.modal-footer').prepend(scaricaSoccorso);
                                }
                                var textarea = modal.find('#testo');
                                var ck = textarea.ckeditor({
                                    toolbarGroups: [
                                        { name: 'clipboard', groups: ['clipboard'] },
                                        { name: 'basicstyles', groups: ['basicstyles'] },
                                        { name: 'paragraph', groups: ['list', 'align'] }],
                                        removePlugins: 'elementspath'
                                });
                                ck.editor.on('change', function () {
                                  var html = ck.val();
                                  textarea.parent().find('control-group widget').data('value', html || null);
                                });

                                ck.editor.on('setData', function (event) {
                                  var html = event.data.dataValue;
                                  textarea.parent().find('control-group widget').data('value', html || null);
                                });
                            });
                        },
                        true,
                        true,
                        true,
                        'Soccorso Istruttorio',
                        [
                            {name: 'jconon_attachment:user',value: common.User.id}
                        ],
                        true,
                        {
                            rel: {
                                'cmis:secondaryObjectTypeIds' : ['P:jconon_attachment:generic_comunicazioni'],
                                'cmis:relObjectTypeId' : 'R:jconon_attachment:response_soccorso_istruttorio'
                             }
                        },
                        function (data) {
                            $.ajax({
                                url: cache.baseUrl + "/rest/application-fp/response-soccorso-istruttorio",
                                type: 'POST',
                                data: {
                                  idDomanda : el['cmis:objectId'],
                                  idDocumento : data['cmis:objectId']
                                },
                                success: function (data) {
                                   window.location = cache.baseUrl + '/my-applications';
                                },
                                error: jconon.error
                            });
                        }
                    );
                  };
                }
           	
              } else {
                if (el['jconon_application:esclusione_rinuncia'] !== 'E' && 
                    el['jconon_application:esclusione_rinuncia'] !== 'N' && 
                    el['jconon_application:esclusione_rinuncia'] !== 'R') {
                  dropdowns['<i class="icon-arrow-down"></i> Escludi'] = function () {
                    allegaDocumentoAllaDomanda('D:jconon_esclusione:attachment',
                      el['cmis:objectId'],
                      function () {
                        jconon.Data.application.reject({
                          type: 'POST',
                          data: {
                            nodeRef : el['cmis:objectId']
                          },
                          success: function () {
                            $('#applyFilter').click();
                          },
                          error: jconon.error
                        });
                      }
                      );
                  };
                }
                if (el['jconon_application:esclusione_rinuncia'] === 'E' ||
                    el['jconon_application:esclusione_rinuncia'] === 'N' ||
                    el['jconon_application:esclusione_rinuncia'] === 'R') {
                  dropdowns['<i class="icon-arrow-up"></i> Riammetti'] = function () {
                    allegaDocumentoAllaDomanda('D:jconon_riammissione:attachment',
                      el['cmis:objectId'],
                      function () {
                        jconon.Data.application.readmission({
                          type: 'POST',
                          data: {
                            nodeRef : el['cmis:objectId']
                          },
                          success: function () {
                            $('#applyFilter').click();
                          },
                          error: jconon.error
                        });
                      }
                      );
                  };
                }
                if (el['jconon_application:esclusione_rinuncia'] !== 'E' &&
                    el['jconon_application:esclusione_rinuncia'] !== 'N' &&
                    el['jconon_application:esclusione_rinuncia'] !== 'R') {
                  dropdowns['<i class="icon-arrow-down"></i> Rinuncia'] = function () {
                    allegaDocumentoAllaDomanda('D:jconon_rinuncia:attachment',
                      el['cmis:objectId'],
                      function () {
                        jconon.Data.application.waiver({
                          type: 'POST',
                          data: {
                            nodeRef : el['cmis:objectId']
                          },
                          success: function () {
                            $('#applyFilter').click();
                          },
                          error: jconon.error
                        });
                      }
                      );
                  };
                }
                dropdowns['<i class="icon-upload"></i> Comunicazione al candidato'] = function () {
                  allegaDocumentoAllaDomanda('D:jconon_comunicazione:attachment', el['cmis:objectId']);
                };
                
                dropdowns['<i class="icon-upload"></i> note al candidato'] = function () {
                    allegaDocumentoAllaDomanda('D:jconon_note:attachment', el['cmis:objectId']);
                  };
                
                dropdowns['<i class="icon-upload"></i> Convocazione al colloquio'] = function () {
                  allegaDocumentoAllaDomanda('D:jconon_convocazione:attachment', el['cmis:objectId']);
                };
                dropdowns['<i class="icon-pencil"></i> Reperibilità'] = function () {
                  var content = $("<div></div>").addClass('modal-inner-fix'),
                    bulkinfo = new BulkInfo({
                    target: content,
                    path: "F:jconon_application:folder",
                    objectId: el['cmis:objectId'],
                    formclass: 'form-horizontal jconon',
                    name: 'reperibilita'
                  });
                  bulkinfo.render();
                  UI.bigmodal('<i class="icon-pencil"></i> Reperibilità', content, function () {
                    var close = UI.progress(), d = bulkinfo.getData();
                    d.push({
                        id: 'cmis:objectId',
                        name: 'cmis:objectId',
                        value: el['cmis:objectId']
                    });
                    jconon.Data.application.main({
                      type: 'POST',
                      data: d,
                      success: function (data) {
                        UI.success(i18n['message.aggiornamento.application.reperibilita']);
                      },
                      complete: close,
                      error: URL.errorFn
                    });
                  });
                };
                dropdowns['<i class="icon-edit"></i> Punteggi'] = function () {
                  var content = $("<div></div>").addClass('modal-inner-fix'),
                    bulkinfo = new BulkInfo({
                    target: content,
                    path: "P:jconon_application:aspect_punteggi",
                    objectId: el['cmis:objectId'],
                    formclass: 'form-horizontal',
                    name: 'default',
                    callback : {
                      afterCreateForm: function (form) {
                        form.find('.control-group').not('.widget').addClass('widget');
                      }
                    }
                  });
                  bulkinfo.render();
                  UI.bigmodal('<i class="icon-edit"></i> Punteggi', content, function () {
                    var close = UI.progress(), d = bulkinfo.getData();
                    d.push(
                      {
                        id: 'cmis:objectId',
                        name: 'cmis:objectId',
                        value: el['cmis:objectId']
                      },
                      {
                        name: 'aspect', 
                        value: 'P:jconon_application:aspect_punteggi'
                      }
                    );
                    jconon.Data.application.main({
                      type: 'POST',
                      data: d,
                      success: function (data) {
                        UI.success(i18n['message.aggiornamento.application.punteggi']);
                      },
                      complete: close,
                      error: URL.errorFn
                    });
                  });
                };
                if (common.User.admin || Call.isRdP(callData['jconon_call:rdp'])) {
                  customButtons.operations = dropdowns;
                }
                if (callData['jconon_call:scheda_valutazione'] === true && (common.User.admin || Call.isCommissario(callData['jconon_call:commissione']))) {
                  customButtons.scheda_valutazione = function () {
                    URL.Data.search.query({
                      queue: true,
                      data: {
                        q: "select cmis:versionSeriesId from jconon_attachment:scheda_valutazione where IN_FOLDER ('" + el['cmis:objectId'] + "')"
                      }
                    }).done(function (rs) {
                      if (rs.totalNumItems === 0 || rs.items[0] === undefined) {
                        UI.confirm('Non &egrave; presente nessuna scheda di valutazione del candidato. Si vuole procedere con la sua predisposizione?', function () {
                          var close = UI.progress();
                          jconon.Data.application.print_scheda_valutazione({
                            type: 'POST',
                            data: {
                              nodeRef : el['cmis:objectId']
                            },
                            success: function (data) {
                              window.location = URL.urls.search.content + '?nodeRef=' + data.nodeRef;
                            },
                            complete: close,
                            error: jconon.error
                          });
                        });
                      } else {
                        window.location = jconon.URL.application.scheda_valutazione + '?applicationId=' + el['cmis:objectId'] + '&nodeRef=' + rs.items[0]['cmis:versionSeriesId'];
                      }
                    });
                  };
                } else {
                  customButtons.scheda_valutazione = false;
                }
              }
            
           
            if (displayActionButton) {
              new ActionButton.actionButton({
                name: el.name,
                nodeRef: el.id,
                baseTypeId: el.baseTypeId,
                objectTypeId: el.objectTypeId,
                mimeType: el.contentType,
                allowableActions: el.allowableActions,
                defaultChoice: defaultChoice
              }, {
                edit: 'CAN_CREATE_DOCUMENT',
                scheda_valutazione: 'CAN_CREATE_DOCUMENT',
                operations: 'CAN_CREATE_DOCUMENT'
              }, customButtons, {
                print: 'fa fa-history',
                printRinnovo : 'fa fa-history',
                printCambioFascia : 'fa fa-history',
                attachments : 'fa fa-paperclip',
                curriculum: 'icon-file-alt',
                schedaAnonima: 'icon-file-alt',
                productList: 'icon-list',
                productSelected: 'icon-list-ol',
                reopen: 'icon-share',
                modificaProfilo : 'icon-share',
                scheda_valutazione: 'icon-table',
                operations: 'icon-list',
                escludi: 'icon-arrow-down',
                invia_iscrizione: 'icon-share',
            	invia_rinnovo: 'icon-share',
        		invia_cambio_fascia: 'icon-share',
        		sblocca_invii: 'icon-unlock',
                comunicazioni: 'icon-envelope text-info',
				visualizza_comunicazioni: 'icon-eye-open',
				rinnovo: 'icon-arrow-up',
				note: 'icon-edit',
				visualizza_note: 'icon-eye-open',
				corsi: 'icon-file-alt',
                comunicazione: 'icon-envelope text-success',
                inserisci: 'icon-arrow-up',
                conferma_cambio_fascia: 'icon-arrow-up',
                assegna_fascia: 'icon-edit',
                preavviso_rigetto: 'icon-dashboard text-error',
                soccorso_istruttorio: 'icon-dashboard text-error'
              }, undefined, true).appendTo(target);
            }
          });
        });
      }
    }
  });

  bulkInfo =  new BulkInfo({
    target: criteria,
    formclass: 'form-horizontal jconon',
    path: rootTypeId,
    name: 'filters',
    callback : {
      afterCreateForm: function () {
        // rearrange btn-group as btn-group-vertical
		console.log("criteria ");
        $('#filters-attivi_scaduti').
          add('#filters-provvisorie_inviate')
          .addClass('btn-group-vertical');
		  
	//	$('#filters-provvisorie_inviate').addClass('hide');
		//  style='border: 1px solid #ccc!important; margin-top: 40px; width: 260px;'
		$('#filters-provvisorie_inviate').before("<div  style='border: 1px solid #ccc !important; margin-top: 40px; width: 268px;'><label class='label label-info' style='width: 260px;'>Filtri applicati</label><label id='filtriApplicati' style='width: 260px;'>Tutte</label></div>")
		$('#filters-provvisorie_inviate').before("<label class='label label-info' style='width: 260px; margin-top: 20px;'>Filtro per stato domanda</label>");
		$('.control-group label.control-label').text("Filtro per nominativo");
		$('.control-group label.control-label[for="filters-da_data"]').text("Filtro per data");
		$('.control-group label.control-label').css("width","263px");
		$('.control-group label.control-label').css("color","white");
		$("#user").css("width","257px");
		$(".widget").css("margin-top","97px");
		$('#filters .filter-date').parent().parent().css("margin-top","0");
		$(".authority").css("margin-top","0px");
	//	$(".controls").css('display','none');
		$('.control-group label.control-label').css("text-align","left");
		$('.control-group label.control-label').addClass("label label-info");
		$('.control-group .control-label').addClass("label label-info");
	//	$('.control-group label').css('display','none');
		$('#applyFilter').hide();
		$("#filters .widget .controls").css("margin-right","110px");
		$("#filters .controls").css("margin-left","0px")
		$("#criteria .pull-right").removeClass("pull-right").addClass("pull-left")
	//	$('#criteria').before("<div class='btn-group pull-left'> <a class=' btn btn-primary' id='mostraFiltri'>Mostra filtri<span style='margin-top: 10px; margin-left: 13px; border-top: 6px solid #ffffff; border-right: 6px solid transparent; border-left: 6px solid transparent;' class='caret'></span></a> </div>");
	//	$("#criteria .pull-left").css('display','none');
	//	$(".controls label").css('display','none');
		
		//$(".controls").css('display','none');
     //   criteria.find('input:not("#user,#filters-da_data,#filters-a_data")').on('change', filter);
		criteria.find('input:not("#user")').on('change', filter);
		
	
		  
		$('#filters-provvisorie_inviate button.span12').on('click', function () {
			$('#filtriApplicati').text($(this).text());

        });
		
//		$('#user .dropdown-menu li').on('click', function () {
//			$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text()+", "+$("#user").val());
//
//        });
	/*	$('#user ').on('change', function () {
			$('#filtriApplicati').text($('#filtriApplicati').text()+', '+$('#user').val());
        });*/
		  
	
		function init(value) {
		      var criteria = ApplicationFp.getCriteriaOIV(bulkInfo, value);
//		      if (value === 'dapubblicare') {
//		        criteria.isNull('root.jconon_call:data_inizio_invio_domande');
//		      }
		 //     criteria.and(new Criteria().lte('jconon_application:data_domanda',new Date()).build());
		      criteria.list(search);
		  }

		

   //    $('#applyFilter').on('click', filter);
		 $('#applyFilter').on('click', function () {
		      init($('#filters-provvisorie_inviate > button.btn.active').attr('data-value'));
			
		    });
        criteria
          .find('.btn-group-vertical')
          .closest('.widget')
          .on('changeData', filter);

        $('#resetFilter').on('click', function () {
        	 criteria.find('input').val('');
             var input =criteria.find('input');
             input.trigger("change");
			 criteria.find('#filters-provvisorie_inviate').trigger("click");
		//	 console.log(" data reset "+criteria.find('.widget').data);
//console.log(" data reset "+criteria.find('.widget').data.value);
          criteria.find('.widget').data('value', '');

          var btns = criteria.find('.btn-group-vertical .btn');

          btns
            .removeClass('active');

          criteria
            .find('.btn-group-vertical')
            .find('.default')
            .addClass('active');
			$('#filtriApplicati').text($('#filters-provvisorie_inviate button.active').text());
        });

        filter();
		
        if (callId) {
          $('#filters .control-group').hide();
          $('#filters .filter-date').parent().parent().show();
          
          $('#filters .authority')
         .show()
            .on('changeData', function (event, key, value) {
              if (value) {
                filter(true);
              }
            });
		//	 $('#filters .authority').css('display','none');
          $('#filters-provvisorie_inviate').parents('.control-group').show();
        } else {
       //   $('#filters .authority').hide();
          $('#export-div').remove();
        }
      }
    }
  });

  manageUrlParams();
  
});