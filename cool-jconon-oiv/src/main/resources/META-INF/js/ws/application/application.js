/*global params*/
define(['jquery', 'header', 'i18n', 'cnr/cnr.ui', 'cnr/cnr.bulkinfo', 'json!common', 'cnr/cnr.jconon', 'cnr/cnr.url',
    'cnr/cnr.application', 'cnr/cnr.attachments', 'json!cache', 'cnr/cnr.call', 'cnr/cnr', 'cnr/cnr.ui.widgets', 'fp/fp.application','cnr/cnr.node',
     'cnr/cnr.ui.wysiwyg', 'cnr/cnr.ui.country', 'cnr/cnr.ui.city'],
  function ($, header, i18n, UI, BulkInfo, common, jconon, URL,
    Application, Attachments, cache, Call, CNR, Widgets, ApplicationFp, Node) {
  "use strict";

  var content = $('#field'), bulkinfo, forms = [], aspects = [],
    cmisObjectId, metadata = {}, dataPeopleUser,
    callId = params.callId,
    callCodice = params.callCodice,
    toolbar = $('#toolbar-call'),
    charCodeAspect = 97,
    preview = params.preview,
    showTitoli, showCurriculum, showProdottiScelti, showProdotti, showSchedeAnonime, showSchedeAnonimeRinnovo, crediti,
    applicationAttachments, curriculumAttachments, prodottiAttachments, schedeAnonimeAttachments, schedeAnonimeRinnovoAttachments, creditiEsperienze=0,
    buttonPeople  = $('<button type="button" class="btn btn-small"><i class="icon-folder-open"></i> ' + i18n['button.explorer.people'] + '</button>'),
    buttonPeopleScelti  = $('<button type="button" class="btn btn-small"><i class="icon-folder-open"></i> ' + i18n['button.explorer.people'] + '</button>'),
    refreshFnProdotti, refreshFnProdottiScelti,
    saved,
    optionPubblico = [
      'Pubblica amministrazione','Forze armate','Magistratura','Università'
    ],
    ruoloLavorativo = [
      {
        key : 'Dirigente',
        defaultLabel: 'Dirigente',
        label: 'label.ruolo.lavorativo.Dirigente',
        group: 'Pubblica amministrazione'
      },{
        key : 'Funzionario',
        defaultLabel: 'Funzionario',
        label: 'label.ruolo.lavorativo.Funzionario',
        group: 'Pubblica amministrazione'
      },{
        key : 'Impiegato',
        defaultLabel: 'Impiegato',
        label: 'label.ruolo.lavorativo.Impiegato',
        group: 'Pubblica amministrazione'
      },{
        key : 'ALTRO',
        defaultLabel: 'ALTRO',
        label: 'label.ruolo.lavorativo.ALTRO',
        group: 'Pubblica amministrazione'
      },{
        key : 'Ufficiale',
        defaultLabel: 'Ufficiale',
        label: 'label.ruolo.lavorativo.Ufficiale',
        group: 'Forze armate'
      },{
        key : 'Sottufficiale/ispettore',
        defaultLabel: 'Sottufficiale/ispettore',
        label: 'label.ruolo.lavorativo.Sottufficiale/ispettore',
        group: 'Forze armate'
      },{
        key : 'Sovraintendente',
        defaultLabel: 'Sovraintendente',
        label: 'label.ruolo.lavorativo.Sovraintendente',
        group: 'Forze armate'
      },{
        key : 'ALTRO',
        defaultLabel: 'ALTRO',
        label: 'label.ruolo.lavorativo.ALTRO',
        group: 'Forze armate'
      },{
        key : 'Magistrato ordinario',
        defaultLabel: 'Magistrato ordinario',
        label: 'label.ruolo.lavorativo.Magistratoordinario',
        group: 'Magistratura'
      },{
        key : 'Magistrato amministrativo',
        defaultLabel: 'Magistrato amministrativo',
        label: 'label.ruolo.lavorativo.Magistratoamministrativo',
        group: 'Magistratura'
      },{
        key : 'Magistrato contabile',
        defaultLabel: 'Magistrato contabile',
        label: 'label.ruolo.lavorativo.Magistratocontabile',
        group: 'Magistratura'
      },{
        key : 'Magistrato onorario',
        defaultLabel: 'Magistrato onorario',
        label: 'label.ruolo.lavorativo.Magistratoonorario',
        group: 'Magistratura'
      },{
        key : 'ALTRO',
        defaultLabel: 'ALTRO',
        label: 'label.ruolo.lavorativo.ALTRO',
        group: 'Magistratura'
      },{
        key : 'Professore ordinario',
        defaultLabel: 'Professore ordinario',
        label: 'label.ruolo.lavorativo.Professoreordinario',
        group: 'Università'
      },{
        key : 'Professore associato',
        defaultLabel: 'Professore associato',
        label: 'label.ruolo.lavorativo.Professoreassociato',
        group: 'Università'
      },{
        key : 'Ricercatore',
        defaultLabel: 'Ricercatore',
        label: 'label.ruolo.lavorativo.Ricercatore',
        group: 'Università'
      },{
        key : 'ALTRO',
        defaultLabel: 'ALTRO',
        label: 'label.ruolo.lavorativo.ALTRO',
        group: 'Università'
      },{
        key : 'Imprenditore',
        defaultLabel: 'Imprenditore',
        label: 'label.ruolo.lavorativo.Imprenditore',
        group: 'Settore privato'
      },{
        key : 'Dirigente',
        defaultLabel: 'Dirigente',
        label: 'label.ruolo.lavorativo.Dirigente',
        group: 'Settore privato'
      },{
        key : 'Quadro',
        defaultLabel: 'Quadro',
        label: 'label.ruolo.lavorativo.Quadro',
        group: 'Settore privato'
      },{
        key : 'Impiegato',
        defaultLabel: 'Impiegato',
        label: 'label.ruolo.lavorativo.Impiegato',
        group: 'Settore privato'
      },{
        key : 'ALTRO',
        defaultLabel: 'ALTRO',
        label: 'label.ruolo.lavorativo.ALTRO',
        group: 'Settore privato'
      },{
        key : 'Avvocato',
        defaultLabel: 'Avvocato',
        label: 'label.ruolo.lavorativo.Avvocato',
        group: 'Libera professione'
      },{
        key : 'Commercialista/revisore',
        defaultLabel: 'Commercialista/revisore',
        label: 'label.ruolo.lavorativo.Commercialista/revisore',
        group: 'Libera professione'
      },{
        key : 'Consulente',
        defaultLabel: 'Consulente',
        label: 'label.ruolo.lavorativo.Consulente',
        group: 'Libera professione'
      },{
        key : 'ALTRO',
        defaultLabel: 'ALTRO',
        label: 'label.ruolo.lavorativo.ALTRO',
        group: 'Libera professione'
      }
    ];
  if (content.hasClass('error-allegati-empty')) {
    UI.alert(content.data('message') || i18n['message.error.allegati.empty'], null, null, true);
  }

  $('.cnr-sidenav').affix({
    offset: {
      top: 540
    //  bottom: 270
    }
  });
  
  if (preview) {
    $('#send,#save,#delete').prop('disabled', true);
  }
  function isSaved() {
    return saved || preview;
  }

  function setObjectValue(obj, value) {
    if (obj) {
      obj.val(value);
      if (obj.parents(".widget").size() > 0) {
        obj.trigger('change');
      } else {
        obj.trigger('blur');
      }
    }
  }

  function createTitoli(affix) {
    return new Attachments({
      isSaved: isSaved,
      affix: affix,
      objectTypes: applicationAttachments,
      cmisObjectId: cmisObjectId,
      search: {
        type: 'jconon_attachment:generic_document',
        displayRow: Application.displayTitoli,
        fetchCmisObject: true,
        maxItems: 1000,
        filter: false
      },
      submission: {
    	  requiresFile: true,
        externalData: [
          {
            name: 'aspect',
            value: 'P:jconon_attachment:generic_document'
          },
          {
            name: 'jconon_attachment:user',
            value: dataPeopleUser.userName
          }
        ]
      }
    });
  }
  function displayTotalNumItems(affix, documents) {
    var label = documents.totalNumItems < 0 ? 
      i18n.prop('label.righe.has.more.items', documents.maxItemsPerPage) :
      i18n.prop('label.righe.visualizzate', documents.totalNumItems);  
    affix.find('h1').after('<sub class="total pull-right">' + label + '</sub>');
  }

  function createProdottiScelti(affix, isMoveable) {
    return new Attachments({
      isSaved: isSaved,
      affix: affix,
      objectTypes: prodottiAttachments,
      cmisObjectId: cmisObjectId,
      search: {
        type: 'cvpeople:commonMetadata',
        join: 'cvpeople:selectedProduct',
        isAspect: true,
        filter: false,
        includeAspectOnQuery: true,
        label: 'label.count.no.prodotti',
        displayRow: function (el, refreshFn) {
          refreshFnProdottiScelti = refreshFn;
          return Application.displayProdottiScelti(el, refreshFn, refreshFnProdotti, isMoveable);
        },
        displayAfter: function (documents, refreshFn, resultSet, isFilter) {
          if (!isFilter) {
            affix.find('sub.total').remove();
            displayTotalNumItems(affix, documents);
          }
        },
        maxItems: 5,
        mapping: function (mapping) {
          mapping.parentId = cmisObjectId;
          return mapping;
        }
      },
      input: {
        rel: {
          "cmis:sourceId" : null,
          "cmis:relObjectTypeId" : 'R:jconon_attachment:in_prodotto'
        }
      },
      submission: {
        externalData: [
          {
            name: 'aspect',
            value: 'P:cvpeople:selectedProduct'
          },
          {
            name: 'jconon_attachment:user',
            value: dataPeopleUser.userName
          }
        ],
        multiple: true,
        bigmodal: true
      },
      otherButtons: [
        {
          button : buttonPeopleScelti,
          add : function (type, cmisObjectId, refreshFn) {
            Application.people(type, cmisObjectId, 'P:cvpeople:selectedProduct', refreshFn, dataPeopleUser);
          }
        }
      ]
    });
  }

  function createProdotti(affix, isMoveable) {
    return new Attachments({
      isSaved: isSaved,
      affix: affix,
      objectTypes: prodottiAttachments,
      cmisObjectId: cmisObjectId,
      search: {
        type: 'cvpeople:commonMetadata',
        join: 'cvpeople:noSelectedProduct',
        isAspect: true,
        displayRow: function (el, refreshFn) {
          refreshFnProdotti = refreshFn;
          return Application.displayProdotti(el, refreshFn, refreshFnProdottiScelti, isMoveable);
        },
        displayAfter: function (documents, refreshFn, resultSet, isFilter) {
          if (!isFilter) {
            affix.find('sub.total').remove();
            displayTotalNumItems(affix, documents);
          }
        },
        maxItems: 5,
        filter: false,
        includeAspectOnQuery: true,
        label: 'label.count.no.prodotti',
        mapping: function (mapping) {
          mapping.parentId = cmisObjectId;
          return mapping;
        }
      },
      submission: {
        externalData: [
          {
            name: 'aspect',
            value: 'P:cvpeople:noSelectedProduct'
          },
          {
            name: 'jconon_attachment:user',
            value: dataPeopleUser.userName
          }
        ],
        requiresFile: false,
        showFile: false,
        bigmodal: true
      },
      otherButtons: [{
        button : buttonPeople,
        add : function (type, cmisObjectId, refreshFn) {
          Application.people(type, cmisObjectId, 'P:cvpeople:noSelectedProduct', refreshFn, dataPeopleUser);
        }
      }]
    });
  }

  function createCurriculum(affix) {
    return new Attachments({
      isSaved: isSaved,
      affix: affix,
      objectTypes: curriculumAttachments,
      cmisObjectId: cmisObjectId,
      search: {
        type: 'jconon_attachment:cv_element',
        displayRow: Application.displayCurriculum,
        displayAfter: function (documents, refreshFn, resultSet, isFilter) {
          if (!isFilter) {
            affix.find('sub.total').remove();
            displayTotalNumItems(affix, documents);
          }
        },
        fetchCmisObject: true,
        maxItems: 5,
        filter: false,
        filterOnType: true,
        includeAspectOnQuery: true,
        label: 'label.count.no.curriculum',
        mapping: function (mapping) {
          mapping.parentId = cmisObjectId;
          mapping['jconon_call:elenco_sezioni_curriculum'] = metadata['jconon_call:elenco_sezioni_curriculum'];
          return mapping;
        }
      },
      buttonUploadLabel: 'Aggiungi riga',
      submission: {
        requiresFile: false,
        showFile: false,
        bigmodal: true,
        externalData: [
          {
            name: 'jconon_attachment:user',
            value: dataPeopleUser.userName
          }
        ]
      }
    });
  }
  
  function createSchedeAnonime(affix) {
	 // var content = $('<div></div>').addClass('modal-inner-fix');
    return new Attachments({
      isSaved: isSaved,
      affix: affix,
      objectTypes: schedeAnonimeAttachments,
      cmisObjectId: cmisObjectId,
      search: {
    //	  el: metadata,
        type: 'jconon_scheda_anonima:document',
   //  displayRow: jconon.findAllegatiProfilo(metadata["cmis:objectId"], $('<div class="modal-inner-fix"></div>'), 'jconon_scheda_anonima:document', true, ApplicationFp.displayEsperienzeOIV, true, metadata),
     //   displayRow: ApplicationFp.displayEsperienzeOIV,
     //   displayRow: ApplicationFp.displayEsperienzeOIVProfilo,
        displayRow: function (el, refreshFn) {
          //  refreshFnProdottiScelti = refreshFn;
            return ApplicationFp.displayEsperienzeOIVProfilo(el, refreshFn, metadata);
          },
        
        displayAfter: function (documents, refreshFn, resultSet, isFilter) {
      //  	window.scrollTo(0, $('#affix_tabSchedaAnonima').offset().top);
//        	var  dataUltimoInvioDomanda,
//   		 dataInvioIscrizione =  metadata ? metadata['jconon_application:data_domanda'] : null,
//   		 dataInvioRinnovo =  metadata ? metadata['jconon_application:data_invio_rinnovo_elenco'] : null,
//   		 dataInvioCambioFascia =  metadata ? metadata['jconon_application:data_invio_cambio_fascia'] : null;
//   		 if( new Date( dataInvioIscrizione ) > new Date( dataInvioRinnovo ) ){
//   			 if( new Date( dataInvioIscrizione ) > new Date( dataInvioCambioFascia ) )
//   				 dataUltimoInvioDomanda = dataInvioIscrizione ;
//   			 else
//   				 dataUltimoInvioDomanda = dataInvioCambioFascia ;
//   		 }else{
//   			 if( new Date( dataInvioRinnovo ) > new Date( dataInvioCambioFascia ) )
//   				 dataUltimoInvioDomanda = dataInvioRinnovo ;
//   			 else
//   				 dataUltimoInvioDomanda = dataInvioCambioFascia ;
//   		 }
//   		$(resultSet).each(function( index, element ) {
//   		   console.log("index "+index+" elen 2 "+element);
//   		   if( dataUltimoInvioDomanda < element["cmis:creationDate"])
//   			$("."+element['cmis:objectId'].split(';')[0]).addClass("editable");
//   		  
//   		  });
	          if (!isFilter) {
	            affix.find('sub.total').remove();
	            displayTotalNumItems(affix, documents);
	          }
        },
        fetchCmisObject: true,
        calculateTotalNumItems: true,
        maxItems: 5,
        filter: false,
        filterOnType: true,
        includeAspectOnQuery: true,
        label: 'label.count.no.curriculum',
        mapping: function (mapping) {
          mapping.parentId = cmisObjectId;
          mapping['jconon_call:elenco_schede_anonime'] = metadata['jconon_call:elenco_schede_anonime'];
          return mapping;
        }
      },
      buttonUploadLabel: 'Aggiungi riga',
      submission: {
        requiresFile: false,
        showFile: false,
        bigmodal: true,
        externalData: [
          {
            name: 'jconon_attachment:user',
            value: dataPeopleUser.userName
          }
        ]
      }
    });
  }

  function createSchedeAnonimeRinnovo(affix) {
	    return new Attachments({
	      isSaved: isSaved,
	      affix: affix,
	      objectTypes: schedeAnonimeAttachments,
	      cmisObjectId: cmisObjectId,
	      search: {
	        type: 'jconon_scheda_anonima:document',
	        displayRow: ApplicationFp.displayEsperienzeOIVRinnovo,
	        displayAfter: function (documents, refreshFn, resultSet, isFilter) {
//	          if (!isFilter) {
//	            affix.find('sub.total').remove();
//	            displayTotalNumItems(affix, documents);
//	          }
	          var totaleGiorniEsp=0, dataIscrizione, dataLimiteProroga = new Date("2018/08/31"), dataScadenzaDomanda, listaRemove=[];
	          if(metadata["jconon_application:data_iscrizione_elenco"] != null ){
	        	  dataIscrizione=new Date(metadata["jconon_application:data_iscrizione_elenco"]);
	        	  if ( dataIscrizione.getTime() <= dataLimiteProroga.getTime()  )
	        		  dataScadenzaDomanda =new Date(new Date(metadata["jconon_application:data_iscrizione_elenco"]).setMonth(dataIscrizione.getMonth()+54));
	        	  else
	        		  dataScadenzaDomanda =new Date(new Date(metadata["jconon_application:data_iscrizione_elenco"]).setMonth(dataIscrizione.getMonth()+36));
        		 
        	  }
	          for (let i = 0; i <  documents.items.length; i++) {
	        	  if ( documents.items[i]['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo') !== -1 /* ||
	        			  documents.items[i]['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente') !== -1 */ ){
	        		  listaRemove.push(documents.items[i]["cmis:objectId"].split(";")[0]);
	        		  continue;
	        	  }
	        	  	var esp_da = new Date( documents.items[i]["jconon_attachment:esperienza_professionale_da"] ).getTime() || new Date( documents.items[i]["jconon_attachment:precedente_incarico_oiv_da"] ).getTime(); 
		        	var esp_a = new Date( documents.items[i]["jconon_attachment:esperienza_professionale_a"] ).getTime() || new Date( documents.items[i]["jconon_attachment:precedente_incarico_oiv_a"] ).getTime(); 

	        	  // controllo su iscrizione elenco
	        	  
		        	if( dataIscrizione != null) {
		        		
		        		if( ( dataIscrizione.getTime() > esp_a || 
		        				dataScadenzaDomanda.getTime() < esp_da || 
		        					!documents.items[i]["jconon_attachment:dirigente_ruolo"] ) || 
		        					documents.items[i]["jconon_attachment:dirigente_ruolo"] == null || 
		        					!documents.items[i]["cmis:objectTypeId"].includes("esperienza_professionale") ||
		        					!documents.items[i]["jconon_attachment:fl_amministrazione_pubblica"]  ){
		        			 esp_da = null;
		        			 esp_a = null;
		        			 listaRemove.push(documents.items[i]["cmis:objectId"].split(";")[0]);
		        		} else {
		        			if( Number.isNaN(esp_a) ){
			        	    	// considerare  fino alla scadenza caso in corso
		        				if( metadata["jconon_application:fl_proroga_rinnovo"] == null &&  new Date().getTime() > dataScadenzaDomanda.getTime() )
		        					esp_a = new Date().getTime();
		        				else
		        					esp_a = dataScadenzaDomanda.getTime();
			        	    }  
		        			if( dataIscrizione.getTime() > esp_da ){
			        	    	// start e end corriscpoindono con iscrizione e scadenza
			        	    	esp_da = dataIscrizione.getTime();
			        	    
			        	    }   
		        		}
		        		
		        	}
		        	
		        	if(esp_da && esp_a){
        		        	 var durata = esp_a - esp_da;
        		        	 var seconds = Math.floor(durata / 1000),
			        	    minutes = Math.floor(seconds / 60),
			        	    hours   = Math.floor(minutes / 60),
			        	    days    = Math.floor(hours / 24),
			        	    months  = Math.floor(days / 30);
			        	 	totaleGiorniEsp = totaleGiorniEsp + days;
				        	}
          	  
	          }
	          if(totaleGiorniEsp >= 1095){
	        	  creditiEsperienze = 40;
	          }else{
	        	  creditiEsperienze = ( 40 * totaleGiorniEsp.toFixed(1) ) / 1095;
	          }
	          
	          $.ajax({
	        		
                  type: 'POST',
                  url: cache.baseUrl + "/rest/application-fp/add-crediti",
                  data: {
                    nodeRef : metadata["cmis:objectId"],
                    mesi: Math.floor(totaleGiorniEsp / 30),
                    creditiEsperienze:  creditiEsperienze.toFixed(1)
                
                   
                  },
                  
                  complete: close,
                  error: jconon.error
                });  
	          
	          
	          if (!isFilter) {
		            affix.find('sub.total').remove();
		            displayTotalNumItems(affix, documents);
		          }
	          
//		      COMMENTATA PER PROVE DA FARE PRIMA DEL RILASCAO DECOMMENTARE
	          
	          setTimeout(function(){ 
	        	  listaRemove.forEach(function( element, index ) {
		        		$(".in #esperienze_rinnovo ."+listaRemove[index]+"").css("display","none");		        		
		        	});
		           // $('.in .control-group').first().html('<p>Le esperienze professionali inserite equivalgono ad un totale di '+creditiEsperienze.toFixed(1)+' crediti pertanto devi inserire di seguito il detttaglio dei crediti mancanti.</p>');	            
		            $('.totCreditiEsperienze').text('Totale dei crediti da maturare   '+(40 - parseFloat(creditiEsperienze).toFixed(1)).toFixed(1));
		            $('.totCrediti').text('Totale dei crediti maturati   '+( parseFloat( crediti ) ));
		            if( creditiEsperienze >= 40 ){    
			            $('.in .control-group').first().html('<p>Di seguito sono riportate le esperienze inserite nel profilo che consentono l’esenzione dai 40 crediti. Per completare l’aggiornamento del profilo è necessario stampare ed inviare il modulo debitamente firmato.</p>');	            
			            $( '.tableCorsi' ).css( "display" , "none" );
			            $( '.in #addCrediti' ).css( "display" , "none" );
			            $( '.buttonAddCredit' ).css( "display" , "none" );
			            $( '.in #myModalLabel' ).html('<i class="icon-upload animated flash"></i> Invia domanda di aggiornamento');
			            $( '.divCrediti' ).css( "display" , "none" );
			            $( '.printBtn' ).removeClass("disabled");	
			            $( '.in .btn[data-dismiss="modal"] ' ).css( "right" , "59%" );
			            $( '.in .printBtn' ).html('<i class="icon-file"></i> Scarica modulo aggiornamento');
			            $( '.totCreditiEsperienze' ).css( "display" , "none" );
			            $( '.totCrediti' ).css( "display" , "none" );
			            //$( '.totCreditiDiv' ).css( "display" , "none" );
			       //     $('.totCreditiDiv').html(' <p  class=""><b>L’articolo 6 del '+i18n['label.link.dm']+' prevede che i <u>dirigenti di ruolo in servizio nelle amministrazioni pubbliche siano esclusi dall’obbligo di maturare 40 crediti formativi.</u></p>');
			            
			            $( '.articoloDM' ).css( "max-width" , "100%" );
		            }else if( creditiEsperienze > 0 ){
			            $('.in .control-group').first().html('<p>Di seguito sono riportate le esperienze inserite nel profilo che consentono l’esenzione parziale dei  crediti. Prima di stampare e inviare il modulo firmato, è necessario inserire il dettaglio dei corsi frequentati per il raggiungimento dei crediti necessari al rinnovo dell’iscrizione.</b></p>');	            
			           
		            	
		            }else if( creditiEsperienze == 0 ){
		            	 $( '.in .well' ).css( "display" , "none" );
				      //   $( '.in .totCreditiEsperienzeDiv' ).css( "display" , "none" );
				         $( '.in .totCreditiCorsiDiv' ).css( "display" , "none" );
				     //    $( '.in .totCreditiDiv' ).css( "display" , "none" );
				         $( '.in .buttonAddCredit' ).text( "Inserire il dettaglio dei corsi frequentati per il raggiungimento dei crediti necessari al rinnovo dell’iscrizione " );
				         
		            }
		            if ( creditiEsperienze + crediti >= 40 ) 
		            	$( '.in .printBtn' ).removeClass("disabled");
		            if ( crediti > 0 )
		            	 $( '.in .tableCorsi' ).css( "display" , "table" );		
	          }, 1000);
	          
	        },
	        fetchCmisObject: true,
	        calculateTotalNumItems: true,
	        maxItems: 100,
	        filter: false,
	        filterOnType: true,
	        includeAspectOnQuery: true,
	        label: 'label.count.no.curriculum',
	        mapping: function (mapping) {
	          mapping.parentId = cmisObjectId;
	          mapping['jconon_call:elenco_schede_anonime'] = metadata['jconon_call:elenco_schede_anonime'];
	          return mapping;
	        }
	      },
	      buttonUploadLabel: 'Aggiungi riga',
	      submission: {
	        requiresFile: false,
	        showFile: false,
	        bigmodal: true,
	        externalData: [
	          {
	            name: 'jconon_attachment:user',
	            value: dataPeopleUser.userName
	          }
	        ]
	      }
	    });
	  }

  function createCorsiRinnovo(affix) {
	    return new Attachments({
	      isSaved: isSaved,
	      affix: affix,
	      objectTypes: schedeAnonimeAttachments,
	      cmisObjectId: cmisObjectId,
	      search: {
	        type: 'jconon_scheda_anonima:document_rinnovo',
	        displayRow: ApplicationFp.displayCorsiOIVRinnovo,
	        displayAfter: function (documents, refreshFn, resultSet, isFilter) {
//	          if (!isFilter) {
//	            affix.find('sub.total').remove();
//	            displayTotalNumItems(affix, documents);
//	          }
	        	
					  
	        	setTimeout(function(){ 		  
				 
		        	$(".in .table").last().css("display","contents");
		        	$(".in .control-group").last().css("display","none");
		        	var totCreditiCorsiSalvati = 0;
		        	$( ".in .numCrediti" ).each(function( index ) {
		        		totCreditiCorsiSalvati += parseFloat( $( this ).text().replace(',','.').replace(' ','') );
		        		});
		        	crediti =  totCreditiCorsiSalvati;
		        	$.ajax({
		        		
  	                  type: 'POST',
  	                  url: cache.baseUrl + "/rest/application-fp/add-crediti-corsi",
  	                  data: {
  	                    nodeRef : metadata["cmis:objectId"],	            	                  
  	                    creditiEsperienze:  crediti
  	                
  	                   
  	                  },
  	                  
  	       //           complete: close,
  	                  error: jconon.error
  	                });  
		        	$('.totCrediti').text('Totale dei crediti maturati '+totCreditiCorsiSalvati);
    	          //  $('.totCreditiCorsiDiv').html('<p style="position: relative; width: 40%; left: 690px;" class="totCreditiCorsi">Totale crediti derivanti da corsi   '+totCreditiCorsiSalvati+'</p>');
    	            if( totCreditiCorsiSalvati >= 40 ){    
		
			            $( '.printBtn' ).removeClass("disabled");		            
		            }else if( totCreditiCorsiSalvati == 0 ){
		            	 $( '.in .tableCorsi' ).css( "display" , "none" );				        
		            }else if( totCreditiCorsiSalvati > 0){
		            	 $( '.in .tableCorsi' ).css( "display" , "table" );			
		            }
	        	  }, 300);
	        	
	        },
	        fetchCmisObject: true,
	        calculateTotalNumItems: true,
	        maxItems: 100,
	        filter: false,
	        filterOnType: true,
	        includeAspectOnQuery: true,
	        label: 'label.count.no.curriculum',
	        mapping: function (mapping) {
	          mapping.parentId = cmisObjectId;
	          mapping['jconon_call:elenco_schede_anonime'] = metadata['jconon_call:elenco_schede_anonime'];
	          return mapping;
	        }
	      },
	      buttonUploadLabel: 'Aggiungi riga',
	      submission: {
	        requiresFile: false,
	        showFile: false,
	        bigmodal: true,
	        externalData: [
	          {
	            name: 'jconon_attachment:user',
	            value: dataPeopleUser.userName
	          }
	        ]
	      }
	    });
	  }
  
  
  function manageIntestazione(call, application) {
	    var descRid = null,
	      existApplication = application && application["jconon_application:stato_domanda"] !== 'I',
	      isTemp = existApplication && application["jconon_application:stato_domanda"] === 'P',
	      lastName = application && application["jconon_application:cognome"] !== undefined ? application["jconon_application:cognome"] : dataPeopleUser.lastName,
	      firstName = application && application["jconon_application:nome"] !== undefined ? application["jconon_application:nome"] : dataPeopleUser.firstName;
	    if (call["cmis:objectTypeId"] === 'F:jconon_call_mobility_open:folder') {
	      $('#application-title').hide();
	    } else if (call["cmis:objectTypeId"] === 'F:jconon_call_mobility:folder') {
	      $('#application-title').append(i18n['application.title.mobility']);
	    } else {
	      $('#application-title').append(i18n['application.title']+ ' - Profilo di '+application["jconon_application:nome"]+' '+application["jconon_application:cognome"] );
	  //    $('#application-title').append('<p><b>Il mio profilo</b></p>');
	      
	    }

	    $('#call-codice')
	      .prepend(i18n['label.jconon_bando_selezione'] + ' ' + call["jconon_call:codice"])
	      .on('click', 'button', function () {
	        Call.displayAttachments(callId);
	      });
	 //   $('#call-desc').append(call["jconon_call:descrizione"]);
	    $('#appl-rich').append(/*i18n['application.text.mio.profilo.header.1']+'</br>' +*/
	    						i18n['application.text.mio.profilo.header.2']+'</br>' +
	    						i18n['application.text.mio.profilo.header.3']+'</br>'/* +
	    						i18n['application.text.mio.profilo.header.4']+'</br>' */);
		$('#appl-rich').css("font-size","21px");
	      $('#call-desc').append(call["jconon_call:descrizione_form"]);
	    if (call["jconon_call:sede"] && call["jconon_call:sede"].length) {
	      descRid = (descRid !== null ? descRid + '</br>' : '') + call["jconon_call:sede"];
	    } else if (call["jconon_call:elenco_settori_tecnologici"] && call["jconon_call:elenco_settori_tecnologici"].length) {
	      /*jslint unparam: true*/
	      $.each(call["jconon_call:elenco_settori_tecnologici"], function (index, el) {
	        descRid = (descRid !== null ? descRid + ' - ' + el : i18n['label.th.jconon_bando_elenco_settori_tecnologici'] + ': ' + el);
	      });
	      /*jslint unparam: false*/
	    } else if (call["jconon_call:elenco_macroaree"] && call["jconon_call:elenco_macroaree"].length) {
	      /*jslint unparam: true*/
	      $.each(call["jconon_call:elenco_macroaree"], function (index, el) {
	        descRid = (descRid !== null ? descRid + ' - ' + el : i18n['label.th.jconon_bando_elenco_macroaree'] + ': ' + el);
	      });
	      /*jslint unparam: false*/
	    }
	  //  $('#call-desc-rid').append(call["jconon_call:descrizione_ridotta"] + (descRid !== null ? descRid : ""));
//	    $('#appl-rich').append(i18n['application.text.sottoscritto.' + (application['jconon_application:sesso'] !== "" ? application['jconon_application:sesso'] : 'M')] + ' ' + firstName.toUpperCase() + ' ' + lastName.toUpperCase() + '</br>' +
//	      (call['cmis:objectTypeId'] === 'F:jconon_call_employees:folder' ? i18n['cm.matricola'] + ': ' + dataPeopleUser.matricola + ' - ' + i18n['cm.email'] + ': ' + dataPeopleUser.email + '</br>' : '') +
//	      (call['cmis:objectTypeId'] === 'F:jconon_call_mobility_open:folder' ? '' : i18n['application.text.chiede.partecipare.predetta.procedura']));
//	  var provvisorio=$(".jumbotron h5").last().text();
//	  
//	  var prov= $(".jumbotron h5 b").last().text();
//	  provvisorio= provvisorio.replace(prov,"");
//	  //provvisorio= provvisorio.toLowerCase();
//
//	if (provvisorio != String.Empty ){
// 		provvisorio = provvisorio.charAt(0).toLowerCase() + provvisorio.slice(1);
// 		}

	// 
	//  $(".jumbotron h5").last().text($(".jumbotron h5 b").last().text());
	//  $(".jumbotron h5 b").last().text(provvisorio);
	 //   $(".jumbotron h5").last().html('<h5>E '+prov+'<br><br>'+provvisorio+'</h5>');
		$(".jumbotron h5").last().remove();
	  }

  function changeActiveState(btn) {
    btn.parents('ul').find('.active').removeClass('active');
    btn.parent('li').addClass('active');
  }

  function onChangeDipendentePubblico(data, onload) {
    var optionsPubblico = content.find('#situazione_lavorativa_settore option').filter(
        function(i, e) {
          return optionPubblico.indexOf($(e).text()) !== -1
        }
    ),optionsPrivato = content.find('#situazione_lavorativa_settore option').filter(
        function(i, e) {
          return optionPubblico.indexOf($(e).text()) === -1
        }
    );
    if (data === 'true') {
//      content.find('label[for="situazione_lavorativa_cod_amm_ipa"]').text('indice IPA');
      optionsPrivato.attr('disabled', 'disabled');
      optionsPubblico.removeAttr('disabled');
    } else if (data === 'false') {
      optionsPubblico.attr('disabled', 'disabled');
      optionsPrivato.removeAttr('disabled');
    }
    if (!onload) {
        content.find('#situazione_lavorativa_settore').val('');
        content.find('#situazione_lavorativa_settore').trigger('change');
    }
  }

  function manangeClickDipendentePubblico() {
    $('#fl_dipendente_pubblico > button.btn').on("click", function () {
      onChangeDipendentePubblico($(this).attr('data-value'), false);
    });
  }

  function onChangeSettore(data, onChange) {
    var select = content.find('#situazione_lavorativa_ruolo'), 
      options = content.find('#situazione_lavorativa_ruolo option'),
      optionsGroup = content.find('#situazione_lavorativa_ruolo optgroup[label!="' + data + '"] option');
    if (onChange) {
      options.removeAttr('selected');
      select.val('');      
    }
    options.removeAttr('disabled');
    optionsGroup.attr('disabled', 'disabled');
    select.trigger('change');
  }

  function manangeClickSettore() {
    $('#situazione_lavorativa_settore').on("change", function () {
      onChangeSettore($("#situazione_lavorativa_settore option:selected" ).text(), true);
    });
  }

  function manageNazioni(value, fieldsItaly, fieldsForeign) {
    if (value && value.toUpperCase() === 'ITALIA') {
      fieldsForeign.val('').trigger('blur');
      fieldsItaly.parents(".control-group").show();
      fieldsForeign.parents(".control-group").hide();
    } else {
      fieldsItaly.val('').trigger('change');
      fieldsItaly.parents(".control-group").hide();
      fieldsForeign.parents(".control-group").show();
    }
  }

  function manageNazioneNascita(value) {
    var fieldsItaly = content.find("#comune_nascita"),
      fieldsForeign = content.find("#comune_nascita_estero");
    manageNazioni(value, fieldsItaly, fieldsForeign);
  }

  function manageNazioneResidenza(value) {
    var fieldsItaly = content.find("#comune_residenza"),
      fieldsForeign = content.find("#comune_residenza_estero");
    manageNazioni(value, fieldsItaly, fieldsForeign);
  }
  
 

  function manageNazioneComunicazioni(value) {
    var fieldsItaly = content.find("#comune_comunicazioni"),
      fieldsForeign = content.find("#comune_comunicazioni_estero");
    manageNazioni(value, fieldsItaly, fieldsForeign);
  }

  function tabAnagraficaFunction() {
    /*jslint unparam: true*/
    $('#nazione_nascita').parents('.widget').bind('changeData', function (event, key, value) {
      if (key === 'value') {
        manageNazioneNascita(value);
      }
    });
    /*jslint unparam: false*/
    manageNazioneNascita($("#nazione_nascita").attr('value'));
  }

  function tabResidenzaFunction() {
    /*jslint unparam: true*/
    $('#nazione_residenza').parents('.widget').bind('changeData', function (event, key, value) {
      if (key === 'value') {
        manageNazioneResidenza(value);
      }
    });
    /*jslint unparam: false*/
    manageNazioneResidenza($("#nazione_residenza").attr('value'));
  }

  function tabReperibilitaFunction() {
    /*jslint unparam: true*/
    $('#nazione_comunicazioni').parents('.widget').bind('changeData', function (event, key, value) {
      if (key === 'value') {
        manageNazioneComunicazioni(value);
      }
    });
    /*jslint unparam: false*/
    manageNazioneComunicazioni($("#nazione_comunicazioni").attr('value'));
    $("#copyFromTabResidenza").click(function () {
      UI.confirm(i18n.prop('message.copy.residenza'), function () {
        var nazioneVal = content.find("#nazione_residenza").val();
        setObjectValue(content.find("#nazione_comunicazioni"), nazioneVal);
        if (nazioneVal.toUpperCase() === 'ITALIA') {
          setObjectValue(content.find("#comune_comunicazioni"), content.find("#comune_residenza").val());
        } else {
          setObjectValue(content.find("#comune_comunicazioni_estero"), content.find("#comune_residenza_estero").val());
        }
        setObjectValue(content.find("#cap_comunicazioni"), content.find("#cap_residenza").val());
        setObjectValue(content.find("#indirizzo_comunicazioni"), content.find("#indirizzo_residenza").val());
        setObjectValue(content.find("#num_civico_comunicazioni"), content.find("#num_civico_residenza").val());
        manageNazioneComunicazioni(nazioneVal);
      });
    });
  }

  function bulkInfoRender(call) {
    cmisObjectId = metadata['cmis:objectId'];
    bulkinfo =  new BulkInfo({
      target: content,
      formclass: 'form-horizontal jconon',
      path: 'F:jconon_application:folder',
      name: forms,
      metadata: metadata,
      callback: {
        beforeCreateElement: function (item) {
          if (item.name === 'elenco_lingue_conosciute') {
            var jsonlistLingueConosciute = [];
            if (call["jconon_call:elenco_lingue_da_conoscere"] !== undefined) {
              $.each(call["jconon_call:elenco_lingue_da_conoscere"], function (index, el) {
                jsonlistLingueConosciute.push({
                  "key" : el,
                  "label" : el,
                  "defaultLabel" : el
                });
              });
              item.jsonlist = jsonlistLingueConosciute;
            }
          }
          if (item.name === 'email_pec_comunicazioni') {
            item.class = 'input-xlarge';
          }
          if (item.name === 'email_comunicazioni') {
            item.class = 'input-xlarge';
          }
          if (item.name === 'situazione_lavorativa_ruolo') {
            item.jsonlist = ruoloLavorativo;
          }
        },
        afterCreateForm: function (form) {
          var rows = form.find('#affix_tabDichiarazioni table tr'),
            labelKey = 'text.jconon_application_dichiarazione_sanzioni_penali_' + call['jconon_call:codice'],
            labelSottoscritto = i18n['application.text.sottoscritto.lower.' + (metadata['jconon_application:sesso'] !== "" ? metadata['jconon_application:sesso'] : 'M')],
            labelValue = i18n.prop(labelKey, labelSottoscritto);
          /*jslint unparam: true*/
          $.each(rows, function (index, el) {
            var td = $(el).find('td:last');
            if (td.find("[data-toggle=buttons-radio]").size() > 0) {
              td.find('label:first').addClass('span10').removeClass('control-label');
              td.find('.controls:first').addClass('span2');
            }
          });
          /*jslint unparam: false*/
          form.find('#affix_tabDichiarazioniConclusive label').addClass('span10').removeClass('control-label');
          form.find('#affix_tabDichiarazioniConclusive .controls').addClass('span2');
          if (labelValue === labelSottoscritto) {
            labelValue = i18n.prop('text.jconon_application_dichiarazione_sanzioni_penali', labelSottoscritto);
          }
          $('#fl_dichiarazione_sanzioni_penali').parents('div.widget').children('label').text(labelValue);
          $('#fl_dichiarazione_dati_personali').parents('div.widget').children('label').text(i18n.prop('text.jconon_application_dichiarazione_dati_personali', labelSottoscritto));
          $.each(call["jconon_call:elenco_field_not_required"], function (index, el) {
            var input = form.find("input[name='" + el + "']"),
              widget = form.find("#" + el.substr(el.indexOf(':') + 1)).parents('.widget');
            if (input.length !== 0) {
              input.rules('remove', 'required');
            }
            if (widget.length !== 0) {
              widget.rules('remove', 'requiredWidget');
            }
          });

      	if($(".labelSchedaAnonima").length < 1){
      	//	$("<label class='labelSchedaAnonima' >Attenzione !!! <br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>").insertBefore($("#affix_tabSchedaAnonima"));
      		$("#affix_tabSchedaAnonima").before("<label id='labelSchedaAnonima' style='margin-top: 60px;margin-bottom: 0px; font-size: larger;text-align: center; cursor: default;' class='well labelSchedaAnonima' ><b>Attenzione !!! </b><br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>");
      		
      	
      	}
      	
      	$("#fl_occupato").click(function () {
            if( $(this).find('button.active').text().includes('Si') ){
            	$("#situazione_lavorativa_cod_amm_ipa").parent().parent().hide();
            	$("#situazione_lavorativa_amministrazione").parent().parent().hide();
            }
      //      console.log("  PROVA ------- +++++"+$(this).find('button.active').text());    
           
       });
      	if ( $("#fl_dipendente_pubblico button[data-value='true']").hasClass("active") )
      		$("#situazione_lavorativa_datore_lavoro").val("");
//      	if( $("#fl_altra_laurea button.active").length == 0 )
//      		$("#fl_altra_laurea button[data-value='false']").addClass("active");
//      	else if( $("#fl_altra_laurea button[data-value='true']").hasClass("active") )
//      		$("#s2id_istituto_laurea").parent().parent().hide();       
//      	
//      	if( $("#fl_altra_tipologia_laurea button.active").length == 0 )
//      		$("#fl_altra_tipologia_laurea button[data-value='false']").addClass("active");
//      	else if( $("#fl_altra_tipologia_laurea button[data-value='true']").hasClass("active") )
//      		$("#s2id_tipo_laurea").parent().parent().hide();       
      	
      	if ( $("#fl_altra_laurea button[data-value='true']").hasClass("active") ){
      		$("#istituto_laurea").val("");
        	$("#istituto_laurea").change();
        	$("#s2id_istituto_laurea").parent().parent().hide();    
      	}
      	
      	if ( $("#fl_altra_tipologia_laurea button[data-value='true']").hasClass("active") ){
      		$("#tipo_laurea").val("");
        	$("#tipo_laurea").change();
        	$("#s2id_tipo_laurea").parent().parent().hide();      
      	}
      			
      	$("#fl_altra_laurea").click(function (e) {
            if( e.target.innerText.includes('Si') ){
            	//$("#s2id_istituto_laurea a span").empty();
            	$("#istituto_laurea").val("");
            	$("#istituto_laurea").change();
            	$("#s2id_istituto_laurea").parent().parent().hide();            	
            } else if( e.target.innerText.includes('No') )
            	$("#s2id_istituto_laurea").parent().parent().show();
            
       });
      	$("#fl_altra_tipologia_laurea").click(function (e) {
            if( e.target.innerText.includes('Si') ){
            //	$("#s2id_tipo_laurea a span").empty();
            	$("#tipo_laurea").val("");
            	$("#tipo_laurea").change();
            	$("#s2id_tipo_laurea").parent().parent().hide();            	
            } else if( e.target.innerText.includes('No') )
            	$("#s2id_tipo_laurea").parent().parent().show();
         
       });
      	
      		
        $("#codice_fiscale").attr('disabled', true);
        $("#email_comunicazioni").attr('disabled', true);
        $('#istituto_laurea').on('change', function ( event ){
  		  if($('#istituto_laurea').val().length > 0 )
  			  $("#fl_altra_laurea button[data-value='false']").click();
  		 
  	  	});
        $('#tipo_laurea').on('change', function ( event ){
    		  if($('#tipo_laurea').val().length > 0 )
    			  $("#fl_altra_tipologia_laurea button[data-value='false']").click();
    		 
    	  	});
        
//        if( metadata["jconon_application:data_domanda"] != null){
//        	  form.find('#s2id_istituto_laurea a').unbind();
//        	  form.find('#s2id_tipo_laurea a').unbind();
//        	  form.find("#data_laurea").attr('disabled', true);
//        	  form.find("#punteggio_laurea").attr('disabled', true);      	 
//        	  form.find('#fl_laurea_equipollente button').attr('disabled','disabled');
//        	  
//        	  form.find("#data_nascita").attr('disabled', true); 
//        	  form.find("#nazione_nascita").attr('disabled', true);
//        	  form.find("#comune_nascita").attr('disabled', true);      	 
//        	  form.find('#sesso button').attr('disabled','disabled');
//        	  form.find('#fl_cittadino_italiano button').attr('disabled','disabled');
//        	  
//        	}
        
//          $("#affix_tabSchedaAnonima").before($("#affix_tabUlterioriDati"));
//      	$("#affix_tabSchedaAnonima").before($("#affix_tabReperibilita"));
      	
//      	$("a[href='#affix_tabSchedaAnonima']").before($("a[href='#affix_tabUlterioriDati']"));
//      	$("a[href='#affix_tabSchedaAnonima']").before($("a[href='#affix_tabReperibilita']"));
      	var dataDomanda, dataAperturaInvioRinnovo, dataCorrente;
      	if( ( metadata["jconon_application:data_iscrizione_elenco"] != null )  ){
      		 dataDomanda=new Date(metadata["jconon_application:data_iscrizione_elenco"]);
      		 dataAperturaInvioRinnovo=new Date(dataDomanda.setMonth(dataDomanda.getMonth()+35));
      		 dataCorrente=new Date();
      	}
        

         if( ( ( metadata["jconon_application:data_iscrizione_elenco"] == null ) ||
        		( metadata["jconon_application:data_invio_rinnovo_elenco"] != null || metadata["jconon_application:data_rinnovo_elenco"] != null ) || 
        			dataCorrente.getTime() < dataAperturaInvioRinnovo.getTime() ||
        			( metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E") )
        	) && metadata['jconon_application:fl_sblocco_invio_rinnovo'] != true ){
	        	 $('#rinnovo').css('display','none');
	         	$('#rinnovo').next().css('display','none');
         }
         
         if( ( ( metadata["jconon_application:data_iscrizione_elenco"] != null ) ||
         		( metadata["jconon_application:data_domanda"] != null  ) ||          			
         			( metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E") )
         	) && metadata['jconon_application:fl_sblocco_invio_iscrizione'] != true ){
 	        	 $('#send').css('display','none');
 	         	$('#send').next().css('display','none');
          }
         if(metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E")){
 			var dataCancellazione=new Date(metadata["jconon_application:data_rimozione_elenco"]);
 			var dataCorrente=new Date();
 			
 			var dataNuovoInvio=new Date(dataCancellazione.setMonth(dataCancellazione.getMonth()+6));
 			
         }
         if( ( metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E") &&
					dataCorrente.getTime() >= dataNuovoInvio.getTime() ) ) {
			        	 $('#send').css('display','block');
			        	 $('#send').next().css('display','block');
					}
         if(  ( ( metadata["jconon_application:data_iscrizione_elenco"] == null ) ||
          //		( metadata["jconon_application:data_domanda"] != null  ) ||          			
          			( metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E") )
          	)  ||
          	metadata["jconon_application:fascia_professionale_validata"] == '3' ||
          	new Date(metadata["jconon_application:data_invio_cambio_fascia"]).getTime() > new Date(metadata["jconon_application:data_cambio_fascia"]).getTime()){
  	        	if ( metadata['jconon_application:fl_sblocco_invio_cambio_fascia'] != true  ) {
		        	 $('#cambio_fascia').css('display','none');
		  	         	$('#cambio_fascia').next().css('display','none');
  	        	}
           }
         
      	if($(".labelSchedaAnonima").length > 0){
      		//	$("<label class='labelSchedaAnonima' >Attenzione !!! <br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>").insertBefore($("#affix_tabSchedaAnonima"));
      			$("#affix_tabSchedaAnonima").before($("#labelSchedaAnonima"));

      		}  
      	
      	var buttonFascia='<div class="control-group"><div class="controls" style="margin-left: 40%;"><button class="btn" type="button" id="button_fascia_professionale_esegui_calcolo" value="">Ricalcola Fascia</button></div></div>';
          form.find('input.datepicker.input-small').addClass('input-medium').removeClass('inpt-small');
		  form.find('#fascia_professionale_attribuita').parents('.control-group').after(buttonFascia);
		
          form.find('#fascia_professionale_attribuita').parents('.control-group').after('<div class="alert alert-warning">Non forzare il ricalcolo della fascia in caso di reinvio della domanda a seguito di soccorso istruttorio o di preavviso di rigetto.</div>');

//          form.find('#fascia_professionale_attribuita').parents('.control-group').after('<div class="alert alert-warning">Il calcolo della fascia verrà eseguito dopo il salvataggio.</div>');
          form.find("label[for='fascia_professionale_attribuita']").addClass('span8').append("&nbsp;&nbsp;");
          tabAnagraficaFunction();
          tabResidenzaFunction();
          tabReperibilitaFunction();
          manangeClickDipendentePubblico();
          manangeClickSettore();
          onChangeDipendentePubblico(String(metadata['jconon_application:fl_dipendente_pubblico']), true);
          onChangeSettore(metadata['jconon_application:situazione_lavorativa_settore']);
          
          form.find('#button_fascia_professionale_esegui_calcolo').off('click').on('click', function () {
              $.ajax({
                url: cache.baseUrl + "/rest/application-fp/applications-ricalcola-fascia",
                type: 'GET',
                data:  {
                  'applicationId' : metadata['cmis:objectId']
                },
                success: function (data) {
					  
                 form.find('#fascia_professionale_attribuita').val(data['jconon_application:fascia_professionale_attribuita']);
                 if(data['jconon_application:fascia_professionale_attribuita']!=null){ 
                	 UI.success("La fascia ricalcolata è: " + data['jconon_application:fascia_professionale_attribuita']);
                 }else{
                	 UI.alert("Non è possibile assegnare nessuna fascia.");
                 }
                },
                error: URL.errorFn
              });
            });
//          if ( urlParams != undefined ){
//        	  var paramValueIscrizione = urlParams.get('iscrizione');
//              var paramValueCambioFascia = urlParams.get('cambio_fascia');
//              var paramValueRinnovo = urlParams.get('rinnovo');
//	          
//	          
//		  	    if( paramValueIscrizione!=null && paramValueIscrizione.includes("true") ){
//				          var close = UI.progress();
//				            setTimeout(function(){ 
//				    //	  $('#send').click();
//				    	  $('#send').trigger('click');
//				    	  close();
//				        }, 5000);
//		  	    }else  if( paramValueCambioFascia!=null && paramValueCambioFascia.includes("true") ){
//			          var close = UI.progress();
//			            setTimeout(function(){ 
//			    //	  $('#cambio_fascia').click();
//			    	  $('#cambio_fascia').trigger('click');
//			    	  close();
//			        }, 5000);
//		  	    }else  if( paramValueRinnovo!=null && paramValueRinnovo.includes("true") ){
//			          var close = UI.progress();
//			            setTimeout(function(){ 
//			    	//  $('#rinnovo').click();
//			    	  $('#rinnovo').trigger('click');
//			    	  close();
//			        }, 5000);
//		  	    }
//          }
        
        },
        afterCreateSection: function (section) {
          var div = section.find(':first-child'),
            jsonlistApplicationNoAspects = (metadata['jconon_application:fl_cittadino_italiano'] ? cache.jsonlistApplicationNoAspectsItalian : cache.jsonlistApplicationNoAspectsForeign),
            loadAspect;
          if (section.attr('id').indexOf('affix') !== -1) {
            div.addClass('well').append('<h1>' + i18n[section.attr('id')] + '</h1><hr></hr>');
            if (section.attr('id') === 'affix_tabDichiarazioni') {
              div.append($('<table></table>').addClass('table table-bordered'));
            } else if (section.attr('id') === 'affix_tabTitoli' && cmisObjectId) {
              showTitoli = createTitoli(div);
              showTitoli();
            } else if (section.attr('id') === 'affix_tabCurriculum' && cmisObjectId) {
              showCurriculum = createCurriculum(div);
              showCurriculum();
            } else if (section.attr('id') === 'affix_tabProdottiScelti' && cmisObjectId) {
              showProdottiScelti = createProdottiScelti(div, call["jconon_call:elenco_sezioni_domanda"].indexOf('affix_tabElencoProdotti') !== -1);
              showProdottiScelti();
            } else if (section.attr('id') === 'affix_tabElencoProdotti' && cmisObjectId) {
              showProdotti = createProdotti(div, call["jconon_call:elenco_sezioni_domanda"].indexOf('affix_tabProdottiScelti') !== -1);
              showProdotti();
            } else if (section.attr('id') === 'affix_tabSchedaAnonima' && cmisObjectId) {
              showSchedeAnonime = createSchedeAnonime(div);
              showSchedeAnonime();
             // window.scrollTo(0, 0);
        //      console.log("doipo schede anonime show");
            } 
          } else {
            loadAspect = true;
            /*jslint unparam: true*/
            $.each(jsonlistApplicationNoAspects, function (index, el) {
              if (el.key === section.attr('id')) {
                loadAspect = false;
              }
            });
            /*jslint unparam: false*/
            if (loadAspect) {
              if ((metadata['jconon_application:fl_cittadino_italiano'] && cache.jsonlistApplicationNoAspectsItalian.indexOf(section.attr('id')) === -1) ||
                  !(metadata['jconon_application:fl_cittadino_italiano'] && cache.jsonlistApplicationNoAspectsForeign.indexOf(section.attr('id')) === -1)) {
                if (call["jconon_call:elenco_aspects"].indexOf(section.attr('id')) !== -1) {
                  $('<tr></tr>')
                    .append('<td>' + String.fromCharCode(charCodeAspect++) + '</td>')
                    .append($('<td>').append(div))
                    .appendTo(content.find("#affix_tabDichiarazioni > :last-child > :last-child"));
                } else if (call["jconon_call:elenco_aspects_sezione_cnr"].indexOf(section.attr('id')) !== -1) {
                  div.appendTo(content.find("#affix_tabDatiCNR > :last-child"));
                } else if (call["jconon_call:elenco_aspects_ulteriori_dati"].indexOf(section.attr('id')) !== -1) {
                  div.appendTo(content.find("#affix_tabUlterioriDati > :last-child"));
                }
              }
            }
            section.hide();
          }
        }
      }
    });
    bulkinfo.render();
    bulkinfo.addFormItem('cmis:parentId', callId);
    /*jslint unparam: true*/
    $.each(aspects, function (index, el) {
      bulkinfo.addFormItem('aspect', el);
    });
    /*jslint unparam: false*/
    bulkinfo.addFormItem('cmis:objectId', metadata['cmis:objectId']);
 
        
  }

  function render(call, application) {
    var ul = $('.cnraffix'),
      print_dic_sost = $('<button class="btn btn-info" type="button">' + i18n['label.print.dic.sost'] + '</button>').on('click', function () {
        window.location = jconon.URL.application.print_dic_sost + '?applicationId=' + cmisObjectId;
      });
    $.each(call["jconon_call:elenco_sezioni_domanda"], function (index, el) {
      forms[index] = el;
      var li = $('<li></li>'),
        a = $('<a href="#' + el + '"><i class="icon-chevron-right"></i>' + i18n.prop(el, el) + '</a>').click(function (eventObject) {
          changeActiveState($(eventObject.target));
        });
      if (index === 0) {
        li.addClass('active');
      }
      li.append(a).appendTo(ul);
    });
 //   $('.cnr-sidenav').attr('style', 'position: relative !important');
    if (call["jconon_call:print_dic_sost"]) {
      $('.cnr-sidenav')
        .append('<br/>')
        .append(print_dic_sost);
    }
    aspects = call["jconon_call:elenco_aspects"]
      .concat(call["jconon_call:elenco_aspects_sezione_cnr"])
      .concat(call["jconon_call:elenco_aspects_ulteriori_dati"]);
    /*jslint unparam: true*/
    $.each(aspects, function (index, el) {
      forms[forms.length] = el;
    });
    /*jslint unparam: false*/
    metadata = $.extend({}, call, application);
    saved = metadata['jconon_application:stato_domanda'] !== 'I';
    bulkInfoRender(call);
    
   
  }


  $('#save').click(function () {
    bulkinfo.resetForm();
    var close = UI.progress();
    jconon.Data.application.main({
      type: 'POST',
      data: bulkinfo.getData(),
      success: function (data) {
        if (!cmisObjectId) {
          cmisObjectId = data.id;
          bulkinfo.addFormItem('cmis:objectId', cmisObjectId);
          UI.success(i18n['message.creazione.application']);
        } else {
          UI.success(i18n['message.jconon_application_salvataggio']);
        }
        $('#fascia_professionale_attribuita').val(data['jconon_application:fascia_professionale_attribuita'] || '');
        saved = true;
      },
      complete: close,
      error: URL.errorFn
    });
  });
  $('#send').click(function () {
	  var errorMess, checkCodFisc ;
	 
	      $.ajax({
	    	  
	          type: 'POST',
	          url: cache.baseUrl + "/rest/application-fp/checkCodiceFiscaleInvio",
	          data: {
		          nome: metadata["jconon_application:nome"],
		          cognome: metadata["jconon_application:cognome"],
		          dataNascita: $('#data_nascita').val(),
		          comuneNascita:  $('#comune_nascita').val(),
		          sesso: $('#sesso button.active').text(),
		          codiceFiscaleInserito: $('#codice_fiscale').val()
	          },
	          success: function(){

	  
	  if(metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E")){
			var data=new Date(metadata["jconon_application:data_rimozione_elenco"]);
			var dataCorrente=new Date();
			
			var dataLimite=new Date(data.setMonth(data.getMonth()+6));
			
	  }
	  if( $('.Curriculum').length < 1 && $('.Documento').length < 1 ){
		//  errorMess = "Attenzione! Inserire il curriculum e il documento di identità, in corso di validità, nella sezione Allegati alla domanda";
		  errorMess = i18n.prop('message.warning.curriculum.documento.mancante');
		  
	  }else if( $('.Curriculum').length < 1 ){
		  errorMess = i18n.prop('message.warning.curriculum.mancante');
		 
	  }else if( $('.Documento').length < 1 ){
		  errorMess = i18n.prop('message.warning.documento.mancante');
		  
	  }else  if( $('.Curriculum').length > 1 && $('.Documento').length > 1 ){
		  errorMess = "Attenzione!! devi inserire  una sola copia per curriculum e documento";
		  
	  }else if( $('.Curriculum').length > 1 ){
		  errorMess = i18n.prop('message.warning.curriculum.doppio');
		  
	  }else if( $('.Documento').length > 1 ){
		  errorMess = i18n.prop('message.warning.documento.doppio');
		 
	  }	  
				
		
		  
if( $('.Curriculum').length == 1 && $('.Documento').length == 1 ){			

	//if( checkCodFisc ){		
	  
				if((metadata["jconon_application:esclusione_rinuncia"] == null ) || ( metadata["jconon_application:esclusione_rinuncia"].includes("E") &&
						dataCorrente.getTime()>=dataLimite.getTime())){
					
						
				    var message = 'message.conferma.application.question',
				      placeholder = '';
				    if (metadata["jconon_call:elenco_sezioni_domanda"].indexOf('affix_tabProdottiScelti') !== -1 &&
				        $('#affix_tabProdottiScelti').find('table:visible').length === 0) {
				      placeholder = i18n.prop('message.conferma.application.prodotti.scelti');
				    }
		//	    UI.confirmInvio("conferma invio", i18n.prop( message, placeholder), function () {
			    
				    	if (bulkinfo.validate()) {
				        jconon.Data.application.main({
				          type: 'POST',
				          data: bulkinfo.getData(),
				          success: function (result) {
							
				            $('#fascia_professionale_attribuita').val(result['jconon_application:fascia_professionale_attribuita'] || '');
				            var container = $('<div class="fileupload fileupload-new" data-provides="fileupload"></div>'),
				              input = $('<div class="input-append"></div>'),
				           //   btn = $('<span class="btn btn-file btn-primary"></span>'),
				              inputFile = $('<h2>Scarica domanda da firmare </h2><h5>La domanda puo\' essere firmata in modo digitale oppure olografico</h5><h5>Ricorda di controllare la validita\' del tuo documento</h5>'),
				              btnPrimary,
				              m,
				              newPrint = $('<button class="btn btn-success " style="right: 10%; position: relative;"><i class="icon-file"></i> Scarica modulo iscrizione</button>'),
							  nextButton = $('<button class="btn btn-success disabled" id="nextButton" data-dismiss="modal"><i class="icon-double-angle-right"></i> Avanti</button>');
				     	
				            input
				           //   .append('<div class="uneditable-input input-xlarge"><i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span></div>')
				              .append(inputFile)
				              .appendTo(container);
	
				            // set widget 'value'
				            function setValue(value) {
				              container.data('value', value);
				            }
				
				            setValue(null);
				            input.append('<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Rimuovi</a>');
				    
				            function sendFile() {
				              var fd = new CNR.FormData();
				              fd.data.append("objectId", cmisObjectId);
				              $.each(inputFile[0].files || [], function (i, file) {
				                fd.data.append('domandapdf', file);
				              });
				              var close = UI.progress();
				              $.ajax({
				                  type: "POST",
				                  url: cache.baseUrl + "/rest/application-fp/send-application",
				                  data:  fd.getData(),
				                  enctype: fd.contentType,
				                  processData: false,
				                  contentType: false,
				                  dataType: "json",
				                  success: function(data){
				                    UI.success(i18n.prop('message.conferma.application.done', data.email_comunicazione), function () {
				                      window.location.href = cache.baseUrl + "/my-applications";
				                    });
				                  },
				                  complete: close,
				                  error: URL.errorFn
				              });
				            }
				
				            m = UI.modal('<i class="icon-upload animated flash"></i> Invia domanda di iscrizione', container, sendFile);
							
							btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
							btnPrimary.after(nextButton);
				            btnPrimary.after(newPrint);
							
							m.on('shown', function () {
								m.find('.modal-footer').find(".btn-primary").css( "display", "none" );
								btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
								btnPrimary.css("right", "20%");
								btnPrimary.text("Annulla");
								btnPrimary.css("position", "relative");
								
							});
					
				            newPrint.click(function () {
							//  $("#nextButton").removeClass("disabled");
							nextButton.removeClass("disabled");
				              window.location = 'rest/application/print-immediate?nodeRef=' + cmisObjectId;
							 
				            });
	
						 nextButton.click(function () {
							if(!nextButton.hasClass("disabled")){
								  var container = $('<div class="fileupload fileupload-new" data-provides="fileupload"></div>'),
								  input = $('<div class="input-append"></div>'),
								  btn = $('<span class="btn btn-file btn-primary"></span>'),
								  htext = $('<h2>Carica la domanda firmata </h2>'),
								  inputFile = $('<input  type="file" name="domandapdf"/>'),
								//  previewPdf=$('<br><br><embed   id="pdfPreview" src="" width="350"  height="300">'),
								  previewPdf=$('<br><br><iframe   id="pdfPreview" src="" width="500"  height="400" style="display: none;">'),
								  removeButton=$('<a href="#" class="btn fileupload-exists disabled" data-dismiss="fileupload">Rimuovi</a>'),
								  btnPrimary,
								  m;

				            btn
				              .append('<span class="fileupload-new"><i class="icon-upload"></i> Upload Domanda firmata</span>')
				              .append('<span class="fileupload-exists">Cambia</span>')
				              .append(inputFile);
						
									
				            input
							.append(htext)
				              .append('<div class="uneditable-input input-xlarge" ><i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span></div>')
				              .append(btn)
							  .append(removeButton)
							  .append(previewPdf)
				              .appendTo(container);
							     m = UI.modal('<i class="icon-upload animated flash"></i> Invia domanda di iscrizione', container, sendFile);
								 
								 m.on('shown', function () {
									//m.find('.modal-footer').find(".btn-primary").css( "display", "none" );
									m.find( ".submit" ).text( "Invia" );
									m.find( ".submit" ).addClass("disabled");
								//	$(".submit").prepend('<i class="icon-send"></i>')
									m.find( ".submit" ).css("position", "relative");
									m.find( ".submit" ).css("right", "-80px");
									btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
									btnPrimary.css("right", "440px");
									btnPrimary.text("Annulla");
									btnPrimary.css("position", "relative");
								//	m.css("left","35%");
									m.find(".modal-body").css("max-height","300px");
								//	m.css("width","1170px");
								});
								 
							// set widget 'value'
				            function setValue(value) {
				              container.data('value', value);
							 
				            }
				
				            setValue(null);
				          //  input.append('<a href="#" class="btn fileupload-exists disabled" data-dismiss="fileupload">Rimuovi</a>');
							removeButton.removeClass("disabled");
							removeButton.click(function () {
								removeButton.addClass("disabled");
								m.find("#pdfPreview").attr("src","");
								m.find( ".submit" ).addClass("disabled");
								m.find(".input-xlarge").css("width","");
								m.find("#pdfPreview").css("display","none");
							});
							
				            inputFile.on('change', function (e) {
				              var path = $(e.target).val();
				              setValue(path);
							 	
							  if(event.target.files!= undefined){
								var src= window.URL.createObjectURL(event.target.files[0]);
								m.find("#pdfPreview").attr("src",src);
								m.find("#pdfPreview").css("display","initial");
							  }else{
								  setTimeout(function(){ 
									  m.find( ".submit" ).addClass("disabled");
									  
								  }, 500);
								  
							  }

							m.find( ".submit" ).removeClass("disabled");
						//	m.find(".input-xlarge").css("width","auto");
				            });
				
				            function sendFile() {
				            	if(!m.find( ".submit" ).hasClass("disabled")){
						              var fd = new CNR.FormData();
						              fd.data.append("objectId", cmisObjectId);
						              $.each(inputFile[0].files || [], function (i, file) {
						                fd.data.append('domandapdf', file);
						              });
						              var close = UI.progress();
						              $.ajax({
						                  type: "POST",
						                  url: cache.baseUrl + "/rest/application-fp/send-application",
						                  data:  fd.getData(),
						                  enctype: fd.contentType,
						                  processData: false,
						                  contentType: false,
						                  dataType: "json",
						                  success: function(data){
						                    UI.success(i18n.prop('message.conferma.application.done', data.email_comunicazione), function () {
						                      window.location.href = cache.baseUrl + "/my-applications";
						                    });
						                  },
						                  complete: close,
						                  error: URL.errorFn
						              });
				            	}else{
									return false;	
								}
				            }	 
							}else{
								return false;
							}	 
				            });
							
				            saved = true;
							
				          },
				          error: URL.errorFn
				        });
				      } else {
				        var msg = content
				          .children('form')
				          .validate()
				          .errorList
				          .map(function (item) {
				            if ($(item.element).hasClass('widget')) {
				              return $(item.element).find('label').text();              
				            } else {
				              return $(item.element).parents('.control-group').find('label').text();
				            }
				          })
				          .filter(function (x) {
				            return x.trim().length > 0;
				          })
				          .map(function (x) {
				            return x.length > 50 ? x.substr(0, 50) + "\u2026" : x;
				          })
				          .join('<br>');
				        UI.alert(i18n['message.improve.required.fields'] + '<br><br>' + msg)
				      }
	    
			//	    });
					
				}else{
					UI.alert(i18n.prop('message.warning.cancellazione')+dataLimite.toLocaleDateString("it-IT"));	
				
				}
		
  }else{
  	UI.alert(errorMess);	

  }
	          },
	       	  error: function(data){	    
	        	    UI.alert( i18n.prop('message.warning.codice.fiscale.non.conforme') );
	    	  },
	          complete: close 
	          
	        });
  
  });
  
  $('#cambio_fascia').click(function () {
	  var errorMess, checkCodFisc ;
	 
	      $.ajax({
	    	  
	          type: 'POST',
	          url: cache.baseUrl + "/rest/application-fp/checkCodiceFiscaleInvio",
	          data: {
		          nome: metadata["jconon_application:nome"],
		          cognome: metadata["jconon_application:cognome"],
		          dataNascita: $('#data_nascita').val(),
		          comuneNascita:  $('#comune_nascita').val(),
		          sesso: $('#sesso button.active').text(),
		          codiceFiscaleInserito: $('#codice_fiscale').val()
	          },
	          success: function(){
	        	  
					  if(metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E")){
							var data=new Date(metadata["jconon_application:data_rimozione_elenco"]);
							var dataCorrente=new Date();	
							var dataLimite=new Date(data.setMonth(data.getMonth()+6));
							
					  }
					  if( $('.Curriculum').length < 1 && $('.Documento').length < 1 ){
						 // errorMess = "Attenzione! Inserire il curriculum e il documento di identità, in corso di validità, nella sezione Allegati alla domanda";					  
						  errorMess = i18n.prop('message.warning.curriculum.documento.mancante');
					  }else if( $('.Curriculum').length < 1 ){
						  errorMess = i18n.prop('message.warning.curriculum.mancante');
						 
					  }else if( $('.Documento').length < 1 ){
						  errorMess = i18n.prop('message.warning.documento.mancante');
						  
					  }else  if( $('.Curriculum').length > 1 && $('.Documento').length > 1 ){
						  errorMess = "Attenzione!! devi inserire  una sola copia per curriculum e documento";
						  
					  }else if( $('.Curriculum').length > 1 ){
						  errorMess = i18n.prop('message.warning.curriculum.doppio');
						  
					  }else if( $('.Documento').length > 1 ){
						  errorMess = i18n.prop('message.warning.documento.doppio');
						 
					  }	  		  
				if( $('.Curriculum').length == 1 && $('.Documento').length == 1 ){			
								if((metadata["jconon_application:esclusione_rinuncia"] == null ) || (metadata["jconon_application:esclusione_rinuncia"].includes("E") &&
										dataCorrente.getTime()>=dataLimite.getTime())){
		
										    var message = 'message.conferma.application.question.cambio.fascia',
										      placeholder = '';
										    if (metadata["jconon_call:elenco_sezioni_domanda"].indexOf('affix_tabProdottiScelti') !== -1 &&
										        $('#affix_tabProdottiScelti').find('table:visible').length === 0) {
										      placeholder = i18n.prop('message.conferma.application.prodotti.scelti');
										    }
									//    UI.confirmInvio("conferma invio cambio fascia", i18n.prop( message, placeholder), function () {
									    
										    	if (bulkinfo.validate()) {
										        jconon.Data.application.main({
										          type: 'POST',
										          data: bulkinfo.getData(),
										          success: function (result) {
													
										            $('#fascia_professionale_attribuita').val(result['jconon_application:fascia_professionale_attribuita'] || '');
										            var container = $('<div class="fileupload fileupload-new" data-provides="fileupload"></div>'),
										              input = $('<div class="input-append"></div>'),
										              inputFile = $('<h2>Scarica domanda da firmare </h2><h5>La domanda puo\' essere firmata in modo digitale oppure olografico</h5><h5>Ricorda di controllare la validita\' del tuo documento</h5>'),
										              btnPrimary,
										              m,
										              newPrint = $('<button class="btn btn-success " style="right: 5%; position: relative;"><i class="icon-file"></i> Scarica modulo di cambio fascia</button>'),
													  nextButton = $('<button class="btn btn-success disabled" id="nextButton" data-dismiss="modal"><i class="icon-double-angle-right"></i> Avanti</button>');
										     	
										            input
										              .append(inputFile)
										              .appendTo(container);
							
										            // set widget 'value'
										            function setValue(value) {
										              container.data('value', value);
										            }
										
										            setValue(null);
										            input.append('<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Rimuovi</a>');
										    
										            function sendFile() {
										              var fd = new CNR.FormData();
										              fd.data.append("objectId", cmisObjectId);
										              $.each(inputFile[0].files || [], function (i, file) {
										                fd.data.append('domandapdf', file);
										              });
										              var close = UI.progress();
										              $.ajax({
										                  type: "POST",
										                  url: cache.baseUrl + "/rest/application-fp/send-cambio-fascia",
										                  data:  fd.getData(),
										                  enctype: fd.contentType,
										                  processData: false,
										                  contentType: false,
										                  dataType: "json",
										                  success: function(data){
										                    UI.success(i18n.prop('message.conferma.cambio.fascia.done', data.email_comunicazione), function () {
										                      window.location.href = cache.baseUrl + "/my-applications";
										                    });
										                  },
										                  complete: close,
										                  error: URL.errorFn
										              });
										            }
										
										            m = UI.modal('<i class="icon-upload animated flash"></i> Invia domanda cambio fascia', container, sendFile);
													
													btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
													btnPrimary.after(nextButton);
										            btnPrimary.after(newPrint);
													
													m.on('shown', function () {
														m.find('.modal-footer').find(".btn-primary").css( "display", "none" );
														btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
														btnPrimary.css("right", "10%");
														btnPrimary.text("Annulla");
														btnPrimary.css("position", "relative");
														
													});
											
										            newPrint.click(function () {
													//  $("#nextButton").removeClass("disabled");
													nextButton.removeClass("disabled");
										              window.location = 'rest/application/print-cambio-fascia-immediate?nodeRef=' + cmisObjectId;
													 
										            });
							
												 nextButton.click(function () {
													if(!nextButton.hasClass("disabled")){
														  var container = $('<div class="fileupload fileupload-new" data-provides="fileupload"></div>'),
														  input = $('<div class="input-append"></div>'),
														  btn = $('<span class="btn btn-file btn-primary"></span>'),
														  htext = $('<h2>Carica la domanda firmata </h2>'),
														  inputFile = $('<input  type="file" name="domandapdf"/>'),
														//  previewPdf=$('<br><br><embed   id="pdfPreview" src="" width="350"  height="300">'),
														  previewPdf=$('<br><br><iframe   id="pdfPreview" src="" width="500"  height="400" style="display: none;">'),
														  removeButton=$('<a href="#" class="btn fileupload-exists disabled" data-dismiss="fileupload">Rimuovi</a>'),
														  btnPrimary,
														  m;
						
										            btn
										              .append('<span class="fileupload-new"><i class="icon-upload"></i> Upload Domanda firmata</span>')
										              .append('<span class="fileupload-exists">Cambia</span>')
										              .append(inputFile);
												
															
										            input
													.append(htext)
										              .append('<div class="uneditable-input input-xlarge" ><i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span></div>')
										              .append(btn)
													  .append(removeButton)
													  .append(previewPdf)
										              .appendTo(container);
													     m = UI.modal('<i class="icon-upload animated flash"></i> Invia domanda cambio fascia', container, sendFile);
														 
														 m.on('shown', function () {
															m.find( ".submit" ).text( "Invia" );
															m.find( ".submit" ).addClass("disabled");						
															m.find( ".submit" ).css("position", "relative");
															m.find( ".submit" ).css("right", "-80px");
															btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
															btnPrimary.css("right", "440px");
															btnPrimary.text("Annulla");
															btnPrimary.css("position", "relative");
															m.find(".modal-body").css("max-height","300px");
														});
														 
													// set widget 'value'
										            function setValue(value) {
										              container.data('value', value);
													 
										            }
										
										            setValue(null);
													removeButton.removeClass("disabled");
													removeButton.click(function () {
														removeButton.addClass("disabled");
														m.find("#pdfPreview").attr("src","");
														m.find( ".submit" ).addClass("disabled");
														m.find(".input-xlarge").css("width","");
														m.find("#pdfPreview").css("display","none");
													});
													
										            inputFile.on('change', function (e) {
										              var path = $(e.target).val();
										              setValue(path);
													 	
													  if(event.target.files!= undefined){
														var src= window.URL.createObjectURL(event.target.files[0]);
														m.find("#pdfPreview").attr("src",src);
														m.find("#pdfPreview").css("display","initial");
													  }else{
														  setTimeout(function(){ 
															  m.find( ".submit" ).addClass("disabled");
															  
														  }, 500);
														  
													  }
						
													m.find( ".submit" ).removeClass("disabled");
										            });
										
										            function sendFile() {
										            	if(!m.find( ".submit" ).hasClass("disabled")){
												              var fd = new CNR.FormData();
												              fd.data.append("objectId", cmisObjectId);
												              $.each(inputFile[0].files || [], function (i, file) {
												                fd.data.append('domandapdf', file);
												              });
												              var close = UI.progress();
												              $.ajax({
												                  type: "POST",
												                  url: cache.baseUrl + "/rest/application-fp/send-cambio-fascia",
												                  data:  fd.getData(),
												                  enctype: fd.contentType,
												                  processData: false,
												                  contentType: false,
												                  dataType: "json",
												                  success: function(data){
												                    UI.success(i18n.prop('message.conferma.cambio.fascia.done', data.email_comunicazione), function () {
												                      window.location.href = cache.baseUrl + "/my-applications";
												                    });
												                  },
												                  complete: close,
												                  error: URL.errorFn
												              });
										            	}else{
															return false;	
														}
										            }	 
													}else{
														return false;
													}	 
										            });
													
										            saved = true;
													
										          },
										          error: URL.errorFn
										        });
										      } else {
										        var msg = content
										          .children('form')
										          .validate()
										          .errorList
										          .map(function (item) {
										            if ($(item.element).hasClass('widget')) {
										              return $(item.element).find('label').text();              
										            } else {
										              return $(item.element).parents('.control-group').find('label').text();
										            }
										          })
										          .filter(function (x) {
										            return x.trim().length > 0;
										          })
										          .map(function (x) {
										            return x.length > 50 ? x.substr(0, 50) + "\u2026" : x;
										          })
										          .join('<br>');
										        UI.alert(i18n['message.improve.required.fields'] + '<br><br>' + msg)
										      }
							    
								//		    });
											
										}else{
											UI.alert(i18n.prop('message.warning.cancellazione')+dataLimite.toLocaleDateString("it-IT"));	
										
										}
						
				  }else{
				  	UI.alert(errorMess);	
				
				  }
	          },
	       	  error: function(data){	    
	        	    UI.alert( i18n.prop('message.warning.codice.fiscale.non.conforme') );
	    	  },
	          complete: close 
	          
	        });
  
  });
  
  $('#close').click(function () {
    UI.confirm(i18n.prop('message.exit.without.saving'), function () {
      window.location.href = document.referrer;
    });
  });
//  $('#print').click(function () {
//    Application.print(cmisObjectId, metadata.allowableActions.indexOf('CAN_CREATE_DOCUMENT') !== -1 ? 'P' : metadata['jconon_application:stato_domanda'], true);
//  });
  $('#home').click(function () {
	  window.location.href = cache.baseUrl + "/my-applications";
	  });
  
  $('#delete').click(function () {
    Application.remove(cmisObjectId, function () {
      window.location.href = cache.redirectUrl + "/home";
    });
  });
  
  //TASTO RINNOVO
  
//  if ( metadata["jconon_application:data_iscrizione_elenco"] != null  ){
//	  var data=new Date(metadata["jconon_application:data_iscrizione_elenco"]);
//		var dataCorrente=new Date();	
//		var dataLimite=new Date(data.setMonth(data.getMonth()+35));
//		if( dataCorrente.getMilliseconds() >= dataLimite.getMilliseconds() ){
  $('#rinnovo').click(function () {
	  
	  var errorMess, checkCodFisc ;
		 
      $.ajax({
    	  
          type: 'POST',
          url: cache.baseUrl + "/rest/application-fp/checkCodiceFiscaleInvio",
          data: {
	          nome: metadata["jconon_application:nome"],
	          cognome: metadata["jconon_application:cognome"],
	          dataNascita: $('#data_nascita').val(),
	          comuneNascita:  $('#comune_nascita').val(),
	          sesso: $('#sesso button.active').text(),
	          codiceFiscaleInserito: $('#codice_fiscale').val()
          },
          success: function(){

  
	  if(metadata["jconon_application:esclusione_rinuncia"] != null && metadata["jconon_application:esclusione_rinuncia"].includes("E")){
			var data=new Date(metadata["jconon_application:data_rimozione_elenco"]);
			var dataCorrente=new Date();
			
			var dataLimite=new Date(data.setMonth(data.getMonth()+6));
			
	  }
	  if( $('.Curriculum').length < 1 && $('.Documento').length < 1 ){
		 // errorMess = "Attenzione! Inserire il curriculum e il documento di identità, in corso di validità, nella sezione Allegati alla domanda";
		  errorMess = i18n.prop('message.warning.curriculum.documento.mancante');
		  
	  }else if( $('.Curriculum').length < 1 ){
		  errorMess = i18n.prop('message.warning.curriculum.mancante');
		 
	  }else if( $('.Documento').length < 1 ){
		  errorMess = i18n.prop('message.warning.documento.mancante');
		  
	  }else  if( $('.Curriculum').length > 1 && $('.Documento').length > 1 ){
		  errorMess = "Attenzione!! devi inserire  una sola copia per curriculum e documento";
		  
	  }else if( $('.Curriculum').length > 1 ){
		  errorMess = i18n.prop('message.warning.curriculum.doppio');
		  
	  }else if( $('.Documento').length > 1 ){
		  errorMess = i18n.prop('message.warning.documento.doppio');
		 
	  }	  
			
	
	  
if( $('.Curriculum').length == 1 && $('.Documento').length == 1 ){		
	
	

				if((metadata["jconon_application:esclusione_rinuncia"] == null ) || (metadata["jconon_application:esclusione_rinuncia"].includes("E") &&
						dataCorrente.getTime()>=dataLimite.getTime())){
					
						
				    var message = 'message.conferma.application.question.rinnovo',
				      placeholder = '';
				    if (metadata["jconon_call:elenco_sezioni_domanda"].indexOf('affix_tabProdottiScelti') !== -1 &&
				        $('#affix_tabProdottiScelti').find('table:visible').length === 0) {
				      placeholder = i18n.prop('message.conferma.application.prodotti.scelti');
				    }
				   UI.confirmRinnovo("conferma rinnovo", i18n.prop(message, placeholder), function () {
				    //	return false; // inserito per collaudo per bloccare funzione
				    	
				      if (bulkinfo.validate()) {
				        jconon.Data.application.main({
				          type: 'POST',
				          data: bulkinfo.getData(),
				          success: function (result) {
							
				           
				            var container = $('<section id="esperienze_rinnovo"><div class=""></div></section>'), showSchedeAnonimeRinnovo, showCorsiRinnovo,
				              input = $('<div class="input-append"></div>'),
				           //   btn = $('<span class="btn btn-file btn-primary"></span>'),
				         //     inputFile = $('<h2>Scarica domanda da firmare </h2><h5>La domanda puo\' essere firmata in modo digitale oppure olografico</h5><h5>Ricorda di controllare la validita\' del tuo documento</h5>'),
				              btnPrimary,
				              m,
				              newPrint = $('<button class="btn btn-success disabled printBtn" style="right: 380px; position: relative;"><i class="icon-file"></i> Scarica modulo aggiornamento</button>'),
							  nextButton = $('<button class="btn btn-success disabled" id="nextButton" data-dismiss="modal"><i class="icon-double-angle-right"></i> Avanti</button>');
						
				            input
				           //   .append('<div class="uneditable-input input-xlarge"><i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span></div>')
				              //.append(inputFile)
				              .appendTo(container);
				            crediti=0;
				            var div = container.find(':first-child');
				            div.addClass('well');
				          
				            showSchedeAnonimeRinnovo = createSchedeAnonimeRinnovo(div);
				            showSchedeAnonimeRinnovo();
				            div.find('table').css('margin-bottom','0px');
				            
				            if(crediti<40){
					              newPrint = $('<button class="btn btn-success disabled printBtn" style="right: 30%; position: relative;"><i class="icon-file"></i> Scarica modulo rinnovo</button>');

					            var insertButtonCrediti=$('<button class="btn btn-success" id="addCrediti"><i class="icon-plus-sign-alt"></i> Aggiungi corso</button>');
					            var table=$('<table class="table table-striped tableCorsi" >'+
					            				'<tbody></tbody>'+
					            			'</table>');
					              
					            var  tdText = $('<td></td>')
					  	      				.addClass('span5');
					  	      	var tR= $('<tr><th style="width: 20%; border: 1px solid #dddddd; text-align: center;">Nome ente</th><th style="width: 12%; border: 1px solid #dddddd; text-align: center;">Data corso</th>'+
					  	      			'<th style="width: 24%; border: 1px solid #dddddd; text-align: center;">Nome corso</th><th style="width: 24%; border: 1px solid #dddddd; text-align: center;">Nome modulo</th>'+
					  	      			'<th style="width: 10%; border: 1px solid #dddddd; text-align: center;">Numero crediti maturati</th><th style="width: 20%; border: 1px solid #dddddd; text-align: center;">Azioni</th></tr>');
					  	      		//	.append(tdText);
					  	      	table=table.append(tR);
					            div.after(' <p style="max-width:50%;" class="articoloDM" ><b>L’articolo 6 del '+i18n['label.link.dm']+' prevede che i dirigenti di ruolo in servizio nelle amministrazioni pubbliche siano esclusi dall’obbligo di maturare 40 crediti formativi.</b> </p> <p class="totCreditiEsperienze">Totale dei crediti da maturare   '+(40 - crediti).toFixed(1)+'</p>  <p style="position: relative; width: 40%; left: 690px;" class="totCrediti">Totale dei crediti maturati   '+crediti+'</p><p class="buttonAddCredit">Inserire il dettaglio dei corsi frequentati</p>');
					            var p= container.find('.buttonAddCredit');
					            insertButtonCrediti.insertAfter( p );
					            var t= container.find('#addCrediti');
					            table.insertAfter( t );
					            
				
					            showCorsiRinnovo = createCorsiRinnovo(table);
					            showCorsiRinnovo();
				
					            insertButtonCrediti.click(function () {
					            	  var type = "D:jconon_scheda_anonima:corso_rinnovo";
					            	  
				
					            	        Node.submission({
				
					            	          nodeRef: metadata["cmis:objectId"],
				
					            	          objectType: type,
				
					            	          crudStatus: "INSERT",
				
					            	          requiresFile: false,
				
					            	          showFile: false,	            	        
				
					            	          multiple: undefined,
				
					            	          bigmodal: true,
					            	         
					            	          modalTitle: "Inserisci corso",
				
					            	          input: undefined,
				
					            	          success: function (attachmentsData, data) {	   
					            	        	
				 	       
					            	        
					           	              crediti = crediti + parseFloat( data["jconon_attachment:numero_crediti_corso"].replace(',','.').replace(' ','') );
						           	           //    $('.totCreditiCorsiDiv').html('<p style="position: relative; width: 40%; left: 690px;" class="totCreditiCorsi">Totale crediti derivanti da corsi   '+crediti+'</p>');
						           	               $('.totCrediti').text('Totale dei crediti maturati   '+( parseFloat( crediti ) ));
						           	               $( '.in .alert' ).css( "display" , "none" );
						           	               if ( ( crediti  + parseFloat( creditiEsperienze.toFixed(1) ) ) >=40 ) {
						           		            	 $( '.printBtn' ).removeClass("disabled");	
						           		           // 	 $( '.in .printBtn' ).html('<i class="icon-file"></i> Scarica modulo aggiornamento');	
						           		            	 $( '.in .tableCorsi' ).css( "display" , "table" );	
						           		            }		  		            					     
						           	               
						           	            $.ajax({
				            		        		
					            	                  type: 'POST',
					            	                  url: cache.baseUrl + "/rest/application-fp/add-crediti-corsi",
					            	                  data: {
					            	                    nodeRef : metadata["cmis:objectId"],	            	                  
					            	                    creditiEsperienze:  crediti
					            	                
					            	                   
					            	                  },
					            	                  
					            	       //           complete: close,
					            	                  error: jconon.error
					            	                });  
						           	        
						                 		
						                 		
						           	         showCorsiRinnovo();
							                	
						                		
						                		
						                	
				
					            	          },
				
					            	          forbidArchives: true,
				
					            	          maxUploadSize: false
				
					            	        });
					            	        
					            });
					        
				            }
				            
				            
				            // set widget 'value'
				            function setValue(value) {
				              container.data('value', value);
				            }
				
				            setValue(null);
				       
				            
				            function sendFile() {
				              var fd = new CNR.FormData();
				              fd.data.append("objectId", cmisObjectId);
				              $.each(inputFile[0].files || [], function (i, file) {
				                fd.data.append('domandapdf', file);
				              });
				              alert(" send file rinnovo ---  "+fd.getData());
				              var close = UI.progress();
				              $.ajax({
				                  type: "POST",
				                  url: cache.baseUrl + "/rest/application-fp/rinnovo-application",
				                  data:  fd.getData(),
				                  enctype: fd.contentType,
				                  processData: false,
				                  contentType: false,
				                  dataType: "json",
				                  success: function(data){
				                    UI.success(i18n.prop('message.conferma.rinnovo.done', data.email_comunicazione), function () {
				                      window.location.href = cache.baseUrl + "/my-applications";
				                    });
				                  },
				                  complete: close,
				                  error: URL.errorFn
				              });
				            }
				
				            m = UI.modal('<i class="icon-upload animated flash"></i> Invio domanda di rinnovo', container, sendFile);
					//		m.find('.control-group').remove();
				            m.css('width','1170px');
				            m.css('margin-left','-585px');
				            
				            m.find('.control-group').removeAttr('style');
				          
				            m.find('.control-group').first().html('<p>Di seguito sono riportate le esperienze inserite nel profilo che consentono l’esenzione parziale dei  crediti. Prima di stampare e inviare il modulo firmato, è necessario inserire il dettaglio dei corsi frequentati per il raggiungimento dei crediti necessari al rinnovo dell’iscrizione.</p>');
							btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
							btnPrimary.after(nextButton);
				            btnPrimary.after(newPrint);
						
							m.on('shown', function () {
								m.find('.modal-footer').find(".btn-primary").css( "display", "none" );
								btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
								btnPrimary.css("right", "64%");
								btnPrimary.text("Annulla");
								btnPrimary.css("position", "relative");
								
							});
				            newPrint.click(function () {
				            	if(!newPrint.hasClass("disabled")){
									nextButton.removeClass("disabled");
									
						            window.location = 'rest/application/print-rinnovo-immediate?nodeRef=' + cmisObjectId;
				            	}else
				            		return false;
				            });
										
						 nextButton.click(function () {
							if(!nextButton.hasClass("disabled")){
								var title = $('.in #myModalLabel').text();
								
								  var container = $('<div class="fileupload fileupload-new" data-provides="fileupload"></div>'),
								  input = $('<div class="input-append"></div>'),
								  btn = $('<span class="btn btn-file btn-primary"></span>');
								 var htext ;
								 if( title.includes("aggiornamento") )
										htext = $('<h2>Carica il modulo di aggiornamento firmato</h2>');
								 else
										htext = $('<h2>Carica la domanda di rinnovo firmata </h2>');
								 var  inputFile = $('<input  type="file" name="domandapdf"/>'),
								//  previewPdf=$('<br><br><embed   id="pdfPreview" src="" width="350"  height="300">'),
								  previewPdf=$('<br><br><iframe   id="pdfPreview" src="" width="500"  height="400" style="display: none;">'),
								  removeButton=$('<a href="#" class="btn fileupload-exists disabled" data-dismiss="fileupload">Rimuovi</a>'),
								  btnPrimary,
								  m;		
					            btn
					              .append('<span class="fileupload-new"><i class="icon-upload"></i> Upload Domanda firmata</span>')
					              .append('<span class="fileupload-exists">Cambia</span>')
					              .append(inputFile);
						
									
					            input
								.append(htext)
					              .append('<div class="uneditable-input input-xlarge" ><i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span></div>')
					              .append(btn)
								  .append(removeButton)
								  .append(previewPdf)
					              .appendTo(container);
					        	
							     m = UI.modal('<i class="icon-upload animated flash"></i> Invio domanda di Rinnovo', container, sendFile);
								 
								 m.on('shown', function () {
									//m.find('.modal-footer').find(".btn-primary").css( "display", "none" );
									 m.find('#myModalLabel').text(title);
									m.find( ".submit" ).text( "Invia" );
									m.find( ".submit" ).addClass("disabled");
								//	$(".submit").prepend('<i class="icon-send"></i>')
									m.find( ".submit" ).css("position", "relative");
									m.find( ".submit" ).css("right", "-80px");
									btnPrimary = m.find(".modal-footer").find(".btn-primary").next();
									btnPrimary.css("right", "650px");
									btnPrimary.text("Annulla");
									btnPrimary.css("position", "relative");
									m.css("left","43%");
									m.find(".modal-body").css("max-height","300px");
									m.css("width","770px");
								});
								 
							// set widget 'value'
					            function setValue(value) {
					              container.data('value', value);
								 
					            }
				
					            setValue(null);
								removeButton.removeClass("disabled");
								removeButton.click(function () {
									removeButton.addClass("disabled");
									m.find("#pdfPreview").attr("src","");
									m.find( ".submit" ).addClass("disabled");
									m.find(".input-xlarge").css("width","");
									m.find("#pdfPreview").css("display","none");
								});
							
					            inputFile.on('change', function (e) {
					              var path = $(e.target).val();
					              setValue(path);
								 
								  if(event.target.files!= undefined){
									var src= window.URL.createObjectURL(event.target.files[0]);
									m.find("#pdfPreview").attr("src",src);
									m.find("#pdfPreview").css("display","initial");
								  }else{
									  setTimeout(function(){ 
										  m.find( ".submit" ).addClass("disabled");
										  
									  }, 500);
									  
								  }
								m.find(".modal-body").css("width","740px");
								m.find( ".submit" ).removeClass("disabled");
					            });
				
					            function sendFile() {
					            	if(!m.find( ".submit" ).hasClass("disabled")){
							              var fd = new CNR.FormData();
							              
							              fd.data.append("objectId", cmisObjectId);
							              $.each(inputFile[0].files || [], function (i, file) {
							                fd.data.append('domandapdf', file);
							              });
							              var close = UI.progress();
							              $.ajax({
							                  type: "POST",
							                  url: cache.baseUrl + "/rest/application-fp/rinnovo-application",
							                  data:  fd.getData(),
							                  enctype: fd.contentType,
							                  processData: false,
							                  contentType: false,
							                  dataType: "json",
							                  success: function(data){
							                    UI.success(i18n.prop('message.conferma.rinnovo.done', data.email_comunicazione), function () {
							                      window.location.href = cache.baseUrl + "/my-applications";
							                    });
							                  },
							                  complete: close,
							                  error: URL.errorFn
							              });
					            	}else{
										return false;	
									}
				            }	 
							}else{
								return false;
							}	 
				            });
							
				            saved = true;
							
				          },
				          error: URL.errorFn
				        });
				      } else {
				        var msg = content
				          .children('form')
				          .validate()
				          .errorList
				          .map(function (item) {
				            if ($(item.element).hasClass('widget')) {
				              return $(item.element).find('label').text();              
				            } else {
				              return $(item.element).parents('.control-group').find('label').text();
				            }
				          })
				          .filter(function (x) {
				            return x.trim().length > 0;
				          })
				          .map(function (x) {
				            return x.length > 50 ? x.substr(0, 50) + "\u2026" : x;
				          })
				          .join('<br>');
				        UI.alert(i18n['message.improve.required.fields'] + '<br><br>' + msg)
				      }
				    });
				}else{
					UI.alert(i18n.prop('message.warning.cancellazione')+dataLimite.toLocaleDateString("it-IT"));	
				
				}
		}else{
		  	UI.alert(errorMess);	

		  }
      },
   	  error: function(data){	    
    	    UI.alert( i18n.prop('message.warning.codice.fiscale.non.conforme') );
	  },
      complete: close
      
      });
  });
 
//  } else
//	  $( "#rinnovo" ).css( "display" , "none");
//  }
  function main() {
	
    var xhr = Call.loadLabels(callId);
    xhr.done(function () {
      URL.Data.node.node({
        data: {
          excludePath : true,
          nodeRef : callId,
          cachable: !preview
        },
        callbackErrorFn: jconon.callbackErrorFn,
        success: function (dataCall) {
          applicationAttachments = Application.completeList(
            dataCall['jconon_call:elenco_association'],
            cache.jsonlistApplicationAttachments
          );
          curriculumAttachments = Application.completeList(
            dataCall['jconon_call:elenco_sezioni_curriculum'],
            cache.jsonlistApplicationCurriculums
          );
          prodottiAttachments = Application.completeList(
            dataCall['jconon_call:elenco_prodotti'],
            cache.jsonlistApplicationProdotti
          );
          schedeAnonimeAttachments = Application.completeList(
            dataCall['jconon_call:elenco_schede_anonime'],
            cache.jsonlistApplicationSchedeAnonime
          );
          
          jconon.Data.application.main({
            type: 'GET',
            queue: true,
            placeholder: {
              callId: callId,
              applicationId: params.applicationId,
              userId: common.User.id,
              preview: preview
              
            },
            error: function (jqXHR, textStatus, errorThrown) {
              var jsonMessage = JSON.parse(jqXHR.responseText);
              if (jsonMessage && jsonMessage.message === 'message.error.domanda.inviata.accesso') {
             //   UI.alert(i18n['message.application.alredy.send'], undefined, function () {
            //	  if( urlParams == undefined )
            		  window.location.href = '/my-applications';
           //     });
              } else {
                URL.errorFn(jqXHR, textStatus, errorThrown, this);
              }
            },
            callbackErrorFn: jconon.callbackErrorFn
          }).done(function (dataApplication) {
            var message = $('#surferror').text() || 'Errore durante il recupero della domanda';
            if (!common.User.admin && common.User.id !== dataApplication['jconon_application:user']) {
              UI.error(i18n['message.error.caller.user'], function () {
                window.location.href = cache.redirectUrl;
              });
            } else {
              URL.Data.proxy.people({
                type: 'GET',
                contentType: 'application/json',
                placeholder: {
                  user_id: dataApplication['jconon_application:user']
                },
                success: function (data) {
                  dataPeopleUser = data;
                  manageIntestazione(dataCall, dataApplication);
                  render(dataCall, dataApplication);
             //     console.log("success load main application");
                  var urlParams = new URLSearchParams(window.location.search);
              //    console.log("success load main application param "+urlParams);
                  
             
                  
                },
                error: function () {
                  UI.error(i18n['message.user.not.found']);
                  window.location.href = cache.redirectUrl;
                }
              });
              
            if ( urlParams != undefined ){
        	  var paramValueIscrizione = urlParams.get('iscrizione');
              var paramValueCambioFascia = urlParams.get('cambio_fascia');
              var paramValueRinnovo = urlParams.get('rinnovo');
	          
	          
		  	    if( paramValueIscrizione!=null && paramValueIscrizione.includes("true") ){
				          var close = UI.progress();
				            setTimeout(function(){ 
				    //	  $('#send').click();
				    	  $('#send').trigger('click');
				    	  close();
				        }, 5000);
		  	    }else  if( paramValueCambioFascia!=null && paramValueCambioFascia.includes("true") ){
			          var close = UI.progress();
			            setTimeout(function(){ 
			    //	  $('#cambio_fascia').click();
			    	  $('#cambio_fascia').trigger('click');
			    	  close();
			        }, 5000);
		  	    }else  if( paramValueRinnovo!=null && paramValueRinnovo.includes("true") ){
			          var close = UI.progress();
			            setTimeout(function(){ 
			    	//  $('#rinnovo').click();
			    	  $('#rinnovo').trigger('click');
			    	  close();
			        }, 5000);
		  	    }
          }
              
            }
            toolbar.show();
          });
       
        }
      });
    });
  }
//  
//  function rinnovo() {
//		 console.log("rinnovo aplication --------------------" );
//	    var xhr = Call.loadLabels(callId);
//	    xhr.done(function () {
//	      URL.Data.node.node({
//	        data: {
//	          excludePath : true,
//	          nodeRef : callId,
//	          cachable: !preview
//	        },
//	        callbackErrorFn: jconon.callbackErrorFn,
//	        success: function (dataCall) {
//	        	
//	        	
//	          applicationAttachments = Application.completeList(
//	            dataCall['jconon_call:elenco_association'],
//	            cache.jsonlistApplicationAttachments
//	          );
//	          curriculumAttachments = Application.completeList(
//	            dataCall['jconon_call:elenco_sezioni_curriculum'],
//	            cache.jsonlistApplicationCurriculums
//	          );
//	          prodottiAttachments = Application.completeList(
//	            dataCall['jconon_call:elenco_prodotti'],
//	            cache.jsonlistApplicationProdotti
//	          );
//	          schedeAnonimeAttachments = Application.completeList(
//	            dataCall['jconon_call:elenco_schede_anonime'],
//	            cache.jsonlistApplicationSchedeAnonime
//	          );
//	          schedeAnonimeRinnovoAttachments = Application.completeList(
//	              dataCall['jconon_call:elenco_schede_anonime_rinnovo'],
//	              cache.jsonlistApplicationSchedeAnonime
//	          );
//	          jconon.Data.application.rinnovo({
//	            type: 'GET',
//	            queue: true,
//	            placeholder: {
//	              callId: callId,
//	              applicationId: params.applicationId,
//	              userId: common.User.id,
//	              preview: preview,
//	              rinnovo:"pippo"
//	            },
//	            error: function (jqXHR, textStatus, errorThrown) {
//	              var jsonMessage = JSON.parse(jqXHR.responseText);
//	              if (jsonMessage && jsonMessage.message === 'message.error.domanda.inviata.accesso') {
//	                UI.alert(i18n['message.application.alredy.send'], undefined, function () {
//	                  window.location.href = '/my-applications';
//	                });
//	              } else {
//	                URL.errorFn(jqXHR, textStatus, errorThrown, this);
//	              }
//	            },
//	            callbackErrorFn: jconon.callbackErrorFn
//	          }).done(function (dataApplication) {
//	            var message = $('#surferror').text() || 'Errore durante il recupero della domanda';
//	            if (!common.User.admin && common.User.id !== dataApplication['jconon_application:user']) {
//	              UI.error(i18n['message.error.caller.user'], function () {
//	                window.location.href = cache.redirectUrl;
//	              });
//	            } else {
//	              URL.Data.proxy.people({
//	                type: 'GET',
//	                contentType: 'application/json',
//	                placeholder: {
//	                  user_id: dataApplication['jconon_application:user']
//	                },
//	                success: function (data) {		// RECUPERO UTENTE 
//	                	
//	                  dataPeopleUser = data;
//	                  manageIntestazione(dataCall, dataApplication);
//	                  render(dataCall, dataApplication);
//	                },
//	                error: function () {
//	                  UI.error(i18n['message.user.not.found']);
//	                  window.location.href = cache.redirectUrl;
//	                }
//	              });
//	            }
//	            toolbar.show();
//	          });
//	        }
//	      });
//	    });
//	  }
  
  $('button', toolbar).tooltip({
    placement: 'bottom',
    container: toolbar
  });
  if (callCodice) {
    URL.Data.search.query({
      data: {
        q: "select cmis:objectId " +
          "from jconon_call:folder where jconon_call:codice = '" + callCodice + "'"
      },
      success: function (data) {
        callId = data.items[0]['cmis:objectId'];
       
        main();
      }
    });

  } else {
	
	  var urlParams = new URLSearchParams(window.location.search);
	   
//	    var paramValue = urlParams.get('rinnovo');
//	    if( paramValue!=null && paramValue.includes("true") )
//	 
//  //  main();
//	    	rinnovo();
//	    else{
	    	main();
	  //  	console.log("dopo main");
	//    }
  }
});