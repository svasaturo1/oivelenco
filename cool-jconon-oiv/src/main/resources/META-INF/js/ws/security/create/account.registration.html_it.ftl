<html>
<body>
<div style="font-size: 11pt">Credenziali di accesso per <b>${account.fullName}</b></div>
<hr/>
<p>Nome utente: <b>${account.userName}</b></p>
<hr/>
<p>${message('mail.account.registration.3')}</p>
<p><a href="${url}/rest/security/confirm-account?userid=${account.userName}&pin=${account.pin}">Attivazione utenza</a></p>
<p>Se il link non dovesse funzionare, copi il testo sottostante e lo incolli nella barra degli indirizzi del suo browser:</p>
<p>${url}/rest/security/confirm-account?userid=${account.userName}&pin=${account.pin}</p>
<br/>
<p>${message('mail.account.registration.5')}</p>
<p><a href="${url}/login">${url}/login</a></p>
<p>${message('mail.confirm.application.5')}</p>
<p>${message('mail.rinnovo.application.10')}</p>

<p>Per richieste di informazioni o assistenza, inviare una segnalazione utilizzando la sezione <a href="${url}/helpdesk">helpdesk</a> del sito.</p>
<hr/>
<p>${message('message.auto')}</p>
<p>Si prega di non rispondere.</p>
</body>
</html>
