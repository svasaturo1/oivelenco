/* javascript closure providing all the search functionalities */
define(['jquery', 'cnr/cnr', 'i18n', 'json!common', 'cnr/cnr.actionbutton', 'cnr/cnr.ui', 'cnr/cnr.jconon', 'cnr/cnr.url',
  'cnr/cnr.search', 'cnr/cnr.criteria', 'cnr/cnr.node', 'cnr/cnr.bulkinfo', 'json!cache', 'cnr/cnr.application', 'cnr/cnr.call', 'cnr/cnr.ui.widgets', 'fp/fp.ui.amministrazioni-ipa', 'fp/fp.ui.universita-ipa', 'fp/fp.ui.tipologie-uni'
  ], function ($, CNR, i18n, common, ActionButton, UI, jconon, URL, Search, Criteria, Node, BulkInfo, cache, Application, Call, Widgets, AmministrazioniIpa, UniversitaIpa, TipologieUni) {
  "use strict";
  var urls = {
    call : {
      publish_esito: 'rest/call-fp/publish-esito'
    },
    ipaAmministrazioni: 'rest/ipa/amministrazioni'
  },
    defaults = {},
    settings = defaults;
  Widgets['ui.amministrazioni-ipa'] = AmministrazioniIpa;
 // console.log("widget uni "+JSON.stringify(UniversitaIpa));
 // console.log("widget amm "+JSON.stringify(AmministrazioniIpa));
  Widgets['ui.universita-ipa'] = UniversitaIpa;
  Widgets['ui.tipologie-uni'] = TipologieUni;
  function init(options) {
    settings = $.extend({}, defaults, options);
  }

  function displayEsperienzeOIV(el, refreshFn) {
	  
	var  dataUltimoInvioDomanda,
		 dataInvioIscrizione =  el.parentProp ? el.parentProp['jconon_application:data_domanda'] : null,
		 dataInvioRinnovo =  el.parentProp ? el.parentProp['jconon_application:data_invio_rinnovo_elenco'] : null,
		 dataInvioCambioFascia =  el.parentProp ? el.parentProp['jconon_application:data_invio_cambio_fascia'] : null;
		 if( new Date( dataInvioIscrizione ) > new Date( dataInvioRinnovo ) ){
			 if( new Date( dataInvioIscrizione ) > new Date( dataInvioCambioFascia ) )
				 dataUltimoInvioDomanda = dataInvioIscrizione ;
			 else
				 dataUltimoInvioDomanda = dataInvioCambioFascia ;
		 }else{
			 if( new Date( dataInvioRinnovo ) > new Date( dataInvioCambioFascia ) )
				 dataUltimoInvioDomanda = dataInvioRinnovo ;
			 else
				 dataUltimoInvioDomanda = dataInvioCambioFascia ;
		 }
    var tdText,
    

    
    
      tdButton,
      isNonCoerente = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente') !== -1,
      isNonCoerenteRinnovo = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo') !== -1,
      isRdP = el.parentProp ? (Call.isRdP(el.parentProp.relationships.parent[0]['jconon_call:rdp']) || common.User.admin) : false,
	//  isRdP2 = el.parentProp ? (Call.isRdP(el.parentProp['jconon_call:rdp']) || common.User.admin) : false,
    		  isVisibile =  el['jconon_attachment:esperienza_non_coerente_visibile'],
    		  isVisibileRinnovo =  el['jconon_attachment:esperienza_non_coerente_rinnovo_visibile'],
      isAdmin =  common.User.admin,
      isEditable = ( new Date( dataUltimoInvioDomanda ) < new Date( el["cmis:creationDate"] ) ) ? true : false,
      isInCorso =  el['jconon_attachment:esperienza_professionale_incorso'] || 
      	el['jconon_attachment:precedente_incarico_oiv_incorso'],
      callId = el.parentProp ? el.parentProp.relationships.parent[0]['cmis:objectId'] : undefined,
//	  callId2 = el.parentProp ? el.parentProp['cmis:objectId'] : undefined,
			//  isRdP = isRdP || isRdP2,
			//  callId = callId || callId2,
      applicationId = el.parentProp ? el.parentProp['cmis:objectId'] : undefined,
      userName = el.parentProp ? el.parentProp['jconon_application:user'] : undefined,
      title = el['jconon_attachment:esperienza_professionale_datore_lavoro'] ||
        el['jconon_attachment:esperienza_professionale_amministrazione'] ||
        el['jconon_attachment:aspect_specializzazioni_universita'] ||
        el['jconon_attachment:precedente_incarico_oiv_amministrazione'],
      ruolo = el['jconon_attachment:esperienza_professionale_ruolo'] ||
        el['jconon_attachment:precedente_incarico_oiv_ruolo'],
      esperienza = el['jconon_attachment:esperienza_professionale_da'] ||
        el['jconon_attachment:esperienza_professionale_a'],
      esperienzaOIV = el['jconon_attachment:precedente_incarico_oiv_da'] ||
        el['jconon_attachment:precedente_incarico_oiv_a'],
      item = $('<a href="#">' + title + '</a>').on('click', function () {
        Node.displayMetadata(el.objectTypeId, el.id, true);
        return false;
      }),
	
      annotazioneIsPresente = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_annotazioni') !== -1,
      
      periodoEsperienzaDa = el['jconon_attachment:esperienza_professionale_da'] ? ('dal ' + CNR.Date.format(el['jconon_attachment:esperienza_professionale_da'], null, 'DD/MM/YYYY')) : '',
      periodoEsperienzaA = el['jconon_attachment:esperienza_professionale_a'] ? (' al ' + CNR.Date.format(el['jconon_attachment:esperienza_professionale_a'], null, 'DD/MM/YYYY')) : '',
      annotationPeriodoEsperienza = $('<span class="muted annotation"><strong>Periodo di attività: </strong>' + periodoEsperienzaDa + periodoEsperienzaA + '</span>'),
      periodoOIVDa = el['jconon_attachment:precedente_incarico_oiv_da'] ? ('dal ' + CNR.Date.format(el['jconon_attachment:precedente_incarico_oiv_da'], null, 'DD/MM/YYYY')) : '',
      periodoOIVA = el['jconon_attachment:precedente_incarico_oiv_a'] ? (' al ' + CNR.Date.format(el['jconon_attachment:precedente_incarico_oiv_a'], null, 'DD/MM/YYYY')) : '',
      annotationPeriodoOIV = $('<span class="muted annotation"><strong>Periodo di attività: </strong>' + periodoOIVDa + periodoOIVA + '</span>'),
      annotationRuolo = $('<span class="muted annotation"><strong>Ruolo:</strong> ' + ruolo  + '</span>'),
      modalTitle = title + ' Ruolo: ' + ruolo,
      tdNonCoerente = $('<td>').addClass('span5'),
      motivazione = el['jconon_attachment:esperienza_non_coerente_motivazione'],
      motivazioneRinnovo = el['jconon_attachment:esperienza_non_coerente_rinnovo_motivazione'],
      annotazione = el['jconon_attachment:esperienza_annotazione_motivazione'],
      annotationAnnotazione = $('<span class="muted annotation text-warning"><strong>Annotazione:</strong> ' + annotazione  + '</span>'),
      spanNonCoerenteRinnovo =  isVisibileRinnovo ?  $('<span>').addClass('text-error animated flash').appendTo(tdNonCoerente) : $('<span>').addClass('animated flash').appendTo(tdNonCoerente),
      spanNonCoerente = isVisibile ? $('<span>').addClass('text-error animated flash').appendTo(tdNonCoerente) : $('<span>').addClass('animated flash').appendTo(tdNonCoerente);
    if (esperienza) {
      item.after(annotationPeriodoEsperienza);      
    }
    if (esperienzaOIV) {
      item.after(annotationPeriodoOIV);      
    }
    if (ruolo) {
      item.after(annotationRuolo);
    }
    if (annotazioneIsPresente && isRdP) {
      item.after(annotationAnnotazione);
    }
    if (isNonCoerente && ( isRdP || isVisibile ) ) {
    	  var dateFormat = "DD/MM/YYYY";
      if (motivazione !== '') {
    	  if ( isVisibile ) {
		        $('<a href="#">').addClass('text-error').append(i18n.prop('label.esperienza.non.coerente', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_data'], '-', dateFormat) ) ).off('click').on('click', function () {
		     //     UI.alert(motivazione);
		        	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazione, undefined, undefined, false);
		          return false;
		        }).appendTo(spanNonCoerente);
    	  }else{
    		  $('<a href="#">').addClass('text-non-visibile').append(i18n.prop('label.esperienza.non.coerente', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_data'], '-', dateFormat) ) ).off('click').on('click', function () {
    			     //     UI.alert(motivazione);
    			        	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazione, undefined, undefined, false);
    			          return false;
    		  }).appendTo(spanNonCoerente);
    	  }
      } else {
        spanNonCoerente.append( i18n.prop('label.esperienza.non.coerente', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_data'], '-', dateFormat) ) );
      }
    }
    
    if (isNonCoerenteRinnovo && ( isRdP || isVisibileRinnovo ) ) {
    	 var dateFormat = "DD/MM/YYYY";
        if (motivazioneRinnovo !== '') {
        	 if ( isVisibileRinnovo ) {
		          $('<a href="#">').addClass('text-error').append(i18n.prop('label.esperienza.non.coerente.rinnovo', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_rinnovo_data'], '-', dateFormat) ) ).off('click').on('click', function () {
		       //     UI.alert(motivazione);
		          	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazioneRinnovo, undefined, undefined, false);
		            return false;
		          }).appendTo(spanNonCoerenteRinnovo);
        	 }else{
        		 $('<a href="#">').addClass('text-non-visibile').append(i18n.prop('label.esperienza.non.coerente.rinnovo', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_rinnovo_data'], '-', dateFormat) ) ).off('click').on('click', function () {
      		       //     UI.alert(motivazione);
      		          	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazioneRinnovo, undefined, undefined, false);
      		            return false;
  		          }).appendTo(spanNonCoerenteRinnovo);
        	 }
        } else {
          spanNonCoerenteRinnovo.append(i18n.prop('label.esperienza.non.coerente.rinnovo', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_rinnovo_data'], '-', dateFormat) ) );
        }
      }
    
    if(el.objectTypeId.includes("esperienza_professionale")){
    	if ( el["jconon_attachment:incarico_articolo_19"] == null ) {
	    	if(el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello generale non di ruolo nella PA</strong></span>');
	
	    	}else if(el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello generale di ruolo nella PA</strong></span>');
	
	    	}else if(el["jconon_attachment:esperienza_dirigenziale"] && !el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello non generale non di ruolo nella PA</strong></span>');
	
	    	}else if(el["jconon_attachment:esperienza_dirigenziale"] && !el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello non generale di ruolo nella PA</strong></span>');
	
	    	}else{
	    	
	    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
	    	}
    	}else if ( el["jconon_attachment:incarico_articolo_19"] ) {
    		
    		if(el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo di prima fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
	
	    	}else if(el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo di prima fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
	
	    	}else if( !el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo di seconda fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
	
	    	}else if( !el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo di seconda fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
	
	    	}else{
	    	
	    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
	    	}
    		
    	}else if ( !el["jconon_attachment:incarico_articolo_19"] ) {
    		
    		if( !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo (incarico non ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
	
	    	}else if( el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
	        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo (incarico non ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
	
	    	}else{
	    	
	    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
	    	}
    		
    	}
    	
	}else{
		var  annotationObjectType = $('<span class="annotation"><strong>' + i18n[el.objectTypeId] + '</strong></span>');
    }
    
    
    
    
//    if(el.objectTypeId.includes("esperienza_professionale")){
//    	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale/Esperienza dirigenziale nella PA</strong></span>');
//    }else{
//    	var  annotationObjectType = $('<span class="annotation"><strong>' + i18n[el.objectTypeId] + '</strong></span>');
//    }
    tdText = $('<td></td>')
      .addClass('span5')
      .append(annotationObjectType)
      .append(item);
    tdButton = $('<td></td>').addClass('span2').append(ActionButton.actionButton({
      name: el.name,
      nodeRef: el.id,
      baseTypeId: el.baseTypeId,
      objectTypeId: el.objectTypeId,
      mimeType: el.contentType,
      allowableActions: el.allowableActions,
      defaultChoice: 'select'
    }, {copy_curriculum: 'CAN_UPDATE_PROPERTIES'}, {
      permissions : false,
      history : false,
      copy: false,
      cut: false,
      update: false,
      remove: ( isRdP || isAdmin || isEditable ) ? function () {
        UI.confirm('Sei sicuro di voler eliminare l\'esperienza/incarico "' +  title  + '"?', function () {
          Node.remove(el.id, refreshFn);
        });
      } : false,
      edit: ( isRdP || isAdmin || isInCorso || isEditable ) ? function () {
        Application.editProdottiEsperienze(el, title, refreshFn, ( isAdmin || isEditable ) );
      } : false,
//      edit: function () {
//          Application.editProdottiEsperienze(el, title, refreshFn, isAdmin);
//        },
      copy_curriculum: ( isRdP || isAdmin )  ? function () {
        Application.editProdotti(el, title, refreshFn, true);
      } : false,
      annotazioni: isRdP ? function () {
       var content = $("<div></div>"),
          bulkinfo = new BulkInfo({
            target: content,
            path: "P:jconon_scheda_anonima:esperienza_annotazioni",
            objectId: el.id,
            formclass: 'form-inline',
            name: 'default'
          });
        bulkinfo.render();
        UI.modal('<i class="icon-edit"></i> ' + modalTitle, content, function () {
          var close = UI.progress(), d = bulkinfo.getData();
          d.push(
            {
              id: 'cmis:objectId',
              name: 'cmis:objectId',
              value: el['cmis:objectId']
            },
            {
              name: 'aspect', 
              value: 'P:jconon_scheda_anonima:esperienza_annotazioni'
            },
            {
              name: 'callId', 
              value: callId
            },            
            {
              name: 'applicationId', 
              value: applicationId
            }                        
          );
          $.ajax({
            url: cache.baseUrl + "/rest/application-fp/esperienza-annotazioni",
            type: 'POST',
            data:  d,
            success: function (data) {
              UI.success(i18n['message.esperienza.annotazioni.eseguito'], refreshFn);
            },
            complete: close,
            error: URL.errorFn
          });
        });
      } : false,
      coerente: isRdP && isNonCoerente ? function () {
          var d = [
            {
              id: 'cmis:objectId',
              name: 'cmis:objectId',
              value: el['cmis:objectId']
            },
            {
              name: 'aspect', 
              value: 'P:jconon_scheda_anonima:esperienza_non_coerente'
            },
            {
              name: 'userName', 
              value: userName
            },            
            {
              name: 'callId', 
              value: callId
            },            
            {
                name: 'applicationId', 
                value: applicationId
              }       
          ];
          $.ajax({
            url: cache.baseUrl + "/rest/application-fp/esperienza-coerente",
            type: 'POST',
            data:  d,
            success: function (data) {
              UI.success(i18n['message.esperienza.coerente.eseguito'], refreshFn);
            },
            complete: close,
            error: URL.errorFn
          });
      } : false,
      noncoerente: isRdP && !isNonCoerente ? function () {
         var content = $("<div></div>"),
            bulkinfo = new BulkInfo({
              target: content,
              path: "P:jconon_scheda_anonima:esperienza_non_coerente",
              objectId: el.id,
              formclass: 'form-inline',
              name: 'default'
            });
          bulkinfo.render();
          UI.modal('<i class="icon-edit"></i> ' + modalTitle, content, function () {
            var close = UI.progress(), d = bulkinfo.getData();
            d.push(
              {
                id: 'cmis:objectId',
                name: 'cmis:objectId',
                value: el['cmis:objectId']
              },
              {
                name: 'aspect', 
                value: 'P:jconon_scheda_anonima:esperienza_non_coerente'
              },
              {
                name: 'callId', 
                value: callId
              },            
              {
                  name: 'applicationId', 
                  value: applicationId
                }                          
            );
            $.ajax({
              url: cache.baseUrl + "/rest/application-fp/esperienza-noncoerente",
              type: 'POST',
              data:  d,
              success: function (data) {
                UI.success(i18n['message.esperienza.noncoerente.eseguito'], refreshFn);
              },
              complete: close,
              error: URL.errorFn
            });
          });
      } : false,
      coerenteRinnovo: isRdP && isNonCoerenteRinnovo ? function () {
          var d = [
            {
              id: 'cmis:objectId',
              name: 'cmis:objectId',
              value: el['cmis:objectId']
            },
            {
              name: 'aspect', 
              value: 'P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo'
            },
            {
              name: 'userName', 
              value: userName
            },            
            {
              name: 'callId', 
              value: callId
            },            
            {
                name: 'applicationId', 
                value: applicationId
              }       
          ];
          $.ajax({
            url: cache.baseUrl + "/rest/application-fp/esperienza-coerente",
            type: 'POST',
            data:  d,
            success: function (data) {
              UI.success(i18n['message.esperienza.coerente.eseguito.rinnovo'], refreshFn);
            },
            complete: close,
            error: URL.errorFn
          });
      } : false,
      noncoerenteRinnovo: isRdP && !isNonCoerenteRinnovo ? function () {
         var content = $("<div></div>"),
            bulkinfo = new BulkInfo({
              target: content,
              path: "P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo",
              objectId: el.id,
              formclass: 'form-inline',
              name: 'default'
            });
          bulkinfo.render();
          UI.modal('<i class="icon-edit"></i> ' + modalTitle, content, function () {
            var close = UI.progress(), d = bulkinfo.getData();
            d.push(
              {
                id: 'cmis:objectId',
                name: 'cmis:objectId',
                value: el['cmis:objectId']
              },
              {
                name: 'aspect', 
                value: 'P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo'
              },
              {
                name: 'callId', 
                value: callId
              },            
              {
                  name: 'applicationId', 
                  value: applicationId
                }                          
            );
            $.ajax({
              url: cache.baseUrl + "/rest/application-fp/esperienza-noncoerente-rinnovo",
              type: 'POST',
              data:  d,
              success: function (data) {
                UI.success(i18n['message.esperienza.noncoerente.eseguito'], refreshFn);
              },
              complete: close,
              error: URL.errorFn
            });
          });
      } : false,
      paste: Application.getTypeForDropDown('jconon_call:elenco_sezioni_curriculum', el, title, refreshFn),
      move: Application.getTypeForDropDown('jconon_call:elenco_sezioni_curriculum', el, title, refreshFn, true)
    }, {copy_curriculum: 'icon-copy', paste: 'icon-paste', move: 'icon-move', noncoerente: 'icon-minus', coerente: 'icon-plus',noncoerenteRinnovo: 'icon-minus', coerenteRinnovo: 'icon-plus', annotazioni: 'icon-pencil'}, refreshFn, true));
    return $('<tr class="esperienze '+el['cmis:objectId'].split(";")[0]+'" ></tr>')
		
      .append(tdText)
      .append(tdNonCoerente)
      .append(tdButton);
  }

  function displayNote(el, refreshFn, permission, showLastModificationDate, showTitleAndDescription, extendButton, customIcons, maxUploadSize) {
	  var titolo="",
	  isRdP = el.parentProp ? (Call.isRdP(el.parentProp.relationships.parent[0]['jconon_call:rdp']) || common.User.admin) : false,
			  isAdmin =  common.User.admin;
	if($(".labelSchedaAnonima").length < 1){
	//	$("<label class='labelSchedaAnonima' >Attenzione !!! <br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>").insertBefore($("#affix_tabSchedaAnonima"));
		$("#affix_tabSchedaAnonima").before("<label id='labelSchedaAnonima' style='margin-top: 60px;margin-bottom: 0px; font-size: larger;text-align: center; cursor: default;' class='well labelSchedaAnonima' ><b>Attenzione !!! </b><br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>");
		
	
	}
console.log("jconon");
//	$("#affix_tabSchedaAnonima").before($("#affix_tabUlterioriDati"));
//	$("#affix_tabSchedaAnonima").before($("#affix_tabReperibilita"));
	
//	$("a[href='#affix_tabSchedaAnonima']").before($("a[href='#affix_tabUlterioriDati']"));
//	$("a[href='#affix_tabSchedaAnonima']").before($("a[href='#affix_tabReperibilita']"));
	
	if($(".labelSchedaAnonima").length > 0){
	//	$("<label class='labelSchedaAnonima' >Attenzione !!! <br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>").insertBefore($("#affix_tabSchedaAnonima"));
		$("#affix_tabSchedaAnonima").before($("#labelSchedaAnonima"));

	}
	
	

	
	  if(el.contentType==null){
		titolo=el["jconon_application:oggetto_notifica_email"];  
		
		  
	  }else{
		titolo=el.name;
		  
	  }
	//  console.log("dispaly "+contesto);
	 //  console.log("dispaly "+contesto.localeCompare('actions.visualizza_comunicazioni'));
//	if(showLastModificationDate && i18n[el.objectTypeId].localeCompare('Comunicazioni al candidato')==0){		&& (i18n[el.objectTypeId].localeCompare('Comunicazioni al candidato')==0 || i18n[el.objectTypeId].includes('esclusione'))
if( i18n[el.objectTypeId].includes("Note")){
	  
		var tdText,
		  tdButton,
		  customButtons,
		  isFolder = el.baseTypeId === 'cmis:folder',
		  item = $('<a href="#">' + titolo + '</a>');
		  if( isAdmin || common.User.id.includes(el["jconon_attachment:user"]) ){
			   customButtons = $.extend({}, {
				history : false,
				copy: false,
				cut: false
//				update: isAdmin ? true : false,
//				remove: isAdmin ? true : false
			  }, extendButton);
			}else{
				   customButtons = $.extend({}, {
					history : false,
					copy: false,
					cut: false,
					update:  false,
					remove: false
				  }, extendButton);
				
			}
	var	  annotationType ,
		
		  annotation = $('<span class="muted annotation">ultima modifica: ' + CNR.Date.format(el.lastModificationDate, null, 'DD/MM/YYYY H:mm') + '</span>');
		if(el.contentType!=null)
			annotationType	 = $('<span class="muted annotation  ">' + el["jconon_application:oggetto_notifica_email"]  + '</span>');
		else 
			annotationType	 = $('<span class="muted annotation  ">' + i18n[el.objectTypeId]  + '</span>');
		
		
//		if (permission !== undefined) {
//		  customButtons.permissions = permission;
//		}
		if(el.contentType!=null){
			item = $('<a href="#">' + titolo + '</a>');
			item.attr('href', URL.urls.search.content + '?nodeRef=' + el.id + '&guest=true');
			  
		}else{
			item = $('<span href="#">' + titolo + '</span>');
		}
		
		item.after(annotationType);
		
		if (showLastModificationDate === false && el.contentType!=null ) {
		  item.after('<span class="muted annotation">' + CNR.fileSize(el.contentStreamLength) + '</span>');
		} else if(el.contentType!=null){
		  item.after(annotation.prepend(', ').prepend(CNR.fileSize(el.contentStreamLength)));
		}else{
			item.after(annotation);
		}
//		if ( isRdP || isAdmin ){
//			 customButtons.edit = true;
//		}

		tdText = $('<td ></td>')
		  .addClass('span10')
		  .append(CNR.mimeTypeIcon(el.contentType, el.name))
		  .append(' ')
		  .append(item);
		tdButton = $('<td ></td>').addClass('span2').append(ActionButton.actionButton({
		  name: el.name,
		  nodeRef: el['alfcmis:nodeRef'],
		  baseTypeId: el.baseTypeId,
		  objectTypeId: el.objectTypeId,
		  mimeType: el.contentType,
		  allowableActions: el.allowableActions
		}, null,  customButtons
		, customIcons, refreshFn, undefined, maxUploadSize));
		return $('<tr></tr>')
		  .append(tdText)
		  .append(tdButton);
	  
}
  
	
	
  }
  
  
  
  function displayEsperienzeOIVProfilo(el, refreshFn, parent) {
	  
		var  dataUltimoInvioDomanda,
			 dataInvioIscrizione =  parent ? parent['jconon_application:data_domanda'] : null,
			 dataInvioRinnovo =  parent ? parent['jconon_application:data_invio_rinnovo_elenco'] : null,
			 dataInvioCambioFascia =  parent ? parent['jconon_application:data_invio_cambio_fascia'] : null;
			 if( new Date( dataInvioIscrizione ) > new Date( dataInvioRinnovo ) ){
				 if( new Date( dataInvioIscrizione ) > new Date( dataInvioCambioFascia ) )
					 dataUltimoInvioDomanda = dataInvioIscrizione ;
				 else
					 dataUltimoInvioDomanda = dataInvioCambioFascia ;
			 }else{
				 if( new Date( dataInvioRinnovo ) > new Date( dataInvioCambioFascia ) )
					 dataUltimoInvioDomanda = dataInvioRinnovo ;
				 else
					 dataUltimoInvioDomanda = dataInvioCambioFascia ;
			 }
	    var tdText,

	      tdButton,
	      isNonCoerente = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente') !== -1,
	      isNonCoerenteRinnovo = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo') !== -1,
	      isRdP = el.parentProp ? (Call.isRdP(el.parentProp.relationships.parent[0]['jconon_call:rdp']) || common.User.admin) : false,
	//	  isRdP2 = el.parentProp ? (Call.isRdP(el.parentProp['jconon_call:rdp']) || common.User.admin) : false,
	    		  isVisibile =  el['jconon_attachment:esperienza_non_coerente_visibile'],
	    		  isVisibileRinnovo =  el['jconon_attachment:esperienza_non_coerente_rinnovo_visibile'],
	      isAdmin =  common.User.admin,
	      isEditable = ( new Date( dataUltimoInvioDomanda ) < new Date( el["cmis:creationDate"] ) ) ? true : false,
	      isInCorso =  el['jconon_attachment:esperienza_professionale_incorso'] || 
	      	el['jconon_attachment:precedente_incarico_oiv_incorso'],
	      callId = el.parentProp ? el.parentProp.relationships.parent[0]['cmis:objectId'] : undefined,
	//	  callId2 = el.parentProp ? el.parentProp['cmis:objectId'] : undefined,
		//		  isRdP = isRdP || isRdP2,
		//		  callId = callId || callId2,
	      applicationId = el.parentProp ? el.parentProp['cmis:objectId'] : undefined,
	      userName = el.parentProp ? el.parentProp['jconon_application:user'] : undefined,
	      title = el['jconon_attachment:esperienza_professionale_datore_lavoro'] ||
	        el['jconon_attachment:esperienza_professionale_amministrazione'] ||
	        el['jconon_attachment:aspect_specializzazioni_universita'] ||
	        el['jconon_attachment:precedente_incarico_oiv_amministrazione'],
	      ruolo = el['jconon_attachment:esperienza_professionale_ruolo'] ||
	        el['jconon_attachment:precedente_incarico_oiv_ruolo'],
	      esperienza = el['jconon_attachment:esperienza_professionale_da'] ||
	        el['jconon_attachment:esperienza_professionale_a'],
	      esperienzaOIV = el['jconon_attachment:precedente_incarico_oiv_da'] ||
	        el['jconon_attachment:precedente_incarico_oiv_a'],
	      item = $('<a href="#">' + title + '</a>').on('click', function () {
	        Node.displayMetadata(el.objectTypeId, el.id, true);
	        return false;
	      }),
		
	      annotazioneIsPresente = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_annotazioni') !== -1,
	      
	      periodoEsperienzaDa = el['jconon_attachment:esperienza_professionale_da'] ? ('dal ' + CNR.Date.format(el['jconon_attachment:esperienza_professionale_da'], null, 'DD/MM/YYYY')) : '',
	      periodoEsperienzaA = el['jconon_attachment:esperienza_professionale_a'] ? (' al ' + CNR.Date.format(el['jconon_attachment:esperienza_professionale_a'], null, 'DD/MM/YYYY')) : '',
	      annotationPeriodoEsperienza = $('<span class="muted annotation"><strong>Periodo di attività: </strong>' + periodoEsperienzaDa + periodoEsperienzaA + '</span>'),
	      periodoOIVDa = el['jconon_attachment:precedente_incarico_oiv_da'] ? ('dal ' + CNR.Date.format(el['jconon_attachment:precedente_incarico_oiv_da'], null, 'DD/MM/YYYY')) : '',
	      periodoOIVA = el['jconon_attachment:precedente_incarico_oiv_a'] ? (' al ' + CNR.Date.format(el['jconon_attachment:precedente_incarico_oiv_a'], null, 'DD/MM/YYYY')) : '',
	      annotationPeriodoOIV = $('<span class="muted annotation"><strong>Periodo di attività: </strong>' + periodoOIVDa + periodoOIVA + '</span>'),
	      annotationRuolo = $('<span class="muted annotation"><strong>Ruolo:</strong> ' + ruolo  + '</span>'),
	      modalTitle = title + ' Ruolo: ' + ruolo,
	      tdNonCoerente = $('<td>').addClass('span5'),
	      motivazione = el['jconon_attachment:esperienza_non_coerente_motivazione'],
	      motivazioneRinnovo = el['jconon_attachment:esperienza_non_coerente_rinnovo_motivazione'],
	      annotazione = el['jconon_attachment:esperienza_annotazione_motivazione'],
	      annotationAnnotazione = $('<span class="muted annotation text-warning"><strong>Annotazione:</strong> ' + annotazione  + '</span>'),
	      spanNonCoerenteRinnovo =  isVisibileRinnovo ?  $('<span>').addClass('text-error animated flash').appendTo(tdNonCoerente) : $('<span>').addClass('animated flash').appendTo(tdNonCoerente),
	    	      spanNonCoerente = isVisibile ? $('<span>').addClass('text-error animated flash').appendTo(tdNonCoerente) : $('<span>').addClass('animated flash').appendTo(tdNonCoerente);
	    if (esperienza) {
	      item.after(annotationPeriodoEsperienza);      
	    }
	    if (esperienzaOIV) {
	      item.after(annotationPeriodoOIV);      
	    }
	    if (ruolo) {
	      item.after(annotationRuolo);
	    }
	    if (annotazioneIsPresente && ( isRdP || isAdmin) ) {
	      item.after(annotationAnnotazione);
	    }
	    if (isNonCoerente && ( isRdP || isVisibile || isAdmin) ) {
	    	  var dateFormat = "DD/MM/YYYY";
	      if (motivazione !== '') {
	    	  if ( isVisibile ) {
			        $('<a href="#">').addClass('text-error').append(i18n.prop('label.esperienza.non.coerente', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_data'], '-', dateFormat) ) ).off('click').on('click', function () {
			     //     UI.alert(motivazione);
			        	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazione, undefined, undefined, false);
			          return false;
			        }).appendTo(spanNonCoerente);
	    	  }else{
	    		  $('<a href="#">').addClass('text-non-visibile').append(i18n.prop('label.esperienza.non.coerente', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_data'], '-', dateFormat) ) ).off('click').on('click', function () {
	    			     //     UI.alert(motivazione);
	    			        	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazione, undefined, undefined, false);
	    			          return false;
	    		  }).appendTo(spanNonCoerente);
	    	  }
//	        $('<a href="#">').addClass('text-error').append(i18n.prop('label.esperienza.non.coerente', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_data'], '-', dateFormat) ) ).off('click').on('click', function () {
//	     //     UI.alert(motivazione);
//	        	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazione, undefined, undefined, false);
//	          return false;
//	        }).appendTo(spanNonCoerente);
	      } else {
	        spanNonCoerente.append( i18n.prop('label.esperienza.non.coerente', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_data'], '-', dateFormat) ) );
	      }
	    }
	    
	    if (isNonCoerenteRinnovo && ( isRdP || isVisibileRinnovo || isAdmin ) ) {
	    	 var dateFormat = "DD/MM/YYYY";
	        if (motivazioneRinnovo !== '') {
	        	 if ( isVisibileRinnovo ) {
			          $('<a href="#">').addClass('text-error').append(i18n.prop('label.esperienza.non.coerente.rinnovo', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_rinnovo_data'], '-', dateFormat) ) ).off('click').on('click', function () {
			       //     UI.alert(motivazione);
			          	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazioneRinnovo, undefined, undefined, false);
			            return false;
			          }).appendTo(spanNonCoerenteRinnovo);
	        	 }else{
	        		 $('<a href="#">').addClass('text-non-visibile').append(i18n.prop('label.esperienza.non.coerente.rinnovo', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_rinnovo_data'], '-', dateFormat) ) ).off('click').on('click', function () {
	      		       //     UI.alert(motivazione);
	      		          	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazioneRinnovo, undefined, undefined, false);
	      		            return false;
	  		          }).appendTo(spanNonCoerenteRinnovo);
	        	 }
//	          $('<a href="#">').addClass('text-error').append(i18n.prop('label.esperienza.non.coerente.rinnovo', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_rinnovo_data'], '-', dateFormat) ) ).off('click').on('click', function () {
//	       //     UI.alert(motivazione);
//	          	 UI.modal('<i class="fa fa-info-circle">  Motivazione</i> ', motivazioneRinnovo, undefined, undefined, false);
//	            return false;
//	          }).appendTo(spanNonCoerenteRinnovo);
	        } else {
	          spanNonCoerenteRinnovo.append(i18n.prop('label.esperienza.non.coerente.rinnovo', CNR.Date.format(el['jconon_attachment:esperienza_non_coerente_rinnovo_data'], '-', dateFormat) ) );
	        }
	      }
	    
	    if(el.objectTypeId.includes("esperienza_professionale")){
	    	if ( el["jconon_attachment:incarico_articolo_19"] == null ) {
		    	if(el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello generale non di ruolo nella PA</strong></span>');
		
		    	}else if(el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello generale di ruolo nella PA</strong></span>');
		
		    	}else if(el["jconon_attachment:esperienza_dirigenziale"] && !el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello non generale non di ruolo nella PA</strong></span>');
		
		    	}else if(el["jconon_attachment:esperienza_dirigenziale"] && !el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello non generale di ruolo nella PA</strong></span>');
		
		    	}else{
		    	
		    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
		    	}
	    	}else if ( el["jconon_attachment:incarico_articolo_19"] ) {
	    		
	    		if(el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo di prima fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if(el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo di prima fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if( !el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo di seconda fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if( !el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo di seconda fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else{
		    	
		    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
		    	}
	    		
	    	}else if ( !el["jconon_attachment:incarico_articolo_19"] ) {
	    		
	    		if( !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo (incarico non ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if( el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo (incarico non ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else{
		    	
		    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
		    	}
	    		
	    	}
	    	
		}else{
			var  annotationObjectType = $('<span class="annotation"><strong>' + i18n[el.objectTypeId] + '</strong></span>');
	    }
	    
	    
	    
	    
//	    if(el.objectTypeId.includes("esperienza_professionale")){
//	    	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale/Esperienza dirigenziale nella PA</strong></span>');
//	    }else{
//	    	var  annotationObjectType = $('<span class="annotation"><strong>' + i18n[el.objectTypeId] + '</strong></span>');
//	    }
	    tdText = $('<td></td>')
	      .addClass('span5')
	      .append(annotationObjectType)
	      .append(item);
	    tdButton = $('<td></td>').addClass('span2').append(ActionButton.actionButton({
	      name: el.name,
	      nodeRef: el.id,
	      baseTypeId: el.baseTypeId,
	      objectTypeId: el.objectTypeId,
	      mimeType: el.contentType,
	      allowableActions: el.allowableActions,
	      defaultChoice: 'select'
	    }, {copy_curriculum: 'CAN_UPDATE_PROPERTIES'}, {
	      permissions : false,
	      history : false,
	      copy: false,
	      cut: false,
	      update: false,
	      remove: ( isRdP || isAdmin || isEditable ) ? function () {
	        UI.confirm('Sei sicuro di voler eliminare l\'esperienza/incarico "' +  title  + '"?', function () {
	          Node.remove(el.id, refreshFn);
	        });
	      } : false,
	      edit: ( isRdP || isAdmin || isInCorso || isEditable ) ? function () {
	        Application.editProdottiEsperienze(el, title, refreshFn, ( isAdmin || isEditable ) );
	      } : false,
//	      edit: function () {
//	          Application.editProdottiEsperienze(el, title, refreshFn, isAdmin);
//	        },
	      copy_curriculum: ( isRdP || isAdmin )  ? function () {
	        Application.editProdotti(el, title, refreshFn, true);
	      } : false,
	      annotazioni: isRdP ? function () {
	       var content = $("<div></div>"),
	          bulkinfo = new BulkInfo({
	            target: content,
	            path: "P:jconon_scheda_anonima:esperienza_annotazioni",
	            objectId: el.id,
	            formclass: 'form-inline',
	            name: 'default'
	          });
	        bulkinfo.render();
	        UI.modal('<i class="icon-edit"></i> ' + modalTitle, content, function () {
	          var close = UI.progress(), d = bulkinfo.getData();
	          d.push(
	            {
	              id: 'cmis:objectId',
	              name: 'cmis:objectId',
	              value: el['cmis:objectId']
	            },
	            {
	              name: 'aspect', 
	              value: 'P:jconon_scheda_anonima:esperienza_annotazioni'
	            },
	            {
	              name: 'callId', 
	              value: callId
	            },            
	            {
	              name: 'applicationId', 
	              value: applicationId
	            }                        
	          );
	          $.ajax({
	            url: cache.baseUrl + "/rest/application-fp/esperienza-annotazioni",
	            type: 'POST',
	            data:  d,
	            success: function (data) {
	              UI.success(i18n['message.esperienza.annotazioni.eseguito'], refreshFn);
	            },
	            complete: close,
	            error: URL.errorFn
	          });
	        });
	      } : false,
	      coerente: isRdP && isNonCoerente ? function () {
	          var d = [
	            {
	              id: 'cmis:objectId',
	              name: 'cmis:objectId',
	              value: el['cmis:objectId']
	            },
	            {
	              name: 'aspect', 
	              value: 'P:jconon_scheda_anonima:esperienza_non_coerente'
	            },
	            {
	              name: 'userName', 
	              value: userName
	            },            
	            {
	              name: 'callId', 
	              value: callId
	            }
	          ];
	          $.ajax({
	            url: cache.baseUrl + "/rest/application-fp/esperienza-coerente",
	            type: 'POST',
	            data:  d,
	            success: function (data) {
	              UI.success(i18n['message.esperienza.coerente.eseguito'], refreshFn);
	            },
	            complete: close,
	            error: URL.errorFn
	          });
	      } : false,
	      noncoerente: isRdP && !isNonCoerente ? function () {
	         var content = $("<div></div>"),
	            bulkinfo = new BulkInfo({
	              target: content,
	              path: "P:jconon_scheda_anonima:esperienza_non_coerente",
	              objectId: el.id,
	              formclass: 'form-inline',
	              name: 'default'
	            });
	          bulkinfo.render();
	          UI.modal('<i class="icon-edit"></i> ' + modalTitle, content, function () {
	            var close = UI.progress(), d = bulkinfo.getData();
	            d.push(
	              {
	                id: 'cmis:objectId',
	                name: 'cmis:objectId',
	                value: el['cmis:objectId']
	              },
	              {
	                name: 'aspect', 
	                value: 'P:jconon_scheda_anonima:esperienza_non_coerente'
	              },
	              {
	                name: 'callId', 
	                value: callId
	              }                        
	            );
	            $.ajax({
	              url: cache.baseUrl + "/rest/application-fp/esperienza-noncoerente",
	              type: 'POST',
	              data:  d,
	              success: function (data) {
	                UI.success(i18n['message.esperienza.noncoerente.eseguito'], refreshFn);
	              },
	              complete: close,
	              error: URL.errorFn
	            });
	          });
	      } : false,
	      paste: Application.getTypeForDropDown('jconon_call:elenco_sezioni_curriculum', el, title, refreshFn),
	      move: Application.getTypeForDropDown('jconon_call:elenco_sezioni_curriculum', el, title, refreshFn, true)
	    }, {copy_curriculum: 'icon-copy', paste: 'icon-paste', move: 'icon-move', noncoerente: 'icon-minus', coerente: 'icon-plus', annotazioni: 'icon-pencil'}, refreshFn, true));
	    return $('<tr class="esperienze '+el['cmis:objectId'].split(";")[0]+'" ></tr>')
			
	      .append(tdText)
	      .append(tdNonCoerente)
	      .append(tdButton);
	  }

//	  function displayNote(el, refreshFn, permission, showLastModificationDate, showTitleAndDescription, extendButton, customIcons, maxUploadSize) {
//		  var titolo="",
//		  isRdP = el.parentProp ? (Call.isRdP(el.parentProp.relationships.parent[0]['jconon_call:rdp']) || common.User.admin) : false,
//				  isAdmin =  common.User.admin;
//		if($(".labelSchedaAnonima").length < 1){
//		//	$("<label class='labelSchedaAnonima' >Attenzione !!! <br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>").insertBefore($("#affix_tabSchedaAnonima"));
//			$("#affix_tabSchedaAnonima").before("<label id='labelSchedaAnonima' style='margin-top: 60px;margin-bottom: 0px; font-size: larger;text-align: center; cursor: default;' class='well labelSchedaAnonima' ><b>Attenzione !!! </b><br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>");
//			
//		
//		}
//	console.log("jconon");
////		$("#affix_tabSchedaAnonima").before($("#affix_tabUlterioriDati"));
////		$("#affix_tabSchedaAnonima").before($("#affix_tabReperibilita"));
//		
////		$("a[href='#affix_tabSchedaAnonima']").before($("a[href='#affix_tabUlterioriDati']"));
////		$("a[href='#affix_tabSchedaAnonima']").before($("a[href='#affix_tabReperibilita']"));
//		
//		if($(".labelSchedaAnonima").length > 0){
//		//	$("<label class='labelSchedaAnonima' >Attenzione !!! <br> Per i dati inseriti di seguito il salvataggio è immediato ed automatico.</label>").insertBefore($("#affix_tabSchedaAnonima"));
//			$("#affix_tabSchedaAnonima").before($("#labelSchedaAnonima"));
//
//		}
//		
//		
//
//		
//		  if(el.contentType==null){
//			titolo=el["jconon_application:oggetto_notifica_email"];  
//			
//			  
//		  }else{
//			titolo=el.name;
//			  
//		  }
//		//  console.log("dispaly "+contesto);
//		 //  console.log("dispaly "+contesto.localeCompare('actions.visualizza_comunicazioni'));
////		if(showLastModificationDate && i18n[el.objectTypeId].localeCompare('Comunicazioni al candidato')==0){		&& (i18n[el.objectTypeId].localeCompare('Comunicazioni al candidato')==0 || i18n[el.objectTypeId].includes('esclusione'))
//	if( i18n[el.objectTypeId].includes("Note")){
//		  
//			var tdText,
//			  tdButton,
//			  isFolder = el.baseTypeId === 'cmis:folder',
//			  item = $('<a href="#">' + titolo + '</a>'),
//			  customButtons = $.extend({}, {
//				history : false,
//				copy: false,
//				cut: false,
//				permissions: false,
//				update: true
//			  }, extendButton),
//			  annotationType ,
//			
//			  annotation = $('<span class="muted annotation">ultima modifica: ' + CNR.Date.format(el.lastModificationDate, null, 'DD/MM/YYYY H:mm') + '</span>');
//			if(el.contentType!=null)
//				annotationType	 = $('<span class="muted annotation  ">' + el["jconon_application:oggetto_notifica_email"]  + '</span>');
//			else 
//				annotationType	 = $('<span class="muted annotation  ">' + i18n[el.objectTypeId]  + '</span>');
//			
//			
////			if (permission !== undefined) {
////			  customButtons.permissions = permission;
////			}
//			if(el.contentType!=null){
//				item = $('<a href="#">' + titolo + '</a>');
//				item.attr('href', URL.urls.search.content + '?nodeRef=' + el.id + '&guest=true');
//				  
//			}else{
//				item = $('<span href="#">' + titolo + '</span>');
//			}
//			
//			item.after(annotationType);
//			
//			if (showLastModificationDate === false && el.contentType!=null ) {
//			  item.after('<span class="muted annotation">' + CNR.fileSize(el.contentStreamLength) + '</span>');
//			} else if(el.contentType!=null){
//			  item.after(annotation.prepend(', ').prepend(CNR.fileSize(el.contentStreamLength)));
//			}else{
//				item.after(annotation);
//			}
////			if ( isRdP || isAdmin ){
////				 customButtons.edit = true;
////			}
//
//			tdText = $('<td ></td>')
//			  .addClass('span10')
//			  .append(CNR.mimeTypeIcon(el.contentType, el.name))
//			  .append(' ')
//			  .append(item);
//			tdButton = $('<td ></td>').addClass('span2').append(ActionButton.actionButton({
//			  name: el.name,
//			  nodeRef: el['alfcmis:nodeRef'],
//			  baseTypeId: el.baseTypeId,
//			  objectTypeId: el.objectTypeId,
//			  mimeType: el.contentType,
//			  allowableActions: el.allowableActions
//			}, null,  customButtons
//			, customIcons, refreshFn, undefined, maxUploadSize));
//			return $('<tr></tr>')
//			  .append(tdText)
//			  .append(tdButton);
//		  
//	}
//	  
//		
//		
//	  }
  
  function displayEsperienzeOIVRinnovo(el, refreshFn) {
	    var tdText,
	      tdButton,
	      isNonCoerente = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente') !== -1,
	      isNonCoerenteRinnovo = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo') !== -1,
	      isRdP = el.parentProp ? (Call.isRdP(el.parentProp.relationships.parent[0]['jconon_call:rdp']) || common.User.admin) : false,
	      callId = el.parentProp ? el.parentProp.relationships.parent[0]['cmis:objectId'] : undefined,
	      applicationId = el.parentProp ? el.parentProp['cmis:objectId'] : undefined,
	      userName = el.parentProp ? el.parentProp['jconon_application:user'] : undefined,
	      title = el['jconon_attachment:esperienza_professionale_datore_lavoro'] ||
	        el['jconon_attachment:esperienza_professionale_amministrazione'] ||
	        el['jconon_attachment:aspect_specializzazioni_universita'] ||
	        el['jconon_attachment:precedente_incarico_oiv_amministrazione'],
	      ruolo = el['jconon_attachment:esperienza_professionale_ruolo'] ||
	        el['jconon_attachment:precedente_incarico_oiv_ruolo'],
	      esperienza = el['jconon_attachment:esperienza_professionale_da'] ||
	        el['jconon_attachment:esperienza_professionale_a'],
	      esperienzaOIV = el['jconon_attachment:precedente_incarico_oiv_da'] ||
	        el['jconon_attachment:precedente_incarico_oiv_a'],
	      item = $('<p style="margin: 0 0 0; width: 45%; display: inline-block;">' + title + '</p>'),
	      annotazioneIsPresente = el['cmis:secondaryObjectTypeIds'].indexOf('P:jconon_scheda_anonima:esperienza_annotazioni') !== -1,
	      annotationObjectType = $('<span class="annotation"><strong>' + i18n[el.objectTypeId] + '</strong></span>'),
	      periodoEsperienzaDa = el['jconon_attachment:esperienza_professionale_da'] ? ('dal ' + CNR.Date.format(el['jconon_attachment:esperienza_professionale_da'], null, 'DD/MM/YYYY')) : '',
	      periodoEsperienzaA = el['jconon_attachment:esperienza_professionale_a'] ? (' al ' + CNR.Date.format(el['jconon_attachment:esperienza_professionale_a'], null, 'DD/MM/YYYY')) : '',
	      annotationPeriodoEsperienza = $('<span class="muted annotation" style="display: inline-block; width: 35%;"><strong>Periodo di attività: </strong>' + periodoEsperienzaDa + periodoEsperienzaA + '</span>'),
	      periodoOIVDa = el['jconon_attachment:precedente_incarico_oiv_da'] ? ('dal ' + CNR.Date.format(el['jconon_attachment:precedente_incarico_oiv_da'], null, 'DD/MM/YYYY')) : '',
	      periodoOIVA = el['jconon_attachment:precedente_incarico_oiv_a'] ? (' al ' + CNR.Date.format(el['jconon_attachment:precedente_incarico_oiv_a'], null, 'DD/MM/YYYY')) : '',
	      annotationPeriodoOIV = $('<span class="muted annotation" style="display: inline;"><strong>Periodo di attività: </strong>' + periodoOIVDa + periodoOIVA + '</span>'),
	      annotationRuolo = $('<span class="muted annotation" style="position: relative;display: inline;left: 45px;"><strong>Mansione:</strong> ' + ruolo  + '</span>'),
	      modalTitle = title + ' Ruolo: ' + ruolo,
	      tdNonCoerente = $('<td>').addClass('span5'),
	      motivazione = el['jconon_attachment:esperienza_non_coerente_motivazione'],
	      annotazione = el['jconon_attachment:esperienza_annotazione_motivazione'],
	      annotationAnnotazione = $('<span class="muted annotation text-warning"><strong>Annotazione:</strong> ' + annotazione  + '</span>'),
	      spanNonCoerente = $('<span>').addClass('text-error animated flash').appendTo(tdNonCoerente);
	      if (isNonCoerenteRinnovo ) 
		     
		          return ;
		      
		    
	    if (esperienza) {
	      item.after(annotationPeriodoEsperienza);      
	    }
	    if (esperienzaOIV) {
	      item.after(annotationPeriodoOIV);      
	    }
	    if (ruolo) {
	      item.after(annotationRuolo);
	    }
	    if (annotazioneIsPresente && isRdP) {
	      item.after(annotationAnnotazione);
	    }
	    if (isNonCoerente && isRdP) {
	      if (motivazione !== '') {
	        $('<a href="#">').addClass('text-error').append(i18n.prop('label.esperienza.non.coerente')).off('click').on('click', function () {
	          UI.alert(motivazione);
	          return false;
	        }).appendTo(spanNonCoerente);
	      } else {
	        spanNonCoerente.append(i18n.prop('label.esperienza.non.coerente'));
	      }
	    }
	    
	    if(el.objectTypeId.includes("esperienza_professionale")){
	    	if ( el["jconon_attachment:incarico_articolo_19"] == null ) {
		    	if(el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello generale non di ruolo nella PA</strong></span>');
		
		    	}else if(el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello generale di ruolo nella PA</strong></span>');
		
		    	}else if(el["jconon_attachment:esperienza_dirigenziale"] && !el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello non generale non di ruolo nella PA</strong></span>');
		
		    	}else if(el["jconon_attachment:esperienza_dirigenziale"] && !el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di livello non generale di ruolo nella PA</strong></span>');
		
		    	}else{
		    	
		    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
		    	}
	    	}else if ( el["jconon_attachment:incarico_articolo_19"] ) {
	    		
	    		if(el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo di prima fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if(el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo di prima fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if( !el["jconon_attachment:amministrazione_pubblica_generale"] && !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo di seconda fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if( !el["jconon_attachment:amministrazione_pubblica_generale"] && el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo di seconda fascia (incarico ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else{
		    	
		    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
		    	}
	    		
	    	}else if ( !el["jconon_attachment:incarico_articolo_19"] ) {
	    		
	    		if( !el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale non di ruolo (incarico non ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else if( el["jconon_attachment:dirigente_ruolo"] && el["jconon_attachment:fl_amministrazione_pubblica"] ){
		        	var  annotationObjectType = $('<span class="annotation"><strong>Esperienza dirigenziale di ruolo (incarico non ai sensi dell’art. 19 d.lgs 165/2001)</strong></span>');
		
		    	}else{
		    	
		    		var  annotationObjectType = $('<span class="annotation"><strong>Esperienza professionale</strong></span>');
		    	}
	    		
	    	}
	    	
		}else{
			var  annotationObjectType = $('<span class="annotation"><strong>' + i18n[el.objectTypeId] + '</strong></span>');
	    }
	    

	    tdText = $('<td></td>')
	      .addClass('span5')
	      .append(annotationObjectType)
	      .append(item);
	    
	    return $('<tr class="esperienze '+el['cmis:objectId'].split(";")[0]+'" ></tr>')
	      .append(tdText);
	     
	      
	  }
  
  function displayCorsiOIVRinnovo(el, refreshFn) {

	  var nomeCorso, dataCorso, nomeEnte, nomeModulo, numCrediti, tdButton, title = el['jconon_attachment:nome_corso'], tR= $('<tr></tr>'),
	  isRdP = el.parentProp ? (Call.isRdP(el.parentProp.relationships.parent[0]['jconon_call:rdp'])  ) : false,
			  isAdmin = 	common.User.admin  ;
	  nomeEnte = $('<td>'+el['jconon_attachment:nome_ente_corso']+'</td>').addClass('nomeEnte');
	  nomeEnte.css("text-align","center");
	  tR.append(nomeEnte);
	  
	  dataCorso = $('<td>'+convertDate(el['jconon_attachment:data_corso'].split("T")[0])+'</td>').addClass('dataCorso');
	  dataCorso.css("text-align","center");
	  tR.append(dataCorso);
	  nomeCorso = $('<td>'+el['jconon_attachment:nome_corso']+'</td>').addClass('nomeCorso');
	  nomeCorso.css("text-align","center");
	  tR.append(nomeCorso);
	  nomeModulo= $('<td>'+el['jconon_attachment:nome_modulo_corso']+'</td>').addClass('nomeModulo');
	  nomeModulo.css("text-align","center");
	  tR.append(nomeModulo);
	  numCrediti = $('<td class="numCrediti">'+el['jconon_attachment:numero_crediti_corso'].replace(',','.').replace(' ','')+'</td>').addClass('creditiCorso');
	  numCrediti.css("text-align","center");
	  tR.append(numCrediti);
	  
	  tdButton = $('<td></td>').addClass('azioniCorso').append(ActionButton.actionButton({
	      name: el.name,
	      nodeRef: el.id,
	      baseTypeId: el.baseTypeId,
	      objectTypeId: el.objectTypeId,
	      mimeType: el.contentType,
	      allowableActions: el.allowableActions,
	      defaultChoice: 'removeCorso'
	    }, {copy_curriculum: 'CAN_UPDATE_PROPERTIES'}, {
	      permissions : false,
	      history : false,
	      copy: false,
	      cut: false,
	      update: false,
	      select: false,
	      remove: false,
	      removeCorso: (!isRdP || isAdmin ) ?function () {
	        UI.confirm('Sei sicuro di voler eliminare il corso di formazione "' +  title  + '"?', function () {
	          Node.remove(el.id, refreshFn);
	        });
	      } : false,
	    
	     
	    }, { paste: 'icon-paste', move: 'icon-move'}, refreshFn, true));
	//  numCrediti.css("text-align","center");
	  tR.append(tdButton);
	  
	  return tR;
      
	  }
  
  function displayDettaglioCorsiOIVRinnovo(el, refreshFn) {

	  var nomeCorso, dataCorso, nomeEnte, nomeModulo, numCrediti, tdButton, title = el['jconon_attachment:nome_corso'], tR= $('<tr></tr>');
	  nomeEnte = $('<td>'+el['jconon_attachment:nome_ente_corso']+'</td>').addClass('nomeEnte');
	  nomeEnte.css("text-align","center");
	  tR.append(nomeEnte);
	  
	  dataCorso = $('<td>'+convertDate(el['jconon_attachment:data_corso'].split("T")[0])+'</td>').addClass('dataCorso');
	  dataCorso.css("text-align","center");
	  tR.append(dataCorso);
	  nomeCorso = $('<td>'+el['jconon_attachment:nome_corso']+'</td>').addClass('nomeCorso');
	  nomeCorso.css("text-align","center");
	  tR.append(nomeCorso);
	  nomeModulo= $('<td>'+el['jconon_attachment:nome_modulo_corso']+'</td>').addClass('nomeModulo');
	  nomeModulo.css("text-align","center");
	  tR.append(nomeModulo);
	  numCrediti = $('<td class="numCrediti">'+el['jconon_attachment:numero_crediti_corso'].replace(',','.').replace(' ','')+'</td>').addClass('creditiCorso');
	  numCrediti.css("text-align","center");
	  tR.append(numCrediti);
	  
	  tdButton = $('<td></td>').addClass('span2').addClass('azioniCorso').append(ActionButton.actionButton({
	      name: el.name,
	      nodeRef: el.id,
	      baseTypeId: el.baseTypeId,
	      objectTypeId: el.objectTypeId,
	      mimeType: el.contentType,
	      allowableActions: el.allowableActions,
	      defaultChoice: 'removeCorso'
	    }, {copy_curriculum: 'CAN_UPDATE_PROPERTIES'}, {
	      permissions : false,
	      history : false,
	      copy: false,
	      cut: false,
	      update: false,
	      select: false,
	      remove: false,
//	      remove: function () {
//	        UI.confirm('Sei sicuro di voler eliminare il corso di formazione "' +  title  + '"?', function () {
//	          Node.remove(el.id, refreshFn);
//	        });
//	      },
	      removeCorso: function () {
		        UI.confirm('Sei sicuro di voler eliminare il corso di formazione "' +  title  + '"?', function () {
		          Node.remove(el.id, refreshFn);
		          
		        });
		      },
     	     
	    }, { paste: 'icon-paste', move: 'icon-move'}, refreshFn, true));
	//  numCrediti.css("text-align","center");
	  
	  tR.append(tdButton);
	  
	  return tR;
	      
	  }
  
  function convertDate(inputFormat) {
	  function pad(s) { return (s < 10) ? '0' + s : s; }
	  var d = new Date(inputFormat)
	  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/')
	}
  
  function displayAttachments(id) {
	 
    var content = $('<div></div>').addClass('modal-inner-fix');
    jconon.findAllegati(id, content, 'jconon_attachment:document', null, function (el, refreshFn, permission) {
      return jconon.defaultDisplayDocument(el, refreshFn, false, false);
    });
    UI.modal(i18n['actions.attachments'], content);
  }

  function displayAttachmentsAllegatiProcedura(id) {
	  console.log("dispay attachment");
    var content = $('<div></div>').addClass('modal-inner-fix');
    jconon.findAllegati(id, content, 'jconon_attachment:document', null, function (el, refreshFn, permission) {
      return jconon.defaultDisplayDocumentAllegatiProcedura(el, refreshFn, false, false);
    });
    UI.modal(i18n['actions.attachments'], content, null, null, true);
  }
  
  function callIsActive(inizio, fine, proroga) {
    var data_inizio = CNR.Date.parse(inizio),
      data_fine = CNR.Date.parse(proroga || fine),
      data_now = CNR.Date.parse(common.now);
    if (data_inizio <= data_now && data_fine >= data_now) {
      return true;
    }
    return false;
  }

  function getCriteria(bulkInfo, attivi_scadutiValue) {
    var propDataInizio = 'root.jconon_call:data_inizio_invio_domande',
      propDataFine = 'root.jconon_call:data_fine_invio_domande',
      propDataProroga = 'root.jconon_call_procedura_comparativa:data_fine_proroga',

      criteria = new Criteria(),
      timestamp = moment(common.now).toDate().getTime(),
      isoDate;

    // il timestamp cambia ogni 10 minuti
    timestamp = timestamp - timestamp % (10 * 60 * 1000);
    isoDate = new Date(timestamp).toISOString();
    $.each(bulkInfo.getFieldProperties(), function (index, el) {
      var propValue = bulkInfo.getDataValueById(el.name),
        re = /^criteria\-/;
      if (el.property) {
        if (el['class'] && propValue) {
          $(el['class'].split(' ')).each(function (index, myClass) {
            if (re.test(myClass)) {
              var fn = myClass.replace(re, '');
              propValue = propValue.replace('\'', '\\\'');
              if (fn === 'contains') {
                criteria[fn](el.property + ':\\\'*' + propValue + '*\\\'', 'root');
              } else {
                criteria[fn](el.property, propValue, el.widget === 'ui.datepicker' ? 'date' : null);
              }
            }
          });
        } else {
          if (propValue) {            
            criteria.equals(el.property, propValue.replace('\'', '\\\''));
          }
        }
      }
      if (el.name === 'filters-attivi_scaduti') {
        if (attivi_scadutiValue ? attivi_scadutiValue === 'attivi' : propValue === 'attivi') {
          criteria.lte(propDataInizio, isoDate, 'date');
          criteria.complex(
            {type: 'open_parenthesis'},
              {type: 'open_parenthesis'},
                 {type: '>=', what: propDataProroga, boolOpAfter: 'AND', to: isoDate, valueType: 'date'},
                 {type: 'NOT NULL', what: propDataProroga},
              {type: 'close_parenthesis'},
              {type: 'NULL', boolOpBefore: 'OR', what: propDataProroga},
            {type: 'close_parenthesis'}
          );
          criteria.complex(
            {type: 'open_parenthesis'},
              {type: 'open_parenthesis'},
                {type: '>=', what: propDataFine, boolOpAfter: 'AND', to: isoDate, valueType: 'date'},
                {type: 'NULL', what: propDataProroga},
              {type: 'close_parenthesis'},
              {type: 'NOT NULL', boolOpBefore: 'OR', what: propDataProroga},
              {type: 'NULL', boolOpBefore: 'OR', what: propDataFine},
            {type: 'close_parenthesis'}
          );
        } else if (attivi_scadutiValue ? attivi_scadutiValue === 'scaduti' : propValue === 'scaduti') {
          criteria.complex(
            {type: 'open_parenthesis'},
              {type: 'open_parenthesis'},
                {type: '<=', what: propDataFine, boolOpAfter: 'AND', to: isoDate, valueType: 'date'},
                {type: 'NULL', what: propDataProroga},
              {type: 'close_parenthesis'},
                {type: '<=', what: propDataProroga,boolOpBefore: 'OR', to: isoDate, valueType: 'date'},
            {type: 'close_parenthesis'}
          );
        } else if (attivi_scadutiValue ? attivi_scadutiValue === 'tutti' : propValue === 'tutti') {
          if (common.User.groupsArray == undefined || common.User.groupsArray.indexOf('GROUP_GESTORI_BANDI') === -1) {
            criteria.lte(propDataInizio, isoDate, 'date');            
          }
        }
      }
    });
    return criteria;
  }
  
  function getCriteriaOIV(bulkInfo, attivi_scadutiValue) {
	    var propDataInizio = 'jconon_application:data_domanda',
	      propDataFine = 'root.jconon_call:data_fine_invio_domande',
	      propDataProroga = 'root.jconon_call_procedura_comparativa:data_fine_proroga',

	      criteria = new Criteria(),
	      timestamp = moment(common.now).toDate().getTime(),
	      isoDate;

	    // il timestamp cambia ogni 10 minuti
	    timestamp = timestamp - timestamp % (10 * 60 * 1000);
	 //   isoDate = new Date(timestamp).toISOString();
	    isoDate = new Date('01/05/2021').toISOString();
	    $.each(bulkInfo.getFieldProperties(), function (index, el) {
	      var propValue = bulkInfo.getDataValueById(el.name),
	        re = /^criteria\-/;
	      if (el.property) {
	        if (el['class'] && propValue) {
	          $(el['class'].split(' ')).each(function (index, myClass) {
	            if (re.test(myClass)) {
	              var fn = myClass.replace(re, '');
	              propValue = propValue.replace('\'', '\\\'');
	              if (fn === 'contains') {
	                criteria[fn](el.property + ':\\\'*' + propValue + '*\\\'', 'root');
	              } else {
	                criteria[fn](el.property, propValue, el.widget === 'ui.datepicker' ? 'date' : null);
	              }
	            }
	          });
	        } else {
	          if (propValue) {            
	            criteria.equals(el.property, propValue.replace('\'', '\\\''));
	          }
	        }
	      }
	      if (el.name === 'filters-provvisorie_inviate') {
	        if (attivi_scadutiValue ? attivi_scadutiValue === 'tutte' : propValue === 'tutte') {
	          criteria.lte(propDataInizio, isoDate, 'date');
	          criteria.complex(
	            {type: 'open_parenthesis'},
	              {type: 'open_parenthesis'},
	                 {type: '>=', what: propDataProroga, boolOpAfter: 'AND', to: isoDate, valueType: 'date'},
	                 {type: 'NOT NULL', what: propDataProroga},
	              {type: 'close_parenthesis'},
	              {type: 'NULL', boolOpBefore: 'OR', what: propDataProroga},
	            {type: 'close_parenthesis'}
	          );
	          criteria.complex(
	            {type: 'open_parenthesis'},
	              {type: 'open_parenthesis'},
	                {type: '>=', what: propDataFine, boolOpAfter: 'AND', to: isoDate, valueType: 'date'},
	                {type: 'NULL', what: propDataProroga},
	              {type: 'close_parenthesis'},
	              {type: 'NOT NULL', boolOpBefore: 'OR', what: propDataProroga},
	              {type: 'NULL', boolOpBefore: 'OR', what: propDataFine},
	            {type: 'close_parenthesis'}
	          );
	        } else if (attivi_scadutiValue ? attivi_scadutiValue === 'scaduti' : propValue === 'scaduti') {
	          criteria.complex(
	            {type: 'open_parenthesis'},
	              {type: 'open_parenthesis'},
	                {type: '<=', what: propDataFine, boolOpAfter: 'AND', to: isoDate, valueType: 'date'},
	                {type: 'NULL', what: propDataProroga},
	              {type: 'close_parenthesis'},
	                {type: '<=', what: propDataProroga,boolOpBefore: 'OR', to: isoDate, valueType: 'date'},
	            {type: 'close_parenthesis'}
	          );
	        } else if (attivi_scadutiValue ? attivi_scadutiValue === 'tutti' : propValue === 'tutti') {
	          if (common.User.groupsArray == undefined || common.User.groupsArray.indexOf('GROUP_GESTORI_BANDI') === -1) {
	            criteria.lte(propDataInizio, isoDate, 'date');            
	          }
	        }
	      }
	    });
	    return criteria;
	  }
  
  $.validator.addMethod('telephone-number',
    function (value) {
      if (value !== "") {
        var regex = /^([0-9]*\-?\ ?\/?[0-9]*)$/;
        return regex.test(value);
      }
      return true;
    }, i18n['message.telephone-number.valid']
    );

  /* Revealing Module Pattern */
  return {
    displayAttachments: displayAttachments,
    displayAttachmentsAllegatiProcedura: displayAttachmentsAllegatiProcedura,
    displayEsperienzeOIV: displayEsperienzeOIV,
    displayEsperienzeOIVRinnovo: displayEsperienzeOIVRinnovo,
    displayEsperienzeOIVProfilo: displayEsperienzeOIVProfilo,
    displayCorsiOIVRinnovo: displayCorsiOIVRinnovo,
    displayNote: displayNote,
    displayDettaglioCorsiOIVRinnovo: displayDettaglioCorsiOIVRinnovo,
    URL: urls,
    Data: URL.initURL(urls),
    init: init,
    callIsActive: callIsActive,
    getCriteriaOIV: getCriteriaOIV,
    getCriteria: getCriteria
  };
});