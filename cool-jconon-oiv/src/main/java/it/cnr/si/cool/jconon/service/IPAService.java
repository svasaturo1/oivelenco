/*
 * Copyright (C) 2019  Consiglio Nazionale delle Ricerche
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.cnr.si.cool.jconon.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import it.cnr.si.cool.jconon.model.IPAAmministrazione;
import it.cnr.si.cool.jconon.model.TipoUniversita;

import javax.management.timer.Timer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class IPAService {
	  private static final Logger LOGGER = LoggerFactory.getLogger(IPAService.class);
    public static final String AMMINISTRAZIONI_IPA = "amministrazioni-ipa";
    public static final String UNIVERSITA_IPA = "universita-ipa";
    public static final String TIPOLOGIE_UNI = "tipologie-uni";
    

    @Value("${ipa.url.amministrazioni}")
    private String iapURLAmministrazioni;
    @Value("${ipa.url.tipologie_uni}")
    private String iapURLTipologie_uni;
    @Value("${ipa.url.amministrazioni_mef}")
    private String iapURLAmministrazioni_Mef;

    @Cacheable(value = AMMINISTRAZIONI_IPA)
    public Map<String, IPAAmministrazione> amministrazioni() throws IOException {
    	
      //  InputStream is = new URL(iapURLAmministrazioni)
    	 InputStream is = new URL(iapURLAmministrazioni_Mef)
                .openConnection().getInputStream();
//        Predicate<String> filterFirstLine =
//                line -> !(
//                        "cod_amm".equals(line.split("\t", -1)[0])
//                                && "des_amm".equals(line.split("\t", -1)[1])
//                );
//       
//            	LOGGER.error("url filtro ipa  "+filterFirstLine);     
//    	 
//        return new BufferedReader(new InputStreamReader(is))
//                .lines()
//                .filter(filterFirstLine)
//                .map(s -> s.split("\t", -1))
//                .map(strings ->
//                        new IPAAmministrazione()
//                                .setCod_amm(strings[0])
//                                .setDes_amm(strings[1])
//                                .setComune(strings[2])
//                                .setNome_resp(strings[3])
//                                .setCognome_resp(strings[4])
//                                .setCap(strings[5])
//                                .setProvincia(strings[6])
//                                .setSito_istituzionale(strings[8])
//                                .setIndirizzo(strings[9])
//                                .setTipologia_amm(strings[12])
//                                .setTipologia_istat(strings[11])
//                                .setAcronimo(strings[13])
//                                .setMail1(strings[16])
//                )
//                .collect(Collectors.toMap(amministrazione -> amministrazione.getCod_amm(), amministrazione -> amministrazione));
    	  	Map<String, IPAAmministrazione> mappaIpa = new HashMap<String, IPAAmministrazione>();
      	  List<String[]> r = null;
    	 CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
   	  try (CSVReader reader = new CSVReaderBuilder(new InputStreamReader(is)).withCSVParser(parser).withSkipLines(1).build()) {
           
 			try {
 				r = reader.readAll();
 			} catch (IOException | CsvException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
 			 for( String[] x : r ){
 				 if(x[36].isEmpty() )
 					 continue;
 				IPAAmministrazione ipa = new IPAAmministrazione();
 				ipa.setCod_amm(x[36]);
 				ipa.setDes_amm(x[1]);
 				ipa.setComune(x[20]);
 				ipa.setNome_resp(x[13]);
 				ipa.setCognome_resp(x[14])  ;
 				ipa.setCap(x[17])  ;
 				ipa.setProvincia(x[23])  ;
 				ipa.setSito_istituzionale(x[12])  ;
 				ipa.setIndirizzo(x[16])  ;
 				ipa.setTipologia_amm(x[41])  ;
 				ipa.setTipologia_istat(x[55])  ;
 				ipa.setData_cancellazione(x[43])  ;
         	//  tipo.setMail1(x[16])  ;
        
          	mappaIpa.put( x[0], ipa );
             }
 				
           		  
         }
   	 return mappaIpa;
    }
    
    @Cacheable(value = UNIVERSITA_IPA)
    public Map<String, IPAAmministrazione> universita() throws IOException {
    	
        InputStream is = new URL(iapURLAmministrazioni_Mef)
                .openConnection().getInputStream();
//        Predicate<String> filterFirstLine =
//                line -> !(
//                        "cod_amm".equals(line.split("\t", -1)[0])
//                                && "des_amm".equals(line.split("\t", -1)[1])
//                );
//        Predicate<String> filterUni =
//                line -> (
//                		line.split("\t", -1)[11]
//                				.contains("Universita")
//                                
//                );
//             
//                          
//        return new BufferedReader(new InputStreamReader(is))
//                .lines()
//                .filter(filterFirstLine)
//                .filter(filterUni)
//                .map(s -> s.split("\t", -1))
//                .map(strings ->
//                        new IPAAmministrazione()
//                                .setCod_amm(strings[0])
//                                .setDes_amm(strings[1])
//                                .setComune(strings[2])
//                                .setNome_resp(strings[3])
//                                .setCognome_resp(strings[4])
//                                .setCap(strings[5])
//                                .setProvincia(strings[6])
//                                .setSito_istituzionale(strings[8])
//                                .setIndirizzo(strings[9])
//                                .setTipologia_amm(strings[12])
//                                .setTipologia_istat(strings[11])
//                                .setAcronimo(strings[13])
//                                .setMail1(strings[16])
//                )
//                .collect(Collectors.toMap(amministrazione -> amministrazione.getCod_amm(), amministrazione -> amministrazione));
    	
  	  	Map<String, IPAAmministrazione> mappaUniversita = new HashMap<String, IPAAmministrazione>();
    	  List<String[]> r = null;
  	 CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
 	  try (CSVReader reader = new CSVReaderBuilder(new InputStreamReader(is)).withCSVParser(parser).withSkipLines(1).build()) {
         
			try {
				r = reader.readAll();
			} catch (IOException | CsvException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 for( String[] x : r ){
				 if( x[36].isEmpty() || !x[55].contains("UNIVERSITA") )
					 continue;
				IPAAmministrazione uni = new IPAAmministrazione();
				uni.setCod_amm(x[36]);
				uni.setDes_amm(x[1]);
				uni.setComune(x[20]);
				uni.setNome_resp(x[13]);
				uni.setCognome_resp(x[14])  ;
				uni.setCap(x[17])  ;
				uni.setProvincia(x[23])  ;
				uni.setSito_istituzionale(x[12])  ;
				uni.setIndirizzo(x[16])  ;
				uni.setTipologia_amm(x[41])  ;
				uni.setTipologia_istat(x[55])  ;
				uni.setData_cancellazione(x[43])  ;
				
				// MANCANO
				//  uni.setMail1(x[16])  ; 
				//  uni.setAcronimo(x[16])  ;
				
        	mappaUniversita.put( x[0], uni );
           }
				
         		  
       }
 	 return mappaUniversita;
    }
    
    @Cacheable(value = TIPOLOGIE_UNI)
    public Map<String, TipoUniversita> tipologieUni() throws IOException {
    	//InputStream is = new URL(iapURLTipologie_uni)
    	InputStream is = new URL(iapURLTipologie_uni)
    			
                .openConnection().getInputStream();
    	Map<String, TipoUniversita> mappaTipologie = new HashMap<String, TipoUniversita>();
    	  List<String[]> r = null;
    	  try (CSVReader reader = new CSVReader(new InputStreamReader(is))) {
            
  			try {
  				r = reader.readAll();
  			} catch (IOException | CsvException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
              for( String[] x : r ){
            	  if( x[1].equalsIgnoreCase("numero classe") )
            		  continue;
            	  TipoUniversita tipo = new TipoUniversita();
            	  tipo.setId(x[0]);
            	  tipo.setNumero_classe(x[1]);
            	  tipo.setTipo_laurea(x[2]);
            	  tipo.setDenominazione_classe(x[3]);
            	  tipo .setNome_gruppo_disciplinare(x[8])  ;
            	  mappaTipologie.put( x[1], tipo );
              }
            		  
          }
          //  	LOGGER.error("url filtro ipa  "+filterFirstLine);     
        return mappaTipologie;
    	
    }

    @Scheduled(fixedRate = Timer.ONE_WEEK)
    @CacheEvict(value = AMMINISTRAZIONI_IPA)
    
    public void clearCache() {
    }
    @Scheduled(fixedRate = Timer.ONE_WEEK)
    @CacheEvict(value = UNIVERSITA_IPA)
    
    public void clearCacheUni() {
    }
    @Scheduled(fixedRate = Timer.ONE_WEEK)
    @CacheEvict(value = TIPOLOGIE_UNI)
    
    public void clearCacheTipologie() {
    }
}
