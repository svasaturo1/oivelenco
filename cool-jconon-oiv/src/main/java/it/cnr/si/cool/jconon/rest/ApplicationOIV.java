/*
 * Copyright (C) 2019  Consiglio Nazionale delle Ricerche
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.cnr.si.cool.jconon.rest;

import freemarker.template.TemplateException;
import it.cnr.cool.cmis.service.CMISService;
import it.cnr.cool.cmis.service.NodeMetadataService;
import it.cnr.cool.security.SecurityChecked;
import it.cnr.cool.web.scripts.exception.CMISApplicationException;
import it.cnr.cool.web.scripts.exception.ClientMessageException;
import it.cnr.si.cool.jconon.service.application.ApplicationOIVService;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("application-fp")
@Component
@SecurityChecked(needExistingSession = true, checkrbac = false)
public class ApplicationOIV {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationOIV.class);
    @Autowired
    private ApplicationOIVService applicationOIVService;
    @Autowired
    private CMISService cmisService;
    @Autowired
    private NodeMetadataService nodeMetadataService;

    private String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1000));
        double number = size / Math.pow(1000, digitGroups);
        return new DecimalFormat("#,##0.#").format(number) + " " + units[digitGroups];
    }

    @POST
    @Path("send-application")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendApplication(@Context HttpServletRequest req) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.sendApplicationOIV(session, req, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (MaxUploadSizeExceededException _ex) {
            LOGGER.error("max size exceeded", _ex);
            String readableFileSize = readableFileSize(req.getContentLength());
            String maxFileSize = readableFileSize(_ex.getMaxUploadSize());
            String message = "Il file ( " + readableFileSize + ") supera la dimensione massima consentita (" + maxFileSize + ")";
            throw new ClientMessageException(message);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }
    
    @POST
    @Path("send-cambio-fascia")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendCambioFascia(@Context HttpServletRequest req) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.sendCambioFasciaOIV(session, req, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (MaxUploadSizeExceededException _ex) {
            LOGGER.error("max size exceeded", _ex);
            String readableFileSize = readableFileSize(req.getContentLength());
            String maxFileSize = readableFileSize(_ex.getMaxUploadSize());
            String message = "Il file ( " + readableFileSize + ") supera la dimensione massima consentita (" + maxFileSize + ")";
            throw new ClientMessageException(message);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @POST
    @Path("rinnovo-application")
    @Produces(MediaType.APPLICATION_JSON)
    public Response rinnovoApplication(@Context HttpServletRequest req) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.rinnovoApplicationOIV(session, req, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (MaxUploadSizeExceededException _ex) {
            LOGGER.error("max size exceeded", _ex);
            String readableFileSize = readableFileSize(req.getContentLength());
            String maxFileSize = readableFileSize(_ex.getMaxUploadSize());
            String message = "Il file ( " + readableFileSize + ") supera la dimensione massima consentita (" + maxFileSize + ")";
            throw new ClientMessageException(message);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }
    @POST
    @Path("add-crediti")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCrediti(@Context HttpServletRequest req, @FormParam("nodeRef") String nodeRef,  @FormParam("mesi") Integer mesi, @FormParam("creditiEsperienze") String crediti) throws IOException {
        ResponseBuilder rb;
       
        
        try {
           Session session = cmisService.getCurrentCMISSession(req);
           boolean res  = applicationOIVService.addCreditiApplicationOIV(session, nodeRef, mesi , crediti);
//            rb = Response.ok(model);
       } catch (MaxUploadSizeExceededException _ex) {
//            LOGGER.error("max size exceeded", _ex);
//            String readableFileSize = readableFileSize(req.getContentLength());
//            String maxFileSize = readableFileSize(_ex.getMaxUploadSize());
//            String message = "Il file ( " + readableFileSize + ") supera la dimensione massima consentita (" + maxFileSize + ")";
//            throw new ClientMessageException(message);
//        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
//            LOGGER.warn("send error", e);
//            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
       }
       return null;
    }
    
    
    @POST
    @Path("checkCodiceFiscaleInvio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkCodiceFiscaleInvio(@Context HttpServletRequest req, @FormParam("nome") String nome, @FormParam("cognome") String cognome, @FormParam("dataNascita") String dataNasciata, @FormParam("comuneNascita") String comuneNascita, @FormParam("sesso") String sesso, @FormParam("codiceFiscaleInserito") String codiceFiscaleInserito) throws IOException {
        ResponseBuilder rb;
              
        try {
           Session session = cmisService.getCurrentCMISSession(req);
           boolean res  = applicationOIVService.checkCodiceFiscaleInvioApplicationOIV( nome, cognome, dataNasciata , comuneNascita, sesso, codiceFiscaleInserito);
           rb = Response.ok();
       } catch (ClientMessageException e) {
    	   rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
       }
       return rb.build();
    }
    
    @POST
    @Path("add-crediti-corsi")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCreditiCorsi(@Context HttpServletRequest req, @FormParam("nodeRef") String nodeRef,  @FormParam("creditiEsperienze") String crediti) throws IOException {
        ResponseBuilder rb;
       
        
        try {
           Session session = cmisService.getCurrentCMISSession(req);
           boolean res  = applicationOIVService.addCreditiCorsiApplicationOIV(session, nodeRef, crediti);
//            rb = Response.ok(model);
       } catch (MaxUploadSizeExceededException _ex) {
//            LOGGER.error("max size exceeded", _ex);
//            String readableFileSize = readableFileSize(req.getContentLength());
//            String maxFileSize = readableFileSize(_ex.getMaxUploadSize());
//            String message = "Il file ( " + readableFileSize + ") supera la dimensione massima consentita (" + maxFileSize + ")";
//            throw new ClientMessageException(message);
//        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
//            LOGGER.warn("send error", e);
//            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
       }
       return null;
    }
    
    @GET
    @Path("check-application")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkApplication(@Context HttpServletRequest req) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            String userId = cmisService.getCMISUserFromSession(req).getId();
            List<String> model = applicationOIVService.checkApplicationOIV(session, userId, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.warn("check error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @GET
    @Path("applications-elenco.xls")
    @Produces(MediaType.APPLICATION_JSON)
    public Response extractionApplicationForSingleCall(@Context HttpServletRequest req, @QueryParam("q") String query, @QueryParam("callId") String callId) throws IOException {
        LOGGER.debug("Extraction application from query:" + query);
        ResponseBuilder rb;
        Session session = cmisService.getCurrentCMISSession(req);
//        session.getSessionParameters().get("org.apache.chemistry.opencmis.password").equalsIgnoreCase("jcononpw");
        try {
            Map<String, Object> model = applicationOIVService.extractionApplicationForElenco(session, query, cmisService.getCMISUserFromSession(req).getId(), callId);
            model.put("fileName", "elenco-oiv");
            rb = Response.ok(model);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        return rb.build();
    }
    
    @GET
    @Path("applications-elenco-esperienze.xls")
    @Produces(MediaType.APPLICATION_JSON)
    public Response extractionApplicationWithExperence(@Context HttpServletRequest req, @QueryParam("q") String query, @QueryParam("callId") String callId) throws IOException {
        LOGGER.debug("Extraction application from query:" + query);
        ResponseBuilder rb;
        Session session = cmisService.getCurrentCMISSession(req);
//        session.getSessionParameters().get("org.apache.chemistry.opencmis.password").equalsIgnoreCase("jcononpw");
        try {
            Map<String, Object> model = applicationOIVService.extractionApplicationWithExperence(session, query, cmisService.getCMISUserFromSession(req).getId(), callId);
            model.put("fileName", "elenco-oiv-con-esperienze");
            rb = Response.ok(model);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        return rb.build();
    }
    
    @GET
    @Path("applications-elenco-filtrato.xls")
    @Produces(MediaType.APPLICATION_JSON)
    public Response extractionApplicationWithFilter(@Context HttpServletRequest req, @QueryParam("q") String query, @QueryParam("callId") String callId, @QueryParam("filtroDataDa") String filtroDataDa, @QueryParam("filtroDataA") String filtroDataA) throws IOException {
        LOGGER.debug("Extraction application from query:" + query);
        LOGGER.error("Extraction application from query:" + filtroDataDa);
        LOGGER.error("Extraction application from query:" + filtroDataA);
        ResponseBuilder rb;
        Session session = cmisService.getCurrentCMISSession(req);
//        session.getSessionParameters().get("org.apache.chemistry.opencmis.password").equalsIgnoreCase("jcononpw");
        try {
            Map<String, Object> model = applicationOIVService.extractionApplicationWithFilter(session, query, cmisService.getCMISUserFromSession(req).getId(), callId, filtroDataDa, filtroDataA );
            model.put("fileName", "elenco-oiv-domande-filtrate");
            rb = Response.ok(model);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        return rb.build();
    }
    
    
    @GET
    @Path("invia-mail")
    @Produces(MediaType.APPLICATION_JSON)
    public void provaScarico1(@Context HttpServletRequest req, @QueryParam("q") String query, @QueryParam("callId") String callId) throws IOException {
        LOGGER.debug("Extraction application from query:" + query);
        LOGGER.error("scarico uno");
        ResponseBuilder rb;
    //    Session session = cmisService.getCurrentCMISSession(req);
//        session.getSessionParameters().get("org.apache.chemistry.opencmis.password").equalsIgnoreCase("jcononpw");
        try {
            applicationOIVService.invioMailAutoElencoOIV(req);
           
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        //return rb.build();
    }
    
    @GET
    @Path("cancellazioneMassiva")
    @Produces(MediaType.APPLICATION_JSON)
    public void provaScarico2(@Context HttpServletRequest req, @QueryParam("q") String query, @QueryParam("callId") String callId) throws IOException {
        LOGGER.debug("Extraction application from query:" + query);
        LOGGER.error("scarico due");
        ResponseBuilder rb;
    //    Session session = cmisService.getCurrentCMISSession(req);
//        session.getSessionParameters().get("org.apache.chemistry.opencmis.password").equalsIgnoreCase("jcononpw");
        try {
            applicationOIVService.cancellazioneAutoElencoOIV(req);
           
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        //return rb.build();
    }
    
//    @GET
//    @Path("prova-scarico2")
//    @Produces(MediaType.APPLICATION_JSON)
//    public void provaScarico2(@Context HttpServletRequest req, @QueryParam("q") String query, @QueryParam("callId") String callId) throws IOException {
//        LOGGER.debug("Extraction application from query:" + query);
//        ResponseBuilder rb;
//        Session session = cmisService.getCurrentCMISSession(req);
////        session.getSessionParameters().get("org.apache.chemistry.opencmis.password").equalsIgnoreCase("jcononpw");
//        try {
//            applicationOIVService.estraiElencoOIV();
//           
//        } catch (Exception e) {
//            LOGGER.error(e.getMessage(), e);
//            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
//        }
//        //return rb.build();
//    }

    

    @POST
    @Path("esperienza-noncoerente")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response esperienzaNonCoerente(@Context HttpServletRequest req,
                                          @FormParam(PropertyIds.OBJECT_ID) String objectId,
                                          @FormParam("callId") String callId,
                                          @FormParam("applicationId") String applicationId,
                                          @FormParam("aspect") String aspect,
                                          @FormParam("jconon_attachment:esperienza_non_coerente_data") String data,
                                          @FormParam("jconon_attachment:esperienza_non_coerente_visibile")  @DefaultValue("false") boolean isVisibile,
                                          @FormParam("jconon_attachment:esperienza_non_coerente_motivazione") String motivazione) throws IOException {
                                        //  @FormParam("motivazione") String motivazione) throws IOException {
        ResponseBuilder rb;
        try {
            String userId = cmisService.getCMISUserFromSession(req).getId();
            applicationOIVService.esperienzaNonCoerente(userId, objectId, callId, applicationId, aspect, data, isVisibile, motivazione);
            rb = Response.ok();
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.error("esperienzaNonCoerente error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }
    @POST
    @Path("esperienza-noncoerente-rinnovo")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response esperienzaNonCoerenteRinnovo(@Context HttpServletRequest req,
                                          @FormParam(PropertyIds.OBJECT_ID) String objectId,
                                          @FormParam("callId") String callId,
                                          @FormParam("applicationId") String applicationId,
                                          @FormParam("aspect") String aspect,
                                          @FormParam("jconon_attachment:esperienza_non_coerente_rinnovo_data") String data,
                                          @FormParam("jconon_attachment:esperienza_non_coerente_rinnovo_visibile") boolean isVisibile,
                                          @FormParam("jconon_attachment:esperienza_non_coerente_rinnovo_motivazione") String motivazione) throws IOException {
                                         // @FormParam("motivazione") String motivazione) throws IOException {
        ResponseBuilder rb;
        try {
            String userId = cmisService.getCMISUserFromSession(req).getId();
            applicationOIVService.esperienzaNonCoerente(userId, objectId, callId, applicationId, aspect, data, isVisibile, motivazione);
            rb = Response.ok();
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.error("esperienzaNonCoerente error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @POST
    @Path("esperienza-coerente")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response esperienzaCoerente(@Context HttpServletRequest req,
                                       @FormParam(PropertyIds.OBJECT_ID) String objectId,
                                       @FormParam("callId") String callId,
                                       @FormParam("applicationId") String applicationId,
                                       @FormParam("aspect") String aspect,
                                       @FormParam("userName") String userName) throws IOException {
        ResponseBuilder rb;
        try {
            String userId = cmisService.getCMISUserFromSession(req).getId();
            applicationOIVService.esperienzaCoerente(userId, objectId, callId, applicationId, aspect, userName);
            rb = Response.ok();
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.error("esperienzaNonCoerente error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @POST
    @Path("esperienza-annotazioni")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response annotazioni(@Context HttpServletRequest req,
                                @FormParam(PropertyIds.OBJECT_ID) String objectId,
                                @FormParam("callId") String callId,
                                @FormParam("applicationId") String applicationId,
                                @FormParam("aspect") String aspect,
                                @FormParam("jconon_attachment:esperienza_annotazione_motivazione") String motivazione) throws IOException {
        ResponseBuilder rb;
        try {
            String userId = cmisService.getCMISUserFromSession(req).getId();
            applicationOIVService.esperienzaAnnotazione(userId, objectId, callId, applicationId, aspect, motivazione);
            rb = Response.ok();
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.error("esperienzaNonCoerente error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @GET
    @Path("applications-ricalcola-fascia")
    @Produces(MediaType.APPLICATION_JSON)
    public Response ricalcolaFascia(@Context HttpServletRequest req, @QueryParam("applicationId") String applicationId) throws IOException {
        ResponseBuilder rb;
        Session session = cmisService.getCurrentCMISSession(req);
        try {
            Map<String, Object> model = applicationOIVService.ricalcolaFascia(session, applicationId);
         //   LOGGER.error(" recalcola - fascia "+model.get(ApplicationOIVService.JCONON_APPLICATION_FASCIA_PROFESSIONALE_ATTRIBUITA));
           
            	rb = Response.ok(model);
           
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        return rb.build();
    }
    @GET
    @Path("reset-fascia")
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetFascia(@Context HttpServletRequest req, @QueryParam("applicationId") String applicationId) throws IOException {
        ResponseBuilder rb;
        Session session = cmisService.getCurrentCMISSession(req);
        try {
    applicationOIVService.resetFascia(session, applicationId);
         
            	rb = Response.ok();
           
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        return rb.build();
    }

    @POST
    @Path("message")
    @Produces(MediaType.APPLICATION_JSON)
    public Response message(@Context HttpServletRequest req,
                            @FormParam("idDomanda") String idDomanda, @FormParam("nodeRefDocumento") String nodeRefDocumento) throws IOException {
        ResponseBuilder rb;
        LOGGER.debug("Message for application:" + idDomanda);

        applicationOIVService.message(cmisService.getCurrentCMISSession(req),
                idDomanda, nodeRefDocumento);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("idDomanda", idDomanda);
        rb = Response.ok(model);
        return rb.build();
    }

    @GET
    @Path("iscrivi-inelenco")
    @Produces(MediaType.APPLICATION_JSON)
    public Response iscriviInElanco(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda) throws IOException {
        LOGGER.debug("Iscrizione in elenco della domanda: {}", idDomanda);
        return Response.ok(Collections.singletonMap("progressivo",
                applicationOIVService.iscriviInElenco(cmisService.getCurrentCMISSession(req), idDomanda))).build();
    }
    
    @POST
    @Path("get-progressivo-temp")
    @Produces(MediaType.APPLICATION_JSON)
    public Long getProgressivoTemp(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda) throws IOException {
        LOGGER.debug("Iscrizione in elenco della domanda: {}", idDomanda);
        return
                applicationOIVService.getProgressivoTemp(cmisService.getCurrentCMISSession(req), idDomanda);
    }
   

    @POST
    @Path("preavviso-rigetto")
    @Produces(MediaType.APPLICATION_JSON)
    public Response preavvisoRigetto(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda, @QueryParam("fileName") String fileName) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.preavvisoRigetto(session, req, idDomanda, fileName, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @POST
    @Path("soccorso-istruttorio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response soccorsoIstruttorio(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda, @QueryParam("fileName") String fileName) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.soccorsoIstruttorio(session, req, idDomanda, fileName, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @GET
    @Path("soccorso-istruttorio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response isFlussosoccorsoIstruttorio(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            rb = Response.ok(applicationOIVService.isStatoFlussoSoccorsoIstruttorio(session, idDomanda));
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @GET
    @Path("scarica-soccorso-istruttorio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response scaricaSoccorsoIstruttorio(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            rb = Response.ok(applicationOIVService.scaricaSoccorsoIstruttorio(session, idDomanda));
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.warn("scarica-soccorso-istruttorio error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @GET
    @Path("scarica-preavviso-rigetto")
    @Produces(MediaType.APPLICATION_JSON)
    public Response scaricaPreavvisoRigetto(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            rb = Response.ok(applicationOIVService.scaricaPreavvisoRigetto(session, idDomanda));
        } catch (ClientMessageException | CMISApplicationException e) {
            LOGGER.warn("scarica-preavviso-rigetto error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @POST
    @Path("response-soccorso-istruttorio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response responseSoccorsoIstruttorio(@Context HttpServletRequest req, @FormParam("idDomanda") String idDomanda, @FormParam("idDocumento") String idDocumento) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.responseSoccorsoIstruttorio(session, req, idDomanda, idDocumento, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @POST
    @Path("response-preavviso-rigetto")
    @Produces(MediaType.APPLICATION_JSON)
    public Response responsePreavvisoRigetto(@Context HttpServletRequest req, @FormParam("idDomanda") String idDomanda, @FormParam("idDocumento") String idDocumento) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.responsePreavvisoRigetto(session, req, idDomanda, idDocumento, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }

    @POST
    @Path("comunicazioni")
    @Produces(MediaType.APPLICATION_JSON)
    public Response comunicazioni(@Context HttpServletRequest req, @QueryParam("idDomanda") String idDomanda, @QueryParam("fileName") String fileName, @QueryParam("type") ApplicationOIVService.PdfType type) throws IOException {
        ResponseBuilder rb;
        try {
            Session session = cmisService.getCurrentCMISSession(req);
            Map<String, Object> model = applicationOIVService.comunicazioni(session, req, idDomanda, fileName, type, cmisService.getCMISUserFromSession(req));
            rb = Response.ok(model);
        } catch (ClientMessageException | CMISApplicationException | TemplateException e) {
            LOGGER.warn("send error", e);
            rb = Response.status(Status.INTERNAL_SERVER_ERROR).entity(Collections.singletonMap("message", e.getMessage()));
        }
        return rb.build();
    }
}