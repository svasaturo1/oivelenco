/*
 * Copyright (C) 2019  Consiglio Nazionale delle Ricerche
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.cnr.si.cool.jconon.model;

import java.io.Serializable;
import java.util.Objects;

public class TipoUniversita implements Serializable {
    private String id;
    private String numero_classe;
    private String tipo_laurea;
    private String denominazione_classe;
    private String nome_gruppo_disciplinare;
   

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNumero_classe() {
		return numero_classe;
	}

	public void setNumero_classe(String numero_classe) {
		this.numero_classe = numero_classe;
	}

	public String getTipo_laurea() {
		return tipo_laurea;
	}

	public void setTipo_laurea(String tipo_laurea) {
		this.tipo_laurea = tipo_laurea;
	}

	public String getDenominazione_classe() {
		return denominazione_classe;
	}

	public void setDenominazione_classe(String denominazione_classe) {
		this.denominazione_classe = denominazione_classe;
	}
	
	public TipoUniversita() {
    }

  

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoUniversita that = (TipoUniversita) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(numero_classe, that.numero_classe) &&
                Objects.equals(tipo_laurea, that.tipo_laurea) &&
                Objects.equals(denominazione_classe, that.denominazione_classe) &&
                Objects.equals(nome_gruppo_disciplinare, that.nome_gruppo_disciplinare) ;
    }
    @Override
    public int hashCode() {

        return Objects.hash(id, numero_classe, tipo_laurea, denominazione_classe, nome_gruppo_disciplinare);
    }


    @Override
    public String toString() {
        return "Tipo Universita{" +
                "id='" + id + '\'' +
                ", numero_classe='" + numero_classe + '\'' +
                ", tipo_laurea='" + tipo_laurea + '\'' +
                ", denominazione_classe='" + denominazione_classe + '\'' +
                ", nome_gruppo_disciplinare='" + nome_gruppo_disciplinare + '\'' +
               '}';
    }

	public String getNome_gruppo_disciplinare() {
		return nome_gruppo_disciplinare;
	}

	public void setNome_gruppo_disciplinare(String nome_gruppo_disciplinare) {
		this.nome_gruppo_disciplinare = nome_gruppo_disciplinare;
	}

}
