/*
 * Copyright (C) 2019  Consiglio Nazionale delle Ricerche
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package it.cnr.si.cool.jconon.service.application;

import it.cnr.bulkinfo.BulkInfoImpl.FieldProperty;
import it.cnr.cool.cmis.model.ACLType;
import it.cnr.cool.cmis.model.CoolPropertyIds;
import it.cnr.cool.cmis.service.ACLService;
import it.cnr.cool.cmis.service.CMISService;
import it.cnr.cool.cmis.service.NodeVersionService;
import it.cnr.cool.security.service.UserService;
import it.cnr.cool.security.service.impl.alfresco.CMISUser;
import it.cnr.cool.util.Pair;
import it.cnr.cool.web.scripts.exception.CMISApplicationException;
import it.cnr.cool.web.scripts.exception.ClientMessageException;
import it.cnr.si.cool.jconon.cmis.model.JCONONFolderType;
import it.cnr.si.cool.jconon.cmis.model.JCONONPropertyIds;
import it.cnr.si.cool.jconon.service.PrintService;
import it.cnr.si.cool.jconon.service.cache.CompetitionFolderService;
import it.cnr.si.cool.jconon.service.call.CallService;
import it.cnr.si.opencmis.criteria.Criteria;
import it.cnr.si.opencmis.criteria.CriteriaFactory;
import it.cnr.si.opencmis.criteria.Order;
import it.cnr.si.opencmis.criteria.restrictions.Restrictions;
import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.client.bindings.spi.BindingSession;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisContentAlreadyExistsException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisStreamNotSupportedException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Primary
public class PrintOIVService extends PrintService {
	 public static final String EMAIL_DOMANDE_OIV = "EMAIL_DOMANDE_OIV";
	
    public static final String JCONON_SCHEDA_ANONIMA_DOCUMENT = "jconon_scheda_anonima:document";
    public static final String P_JCONON_APPLICATION_ASPECT_FASCIA_PROFESSIONALE_ATTRIBUITA = "P:jconon_application:aspect_fascia_professionale_attribuita";
    public static final String P_JCONON_APPLICATION_ASPECT_SBLOCCO_INVIO_DOMANDE = "P:jconon_application:aspect_sblocco_invio_domande";

    public static final String JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_A = "jconon_attachment:esperienza_professionale_a";
    public static final String JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_RUOLO = "jconon_attachment:esperienza_professionale_ruolo";
    public static final String JCONON_SCHEDA_ANONIMA_ESPERIENZA_PROFESSIONALE = "jconon_scheda_anonima:esperienza_professionale";
    public static final String JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_DATORE_LAVORO = "jconon_attachment:esperienza_professionale_datore_lavoro";
    public static final String EMAIL = "Email";
    public static final String EMAIL_REGISTRAZIONE = "Email Registrazione";
    public static final String INDIRIZZO_DI_REPERIBILITA = "Indirizzo di Reperibilita'";
    public static final String LAUREA = "Laurea";
    public static final String UNIVERSITA = "Università";
    public static final String DATA_DI_INIZIO_RAPPORTO_DI_LAVORO = "Data di inizio rapporto di lavoro";
    public static final String DATORE_DI_LAVORO_ATTUALE = "Datore di lavoro attuale";
    public static final String RUOLO = "Ruolo";
    public static final String SETTORE = "Settore";
    public static final String DIPENDENTE_PUBBLICO = "Dipendente Pubblico";
    public static final String POSIZIONE = "Posizione";
    public static final String OCCUPATO = "Occupato";
    public static final String FASCIA_PROFESSIONALE_VALIDATA = "Fascia Professionale Validata";
    public static final String DATORE_DI_LAVORO = "Datore di Lavoro";
    public static final String ANNOTAZIONE = "Annotazione";
    public static final String DATA_DI_NASCITA = "Data di nascita";
    public static final String SESSO = "Sesso";
    public static final String NAZIONE_DI_NASCITA = "Nazione di nascita";
    public static final String LUOGO_DI_NASCITA = "Luogo di nascita";
    public static final String PROV_DI_NASCITA = "Prov. di nascita";
    public static final String NAZIONE_DI_RESIDENZA = "Nazione di Residenza";
    public static final String PROVINCIA_DI_RESIDENZA = "Provincia di Residenza";
    public static final String COMUNE_DI_RESIDENZA = "Comune di Residenza";
    public static final String INDIRIZZO_DI_RESIDENZA = "Indirizzo di Residenza";
    public static final String CAP_DI_RESIDENZA = "CAP di Residenza";
    public static final String CODICE_FISCALE = "Codice Fiscale";
    public static final String EMAIL_PEC = "Email PEC";
    public static final String NAZIONE_REPERIBILITA = "Nazione Reperibilita'";
    public static final String PROVINCIA_DI_REPERIBILITA = "Provincia di Reperibilita'";
    public static final String COMUNE_DI_REPERIBILITA = "Comune di Reperibilita'";
    public static final String CAP_DI_REPERIBILITA = "CAP di Reperibilita'";
    public static final String TELEFONO = "Telefono";
    public static final String DATA_INVIO_DOMANDA = "Data Invio Domanda";
    public static final String FASCIA_PROFESSIONALE_ATTRIBUITA = "Fascia Professionale Attribuita";
    public static final String NOME = "Nome";
    public static final String COGNOME = "Cognome";
    public static final String DATA_ULTIMA_MODIFICA = "Data Ultima Modifica";
    public static final String DATA_INVIO_RINNOVO = "Data Invio Rinnovo";
    public static final String DATA_RINNOVO = "Data Rinnovo";
    public static final String DATA_CAMBIO_FASCIA = "Data Cambio Fascia";
    public static final String DATA_RIMOZIONE_ELENCO = "Data Cancellazione Elenco";
    public static final String DATA_INVIO_CAMBIO_FASCIA = "Data Invio Cambio Fascia";
    public static final String DATA_APPROVAZIONE_RINNOVO = "Data Approvazione Rinnovo";
    public static final String DATA_PRIMA_COMUNICAZIONE_RINNOVO = "Data Comunicazione Rinnovo 30gg";
    public static final String DATA_SECONDA_COMUNICAZIONE_RINNOVO = "Data Comunicazione Rinnovo 7gg";
    public static final String DATA_ISCRIZIONE_IN_ELENCO = "Data Iscrizione in Elenco";
    public static final String TIPOLOGIA_ESPERIENZA_PROFESSIONALE_OIV = "Tipologia esperienza (Professionale/OIV)";
    public static final String AREA_DI_SPECIALIZZAZIONE = "Area di specializzazione";
    public static final String ATTIVITÀ_SVOLTA_NELL_AREA_DI_SPECIALIZZAZIONE_INDICATA = "Attività svolta nell’area di specializzazione indicata";
    public static final String DATA_INIZIO_TIPOLOGIA_ESPERIENZA = "Data inizio(Tipologia esperienza)";
    public static final String DATA_FINE_TIPOLOGIA_ESPERIENZA = "Data fine(Tipologia esperienza)";
    public static final String NON_COERENTE = "Non coerente";
    public static final String MOTIVAZIONE = "Motivazione";
    public static final String NON_COERENTE_RINNOVO = "Non coerente rinnovo";
    public static final String VISIBILE = "Visibile utente";
    public static final String DA = "Da";
    public static final String A = "A";
    public static final String AMMINISTRAZIONE_PUBBLICA = "Amministrazione pubblica";
    public static final String SEDE = "Sede";
    public static final String COMUNE = "Comune";
    public static final String INDIRIZZO = "Indirizzo";
    public static final String CAP = "CAP";
    public static final String N_DIPENDENTI = "N. dipendenti";
    public static final String DATORE_DI_LAVORO_COMMITTENTE = "Datore di Lavoro/Committente";
    public static final String STATO = "Stato";
    public static final String ANNOTAZIONI = "Annotazioni";
    public static final String JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO = "jconon_application:progressivo_iscrizione_elenco";
    public static final String JCONON_APPLICATION_USER = "jconon_application:user";
    public static final String SHEET_DOMANDE = "domande";
    private static final Logger LOGGER = LoggerFactory.getLogger(PrintService.class);
    private static final String NO = "No";
    private static final String SI = "Si";
    private final static String PRECEDENTE_INCARICO_OIV = "Precedente Incarico OIV\n";
    private final static String ESPERIENZA_PROFESSIONALE = "Esperienza professionale\n";
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"),
            dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private List<String> headCSVApplication = Arrays.asList(
            "Id",
            DATA_ISCRIZIONE_IN_ELENCO,
            DATA_RINNOVO,
            DATA_CAMBIO_FASCIA,
            DATA_RIMOZIONE_ELENCO,
            DATA_APPROVAZIONE_RINNOVO,
            COGNOME,
            NOME,
            DATA_DI_NASCITA,
            SESSO,
            NAZIONE_DI_NASCITA,
            LUOGO_DI_NASCITA,
            PROV_DI_NASCITA,
            NAZIONE_DI_RESIDENZA,
            PROVINCIA_DI_RESIDENZA,
            COMUNE_DI_RESIDENZA,
            INDIRIZZO_DI_RESIDENZA,
            CAP_DI_RESIDENZA,
            CODICE_FISCALE,
            EMAIL,
            EMAIL_REGISTRAZIONE,
            EMAIL_PEC,
            NAZIONE_REPERIBILITA,
            PROVINCIA_DI_REPERIBILITA,
            COMUNE_DI_REPERIBILITA,
            INDIRIZZO_DI_REPERIBILITA,
            CAP_DI_REPERIBILITA,
            TELEFONO,
            DATA_INVIO_DOMANDA,
            DATA_INVIO_RINNOVO,
            DATA_INVIO_CAMBIO_FASCIA,
            DATA_ULTIMA_MODIFICA,
            DATA_PRIMA_COMUNICAZIONE_RINNOVO,
            DATA_SECONDA_COMUNICAZIONE_RINNOVO,
            LAUREA,
            UNIVERSITA,
            FASCIA_PROFESSIONALE_ATTRIBUITA,
            RUOLO,
            DATORE_DI_LAVORO,
            ANNOTAZIONE,
            FASCIA_PROFESSIONALE_VALIDATA,
            OCCUPATO,
            POSIZIONE,
            DIPENDENTE_PUBBLICO,
            SETTORE,
            RUOLO,
            DATORE_DI_LAVORO_ATTUALE,
            DATA_DI_INIZIO_RAPPORTO_DI_LAVORO
    );
    private List<String> headDetailCSVApplication = Stream.concat(headCSVApplication.stream()
                    .filter(x -> !Arrays.asList(
                            DATA_DI_NASCITA,
                            SESSO,
                            NAZIONE_DI_NASCITA,
                            LUOGO_DI_NASCITA,
                            PROV_DI_NASCITA,
                            NAZIONE_DI_RESIDENZA,
                            PROVINCIA_DI_RESIDENZA,
                            COMUNE_DI_RESIDENZA,
                            INDIRIZZO_DI_RESIDENZA,
                            CAP_DI_RESIDENZA,
                            CODICE_FISCALE,
                            EMAIL,
                            EMAIL_REGISTRAZIONE,
                            EMAIL_PEC,
                            NAZIONE_REPERIBILITA,
                            COMUNE_DI_REPERIBILITA,
                            PROVINCIA_DI_REPERIBILITA,
                            INDIRIZZO_DI_REPERIBILITA,
                            CAP_DI_REPERIBILITA,
                            TELEFONO,
                            LAUREA,
                            UNIVERSITA,
                            RUOLO,
                            ANNOTAZIONE,
                            FASCIA_PROFESSIONALE_VALIDATA,
                            DATORE_DI_LAVORO,
                            OCCUPATO,
                            POSIZIONE,
                            DIPENDENTE_PUBBLICO,
                            SETTORE,
                            RUOLO,
                            DATORE_DI_LAVORO_ATTUALE,
                            DATA_DI_INIZIO_RAPPORTO_DI_LAVORO
                    ).contains(x)),
            Arrays.asList(
                    TIPOLOGIA_ESPERIENZA_PROFESSIONALE_OIV,
                    AREA_DI_SPECIALIZZAZIONE,
                    ATTIVITÀ_SVOLTA_NELL_AREA_DI_SPECIALIZZAZIONE_INDICATA,
                    RUOLO,

                    ESPERIENZA_PROFESSIONALE + DATORE_DI_LAVORO_COMMITTENTE,
                    PRECEDENTE_INCARICO_OIV + AMMINISTRAZIONE_PUBBLICA,
                    SEDE,
                    COMUNE,

                    DATA_INIZIO_TIPOLOGIA_ESPERIENZA,
                    DATA_FINE_TIPOLOGIA_ESPERIENZA,

                    PRECEDENTE_INCARICO_OIV + N_DIPENDENTI,
                    
                    NON_COERENTE,
                    MOTIVAZIONE,
                    VISIBILE,
                    
                    NON_COERENTE_RINNOVO,
                    MOTIVAZIONE,
                    VISIBILE,
                    
                    ANNOTAZIONE,
                    FASCIA_PROFESSIONALE_VALIDATA
            ).stream()).collect(Collectors.toList());
    private List<String> headCSVElenco = Arrays.asList(
            "Id", NOME, COGNOME, DATA_ISCRIZIONE_IN_ELENCO);
    private List<String> headCSVApplicationAllIscritti = Arrays.asList( 		// scarico con esperienze solo via mail
            "Id",
            DATA_ISCRIZIONE_IN_ELENCO,
            COGNOME,
            NOME,
            DATA_DI_NASCITA,
            SESSO,
            NAZIONE_DI_NASCITA,
            LUOGO_DI_NASCITA,
            PROV_DI_NASCITA,
            NAZIONE_DI_RESIDENZA,
            PROVINCIA_DI_RESIDENZA,
            COMUNE_DI_RESIDENZA,
            INDIRIZZO_DI_RESIDENZA,
            CAP_DI_RESIDENZA,
            CODICE_FISCALE,
            EMAIL,
         //   EMAIL_REGISTRAZIONE,
            EMAIL_PEC,
            NAZIONE_REPERIBILITA,
            PROVINCIA_DI_REPERIBILITA,
            COMUNE_DI_REPERIBILITA,
            INDIRIZZO_DI_REPERIBILITA,
            CAP_DI_REPERIBILITA,
            TELEFONO,
            DATA_INVIO_DOMANDA,
            DATA_ULTIMA_MODIFICA,
            LAUREA,
            UNIVERSITA,
            FASCIA_PROFESSIONALE_ATTRIBUITA,
            FASCIA_PROFESSIONALE_VALIDATA,
            OCCUPATO,
            POSIZIONE,
            DIPENDENTE_PUBBLICO,
            SETTORE,
            RUOLO,
            DATORE_DI_LAVORO_ATTUALE,
            DATA_DI_INIZIO_RAPPORTO_DI_LAVORO,
            PRECEDENTE_INCARICO_OIV + DA,
            PRECEDENTE_INCARICO_OIV + A,
            PRECEDENTE_INCARICO_OIV + AMMINISTRAZIONE_PUBBLICA,
            PRECEDENTE_INCARICO_OIV + SEDE,
            PRECEDENTE_INCARICO_OIV + COMUNE,
            PRECEDENTE_INCARICO_OIV + INDIRIZZO,
            PRECEDENTE_INCARICO_OIV + CAP,
            PRECEDENTE_INCARICO_OIV + TELEFONO,
            PRECEDENTE_INCARICO_OIV + N_DIPENDENTI,
            PRECEDENTE_INCARICO_OIV + RUOLO,
            ESPERIENZA_PROFESSIONALE + DA,
            ESPERIENZA_PROFESSIONALE + A,
            ESPERIENZA_PROFESSIONALE + AREA_DI_SPECIALIZZAZIONE,
            ESPERIENZA_PROFESSIONALE + ATTIVITÀ_SVOLTA_NELL_AREA_DI_SPECIALIZZAZIONE_INDICATA,
            ESPERIENZA_PROFESSIONALE + DATORE_DI_LAVORO_COMMITTENTE,
            ESPERIENZA_PROFESSIONALE + RUOLO,
            ESPERIENZA_PROFESSIONALE + COMUNE,
            ESPERIENZA_PROFESSIONALE + STATO,
            ANNOTAZIONI
    );
    @Autowired
    private CMISService cmisService;
    @Autowired
    private UserService userService;
    @Autowired
    private CompetitionFolderService competitionService;
    @Autowired
    private NodeVersionService nodeVersionService;
    @Autowired
    private CallService callService;
    @Autowired
    private ACLService aclService;
    @Override
    public Pair<String, byte[]> printApplicationImmediate(Session cmisSession,
                                                          String nodeRef, String contextURL, Locale locale) {
        Pair<String, byte[]> result = super.printApplicationImmediate(cmisSession, nodeRef, contextURL, locale);
        Folder application = (Folder) cmisSession.getObject(nodeRef);
        String archiviaRicevutaReportModel = archiviaRicevutaReportModel(cmisService.createAdminSession(), application, new ByteArrayInputStream(result.getSecond()),
                getNameRicevutaReportModel(cmisSession, application, locale), false);
        cmisService.createAdminSession().getObject(archiviaRicevutaReportModel).updateProperties(Collections.emptyMap(),
                Collections.singletonList(P_JCONON_APPLICATION_ASPECT_FASCIA_PROFESSIONALE_ATTRIBUITA), Collections.emptyList());
        return result;
    }

    
    @Override
    public Pair<String, byte[]> printRinnovoApplicationImmediate(Session cmisSession,
                                                          String nodeRef, String contextURL, Locale locale) {
        Pair<String, byte[]> result = super.printRinnovoApplicationImmediate(cmisSession, nodeRef, contextURL, locale);
        Folder application = (Folder) cmisSession.getObject(nodeRef);
        String archiviaRicevutaReportModel = archiviaRicevutaRinnovoReportModel(cmisService.createAdminSession(), application, new ByteArrayInputStream(result.getSecond()),
        		getNameRicevutaRinnovoReportModel(cmisSession, application, locale), false);
        cmisService.createAdminSession().getObject(archiviaRicevutaReportModel).updateProperties(Collections.emptyMap(),
                Collections.singletonList(P_JCONON_APPLICATION_ASPECT_FASCIA_PROFESSIONALE_ATTRIBUITA), Collections.emptyList());
        return result;
    }
    
    @Override
    public Pair<String, byte[]> printCambioFasciaApplicationImmediate(Session cmisSession,
                                                          String nodeRef, String contextURL, Locale locale) {
        Pair<String, byte[]> result = super.printCambioFasciaApplicationImmediate(cmisSession, nodeRef, contextURL, locale);
        Folder application = (Folder) cmisSession.getObject(nodeRef);
        String archiviaRicevutaReportModel = archiviaRicevutaCambioFasciaReportModel(cmisService.createAdminSession(), application, new ByteArrayInputStream(result.getSecond()),
        		getNameRicevutaCambioFasciaReportModel(cmisSession, application, locale), false);
        cmisService.createAdminSession().getObject(archiviaRicevutaReportModel).updateProperties(Collections.emptyMap(),
                Collections.singletonList(P_JCONON_APPLICATION_ASPECT_FASCIA_PROFESSIONALE_ATTRIBUITA), Collections.emptyList());
        return result;
    }
    
    @Override
    protected String getTitle(int i, Dichiarazioni dichiarazione) {
        if (dichiarazione.equals(Dichiarazioni.datiCNR) || dichiarazione.equals(Dichiarazioni.ulterioriDati))
            return "";
        return super.getTitle(i, dichiarazione);
    }

    @Override
    protected int getFirstLetterOfDichiarazioni() {
        return 97;
    }

	/*
	 * @Override public String getNameRicevutaReportModel(Session cmisSession,
	 * Folder application, Locale locale) throws CMISApplicationException { Folder
	 * call = (Folder) cmisSession.getObject(application.getParentId()); LocalDate
	 * data =
	 * Optional.ofNullable(application.<Calendar>getPropertyValue((JCONONPropertyIds
	 * .APPLICATION_DATA_DOMANDA.value()))). map(x ->
	 * x.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).orElse(LocalDate.
	 * now()); return call.getPropertyValue(JCONONPropertyIds.CALL_CODICE.value()) +
	 * "-RD-" +
	 * application.getPropertyValue(JCONONPropertyIds.APPLICATION_USER.value()) +
	 * "-" + data + ".pdf"; }
	 */
    
	@Override
	public String getNameRicevutaReportModel(Session cmisSession, Folder application, Locale locale)
			throws CMISApplicationException {
		Folder call = (Folder) cmisSession.getObject(application.getParentId());
//        LocalDateTime data = Optional.ofNullable(application.<Calendar>getPropertyValue((JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value()))).
//                map(x -> x.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()).orElse(LocalDateTime.now());
//    
		LOGGER.error(" name ricevuta stato domanda "+application.getPropertyValue((JCONONPropertyIds.APPLICATION_STATO_DOMANDA.value())));
		LocalDateTime data = Optional
				.ofNullable(
						application.<Calendar>getPropertyValue((JCONONPropertyIds.APPLICATION_DATA_ULTIMA_MODIFICA.value())))
				.map(x -> x.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()).orElse(LocalDateTime.now());

		String dataNomeFile = data.toString();
		dataNomeFile = dataNomeFile.replaceAll(":", "_");
		dataNomeFile = dataNomeFile.replaceAll(":", "_");

		String dataFomattata = (String) dataNomeFile.subSequence(0, dataNomeFile.indexOf("."));

		return call.getPropertyValue(JCONONPropertyIds.CALL_CODICE.value()) + "-RD-"
				+ application.getPropertyValue(JCONONPropertyIds.APPLICATION_USER.value()) + "-" + dataFomattata
				+ ".pdf";
	}
	
	@Override
	public String getNameRicevutaCambioFasciaReportModel(Session cmisSession, Folder application, Locale locale)
			throws CMISApplicationException {
		Folder call = (Folder) cmisSession.getObject(application.getParentId());
//        LocalDateTime data = Optional.ofNullable(application.<Calendar>getPropertyValue((JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value()))).
//                map(x -> x.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()).orElse(LocalDateTime.now());
//    
		LOGGER.error(" name ricevuta stato domanda cambio "+application.getPropertyValue((JCONONPropertyIds.APPLICATION_STATO_DOMANDA.value())));
		LocalDateTime data = Optional
				.ofNullable(     //       DATA_RINNOVO,
						application.<Calendar>getPropertyValue((JCONONPropertyIds.APPLICATION_DATA_ULTIMA_MODIFICA.value())))
				.map(x -> x.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()).orElse(LocalDateTime.now());

		String dataNomeFile = data.toString();
		dataNomeFile = dataNomeFile.replaceAll(":", "_");
		dataNomeFile = dataNomeFile.replaceAll(":", "_");

		String dataFomattata = (String) dataNomeFile.subSequence(0, dataNomeFile.indexOf("."));

		return call.getPropertyValue(JCONONPropertyIds.CALL_CODICE.value()) + "-CAMBIO_FASCIA-"
				+ application.getPropertyValue(JCONONPropertyIds.APPLICATION_USER.value()) + "-" + dataFomattata
				+ ".pdf";
	}
	
	
	@Override
	public String getNameRicevutaRinnovoReportModel(Session cmisSession, Folder application, Locale locale)
			throws CMISApplicationException {
		Folder call = (Folder) cmisSession.getObject(application.getParentId());
//        LocalDateTime data = Optional.ofNullable(application.<Calendar>getPropertyValue((JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value()))).
//                map(x -> x.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()).orElse(LocalDateTime.now());
//    
		LOGGER.error(" name ricevuta stato domanda "+application.getPropertyValue((JCONONPropertyIds.APPLICATION_STATO_DOMANDA.value())));
		LocalDateTime data = Optional
				.ofNullable(     //       DATA_RINNOVO,
						application.<Calendar>getPropertyValue((JCONONPropertyIds.APPLICATION_DATA_ULTIMA_MODIFICA.value())))
				.map(x -> x.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()).orElse(LocalDateTime.now());

		String dataNomeFile = data.toString();
		dataNomeFile = dataNomeFile.replaceAll(":", "_");
		dataNomeFile = dataNomeFile.replaceAll(":", "_");

		String dataFomattata = (String) dataNomeFile.subSequence(0, dataNomeFile.indexOf("."));

		return call.getPropertyValue(JCONONPropertyIds.CALL_CODICE.value()) + "-RINNOVO-"
				+ application.getPropertyValue(JCONONPropertyIds.APPLICATION_USER.value()) + "-" + dataFomattata
				+ ".pdf";
	}
	
    
    private OperationContext getMinimalContext(Session session) {
        OperationContext context = new OperationContextImpl(session.getDefaultContext());
        context.setIncludeAcls(false);
        context.setIncludeAllowableActions(false);
        context.setIncludePathSegments(false);
        return context;
    }

    private Stream<Folder> getAllApplication(Session session, String stato, String esclusioneRinuncia, Boolean attive) {
        Criteria criteria = CriteriaFactory.createCriteria(JCONONFolderType.JCONON_CALL.queryName());
        criteria.addColumn(PropertyIds.OBJECT_ID);
        criteria.add(Restrictions.eq(JCONONPropertyIds.CALL_CODICE.value(), "OIV"));
        ItemIterable<QueryResult> iterable = criteria.executeQuery(session, false, session.getDefaultContext());
        List<Folder> applicationList = new ArrayList<Folder>();
        for (QueryResult queryResult : iterable.getPage(Integer.MAX_VALUE)) {
            Folder call = (Folder) session.getObject(String.valueOf(queryResult.getPropertyById(PropertyIds.OBJECT_ID).getFirstValue()));
            final ItemIterable<CmisObject> cmisObjects = call.getChildren(getMinimalContext(session));
            for (CmisObject application : cmisObjects.getPage(Integer.MAX_VALUE)) {
                if (!application.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER))
                    continue;
                if (!application.getType().getId().equals(JCONONFolderType.JCONON_APPLICATION.value()))
                    continue;
                if (application.getPropertyValue(JCONONPropertyIds.APPLICATION_STATO_DOMANDA.value()).equals("I"))
                    continue;
                if (Optional.ofNullable(stato).isPresent() && !application.getPropertyValue(JCONONPropertyIds.APPLICATION_STATO_DOMANDA.value()).equals(stato))
                    continue;
                if (Optional.ofNullable(esclusioneRinuncia).isPresent() &&
                        Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_ESCLUSIONE_RINUNCIA.value()))
                                .map(o -> !o.equals(esclusioneRinuncia))
                                .orElse(true))
                    continue;
                if (Optional.ofNullable(attive).isPresent() && application.getPropertyValue(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO) == null)
                    continue;
            
                applicationList.add((Folder) application);
            }
        }
        return applicationList.stream().sorted((app1, app2) ->
                Optional.ofNullable(app1.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value())).orElse(Calendar.getInstance()).compareTo(
                        Optional.ofNullable(app2.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value())).orElse(Calendar.getInstance())));
    }
    
  
	private Stream<Folder> getAllApplicationFiltered(Session session, String stato, String filtroDataDa, String filtroDataA, String filtro, String esclusioneRinuncia, Boolean attive) {
        Criteria criteria = CriteriaFactory.createCriteria(JCONONFolderType.JCONON_CALL.queryName());
        criteria.addColumn(PropertyIds.OBJECT_ID);
        criteria.add(Restrictions.eq(JCONONPropertyIds.CALL_CODICE.value(), "OIV"));
        ItemIterable<QueryResult> iterable = criteria.executeQuery(session, false, session.getDefaultContext());
        List<Folder> applicationList = new ArrayList<Folder>();
        for (QueryResult queryResult : iterable.getPage(Integer.MAX_VALUE)) {
            Folder call = (Folder) session.getObject(String.valueOf(queryResult.getPropertyById(PropertyIds.OBJECT_ID).getFirstValue()));
            final ItemIterable<CmisObject> cmisObjects = call.getChildren(getMinimalContext(session));
            for (CmisObject application : cmisObjects.getPage(Integer.MAX_VALUE)) {
                if (!application.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER))
                    continue;
                if (!application.getType().getId().equals(JCONONFolderType.JCONON_APPLICATION.value()))
                    continue;
                if (application.getPropertyValue(JCONONPropertyIds.APPLICATION_STATO_DOMANDA.value()).equals("I"))
                    continue;
                if (Optional.ofNullable(stato).isPresent() && !application.getPropertyValue(JCONONPropertyIds.APPLICATION_STATO_DOMANDA.value()).equals(stato))
                    continue;
              
//                if (!Optional.ofNullable(filtroDataDa).isPresent())
//                	LOGGER.error(" filtro xls data da "+Optional.ofNullable(filtroDataDa));
//                LOGGER.error("2 filtro xls data da "+Optional.ofNullable(filtroDataDa).isPresent());
//                LOGGER.error(" filtro xls data a "+Optional.ofNullable(filtroDataA));
//                LOGGER.error(" filtro xls "+Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_RINNOVO_ELENCO.value())).isPresent());
                if ( ( Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Rinnovo inviato") &&
                		!Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_INVIO_RINNOVO_ELENCO.value())).isPresent() ) ||
                		( Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Rinnovo inviato") && Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_RINNOVO_ELENCO.value())).isPresent()) )               
                 continue;
               
                if (Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Rinnovato") &&
                    		!Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_RINNOVO_ELENCO.value())).isPresent())       	
                 continue;
                
               
                if ( ( Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Cambio Fascia Inviato") &&
                		!Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_INVIO_CAMBIO_FASCIA.value())).isPresent() ) ||
                		( Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Cambio Fascia Inviato") &&	Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CAMBIO_FASCIA.value())).isPresent()) )               
                 continue;
              
                if (Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Fascia cambiata") &&
                		!Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CAMBIO_FASCIA.value())).isPresent())       	
                	continue;
             
//                if (Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Iscritto") &&
//                    		!Optional.ofNullable(application.getPropertyValue("jconon_application:progressivo_iscrizione_elenco")).isPresent())
//                    continue; 
                if (Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Escluso") &&
                    		!Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_ESCLUSIONE_RINUNCIA.value())).isPresent())               
                 continue;
             
                if ( ( Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Inviato") &&
                    		!Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value())).isPresent() ) || 
                    			(  Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Inviato") && application.getPropertyValue(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO) != null) )              	
                 continue;
              
                if (Optional.ofNullable(esclusioneRinuncia).isPresent() &&
                        Optional.ofNullable(application.getPropertyValue(JCONONPropertyIds.APPLICATION_ESCLUSIONE_RINUNCIA.value()))
                                .map(o -> !o.equals(esclusioneRinuncia))
                                .orElse(true))                	
                    continue;
                
                if (Optional.ofNullable(attive).isPresent() && application.getPropertyValue(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO) == null)
                    continue;    
                
                if ( Optional.ofNullable(filtroDataDa).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Rinnovo inviato") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_INVIO_RINNOVO_ELENCO.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataA).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Rinnovato") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_RINNOVO_ELENCO.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataDa).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Cambio Fascia Inviato") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_INVIO_CAMBIO_FASCIA.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataA).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Cambio Fascia Inviato") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_INVIO_CAMBIO_FASCIA.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataDa).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Fascia cambiata") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CAMBIO_FASCIA.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataA).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Fascia cambiata") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CAMBIO_FASCIA.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataDa).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Inviato") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataA).isPresent() && Optional.ofNullable(filtro).isPresent() && filtro.equalsIgnoreCase("Inviato") &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataDa).isPresent() && Optional.ofNullable(attive).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_ISCRIZIONE_ELENCO.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                	continue;
                
                if ( Optional.ofNullable(filtroDataA).isPresent() && Optional.ofNullable(attive).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_ISCRIZIONE_ELENCO.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                	continue;
                
                if ( Optional.ofNullable(filtroDataDa).isPresent() && Optional.ofNullable(esclusioneRinuncia).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_ESCLUSIONE.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataA).isPresent() && Optional.ofNullable(esclusioneRinuncia).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_ESCLUSIONE.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataDa).isPresent() && Optional.ofNullable(stato).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CREAZIONE.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataA).isPresent() && Optional.ofNullable(stato).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CREAZIONE.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                   	 continue;
                if ( Optional.ofNullable(filtroDataDa).isPresent() && !Optional.ofNullable(filtro).isPresent()  && !Optional.ofNullable(stato).isPresent()  && !Optional.ofNullable(attive).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CREAZIONE.value()).getTimeInMillis() < Long.valueOf(filtroDataDa) ) ) 
                	continue;
                
                if ( Optional.ofNullable(filtroDataA).isPresent() && !Optional.ofNullable(stato).isPresent()  && !Optional.ofNullable(filtro).isPresent()  && !Optional.ofNullable(attive).isPresent()  &&
                		(application.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_CREAZIONE.value()).getTimeInMillis() > Long.valueOf(filtroDataA) ) ) 
                	continue;
                
                LOGGER.error("filtro xls  attive con data "+   application.<String>getPropertyValue("jconon_application:cognome"));
                applicationList.add((Folder) application);
            }
        }
        return applicationList.stream().sorted((app1, app2) ->
                Optional.ofNullable(app1.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value())).orElse(Calendar.getInstance()).compareTo(
                        Optional.ofNullable(app2.<Calendar>getPropertyValue(JCONONPropertyIds.APPLICATION_DATA_DOMANDA.value())).orElse(Calendar.getInstance())));
     
    }

    private int generateAllEsperienze(Session session, Folder applicationObject, int applicationNumber, int index, CMISUser user, HSSFSheet sheet) {
        Criteria criteriaOIV = CriteriaFactory.createCriteria(JCONON_SCHEDA_ANONIMA_DOCUMENT);
        criteriaOIV.add(Restrictions.inTree(applicationObject.getId()));
        ItemIterable<QueryResult> iterableOIV = criteriaOIV.executeQuery(session, false, getMinimalContext(session));
        for (QueryResult oiv : iterableOIV.getPage(Integer.MAX_VALUE)) {
            Document oivObject = (Document) session.getObject(String.valueOf(oiv.getPropertyById(PropertyIds.OBJECT_ID).getFirstValue()), getMinimalContext(session));
            getRecordCSV(session, applicationObject, oivObject, applicationNumber, user, sheet, null, null, index++, true);
        }
        return index;
    }

    private int generateLastEsperienze(Session session, Folder applicationObject, int applicationNumber, int index, CMISUser user, HSSFSheet sheet) {
        QueryResult lastEsperienza = getLastEsperienza(session, applicationObject, getMinimalContext(session));
        getRecordCSV(session, applicationObject, null, applicationNumber, user, sheet,
                Optional.ofNullable(lastEsperienza).map(x -> x.<String>getPropertyValueById(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_RUOLO)).orElse(""),
                Optional.ofNullable(lastEsperienza).map(x -> x.<String>getPropertyValueById(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_DATORE_LAVORO)).orElse(""),
                index++, false);
        return index;
    }

    public HSSFWorkbook generateXLS(Session session, String stato, String esclusioneRinuncia, Boolean attive, boolean detail, boolean withEsperienze) {
        HSSFWorkbook wb = createHSSFWorkbook(detail ? headDetailCSVApplication : headCSVApplication);
        HSSFSheet sheet = wb.getSheet(SHEET_DOMANDE);
        LOGGER.error("scarico last "+detail);
        LOGGER.error("scarico last "+sheet);
        int index = 1;
        Stream<Folder> sorted = getAllApplication(session, stato, esclusioneRinuncia, attive);
        if (withEsperienze) {
            int applicationNumber = 0;
            for (Folder applicationObject : sorted.collect(Collectors.toList())) {
            	CMISUser user = userService.loadUserForConfirm(applicationObject.getPropertyValue(JCONON_APPLICATION_USER));
                applicationNumber++;
                if (detail) {
                    index = generateAllEsperienze(session, applicationObject, applicationNumber, index, null, sheet);
                } else {
                    index = generateLastEsperienze(session, applicationObject, applicationNumber, index, user, sheet);
                }
            }
        }
        autoSizeColumns(wb);
        return wb;
    }
    
    public HSSFWorkbook generateXLSFiltrati(Session session, String stato, String filtro, String filtroDataDa, String filtroDataA, String esclusioneRinuncia, Boolean attive, boolean detail, boolean withEsperienze) {
        HSSFWorkbook wb = createHSSFWorkbook(detail ? headDetailCSVApplication : headCSVApplication);
        HSSFSheet sheet = wb.getSheet(SHEET_DOMANDE);
        int index = 1;
        Stream<Folder> sorted = getAllApplicationFiltered(session, stato,  filtroDataDa, filtroDataA, filtro, esclusioneRinuncia, attive);
        if (withEsperienze) {
            int applicationNumber = 0;
            for (Folder applicationObject : sorted.collect(Collectors.toList())) {
                applicationNumber++;
                if (detail) {
                    index = generateAllEsperienze(session, applicationObject, applicationNumber, index, null, sheet);
                } else {
                    index = generateLastEsperienze(session, applicationObject, applicationNumber, index, null, sheet);
                }
            }
        }
        autoSizeColumns(wb);
        return wb;
    }

    public HSSFWorkbook createHSSFWorkbookAllEsperienze() {
        return createHSSFWorkbook(headDetailCSVApplication);
    }

    public HSSFWorkbook createHSSFWorkbookLastEsperienze() {
        return createHSSFWorkbook(headCSVApplication);
    }

    public void generateXLS(Session session, HSSFWorkbook wbAllEsperienze, HSSFWorkbook wbLastEsperienze) {
    	HSSFSheet sheetAllEsperienze = null;
    	if( wbAllEsperienze != null )
    		sheetAllEsperienze = wbAllEsperienze.getSheet(SHEET_DOMANDE);
        HSSFSheet sheetLastEsperienze = null;
        if( wbLastEsperienze != null )
        	sheetLastEsperienze = wbLastEsperienze.getSheet(SHEET_DOMANDE);
        int indexAllEsperienze = 1;
        int indexLastEsperienze = 1;
        Stream<Folder> sorted = getAllApplication(session, ApplicationService.StatoDomanda.CONFERMATA.getValue(), null, null);
        int applicationNumber = 0;
        for (Folder applicationObject : sorted.collect(Collectors.toList())) {
            CMISUser user = userService.loadUserForConfirm(applicationObject.getPropertyValue(JCONON_APPLICATION_USER));
            LOGGER.error("scarico user "+user);
            LOGGER.error("scarico user "+applicationObject.getPropertyValue(JCONON_APPLICATION_USER));
            applicationNumber++;
            if( sheetAllEsperienze != null )
            	indexAllEsperienze = generateAllEsperienze(session, applicationObject, applicationNumber, indexAllEsperienze, user, sheetAllEsperienze);
            if( sheetLastEsperienze != null )
            	indexLastEsperienze = generateLastEsperienze(session, applicationObject, applicationNumber, indexLastEsperienze, user, sheetLastEsperienze);
        }
        if( wbAllEsperienze != null )
        	autoSizeColumns(wbAllEsperienze);
        if( wbLastEsperienze != null )
        	autoSizeColumns(wbLastEsperienze);
    }
    
    public Map<String, Object> extractionApplicationForAllIscritti(
            Session session, String query, String contexURL, String userId)
            throws IOException {
        Map<String, Object> model = new HashMap<String, Object>();
        HSSFWorkbook wb = generateXLSAllIscritti(session, query);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        wb.write(stream);
        Files.write(Paths.get("/home/mspasiano/iscritti_oiv.xls"), stream.toByteArray());

        //Document doc = createXLSDocument(session, wb, userId);
        //model.put("objectId", doc.getId());
        //model.put("nameBando", "OIV");
        return model;
    }

    public HSSFWorkbook generateXLSAllIscritti(Session session, String query) {
        HSSFWorkbook wb = createHSSFWorkbook(headCSVApplicationAllIscritti);
        HSSFSheet sheet = wb.getSheet(SHEET_DOMANDE);
        int index = 1;
        ItemIterable<QueryResult> applications = session.query(query, false);
        OperationContext context = session.getDefaultContext();
        context.setIncludeAcls(true);
        int applicationNumber = 0;
        for (QueryResult application : applications.getPage(Integer.MAX_VALUE)) {
            Folder applicationObject = (Folder) session.getObject(String.valueOf(application.getPropertyById(PropertyIds.OBJECT_ID).getFirstValue()), context);
            CMISUser user = new CMISUser("admin");// userService.loadUserForConfirm(applicationObject.getPropertyValue("jconon_application:user"));
            applicationNumber++;
            Criteria criteriaOIV = CriteriaFactory.createCriteria(JCONON_SCHEDA_ANONIMA_DOCUMENT);
            criteriaOIV.add(Restrictions.inFolder(applicationObject.getId()));
            ItemIterable<QueryResult> iterableOIV = criteriaOIV.executeQuery(session, false, session.getDefaultContext());
            for (QueryResult oiv : iterableOIV.getPage(Integer.MAX_VALUE)) {
                Document oivObject = (Document) session.getObject(String.valueOf(oiv.getPropertyById(PropertyIds.OBJECT_ID).getFirstValue()));
                getRecordCSVAllIscritti(session, applicationObject, oivObject, applicationNumber, user, sheet, null, null, index++);
            }
        }
        autoSizeColumns(wb);
        return wb;
    }

    private QueryResult getLastEsperienza(Session session, Folder applicationObject, OperationContext context) {
        Criteria criteriaEsperienza = CriteriaFactory.createCriteria(JCONON_SCHEDA_ANONIMA_ESPERIENZA_PROFESSIONALE);
        criteriaEsperienza.addColumn(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_RUOLO);
        criteriaEsperienza.addColumn(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_DATORE_LAVORO);
        criteriaEsperienza.add(Restrictions.inTree(applicationObject.getId()));
        criteriaEsperienza.addOrder(Order.desc(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_A));
        ItemIterable<QueryResult> iterableEsperienza = criteriaEsperienza.executeQuery(session, false, context);
        for (QueryResult esperienza : iterableEsperienza.getPage(1)) {
            return esperienza;
        }
        return null;
    }

    @Override
    public Map<String, Object> extractionApplicationForSingleCall(
            Session session, String query, String contexURL, String userId)
            throws IOException {
        Map<String, Object> model = new HashMap<String, Object>();
        Boolean attive = query.indexOf(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO) != -1 ? true : null;
        String stato = query.indexOf("jconon_application:stato_domanda = 'C'") != -1 ? "C" : null;
        HSSFWorkbook wb = generateXLS(session, stato, null, attive, false, true);
        Document doc = createXLSDocument(session, wb, userId);
        model.put("objectId", doc.getId());
        model.put("nameBando", "OIV");
        return model;
    }

    public HSSFWorkbook getWorkbookForElenco(Session session, String query, String userId, String callId) throws IOException {
        HSSFWorkbook wb = createHSSFWorkbook(headCSVElenco);
        HSSFSheet sheet = wb.getSheet(SHEET_DOMANDE);
        Criteria criteria = CriteriaFactory.createCriteria(JCONONFolderType.JCONON_APPLICATION.queryName());
        criteria.addColumn(PropertyIds.OBJECT_ID);
        criteria.add(Restrictions.inTree(callId));
        criteria.add(Restrictions.isNotNull(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO));
        criteria.addOrder(Order.asc(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO));
        ItemIterable<QueryResult> iterable = criteria.executeQuery(session, false, session.getDefaultContext());
        int index = 1;
        for (QueryResult queryResult : iterable.getPage(Integer.MAX_VALUE)) {
            Folder application = (Folder) session.getObject(String.valueOf(queryResult.getPropertyById(PropertyIds.OBJECT_ID).getFirstValue()));
            /**
             * Test se rimosso dall'elenco
             */
            LOGGER.error("mail  "+application.getProperty("jconon_application:email_comunicazioni"));
            if (!Optional.ofNullable(application.getProperty("jconon_application:fl_rimosso_elenco"))
                    .map(objectProperty -> objectProperty.<Boolean>getValue())
                    .orElse(false)) {
                getRecordElencoCSV(session, application, sheet, index++);
            }
        }
        autoSizeColumns(wb);
        return wb;
    }

    public Map<String, Object> extractionApplicationForElenco(Session session, String query, String userId, String callId) throws IOException {
        Map<String, Object> model = new HashMap<String, Object>();
        HSSFWorkbook wb = getWorkbookForElenco(session, query, userId, callId);
        Document doc = createXLSDocument(session, wb, userId);
        model.put("objectId", doc.getId());
        return model;
    }
    private Document createXLSDocument(Session session, Folder call, ByteArrayOutputStream stream, String name) {
   	 //  LOGGER.error("create xls ");
       final BindingSession adminSession = cmisService.getAdminSession();
       ContentStreamImpl contentStream = new ContentStreamImpl();
       contentStream.setMimeType("application/vnd.ms-excel");
       contentStream.setStream(new ByteArrayInputStream(stream.toByteArray()));
       LOGGER.error("content strema xls "+contentStream);
       String docId = callService.findAttachmentName(session, call.getId(), name);
       LOGGER.error("create xls primo blocco docid "+docId);
       return Optional.ofNullable(docId)
               .map(s -> {
                   final Document doc = Optional.ofNullable(session.getObject(docId))
                           .filter(Document.class::isInstance)
                           .map(Document.class::cast)
                           .orElseThrow(() -> new RuntimeException("Document for estraiExcelOIV not fount id:" + s));
                   doc.setContentStream(contentStream, true);
                   LOGGER.error("create xls primo blocco ");
                   return doc;
               }).orElseGet(() -> {
                   Map<String, Object> properties = new HashMap<String, Object>();
                   properties.put(PropertyIds.NAME, name);
                   properties.put(PropertyIds.OBJECT_TYPE_ID, BaseTypeId.CMIS_DOCUMENT.value());
                   LOGGER.error("create xls "+properties);
                  
                   Document createDocument = call.createDocument(properties, contentStream, VersioningState.MAJOR);
                   aclService.setInheritedPermission(adminSession, createDocument.getProperty(CoolPropertyIds.ALFCMIS_NODEREF.value()).getValueAsString(), false);

                   Map<String, ACLType> aces = new HashMap<String, ACLType>();
                   aces.put("GROUP_" + EMAIL_DOMANDE_OIV, ACLType.Consumer);
                   aclService.addAcl(adminSession, createDocument.getProperty(CoolPropertyIds.ALFCMIS_NODEREF.value()).getValueAsString(), aces);

                   nodeVersionService.addAutoVersion(createDocument, false);
                   return createDocument;
               });
    }
    public Map<String, Object> extractionApplicationWithExperence(Session session, String query, String userId, String callId) throws IOException {
        Map<String, Object> model = new HashMap<String, Object>();
        HSSFWorkbook wb = createHSSFWorkbookAllEsperienze();
        generateXLS(cmisService.createAdminSession(), wb, null);
        ByteArrayOutputStream streamAllEsperienze = new ByteArrayOutputStream();
        wb.write(streamAllEsperienze);
        Folder call = (Folder) session.getObject(String.valueOf(callId));
         
      //  Document documentAllEsperienze = createXLSDocument(session, call, streamAllEsperienze, ELENCO_OIV_DOMANDE_XLS);
        Document doc = createXLSDocument(session, call, streamAllEsperienze, "elenco-oiv-domande.xls");
        model.put("objectId", doc.getId());
        return model;
    }

    public Map<String, Object> extractionApplicationWithFilter(Session session, String query, String userId, String callId,  String filtroDataDa, String filtroDataA) throws IOException {
        Map<String, Object> model = new HashMap<String, Object>();
        String filtro = null;
        LOGGER.error("estrai exl filtri "+query);
        LOGGER.error("estrai exl filtri "+filtroDataDa);
        LOGGER.error("estrai exl filtri "+filtroDataA);
      //  HSSFWorkbook wb = createHSSFWorkbookLastEsperienze();
        Boolean attive = query.indexOf(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO) != -1 ? true : null;
        String stato = query.indexOf("jconon_application:stato_domanda = 'C'") != -1 ? "C" : null;
      //  generateXLS(cmisService.createAdminSession(), null, wb);
        if ( query.indexOf("jconon_application:stato_domanda = 'C'") != -1 )
        	filtro =  "Inviato" ;
        if ( query.indexOf("jconon_application:data_invio_rinnovo_elenco is not null") != -1 )
        	filtro =   "Rinnovo inviato" ;
        if ( query.indexOf("jconon_application:data_rinnovo_elenco is not null") != -1 )
        	filtro =  "Rinnovato";
        
        if ( query.indexOf("jconon_application:data_invio_cambio_fascia is not null") != -1 )
        	filtro =  "Cambio Fascia Inviato";
        if ( query.indexOf("jconon_application:data_cambio_fascia is not null") != -1 )
        	filtro =  "Fascia cambiata";
        
//        if ( query.indexOf("jconon_application:progressivo_iscrizione_elenco is not null") != -1 )
//        	filtro = "Iscritto";
        if ( query.indexOf("jconon_application:esclusione_rinuncia is not null") != -1 )
        	filtro = "Escluso";
        
		stato = query.indexOf("jconon_application:stato_domanda = 'P'") != -1 ? "P" : null;
	
			if ( filtro != null && filtro.equalsIgnoreCase("Inviato") )
				LOGGER.error("estrai exl filtri index "+query.indexOf("jconon_application:data_domanda"));
	        if ( filtro != null && filtro.equalsIgnoreCase("Rinnovo inviato") )
	        	LOGGER.error("estrai exl filtri index "+query.indexOf("jconon_application:data_invio_rinnovo_elenco"));
	        if ( filtro != null && filtro.equalsIgnoreCase("Rinnovato") )
	        	LOGGER.error("estrai exl filtri index "+query.indexOf("jconon_application:data_rinnovo_elenco"));
	        
	        if ( filtro != null && filtro.equalsIgnoreCase("Cambio Fascia Inviato") )
	        	LOGGER.error("estrai exl filtri index "+query.indexOf("jconon_application:data_invio_cambio_fascia"));
	        if ( filtro != null && filtro.equalsIgnoreCase("Fascia cambiata") )
	        	LOGGER.error("estrai exl filtri index "+query.indexOf("jconon_application:data_cambio_fascia"));
	        if ( stato != null && stato.equalsIgnoreCase("C") )
	        	LOGGER.error("estrai exl filtri index "+query.indexOf("jconon_application:data_domanda"));
	        if ( stato != null && stato.equalsIgnoreCase("P") )
	        	LOGGER.error("estrai exl filtri index "+query.indexOf("cmis:creationDate"));
	        
//	        if ( query.indexOf("jconon_application:progressivo_iscrizione_elenco is not null") != -1 )
//	        	filtro = "Iscritto";
	        if ( filtro != null && filtro.equalsIgnoreCase("Escluso") )
	        	LOGGER.error("estrai exl filtri index "+query.indexOf("jconon_application:data_rimozione_elenco"));
	
        HSSFWorkbook wb = generateXLSFiltrati(session, stato, filtro, filtroDataDa, filtroDataA, null, attive, false, true);
        ByteArrayOutputStream streamAllEsperienze = new ByteArrayOutputStream();
        wb.write(streamAllEsperienze);
        Folder call = (Folder) session.getObject(String.valueOf(callId));
         
      //  Document documentAllEsperienze = createXLSDocument(session, call, streamAllEsperienze, ELENCO_OIV_DOMANDE_XLS);
        Document doc = createXLSDocument(session, call, streamAllEsperienze, "elenco-oiv-domande.xls");
        model.put("objectId", doc.getId());
        return model;
    }
    
    private void getRecordElencoCSV(Session session, Folder application, HSSFSheet sheet, int index) {
        int column = 0;
        HSSFRow row = sheet.createRow(index);
        row.createCell(column++).setCellValue(Optional.ofNullable(application.getPropertyValue(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO)).map(numero -> String.valueOf(numero)).orElse(""));
        row.createCell(column++).setCellValue(application.<String>getPropertyValue("jconon_application:nome").toUpperCase());
        row.createCell(column++).setCellValue(application.<String>getPropertyValue("jconon_application:cognome").toUpperCase());
        row.createCell(column++).setCellValue(Optional.ofNullable(application.getPropertyValue("jconon_application:data_iscrizione_elenco")).map(map -> dateFormat.format(((Calendar) application.getPropertyValue("jconon_application:data_iscrizione_elenco")).getTime())).orElse(""));
    }

    public String archiviaRicevutaReportModel(Session cmisSession, Folder                 application, Map<String, Object> properties,
                                              InputStream is, String nameRicevutaReportModel, boolean confermata) throws CMISApplicationException {
        try {
            ContentStream contentStream = new ContentStreamImpl(nameRicevutaReportModel,
                    BigInteger.valueOf(is.available()),
                    "application/pdf",
                    is);
            String docId = findRicevutaApplicationId(cmisSession, application);
            if (docId != null) {
                try {
                    Document doc = (Document) cmisSession.getObject(docId);
                    if (confermata) {
                        doc.updateProperties(properties, true);
                        doc.setContentStream(contentStream, true, true);
                        doc = doc.getObjectOfLatestVersion(false);
                        LOGGER.info("Start checkin application:{} with name {}", doc.getId(), nameRicevutaReportModel);
                        docId = checkInPrint(cmisService.getAdminSession(), doc.getPropertyValue(CoolPropertyIds.ALFCMIS_NODEREF.value()), is, nameRicevutaReportModel);
                        LOGGER.info("End checkin application:{} with name {}", doc.getId(), nameRicevutaReportModel);
                    } else {
                        doc = cmisSession.getLatestDocumentVersion(doc.updateProperties(properties, true));
                        doc.setContentStream(contentStream, true, true);
                        doc = doc.getObjectOfLatestVersion(false);
                        docId = doc.getId();
                    }
                } catch (CmisObjectNotFoundException e) {
                    LOGGER.warn("cmis object not found {}", nameRicevutaReportModel, e);
                    docId = createApplicationDocument(application, contentStream, properties);
                } catch (CmisStreamNotSupportedException ex) {
                    LOGGER.error("Cannot set Content Stream on id:" + docId + " ------" + ex.getErrorContent(), ex);
                    throw ex;
                }
            } else {
            	  LOGGER.warn("primo documento {}", properties);
            	  LOGGER.warn("primo documento {}", contentStream);
            	  LOGGER.warn("primo documento {}", application);
                docId = createApplicationDocument(application, contentStream, properties);
            }
            return docId;
        } catch (CmisContentAlreadyExistsException _ex) {
            LOGGER.warn("File della domanda {} alredy exist", nameRicevutaReportModel, _ex);
            throw new ClientMessageException("Il file " + nameRicevutaReportModel + " è già presente come allegato!");
        } catch (Exception e) {
            throw new CMISApplicationException("Error in JASPER", e);
        }
    }

    public String archiviaRicevutaRinnovoReportModel(Session cmisSession, Folder application, Map<String, Object> properties,
            InputStream is, String nameRicevutaReportModel, boolean confermata) throws CMISApplicationException {
			try {
				ContentStream contentStream = new ContentStreamImpl(nameRicevutaReportModel,
				BigInteger.valueOf(is.available()),
				"application/pdf",
				is);
				String docId = findRicevutaRinnovoId(cmisSession, application);
		        LOGGER.error("archivia --- rinnovo applicationoiv : {}", docId);

			if (docId != null) {
				try {
					Document doc = (Document) cmisSession.getObject(docId);
					if (confermata) {
						 LOGGER.error("archivia --- rinnovo applicationoiv : {}", doc.getVersionLabel());
						int pointPosition = nameRicevutaReportModel.lastIndexOf('.');
						String nameRicevutaReportModels = nameRicevutaReportModel.substring(0, pointPosition).
						concat("-").concat(doc.getVersionLabel()).concat(".pdf");
						doc.setContentStream(contentStream, true, true);
						doc = doc.getObjectOfLatestVersion(false);
						docId = checkInPrint(cmisService.getAdminSession(), doc.getPropertyValue(CoolPropertyIds.ALFCMIS_NODEREF.value()), is, nameRicevutaReportModels);
					} else {
						 LOGGER.error("archivia else --- rinnovo applicationoiv : {}", doc.getVersionLabel());
						doc = cmisSession.getLatestDocumentVersion(doc.updateProperties(properties, true));
						doc.setContentStream(contentStream, true, true);
						doc = doc.getObjectOfLatestVersion(false);
						docId = doc.getId();
					}
				} catch (CmisObjectNotFoundException e) {
					LOGGER.warn("cmis object not found {}", nameRicevutaReportModel, e);
					docId = createApplicationDocument(application, contentStream, properties);
				} catch (CmisStreamNotSupportedException ex) {
					LOGGER.error("Cannot set Content Stream on id:" + docId + " ------" + ex.getErrorContent(), ex);
					throw ex;
				}
			} else {
				docId = createApplicationDocument(application, contentStream, properties);
			}
				return docId;
			} catch (Exception e) {
				throw new CMISApplicationException("Error in JASPER", e);
			}
	}
    
    public String archiviaRicevutaCambioFasciaReportModel(Session cmisSession, Folder application, Map<String, Object> properties,
            InputStream is, String nameRicevutaReportModel, boolean confermata) throws CMISApplicationException {
    	LOGGER.error("archivia --- cambio fascia applicationoiv oiv : {}", properties);
			try {
				ContentStream contentStream = new ContentStreamImpl(nameRicevutaReportModel,
				BigInteger.valueOf(is.available()),
				"application/pdf",
				is);
				String docId = findRicevutaCambioFasciaId(cmisSession, application);
		        LOGGER.error("archivia --- cambio fascia applicationoiv : {}", docId);

			if (docId != null) {
				try {
					Document doc = (Document) cmisSession.getObject(docId);
					if (confermata) {
						 LOGGER.error("archivia --- cambio fascia applicationoiv : {}", doc.getVersionLabel());
						int pointPosition = nameRicevutaReportModel.lastIndexOf('.');
						String nameRicevutaReportModels = nameRicevutaReportModel.substring(0, pointPosition).
						concat("-").concat(doc.getVersionLabel()).concat(".pdf");
						doc.setContentStream(contentStream, true, true);
						doc = doc.getObjectOfLatestVersion(false);
						docId = checkInPrint(cmisService.getAdminSession(), doc.getPropertyValue(CoolPropertyIds.ALFCMIS_NODEREF.value()), is, nameRicevutaReportModels);
					} else {
						 LOGGER.error("archivia else --- cambio fascia applicationoiv : {}", doc.getVersionLabel());
						doc = cmisSession.getLatestDocumentVersion(doc.updateProperties(properties, true));
						doc.setContentStream(contentStream, true, true);
						doc = doc.getObjectOfLatestVersion(false);
						docId = doc.getId();
					}
				} catch (CmisObjectNotFoundException e) {
					LOGGER.warn("cmis object not found {}", nameRicevutaReportModel, e);
					docId = createApplicationDocument(application, contentStream, properties);
				} catch (CmisStreamNotSupportedException ex) {
					LOGGER.error("Cannot set Content Stream on id:" + docId + " ------" + ex.getErrorContent(), ex);
					throw ex;
				}
			} else {
				LOGGER.warn("primo documento oiv {}", properties);
	          	  LOGGER.warn("primo documento {}", contentStream);
	          	  LOGGER.warn("primo documento {}", application);
				docId = createApplicationDocument(application, contentStream, properties);
			}
				return docId;
			} catch (Exception e) {
				throw new CMISApplicationException("Error in JASPER", e);
			}
	}
    
    @Override
    protected boolean isConfirmed(Folder application) {
        return false;
    }                

    private String createApplicationDocument(Folder application, ContentStream contentStream, Map<String, Object> properties) {
        Document doc = application.createDocument(properties, contentStream, VersioningState.MINOR);
        LOGGER.error("create dovument oiv "+doc);
        nodeVersionService.addAutoVersion(doc, false);
        return doc.getId();
    }

    private void getRecordCSV(Session session, Folder applicationObject, Document oivObject, int applicationNumber, CMISUser user, HSSFSheet sheet, String lastRuolo, String lastDatoreLavoro, int index, boolean all) {
    	LOGGER.error("nome scarico "+applicationObject.<String>getPropertyValue("jconon_application:cognome")+" "+applicationObject.<String>getPropertyValue("jconon_application:nome"));
    	LOGGER.error("user "+user);
    	int column = 0;
        HSSFRow row = sheet.createRow(index);
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO)).
                map(numero -> String.valueOf(numero)).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:data_iscrizione_elenco")).map(map ->
                dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:data_iscrizione_elenco")).getTime())).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:data_rinnovo_elenco")).map(map ->
        	dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:data_rinnovo_elenco")).getTime())).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:data_cambio_fascia")).map(map ->
    		dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:data_cambio_fascia")).getTime())).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:data_rimozione_elenco")).map(map ->
			dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:data_rimozione_elenco")).getTime())).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:data_approvazione_rinnovo_elenco")).map(map ->
    		dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:data_approvazione_rinnovo_elenco")).getTime())).orElse(""));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:cognome").toUpperCase());
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nome").toUpperCase());
        if (!all) {			// caso scarico tasto blu last exp
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getProperty("jconon_application:data_nascita").getValue()).map(
                    map -> dateFormat.format(((Calendar) map).getTime())).orElse(""));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:sesso"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nazione_nascita"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:comune_nascita"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:provincia_nascita"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nazione_residenza"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:provincia_residenza"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:comune_residenza"));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getProperty("jconon_application:indirizzo_residenza")).map(Property::getValueAsString).orElse("").concat(" - ").concat(
                    Optional.ofNullable(applicationObject.getProperty("jconon_application:num_civico_residenza")).map(Property::getValueAsString).orElse("")));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:cap_residenza"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:codice_fiscale"));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:email_comunicazioni"))
                    .filter(s -> !s.isEmpty())
                    .orElse(Optional.ofNullable(user).map(CMISUser::getEmail).orElse("")));
            row.createCell(column++).setCellValue(Optional.ofNullable(user).map(CMISUser::getEmail).orElse(""));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:email_pec_comunicazioni"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nazione_comunicazioni"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:provincia_comunicazioni"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:comune_comunicazioni"));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getProperty("jconon_application:indirizzo_comunicazioni")).map(Property::getValueAsString).orElse("").concat(" - ").concat(
                    Optional.ofNullable(applicationObject.getProperty("jconon_application:num_civico_comunicazioni")).map(Property::getValueAsString).orElse("")));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:cap_comunicazioni"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:telefono_comunicazioni"));
        }
        Calendar data = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("jconon_application:data_domanda")).orElse(applicationObject.getPropertyValue("jconon_application:data_ultimo_invio"));
        row.createCell(column++).setCellValue(Optional.ofNullable(data).map(map -> dateTimeFormat.format((data).getTime())).orElse(""));
        Calendar dataInvioRinnovo = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("jconon_application:data_invio_rinnovo_elenco")).orElse(
                applicationObject.getPropertyValue("jconon_application:data_invio_rinnovo_elenco"));
        row.createCell(column++).setCellValue(Optional.ofNullable(dataInvioRinnovo).map(map ->
        dateTimeFormat.format((dataInvioRinnovo).getTime())).orElse(""));
        Calendar dataInvioCambioFascia = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("jconon_application:data_invio_cambio_fascia")).orElse(
                applicationObject.getPropertyValue("jconon_application:data_invio_cambio_fascia"));
        row.createCell(column++).setCellValue(Optional.ofNullable(dataInvioCambioFascia).map(map ->
        dateTimeFormat.format((dataInvioCambioFascia).getTime())).orElse(""));
        
        
        Calendar dataUltimaMod = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("cmis:lastModificationDate")).orElse(
                applicationObject.getPropertyValue("cmis:lastModificationDate"));
        row.createCell(column++).setCellValue(Optional.ofNullable(dataUltimaMod).map(map ->
                dateTimeFormat.format((dataUltimaMod).getTime())).orElse(""));
        
        Calendar dataPrimaComunicazioneRinnovo = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("jconon_application:data_prima_comunicazione_rinnovo")).orElse(
                applicationObject.getPropertyValue("jconon_application:data_prima_comunicazione_rinnovo"));
        row.createCell(column++).setCellValue(Optional.ofNullable(dataPrimaComunicazioneRinnovo).map(map ->
                dateTimeFormat.format((dataPrimaComunicazioneRinnovo).getTime())).orElse(""));
        Calendar dataSecondaComunicazioneRinnovo = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("jconon_application:data_seconda_comunicazione_rinnovo")).orElse(
                applicationObject.getPropertyValue("jconon_application:data_seconda_comunicazione_rinnovo"));
        row.createCell(column++).setCellValue(Optional.ofNullable(dataSecondaComunicazioneRinnovo).map(map ->
                dateTimeFormat.format((dataSecondaComunicazioneRinnovo).getTime())).orElse(""));
        
        if (!all) {
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:tipo_laurea"));
            row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:istituto_laurea"));
        }
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:fascia_professionale_attribuita")).orElse(""));
        if (oivObject != null) {
            row.createCell(column++).setCellValue(oivObject.getType().getDisplayName());
            if (oivObject.getType().getId().equalsIgnoreCase("D:jconon_scheda_anonima:precedente_incarico_oiv")) {
                row.createCell(column++).setCellValue("");
                row.createCell(column++).setCellValue("");
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_ruolo"));
                row.createCell(column++).setCellValue("");
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_amministrazione"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_sede"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_comune"));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_da")).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_da")).getTime())).orElse(""));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_a")).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_a")).getTime())).orElse(""));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_numero_dipendenti"));
            } else if (oivObject.getType().getId().equalsIgnoreCase("D:jconon_scheda_anonima:esperienza_professionale")) {
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_area_specializzazione")).orElse(""));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_attivita_svolta")).orElse(""));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_RUOLO));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_datore_lavoro"));
                row.createCell(column++).setCellValue("");
                row.createCell(column++).setCellValue("");
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_citta"));

                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue("jconon_attachment:esperienza_professionale_da")).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue("jconon_attachment:esperienza_professionale_da")).getTime())).orElse(""));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_A)).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_A)).getTime())).orElse(""));
                row.createCell(column++).setCellValue("");
            }
     //       ArrayList lista= applicationObject.getPropertyValue("cmis:secondaryObjectTypeIds");
            ArrayList lista= oivObject.getPropertyValue("cmis:secondaryObjectTypeIds");
            if( lista.contains("P:jconon_scheda_anonima:esperienza_non_coerente") ) {
            	 row.createCell(column++).setCellValue("Si");
            }else
            	 row.createCell(column++).setCellValue("No");
           
            row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_non_coerente_motivazione")).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<Boolean>getPropertyValue("jconon_attachment:esperienza_non_coerente_visibile")).orElse(false));
        
            if( lista.contains("P:jconon_scheda_anonima:esperienza_non_coerente_rinnovo") ) {
           	 row.createCell(column++).setCellValue("Si");
           }else
           	 row.createCell(column++).setCellValue("No");
            
            row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_non_coerente_rinnovo_motivazione")).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<Boolean>getPropertyValue("jconon_attachment:esperienza_non_coerente_rinnovo_visibile")).orElse(false));

            row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_annotazione_motivazione")).orElse(""));

        } else {
            row.createCell(column++).setCellValue(lastRuolo);
            row.createCell(column++).setCellValue(lastDatoreLavoro);
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_attachment:esperienza_annotazione_motivazione")).orElse(""));
        }
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:fascia_professionale_validata")).orElse(""));
        if (!all) {
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<Boolean>getPropertyValue("jconon_application:fl_occupato")).map(x -> x ? SI : NO).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:non_occupato")).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<Boolean>getPropertyValue("jconon_application:fl_dipendente_pubblico")).map(x -> x ? SI : NO).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:situazione_lavorativa_settore")).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:situazione_lavorativa_ruolo")).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:situazione_lavorativa_datore_lavoro")).orElse(""));
            row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:situazione_lavorativa_data_inizio_lavoro")).map(map ->
                    dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:situazione_lavorativa_data_inizio_lavoro")).getTime())).orElse(""));
        }
    }

    private void getRecordCSVAllIscritti(Session session, Folder applicationObject, Document oivObject, int applicationNumber, CMISUser user, HSSFSheet sheet, String lastRuolo, String lastDatoreLavoro, int index) {
    	
    	int column = 0;
        HSSFRow row = sheet.createRow(index);
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue(JCONON_APPLICATION_PROGRESSIVO_ISCRIZIONE_ELENCO)).
                map(numero -> String.valueOf(numero)).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:data_iscrizione_elenco")).map(map ->
                dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:data_iscrizione_elenco")).getTime())).orElse(""));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:cognome").toUpperCase());
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nome").toUpperCase());
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getProperty("jconon_application:data_nascita").getValue()).map(
                map -> dateFormat.format(((Calendar) map).getTime())).orElse(""));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:sesso"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nazione_nascita"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:comune_nascita"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:provincia_nascita"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nazione_residenza"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:provincia_residenza"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:comune_residenza"));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getProperty("jconon_application:indirizzo_residenza")).map(Property::getValueAsString).orElse("").concat(" - ").concat(
                Optional.ofNullable(applicationObject.getProperty("jconon_application:num_civico_residenza")).map(Property::getValueAsString).orElse("")));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:cap_residenza"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:codice_fiscale"));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:email_comunicazioni")).filter(s -> !s.isEmpty()).orElse(user.getEmail()));
       // row.createCell(column++).setCellValue(Optional.ofNullable(user.getEmail()).orElse(""));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:email_pec_comunicazioni"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:nazione_comunicazioni"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:provincia_comunicazioni"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:comune_comunicazioni"));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getProperty("jconon_application:indirizzo_comunicazioni")).map(Property::getValueAsString).orElse("").concat(" - ").concat(
                Optional.ofNullable(applicationObject.getProperty("jconon_application:num_civico_comunicazioni")).map(Property::getValueAsString).orElse("")));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:cap_comunicazioni"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:telefono_comunicazioni"));

        Calendar data = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("jconon_application:data_domanda")).orElse(
                applicationObject.getPropertyValue("jconon_application:data_ultimo_invio"));
        row.createCell(column++).setCellValue(Optional.ofNullable(data).map(map ->
                dateTimeFormat.format((data).getTime())).orElse(""));
        Calendar dataUltimaMod = Optional.ofNullable(applicationObject.<Calendar>getPropertyValue("cmis:lastModificationDate")).orElse(
                applicationObject.getPropertyValue("cmis:lastModificationDate"));
        row.createCell(column++).setCellValue(Optional.ofNullable(dataUltimaMod).map(map ->
                dateTimeFormat.format((dataUltimaMod).getTime())).orElse(""));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:tipo_laurea"));
        row.createCell(column++).setCellValue(applicationObject.<String>getPropertyValue("jconon_application:istituto_laurea"));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:fascia_professionale_attribuita")).orElse(""));

        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:fascia_professionale_validata")).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<Boolean>getPropertyValue("jconon_application:fl_occupato")).map(x -> x ? SI : NO).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:non_occupato")).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<Boolean>getPropertyValue("jconon_application:fl_dipendente_pubblico")).map(x -> x ? SI : NO).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:situazione_lavorativa_settore")).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:situazione_lavorativa_ruolo")).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.<String>getPropertyValue("jconon_application:situazione_lavorativa_datore_lavoro")).orElse(""));
        row.createCell(column++).setCellValue(Optional.ofNullable(applicationObject.getPropertyValue("jconon_application:situazione_lavorativa_data_inizio_lavoro")).map(map ->
                dateFormat.format(((Calendar) applicationObject.getPropertyValue("jconon_application:situazione_lavorativa_data_inizio_lavoro")).getTime())).orElse(""));

        row.createCell(column++).setCellValue(ApplicationService.StatoDomanda.fromValue(applicationObject.getPropertyValue("jconon_application:stato_domanda")).displayValue());
        if (applicationObject.getAcl() != null && applicationObject.getAcl().getAces().stream().anyMatch(
                x -> x.isDirect() && x.getPermissions().stream().anyMatch(permission -> permission.contains(ACLType.Consumer.name()))
                        && x.getPrincipal().getId().equals(applicationObject.<String>getPropertyValue(JCONON_APPLICATION_USER)))) {
            if (applicationObject.<String>getPropertyValue("jconon_application:esclusione_rinuncia") != null) {
                row.createCell(column++).setCellValue("ESCLUSA");
            } else {
                row.createCell(column++).setCellValue("INVIATA");
            }
        } else {
            row.createCell(column++).setCellValue("MODIFICA PROFILO");
        }
        if (oivObject != null) {
            if (oivObject.getType().getId().equalsIgnoreCase("D:jconon_scheda_anonima:precedente_incarico_oiv")) {
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_da")).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_da")).getTime())).orElse(""));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_a")).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue("jconon_attachment:precedente_incarico_oiv_a")).getTime())).orElse(""));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_amministrazione"));
           //     row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_sede"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_comune"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_indirizzo"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_cap"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_telefono"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_numero_dipendenti"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:precedente_incarico_oiv_ruolo"));
                column = column + 8;
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_annotazione_motivazione")).orElse(""));
            } else if (oivObject.getType().getId().equalsIgnoreCase("D:jconon_scheda_anonima:esperienza_professionale")) {
                column = column + 10;
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue("jconon_attachment:esperienza_professionale_da")).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue("jconon_attachment:esperienza_professionale_da")).getTime())).orElse(""));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.getPropertyValue(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_A)).map(map ->
                        dateFormat.format(((Calendar) oivObject.getPropertyValue(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_A)).getTime())).orElse(""));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_area_specializzazione")).orElse(""));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_attivita_svolta")).orElse(""));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_datore_lavoro"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue(JCONON_ATTACHMENT_ESPERIENZA_PROFESSIONALE_RUOLO));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_citta"));
                row.createCell(column++).setCellValue(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_professionale_stato_estero"));
                row.createCell(column++).setCellValue(Optional.ofNullable(oivObject.<String>getPropertyValue("jconon_attachment:esperienza_annotazione_motivazione")).orElse(""));
            }
        }
    }
}