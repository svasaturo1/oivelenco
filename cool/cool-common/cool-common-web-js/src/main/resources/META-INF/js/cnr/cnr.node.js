// CMS Node management
define(['jquery', 'cnr/cnr', 'cnr/cnr.ui', 'cnr/cnr.bulkinfo', 'i18n', 'cnr/cnr.url', 'behave', 'fileupload',  'bootstrap-fileupload'], function ($, CNR, UI, BulkInfo, i18n, URL, Behave) {
  "use strict";

  var defaultObjectTypeDocument = "cmis:document",
    reNodeRef = new RegExp("([a-z]+)\\:\/\/([a-z]+)\/(.*)", 'gi');

  function displayOutcome(data, isDelete) {
    var msg = isDelete ? "Operazione effettuata con successo" : (Object.keys(data.attachments).length + ' files ok');
    UI.success(msg);
  }
  function manageNazioneEsperienze(value) {
	    var fieldsItaly = $(".in #esperienza_professionale_citta"),
	      fieldsForeign = $(".in #esperienza_professionale_estero_citta");
	    manageNazioni(value, fieldsItaly, fieldsForeign);
	  }
  function manageNazioni(value, fieldsItaly, fieldsForeign) {
	    if (value && value.toUpperCase() === 'ITALIA') {
	      fieldsForeign.val('').trigger('blur');
	      fieldsItaly.parents(".control-group").show();
	      fieldsForeign.parents(".control-group").hide();
	    } else {
	      fieldsItaly.val('').trigger('change');
	      fieldsItaly.parents(".control-group").hide();
	      fieldsForeign.parents(".control-group").show();
	    }
  }

  // operations on documents: insert, update and delete
  function manageNode(nodeRef, operation, input, rel, forbidArchives, maxUploadSize) {

    var httpMethod = "GET",
      fd = new CNR.FormData();

    fd.data.append("cmis:objectId", nodeRef.split(';')[0]);

    if (operation === "INSERT" || operation === "UPDATE") {

      if (!window.FormData) {
        UI.error("Impossibile eseguire l'operazione");
        return;
      }

      fd.data.append("cmis:objectTypeDocument", defaultObjectTypeDocument);
      fd.data.append("crudStatus", operation);
      if (rel) {
        $.each(rel, function (key, value) {
          fd.data.append(key, value);
        });
      }
      if (forbidArchives) {
        fd.data.append('forbidArchives', true);
      }
      $.each(input[0].files || [], function (i, file) {
        fd.data.append('file-' + i, file);
      });
      httpMethod = "POST";
    } else if (operation === "DELETE") {
      httpMethod = "DELETE";
    }
    if (operation === "GET") {
      window.location = URL.urls.search.content + '?nodeRef=' + nodeRef;
    } else {
      return URL.Data.node.node({
        data: fd.getData(),
        contentType: fd.contentType,
        processData: false,
        type: httpMethod,
        placeholder : {
          maxUploadSize : maxUploadSize || false
        }
      });
    }
  }

  function updateMetadata(data, cb) {
    URL.Data.node.metadata({
      type: 'POST',
      data: data,
      success: cb
    });
  }

  function updateMetadataNode(nodeRef, data, success) {
    var metadataToUpdate = {};
    $.map(data, function (metadata) {
      metadataToUpdate[metadata.name] = metadata.value;
    });
    CNR.log(metadataToUpdate);
    URL.Data.proxy.metadataNode({
      placeholder: {
        'store_type' : nodeRef.replace(reNodeRef, '$1'),
        'store_id' : nodeRef.replace(reNodeRef, '$2'),
        'id' : nodeRef.replace(reNodeRef, '$3')
      },
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        "properties" : metadataToUpdate
      }),
      success: success
    });
  }

  // file uploader for Internet Explorer
  function manageIE(selectedFolder, crudStatus, input, setValue, rel, forbidArchives) {
    var myData,
      success = null,
      fd = {
        "cmis:objectTypeDocument": defaultObjectTypeDocument,
        "crudStatus" : crudStatus,
        "cmis:objectId": selectedFolder
      };
    if (rel) {
      fd["cmis:sourceId"] = rel.sourceId;
      fd["cmis:relObjectTypeId"] = rel.objectTypeId;
    }

    if (forbidArchives) {
      fd.forbidArchives = true;
    }

    input
      .fileupload({
        url: URL.urls.node + '.html',
        formData: fd,
        add: function (e, data) {
          myData = data;
        },
        done: function (e, data) {
          var content, j;
          if ($.browser.safari) {
            content = $(data.result).val();
          } else {
            content = $(data.result[0].documentElement).find('textarea').val();
          }
          try {
            j = JSON.parse(content);
            displayOutcome(j, false);
            if (typeof success === 'function') {
              success(j);
            }
          } catch (error) {
            UI.error("Errore nel caricamento del file");
          }
        }
      })
      .bind('fileuploadchange', function (e, data) {
        var path = data.files[0].name;
        if (typeof setValue === 'function') {
          setValue(path);
        }
      });

    return function (nodeRef, status, successFn, relationship) {
      if (nodeRef) {
        if (relationship) {
          fd['cmis:sourceId'] = relationship['cmis:sourceId'];
          fd['cmis:relObjectTypeId'] = relationship['cmis:relObjectTypeId'];
        }
        fd["cmis:objectId"] = nodeRef;
        fd.crudStatus = status || fd.crudStatus;
        myData.formData = fd;
      }
      success = successFn;
      myData.submit();
      return false;
    };
  }



  /**
   * Create a new input file ("widget") powered by fileupload
   *
   * manages the fallback operations (e.g. FormData) in InternetExplorer
   * write the file name in .data('value')
   *
   */
  function inputWidget(folder, crudStatus, rel, forbidArchives, maxUploadSize) {

    var container = $('<div class="fileupload fileupload-new" data-provides="fileupload"></div>'),
      input = $('<div class="input-append"></div>'),
      btn = $('<span class="btn btn-file"></span>'),
      inputFile = $('<input type="file" />'),
      submitFn,
      isExplorer = window.FormData && window.FileReader ? false : true;

    btn
      .append('<span class="fileupload-new">Aggiungi allegato</span>')
      .append('<span class="fileupload-exists">Cambia</span>')
      .append(inputFile);

    input
      .append('<div class="uneditable-input input-xlarge"><i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span></div>')
      .append(btn)
      .appendTo(container);

    // set widget 'value'
    function setValue(value) {
      container.data('value', value);
      if(value != null){
	      var tmp = value.split('\\');
	      if(container.parent().find("form[id='cmis:document']").length > 0){
	    	  container.parent().find("form[id='cmis:document']").find("input[id='name']").val(tmp[tmp.length-1]);
	      }
      }
    }

    setValue(null);

    if (isExplorer) {
      inputFile.attr('name', 'file-0');
      submitFn = manageIE(folder, crudStatus, inputFile, setValue, rel, forbidArchives, maxUploadSize);
    } else {
      input.append('<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Rimuovi</a>'); //remove cannot be use in IE-compatible mode
      submitFn = function (nodeRef, status, success, relationship) {
        var xhr = manageNode(nodeRef || folder, status || crudStatus, inputFile, relationship || rel, forbidArchives, maxUploadSize);
        xhr.done(function (data) {
          if (success) {
            success(data);
          }
        });
        return xhr;
      };
      inputFile.on('change', function (e) {
        var path = $(e.target).val();
        setValue(path);
      });
    }

    return {
      item: container,
      fn: submitFn
    };
  }

  /**
   * Submission of a new document.
   *
   * opens a modal window containing the input form for the specified object type and an input file
   * create/update the document
   *
   * @param {string} nodeRef the parent folder OR the document to update
   * @param {string} objectType the type of the node to create (e.g. 'cmis:document')
   * @param {string} crudStatus the type of the operation to perform (i.e. 'UPDATE' or 'INSERT')
   * @param {boolean} requiresFile if the submission require (at least) one file, true by default
   * @param {boolean} showFile if the submission show an input file
   * @param [array] externalData add to submission
   * @param {boolean} multiple if the submission allow multiple files
   *
   */
  function submission(opts) {

    opts.objectType = opts.objectType || 'cmis:document';

    var content = $("<div></div>").addClass('modal-inner-fix'),
      bulkinfo,
      fileInputs = [],
      modal,
      isInsert = opts.crudStatus === 'INSERT',
      addFileUploadInput,
      regex = /^.*:([^:]*)/gi;


    function addPlusButton(element) {
      var btn = $('<button class="btn">+</button>').click(addFileUploadInput);
      btn.appendTo(element.find('.input-append'));
    }

    addFileUploadInput = function () {
      var w = inputWidget(null, "UPDATE", (opts.input ? opts.input.rel : undefined), opts.forbidArchives, opts.maxUploadSize);
      fileInputs.push(w);
      addPlusButton(w.item);
      content.append(w.item);
    };

    if (opts.showFile !== false) {
      fileInputs.push(inputWidget(null, "UPDATE", undefined, opts.forbidArchives, opts.maxUploadSize));
      if (opts.multiple) {
        addPlusButton(fileInputs[0].item);
      }
      content.append(fileInputs[0].item);
    }

    bulkinfo = new BulkInfo({
      target: content,
      path:   opts.objectType,   //  "D:jconon_documento_riconoscimento:attachment", //
      objectId: isInsert ? null : opts.nodeRef,
      callback: {
        afterCreateForm: function () {
			if(opts.objectType.includes("comunicazione") && isInsert ){
				var titolo="Invia comunicazione all\' utente";
			}else if(opts.objectType.includes("note") && isInsert ){
				var titolo="note interne ";
			}else if(opts.objectType.includes("esperienza_professionale") && isInsert ){
				opts.modalTitle="Esperienza professionale/Esperienza dirigenziale nella PA ";
			}else if( (opts.objectType.includes("iscrizione") && isInsert )  ){
				opts.modalTitle="Conferma iscrizione ";
			}else if( (opts.objectType.includes("rinnovo_effettuato") && isInsert )  ){
				opts.modalTitle="Conferma rinnovo ";
			}else if( (opts.objectType.includes("cambio_fascia_effettuato") && isInsert )  ){
				opts.modalTitle="Conferma cambio fascia ";
			}else{
				var titolo="Cancellazione dall\' elenco ";
			}
			
      //    modal = UI.modal(opts.modalTitle || (isInsert ? 'Inserimento allegato' : 'Aggiornamento allegato'), content, function () {
         /*   if (!bulkinfo.validate()) {
              UI.alert("alcuni campi non sono corretti");
              return false;
            }*/
		 
			
			modal = UI.modal(opts.modalTitle || titolo, content, function () {
				
//				$('#fl_invia_notifica_email').on('click', function () {
//						modal.find('#oggetto_notifica_email').val(i18n['app.name'] + ' - ' + i18n['mail.subject.comunicazione']);
//						var testo = i18n['mail.confirm.application.1'];
//							testo += el['jconon_application:sesso'] === 'M' ? ' dott.' : ' dott.ssa';
//							testo += ' <b style="text-transform: capitalize;">' + nome + ' ' + cognome + '</b>,';
//							testo += callData['jconon_call:requisiti_en'];
//
//						var textarea = modal.find('#testo_notifica_email');
//						textarea.val(testo);
//						var ck = textarea.ckeditor({
//							toolbarGroups: [
//								{ name: 'clipboard', groups: ['clipboard'] },
//								{ name: 'basicstyles', groups: ['basicstyles'] },
//								{ name: 'paragraph', groups: ['list', 'align'] }],
//								removePlugins: 'elementspath'
//					});		
//					
//				});	
//				
				
				// CARICAMENTO COMUNICAZIONE
		if(!opts.objectType.includes("note") && !opts.objectType.includes("esperienza") && !opts.objectType.includes("corso") && (opts.modalTitle == undefined || !opts.modalTitle.includes("allegato")) )	{	
			if(!$(".in #fl_invia_notifica_email button.active").text().localeCompare('')==0){
				if(!$(".in #fl_invia_notifica_email button.active").text().localeCompare('Si')==0  ){
					if($(".in .fileupload-preview" ).text().localeCompare('')==0){
						UI.alert("Alcuni campi non sono corretti");
						return false;
					}
					$("#oggetto_notifica_email").val("");
					$("#testo_notifica_email").val("");
				}
			}else{
				if (!bulkinfo.validate()) {
				  UI.alert("alcuni campi non sono corretti");
				  return false;
				}
				
			}
		}else if(opts.objectType.includes("note")){
			
			if($(".in #oggetto_notifica_email").val().localeCompare('')==0){
				UI.alert("Titlo obbligatorio");
				return false;
			}else if(($(".in .fileupload-preview" ).text().localeCompare('')==0 && $(".in #testo_notifica_email" ).val().localeCompare('')==0)){
				
					UI.alert("Allegato o testo obbligatori.");
					return false;
				
			}	
				
				
		}else if(opts.objectType.includes("esperienza")){
			
//			if( modal.find('#amministrazione_pubblica_generale button[data-value="true"]').hasClass("active") ||
//					modal.find('#amministrazione_pubblica_generale button[data-value="false"]').hasClass("active") ) {
//				if(	modal.find("#esperienza_professionale_area_specializzazione").val() == "")
//					
//				//	modal.find("#esperienza_professionale_attivita_svolta").parent().parent().css('display','none');
//					modal.find("#s2id_esperienza_professionale_area_specializzazione").parent().parent().css('display','none');
//				else{
//					modal.find("#esperienza_professionale_attivita_svolta").parent().parent().css('display','none');
//					modal.find("#s2id_esperienza_professionale_area_specializzazione").parent().parent().css('display','none');
//					
//				}
//			}
//			if( modal.find('.dirigente[data-value="true"]').hasClass('active')  ) {
//				
//				modal.find("#esperienza_professionale_attivita_svolta").parent().parent().css('display','none');
//				modal.find("#s2id_esperienza_professionale_area_specializzazione").parent().parent().css('display','none');
//				
//			}
			if (!bulkinfo.validate()) {
			//	  modal.find("#esperienza_professionale_attivita_svolta").parent().parent().css('display','inline-block');
				  modal.find("#s2id_esperienza_professionale_area_specializzazione").parent().parent().css('display','inline-block');
				  UI.alert("alcuni campi non sono corretti");
				  
				  return false;
			}
			 modal.find("#esperienza_professionale_attivita_svolta").parent().parent().css('display','inline-block');
			  modal.find("#s2id_esperienza_professionale_area_specializzazione").parent().parent().css('display','inline-block');
		
		}else if(opts.objectType.includes("corso")){
			if(!isNaN(parseFloat(modal.find("#numero_crediti_corso").val()))) {
				if (!bulkinfo.validate()) {
					  UI.alert("alcuni campi non sono corretti");
					  return false;
				}
			}else{
				 UI.alert("il campo numero crediti deve essere numerico");
				  return false;
			}
			
//			var input = modal.find('.corso');
//        	var table=$('.tableCorsi');
//        	if( input.length > 0){
//        		var tR= $('<tr></tr>'), tdButton, title = "elimina";	            	
//            	input.each(function( index, element ) {
//            		var tdText = $('<td>'+element.value+'</td>')
//    				.addClass('span5');
//            		tdText.css("text-align","center");
//            	   tR.append(tdText);
//            	  });	
//            	
////            	 tdButton = $('<td></td>').addClass('span2').append(ActionButton.actionButton({
////           	      name: el.name,
////           	      nodeRef: el.id,
////           	      baseTypeId: el.baseTypeId,
////           	      objectTypeId: el.objectTypeId,
////           	      mimeType: el.contentType,
////           	      allowableActions: el.allowableActions,
////           	      defaultChoice: 'select'
////           	    }, {copy_curriculum: 'CAN_UPDATE_PROPERTIES'}, {
////           	      permissions : false,
////           	      history : false,
////           	      copy: false,
////           	      cut: false,
////           	      update: false,
////           	      remove: function () {
////           	        UI.confirm('Sei sicuro di voler eliminare l\'esperienza/incarico "' +  title  + '"?', function () {
////           	          Node.remove(el.id, refreshFn);
////           	        });
////           	      },
////           	      
////    	     
////           	    }, { paste: 'icon-paste', move: 'icon-move'}, refreshFn, true));
////           	//  numCrediti.css("text-align","center");
////           	  tR.append(tdButton);
//	    	table=table.append(tR);
//	
//        	}
			modal.find('.close').click();
//			table.css( "display" , "table" );
			modal.find(".tableCorsi .alert").css("display","none")
		}
			
			
            function filterFileInputs() {
              var filtered = $(fileInputs).filter(function (index, el) {
                return el.item.find('span.fileupload-preview').text();
              });
              return $.makeArray(filtered);
            }

            var data = bulkinfo.getData(),
              filteredFileInputs = filterFileInputs(),
              inputName,
              fileName,
              fileinput = filteredFileInputs[0];
            
            var  displayedFileName;
            if (fileinput)
            	displayedFileName    = fileinput.item.find('span.fileupload-preview').text() ;
            else 
            	displayedFileName    = opts.nomeFile ? opts.nomeFile : null ;

            if (opts.externalData) {
              $.each(opts.externalData, function (i, exData) {
                data.push(exData);
              });
            }

            if (isInsert) {
              data.push({name: 'cmis:parentId', value: opts.nodeRef});
			 
              data.push({name: 'cmis:objectTypeId',  value: opts.objectType  }); //      value: "D_jconon_comunicazione_note"
			
				
              inputName = $(data).filter(function (index, el) {
                return el.name === 'cmis:name';
              })[0];
			  
			if(inputName!= undefined){
              data.splice(data.indexOf(inputName), 1);
			}
			
              if (inputName && inputName.value && !opts.multiple) {
                fileName = inputName.value;
              } else if (displayedFileName && !opts.multiple) {
                fileName = displayedFileName;
              } else {
                if (regex.test(opts.objectType)) {
                  fileName = opts.objectType.replace(regex, "$1");
                } else {
                  fileName = 'doc';
                }
                fileName += '_' + (new Date().getTime());
              }
              data.push({name: 'cmis:name', value: fileName});
            } else {
              // update object with 'cmis:objectId' === nodeRef
              data.push({
                name: 'cmis:objectId',
                value: opts.nodeRef.split(';')[0]
              });
              if (inputName && inputName.value && !opts.multiple) {
                  fileName = inputName.value;
                } else if (displayedFileName && !opts.multiple) {
                  fileName = displayedFileName;
                } else {
                  if (regex.test(opts.objectType)) {
                    fileName = opts.objectType.replace(regex, "$1");
                  } else {
                    fileName = 'doc';
                  }
                  fileName += '_' + (new Date().getTime());
                }
                data.push({name: 'cmis:name', value: fileName});
            }

            if (opts.requiresFile !== false && !fileinput) {
              UI.alert("inserire un allegato!");
              return false;
            } else {
              updateMetadata(data, function (data) {
                if (fileinput && fileinput.item.data('value')) {
                  var close = UI.progress(),
                    xhrs = $.map(filteredFileInputs, function (f) {
                      if (opts.multiple) {
                        return f.fn(opts.nodeRef, "INSERT", function (attachmentsData) {
                          close();
                          if (typeof opts.success === 'function') {
                            opts.success(attachmentsData, data);
                          }
                        },
                            $.extend({'cmis:sourceId' : data['cmis:objectId']}, opts.input.rel)
                          );
                      } else {
                        return f.fn(data['cmis:objectId'], null, function (attachmentsData) {
                          close();
                          if (typeof opts.success === 'function') {
                            opts.success(attachmentsData, data);
                          }
                        });
                      }
                    });
                  $.when.apply(this, xhrs)
                    .done(function () {
                      if (typeof opts.successMetadata === 'function') {
                        opts.successMetadata(data);
                      }
                      if (xhrs && xhrs[0]) {
                        close();
                        UI.success((filteredFileInputs.length === 1 ? 'allegato inserito' : 'allegati inseriti') + ' correttamente');
                      }
                    })
                    .fail(function (xhr) {
                      close();
                      if (typeof opts.success === 'function') {
                        opts.success();
                      }
                    });
                } else {
                	if ( opts.objectType.includes('D_jconon_iscrizione_attachment') ){
                		var close = UI.progress();
                		setTimeout(function(){ 
                			close();
                		 }, 4000);
                		setTimeout(function(){ 
	                		UI.success('Iscrizione avvenuta correttamente.',function () {
	                			 $('#applyFilter').click();
	                		});
                		 }, 4000);
                		
                	}else if ( opts.objectType.includes('D_jconon_rinnovo_effettuato_attachment') ){
                		var close = UI.progress();
                		setTimeout(function(){ 
                			close();
                		 }, 3000);
                		setTimeout(function(){ 
	                		UI.success('Rinnovo avvenuto correttamente.',function () {
	                			 $('#applyFilter').click();
	                		});
                		 }, 2000);
                		
                	}else if ( opts.objectType.includes('D_jconon_cambio_fascia_effettuato_attachment') ){
                		var close = UI.progress();
                		setTimeout(function(){ 
                			close();
                		 }, 3000);
                		setTimeout(function(){ 
	                		UI.success('Cambio fascia avvenuto correttamente.',function () {
	                			 $('#applyFilter').click();
	                		});
                		 }, 2000);
                		
                	}else
                		UI.success('Dato inserito correttamente.');
                  opts.success(undefined, data);
                }
              });
            }
          }, undefined, opts.bigmodal);
		
			if(opts.objectType.includes("corso")){
				 modal.find('#title').parent().parent().hide();	  
     	        modal.find('#description').parent().parent().hide();	 
			}
			
          if (opts.callbackModal) {
            opts.callbackModal(modal, content);
          }
          if(opts.objectType.includes("esperienza")){
        	  
        	  modal.removeAttr("tabindex");
        	  modal.find("#esperienza_professionale_attivita_svolta").parent().parent().hide();
        	  modal.find(".widget #fl_amministrazione_pubblica ").parent().parent().css("display","inline-block");
        	  modal.find(".widget #fl_amministrazione_pubblica ").parent().parent().css("position","relative");
        	  modal.find(".widget #fl_amministrazione_pubblica ").parent().parent().css("bottom","45px");
        	  modal.find("#fl_amministrazione_pubblica").parent().parent().after("<label class='label_specifiche_scheda_anonima label_specifiche_scheda_anonima_fl_amministrazione_pubblica ' >"+i18n['label.label_specifiche_scheda_anonima.amministrazione.pubblica']+"</label>");
        	  modal.find('#esperienza_professionale_stato_estero').parents('.widget').bind('changeData', function (event, key, value) {
    		      if ( key === 'value') {
    		    	  manageNazioneEsperienze(value);
    		      }
    		    });
        	  modal.find('#esperienza_professionale_area_specializzazione').on('change', function ( event ){
        		  if( modal.find('#esperienza_professionale_area_specializzazione').val().length > 0 )
        			  modal.find("#esperienza_professionale_attivita_svolta").parent().parent().show();
        		  else
        			  modal.find("#esperienza_professionale_attivita_svolta").parent().parent().hide();
        	  });
    		    /*jslint unparam: false*/
  //      	  manageNazioneEsperienze($("#esperienza_professionale_stato_estero").attr('value'));
        	  modal.find('#fl_amministrazione_pubblica').on('click', function ( event ){
	 //       	  if(modal.find('#fl_amministrazione_pubblica button[data-value="false"]').hasClass("active")){
        		  if( event.target.firstChild.data.includes("Si") ){
        		 
        			  modal.find("#esperienza_professionale_estero_citta ").parent().parent().hide();
		        	  modal.find(".widget #fl_amministrazione_pubblica ").parent().parent().css("display","inline-block");
		        	  modal.find(".widget #fl_amministrazione_pubblica ").parent().parent().css("position","relative");
		        	  modal.find(".widget #fl_amministrazione_pubblica ").parent().parent().css("bottom","45px");
		        	  
		        	  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("position","relative");
		           	  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("bottom","25px");		           	  
		           	 
		           	  modal.find(".widget #dirigente_ruolo ").parent().parent().css("position","relative");
			          modal.find(".widget #dirigente_ruolo ").parent().parent().css("bottom","50px");
			          modal.find(".widget .control-label[for='dirigente_ruolo'] ").css("position","relative");
			          modal.find(".widget .control-label[for='dirigente_ruolo'] ").css("top","0px");
			        	  
//		        	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("position","relative");
//		        	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("bottom","65px");
//		        	  
//		        	//  modal.find("#fl_amministrazione_pubblica").parent().parent().after("<label class='label_specifiche_scheda_anonima'>"+i18n['label.label_specifiche_scheda_anonima.amministrazione.pubblica']+"</label>");
//		        	//  modal.find(".widget #fl_amministrazione_pubblica ").parent().parent().css("display","inline-block");
//		        	  modal.find("#amministrazione_pubblica_generale").parent().parent().after("<label class='label_specifiche_scheda_anonima'>"+i18n['label.label_specifiche_scheda_anonima.livello.generale']+"</label>");
		           	if( modal.find(".label_specifiche_dirigente_ruolo").length < 1 )
		           //		modal.find("#esperienza_dirigenziale").parent().parent().after("<label class='label_specifiche_scheda_anonima label_specifiche_scheda_anonima_esperienza_dirigenziale ' >"+i18n['label.label_specifiche_scheda_anonima.esperienza.dirigenziale']+"</label>");
		        		  modal.find("#dirigente_ruolo").parent().parent().after("<label class='label_specifiche_dirigente_ruolo label_specifiche_scheda_anonima' >"+i18n['label.label_specifiche_scheda_anonima.dirigente.ruolo']+"</label>");

		           		else
			        		 
		           			modal.find(".label_specifiche_dirigente_ruolo").css("display", "inline-block");
		           			//modal.find(".label_specifiche_scheda_anonima_esperienza_dirigenziale").css("display", "inline-block");
		
	        	  }else{
	        		  modal.find(".label_specifiche_scheda_anonima").css("display", "none");
	        		  modal.find(".label_specifiche_scheda_anonima_fl_amministrazione_pubblica").css("display", "inline-block");
	        		  modal.find("#amministrazione_pubblica_generale").parent().parent().css("display", "none");
	        		  modal.find("#incarico_articolo_19").parent().parent().css("display", "none");
	        		  modal.find("#dirigente_ruolo").parent().parent().css("display", "none");
	        		  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("display", "none");
	        	  }
        	  }); 
        	  
        	  modal.find('#esperienza_dirigenziale').on('click', function ( event ){
        			 //       	  if(modal.find('#fl_amministrazione_pubblica button[data-value="false"]').hasClass("active")){
       		     if( event.target.firstChild.data.includes("Si") ){
        		        		 
	  
	 					  modal.find(".widget #incarico_articolo_19").parent().parent().css("display","inline-block");
			        	  modal.find(".widget #incarico_articolo_19 ").parent().parent().css("position","relative");
			        	  modal.find(".widget #incarico_articolo_19 ").parent().parent().css("bottom","50px");
			        	  modal.find(".widget .control-label[for='incarico_articolo_19'] ").css("position","relative");
			        	  modal.find(".widget .control-label[for='incarico_articolo_19'] ").css("bottom","14px");
			        	  modal.find('#esperienza_professionale_ruolo').val("Dirigente");
			        	  modal.find('#esperienza_professionale_ruolo').attr('disabled','disabled');
//        				        	  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("position","relative");
//        				           	  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("bottom","60px");
////        				        	  
			 //       	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("position","relative");
			  //      	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("bottom","65px");
			        	 
			        	  if( modal.find(".label_specifiche_incarico_articolo").length < 1 ){
			        		  modal.find("#incarico_articolo_19").parent().parent().after("<label class='label_specifiche_scheda_anonima label_specifiche_incarico_articolo'>"+i18n['label.label_specifiche_incarico_articolo.19']+"</label>");
//         				        		  modal.find("#dirigente_ruolo").parent().parent().after("<label class='label_specifiche_scheda_anonima' >"+i18n['label.label_specifiche_scheda_anonima.dirigente.ruolo']+"</label>");
			        	  } else
			        		  modal.find(".label_specifiche_incarico_articolo").css("display", "inline-block");
        				        	
	        	  }else{
		        		  modal.find(".label_specifiche_scheda_anonima").css("display", "none");
		        		  modal.find(".label_specifiche_scheda_anonima_fl_amministrazione_pubblica").css("display", "inline-block");
		        		  modal.find(".label_specifiche_esperienza_dirigenziale").css("display", "inline-block");
		        		  modal.find(".label_specifiche_dirigente_ruolo").css("display", "inline-block");
		        		  modal.find('#esperienza_professionale_ruolo').val("");
		        		  modal.find('#esperienza_professionale_ruolo').removeAttr('disabled');
        			        		  
	        	  }
        	  }); 
        	  
        	  modal.find('#dirigente_ruolo').on('click', function ( event ){
     			 //       	  if(modal.find('#fl_amministrazione_pubblica button[data-value="false"]').hasClass("active")){
    		     if( event.target.firstChild.data.includes("Si") ){
	        		 
	        		//  modal.find(".widget .generale").parent().parent().parent().css("display","inline-block");
			        	// modal.find(".widget .nonGenerale").parent().parent().parent().css("display","block");
    		    	 	modal.find(".widget #incarico_articolo_19").parent().parent().css("display","inline-block");
    		    	 	modal.find(".widget #incarico_articolo_19 ").parent().parent().css("position","relative");
    		    	 	modal.find(".widget #incarico_articolo_19 ").parent().parent().css("bottom","50px");
    		    	 	modal.find(".widget .control-label[for='incarico_articolo_19'] ").css("position","relative");
    		    	 	modal.find(".widget .control-label[for='incarico_articolo_19'] ").css("bottom","14px");
    		    	 	modal.find(".label_specifiche_esperienza_dirigenziale").css("display", "none");
    		    	 	modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("display", "none");
    		    	 	modal.find('#esperienza_professionale_ruolo').val("Dirigente");
    		    	 	modal.find('#esperienza_professionale_ruolo').attr('disabled','disabled');
			        	 if( modal.find(".label_specifiche_incarico_articolo").length < 1 ){
			        		  modal.find("#incarico_articolo_19").parent().parent().after("<label class='label_specifiche_scheda_anonima label_specifiche_incarico_articolo'>"+i18n['label.label_specifiche_incarico_articolo.19']+"</label>");
//     				        		  modal.find("#dirigente_ruolo").parent().parent().after("<label class='label_specifiche_scheda_anonima' >"+i18n['label.label_specifiche_scheda_anonima.dirigente.ruolo']+"</label>");
			        	  } else
			        		  modal.find(".label_specifiche_incarico_articolo").css("display", "inline-block");
			        	 if(	modal.find(".widget #esperienza_dirigenziale ").parent().parent().hasClass("valid") ){
 			        		modal.find(".widget #esperienza_dirigenziale ").parent().parent().removeClass("valid");
 			        		modal.find(".widget #esperienza_dirigenziale ").parent().parent().removeClass("success");
 			        		modal.find(".widget #esperienza_dirigenziale ").parent().parent().addClass("error");
 			        		modal.find(".widget #esperienza_dirigenziale button").removeClass("active");
 			        	}
     				        	
	        	  }else{
		        		  modal.find(".label_specifiche_scheda_anonima").css("display", "none");
		        		 modal.find(".label_specifiche_incarico_articolo").css("display", "none");
		        		 modal.find(".label_specifiche_esperienza_dirigenziale").css("display", "none");
		        		
		        		modal.find(".widget #incarico_articolo_19 ").parent().parent().css("display", "none");
		        		
 			        	if(	modal.find(".widget #incarico_articolo_19 ").parent().parent().hasClass("valid") ){
 			        		modal.find(".widget #incarico_articolo_19 ").parent().parent().removeClass("valid");
 			        		modal.find(".widget #incarico_articolo_19 ").parent().parent().removeClass("success");
 			        		modal.find(".widget #incarico_articolo_19 ").parent().parent().addClass("error");
 			        		modal.find(".widget #incarico_articolo_19 button").removeClass("active");
 			        	}
		        		
		        		modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("display", "none");
		        		 modal.find(".widget .control-label[for='esperienza_dirigenziale'] ").css("position","relative");
				          modal.find(".widget .control-label[for='esperienza_dirigenziale'] ").css("top","0px");
		        		 modal.find(".widget #amministrazione_pubblica_generale").parent().parent().css("display", "none");
		        		 modal.find(".label_specifiche_amministrazione_generale").css("display", "none");
		        		  modal.find(".label_specifiche_scheda_anonima_fl_amministrazione_pubblica").css("display", "inline-block");
		        		  modal.find(".label_specifiche_dirigente_ruolo").css("display", "inline-block");
		        		  modal.find('#esperienza_professionale_ruolo').val("");
		        		  modal.find('#esperienza_professionale_ruolo').removeAttr('disabled');
//     			        		  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("position","relative");
//     			        		  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("bottom","60px");
		        		 if( modal.find(".label_specifiche_esperienza_dirigenziale").length < 1 ){
		        			 modal.find("#esperienza_dirigenziale").parent().parent().after("<label class='label_specifiche_esperienza_dirigenziale label_specifiche_scheda_anonima'>"+i18n['label.label_specifiche_scheda_anonima.esperienza.dirigenziale']+"</label>");
		        			 modal.find("#esperienza_dirigenziale").parent().parent().css("display", "inline-block");
		        		 } else{
		        			 modal.find(".label_specifiche_esperienza_dirigenziale").css("display", "inline-block");
		        			modal.find("#esperienza_dirigenziale").parent().parent().css("display", "inline-block");
		        		 }
	        	  }
        	  }); 
        	  modal.find('#incarico_articolo_19').on('click', function ( event ){
     			 //       	  if(modal.find('#fl_amministrazione_pubblica button[data-value="false"]').hasClass("active")){
    		     if( event.target.firstChild.data.includes("Si") ){
     		        		 
     		        		  modal.find(".widget .generale").parent().parent().parent().css("display","inline-block");
     		        		  modal.find(".widget #amministrazione_pubblica_generale").parent().parent().css("display","inline-block");
 				        	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("position","relative");
 				        	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("bottom","50px");
 				        	  modal.find(".widget .control-label[for='amministrazione_pubblica_generale'] ").css("position","relative");
					          modal.find(".widget .control-label[for='amministrazione_pubblica_generale'] ").css("top","0px");
     				        	//  modal.find('#amministrazione_pubblica_generale').val("Dirigente");
     				      // 	  modal.find('#esperienza_professionale_ruolo').attr('disabled','disabled');
     				        	  
//     				        	  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("position","relative");
//     				           	  modal.find(".widget #esperienza_dirigenziale ").parent().parent().css("bottom","60px");
////     				        	  
 				        	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("position","relative");
 				        	  modal.find(".widget #amministrazione_pubblica_generale ").parent().parent().css("bottom","65px");
 				        	 
 				        	  if( modal.find(".label_specifiche_amministrazione_generale").length < 1 ){
 				        		  modal.find("#amministrazione_pubblica_generale").parent().parent().after("<label class='label_specifiche_amministrazione_generale label_specifiche_scheda_anonima'>"+i18n['label.label_specifiche_scheda_anonima.livello.generale']+"</label>");
//     				        		  modal.find("#dirigente_ruolo").parent().parent().after("<label class='label_specifiche_scheda_anonima' >"+i18n['label.label_specifiche_scheda_anonima.dirigente.ruolo']+"</label>");
 				        	  } else
 				        		  modal.find(".label_specifiche_amministrazione_generale").css("display", "inline-block");
     				        	
	        	  }else{
     			        	//	  modal.find(".label_specifiche_scheda_anonima").css("display", "none");
 			        		  modal.find(".widget #amministrazione_pubblica_generale").parent().parent().css("display", "none");
 			        		  modal.find(".label_specifiche_amministrazione_generale").css("display", "none");
 			        		  modal.find(".label_specifiche_scheda_anonima_fl_amministrazione_pubblica").css("display", "inline-block");
 			        		  modal.find(".label_specifiche_scheda_anonima_esperienza_dirigenziale").css("display", "inline-block");
 			        		  modal.find('#esperienza_professionale_ruolo').val("");
 			        		  modal.find('#esperienza_professionale_ruolo').removeAttr('disabled');
     			        		 
	        	  }
        	  }); 
        	 	
          }
          if(opts.modalTitle!=undefined && opts.modalTitle.includes("nota")){
 	         modal.find(".modifica").css("width","-moz-available");
 	      	 modal.find('.form-horizontal #default .widget').css("display","none");
 	      	 modal.css("width","860px");
 	    	 modal.css("left", "40%");
 	    	 modal.find("#default .controls #name").parent().parent().css("display","none");
 	    	 modal.find("#title").css("width","-moz-available");
 	    	 modal.find("#description").css("width","-moz-available");
 	    	 modal.find("label[for='title']").text("Tipo allegato");
 	    	 modal.find("label[for='description']").text("Descrizione allegato");
           }else if(opts.modalTitle!=undefined && opts.modalTitle.includes("comunicazione")){
         	  modal.find(".modifica").css("width","-moz-available");
         	  modal.css("width","860px");
  	    	 modal.css("left", "40%");
           }
        }
      }
    });

    bulkinfo.render();
  }

  /**
   *
   *  Update the content of a given node using an editor (Behave.js) supporting IDE-like features such as parenthesis autocompletion, Auto Indent
   *
   */
  function updateContentEditor(content, mimeType, nodeRef) {
    var textarea = $('<textarea class="input-block-level" rows="15"></textarea>').val(content), editor;

    editor = new Behave({
      textarea: textarea[0],
      tabSize: 2,
      autoIndent: true
    });
    UI.modal('Aggiornamento di ' + name, textarea, function () {
      var file = new window.Blob([textarea.val()], {type: mimeType}),
        input = [{
          files: [file]
        }];

      manageNode(nodeRef, "UPDATE", input);
    });
  }

  return {
    updateMetadata: updateMetadata,
    updateMetadataNode: updateMetadataNode,
    // display object metadata using bulkinfo
    displayMetadata : function (bulkInfo, nodeRef, isCmis, callback) {
      if (!nodeRef) {
        UI.alert("No information found");
      } else {
		  console.log(" metadata ");
        var f = isCmis ? URL.Data.node.node : URL.Data.proxy.metadata;
        f({
          data: {
            "nodeRef" : nodeRef,
            "shortQNames" : true
          }
        }).done(function (metadata) {
        	var campiDelete = new Array();
        	if( metadata["cmis:objectTypeId"] != undefined && metadata["cmis:objectTypeId"].includes("esperienza_professionale") ){
        		if( metadata["jconon_attachment:esperienza_professionale_a"] == null )
        			campiDelete.push(7);
        		if( metadata["jconon_attachment:esperienza_professionale_citta"] == null )
    				campiDelete.push(15);
    			if( metadata["jconon_attachment:esperienza_professionale_estero_citta"] == null )
    				campiDelete.push(16);
    			//	delete metadata["jconon_attachment:esperienza_professionale_a"];
        		if( !metadata["jconon_attachment:fl_amministrazione_pubblica"]  ){
        			campiDelete.push(2);
        	//		delete metadata["jconon_attachment:amministrazione_pubblica_generale"]; 2
        			campiDelete.push(3);
        		//	delete metadata["jconon_attachment:dirigente_ruolo"]; 3
        			campiDelete.push(1);
        	//		delete metadata["jconon_attachment:esperienza_dirigenziale"]; 1
        			campiDelete.push(4); // art.19
        			campiDelete.push(11);
        	//		delete metadata["jconon_attachment:esperienza_professionale_amministrazione"]; 10
        			campiDelete.push(10);
        	//		delete metadata["jconon_attachment:esperienza_professionale_cod_amm_ipa"]; 9
        			
        			
        		}else if ( !metadata["jconon_attachment:esperienza_dirigenziale"] ) {
        			campiDelete.push(2);
        			campiDelete.push(12);
        			campiDelete.push(14);
        		//	delete metadata["jconon_attachment:amministrazione_pubblica_generale"]; 2
        			campiDelete.push(1);
        		//	delete metadata["jconon_attachment:dirigente_ruolo"]; 1
        			if( !metadata["jconon_attachment:esperienza_professionale_area_specializzazione"] == null )
        				campiDelete.push(8);
        			//	delete metadata["jconon_attachment:esperienza_professionale_area_specializzazione"];  8
        			if( !metadata["jconon_attachment:esperienza_professionale_attivita_svolta"] == null )
        				campiDelete.push(9);
        			//	delete metadata["jconon_attachment:esperienza_professionale_attivita_svolta"];  9
        			if( metadata["jconon_attachment:incarico_articolo_19"] == null )
        				campiDelete.push(3);
        			if( metadata["jconon_attachment:amministrazione_pubblica_generale"] == null )
        				campiDelete.push(4);
        		}else{
        			campiDelete.push(12);
        			campiDelete.push(14);
        			if( metadata["jconon_attachment:esperienza_professionale_area_specializzazione"] == null )
        				campiDelete.push(8);
//        			if( metadata["jconon_attachment:esperienza_professionale_citta"] == null )
//        				campiDelete.push(14);
//        			if( metadata["jconon_attachment:esperienza_professionale_estero_citta"] == null )
//        				campiDelete.push(15);
        			//	delete metadata["jconon_attachment:esperienza_professionale_area_specializzazione"]; 8
        			if( metadata["jconon_attachment:esperienza_professionale_attivita_svolta"] == null )
        				campiDelete.push(9);
        			//	delete metadata["jconon_attachment:esperienza_professionale_attivita_svolta"]; 7
        			if( metadata["jconon_attachment:incarico_articolo_19"] == null )
        				campiDelete.push(3);
        			if( metadata["jconon_attachment:amministrazione_pubblica_generale"] == null )
        				campiDelete.push(4);
        		} 
        	}else if( metadata["cmis:objectTypeId"] != undefined && metadata["cmis:objectTypeId"].includes("documento_riconoscimento") ){
        		campiDelete.push(5);
        		campiDelete.push(6);
        	}
          new BulkInfo({
            handlebarsId: 'zebra',
            path: bulkInfo,
            metadata: isCmis ? metadata : metadata.properties
          }).handlebars().done(function (html) {
			
			 
            var content = $('<div></div>').addClass('modal-inner-fix').append(html),
			
            title= "";
			if(html.includes("Oggetto")){
				title=i18n.prop("modal.title.view." + bulkInfo, 'Visualizza comunicazione');
			}else{
				title=i18n.prop("modal.title.view." + bulkInfo, 'Propriet&agrave;');
			}
            if (callback) {
              callback(content);
            }
			if(html.length>233){
				var m=	UI.modal(title, content);
				
			}else{
				 UI.alert("Nessun testo allegato");
			}
			if(m!= undefined){
				m.find('.table-striped tr').each(function(  index, element ) {
					 if( campiDelete.includes( index ) ) 
						 $(element).remove();
				});
				$(m).css("width","1070px");
				$(m).css("margin-left","-540px");
			}
          });
          	
        });
      }
    },
    updateContentEditor: updateContentEditor,
    submission: submission,
    inputWidget: inputWidget,
    remove: function (nodeRef, refreshFn, showMessage) {
      manageNode(nodeRef, "DELETE").done(function (data) {
        if (refreshFn) {
          if (showMessage !== false) {
            displayOutcome(data, true);
          }
          refreshFn(data);
        }
      });
    }
  };
});